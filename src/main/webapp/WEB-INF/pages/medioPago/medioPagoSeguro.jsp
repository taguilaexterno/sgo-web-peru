<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>	
		<script type="text/javascript" src="js/mpseguro.js"></script>
		
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href="#">SGO</a></li>
						<li class="active">Variables de validaci&oacute;n para medios de pago</li>
					</ol>

					<article id="product-list-manual">
						<h3>Variables de validaci&oacute;n para medios de pago</h3>
		<s:form id="admMPSeguroForm" action="admMPSeguro">
		
		<s:hidden id="id" name="reglaMP.id" />
		<s:hidden id="estado" name="reglaMP.estado" />
		<s:hidden id="metodo" name="reglaMP.metodo" />
		<s:hidden id="idValidacion" name="reglaMP.idValidacion" />
		<s:hidden id="valor" name="reglaMP.valor" />
		
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Id regla</th>
										<th>Condición</th>
										<s:if test="#session.nivelPermiso == 1">
											<th colspan="2">Acciones</th>
										</s:if>
									</tr>
								</thead>
								<tbody>
								<s:iterator value="reglasMpSeguro">
									<tr>
										<td><s:property value="id"/></td>
										<td><s:property value="descripcion"/> 
											<s:if test="valor!=0">
												: <b><s:property value="valor"/></b>
											</s:if>
										</td>
									
										<s:if test="#session.nivelPermiso == 1">
											<td>										
												<s:if test="estado==1">								
													<div class="center" id="<s:property value="id"/>" onclick="setEstado(1,<s:property value="id"/>,<s:property 
														value="idValidacion"/>,<s:property value="valor"/>);" data-toggle="modal" data-target="#confirmar">
														<span class="label label-success">
														<span class="fa fa-check"></span>
														Activo</span>
													</div>
												</s:if>
												<s:else>
													<div class="center" id="<s:property value="id"/>" onclick="setEstado(0,<s:property value="id"/>,<s:property 
														value="idValidacion"/>,<s:property value="valor"/>);" data-toggle="modal" data-target="#confirmar">
														<span class="label label-danger">
														<span class="fa fa-check">
														Inactivo
														</span>
														</span>
													</div>
												</s:else>
											</td>
											<td>
												<s:if test="valor!=0">								
													<div class="center" 
														id="<s:property value="id"/>" 
														onclick='javascript:setEstado(1,
														<s:property value="id"/>,
														<s:property value="idValidacion"/>,
														<s:property value="valor"/>);'													
														data-toggle="modal" data-target="#editar-regla">
														<span class="label label-default">
														Editar</span>
													</div>
												</s:if>
											</td>
										</s:if>
									</tr>
								</s:iterator>
								</tbody>
							</table>
							
						</div>
		
		</s:form>
					</article>
						
				</div>
			</div>
		</div>
		
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">confirmar regla de Pago Seguro</h4>
					</div>
					<div class="modal-body">
						<span>¿Está Seguro?</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button type="button" class="btn btn-primary" onclick="updateEstado();">Guardar</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="editar-regla" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Editar regla de Pago Seguro</h4>
						</div>
						
						<s:form id="admMPSeguroForm" action="admMPSeguro" theme="simple" role="form" data-toggle="validator">
							<div class="modal-body">
								<div class="form-group">
									<label>Valor Menor</label>
									<input id="inptvalor" min="0" max="1000000" type="number" class="form-control input-lg" 
									placeholder="<s:property value="reglaMP.valor"/>" required>
									<div id="bootError" class="help-block with-errors"></div>
								</div>
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
								<button type="button" class="btn btn-primary" onclick="updateValMenor();">Guardar</button>
							</div>
						</s:form>
				</div>
			</div>
		</div>
	</s:if>
		
	<s:include value="/commons/footer.jsp"></s:include>