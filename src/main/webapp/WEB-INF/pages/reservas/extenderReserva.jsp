<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>

<script type="text/javascript"
	src='<s:url value="/js/configurarCaja.js"/>'></script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Extender per&iacute;odo de vigencia de
					reservas</li>
			</ol>

			<article id="product-list-manual">
				<h3>Extender per&iacute;odo de vigencia de reservas</h3>
				<s:if test="mensaje != null">
					<div id="mensajeExito" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="clear"></div>
				<div class="col-md-12">
					<s:form action="extenderReserva" theme="simple" class="form-inline"
						role="form" data-toggle="validator">
						<s:hidden name="estado" value="1" />
						<div class="form-group">
							<div class="clear"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Ingresa Nº de reserva</label>
									<s:textfield type="number" name="numeroReserva"
										cssClass="form-control" placeholder="Ingresa nº de reserva"
										data-error="Debe llenar este campo" data-minlength="2"
										required="" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Ingresa nueva fecha de vigencia</label>
									<div>
										<input id="fechaHasta" name="fechaVigencia" class="form-control"
											data-date-format="yyyy/mm/dd hh:ii:ss" value="${fechaHasta}"
											onclick="$('#fechaHasta').datepicker('show')" required="">
									</div>
								</div>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Hora extenci&oacute;n de reserva</label>
								<div class='input-group date'>
									<s:textfield id="horaCierre"
										name="horaVigencia"
										cssClass="form-control" placeholder="22:00:00"
										data-error="Debe llenar este campo" required="" />
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-time"></span>
									</span>
								</div>
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="col-md-12">
							<button class="btn btn-primary">Extender Vigencia</button>
						</div>
					</s:form>
				</div>
			</article>
			<%-- FIN SECCION PRINCIPAL --%>
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>