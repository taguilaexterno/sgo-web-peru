<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/buscarReservaLista.js"/>'></script>
	
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li><a href="javascript:history.back();">B&uacute;squeda de
						reservas</a></li>
				<li class="active">Detalle Reserva</li>
			</ol>

			<article id="product-list-manual">
				<h3>B&uacute;squeda de Reservas</h3>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Orden de compra</th>
								<th>Estado</th>
								<th>Nombre del cliente</th>
								<th>Fecha de creaci&oacute;n</th>
								<th>Ver Reserva</th>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="listaReservas" var="det">
								<tr>
									<td><s:property value="#det.ordenCompra" /></td>
									<td><s:property value="#det.estado" /></td>
									<td><s:property value="#det.nombreCliente" /></td>
									<td><s:property value="#det.fechaCreacion" /></td>
									<td><s:form action="buscarReserva" name="form" id="form_%{#det.ordenCompra}">
											<s:hidden name="estado" value="1" />
											<input type="hidden" name="numeroReserva"
												value="<s:property value="#det.ordenCompra" />" />
											<input class="btn btn-primary" type="submit" value="Ver [+]" />
										</s:form>
								</tr>
							</s:iterator>
						</tbody>
					</table>
					<s:form id="findPage" action="buscarReserva" >
						<s:hidden id="estado"		name="estado"		value="1"></s:hidden>
						<s:hidden id="numeroReserva"name="numeroReserva"value="%{numeroReserva}"></s:hidden>
						<s:hidden id="rutCliente"	name="rutCliente" 	value="%{rutCliente}"></s:hidden>
						<s:hidden id="estadoReserva"name="estadoReserva"value="%{estadoReserva}"></s:hidden>
						<s:hidden id="paginaActual" name="paginaActual"	value="%{paginaActual}"></s:hidden>
						<s:hidden id="ultimaPagina" name="ultimaPagina"	value="%{ultimaPagina}"></s:hidden>
					</s:form>
					<ul class="pagination">
					  <s:iterator value="paginas" var="pag">
					  	<li class="<s:property value="#pag.estado" />"><a href="javascript:getPaginaJs('<s:property value="#pag.pagina" />');"><s:property value="#pag.pagina" /></a></li>
					  </s:iterator> 
					</ul>
				</div>
			</article>
		</div>
	</div>
</div>
I
<s:include value="/commons/footer.jsp"></s:include>