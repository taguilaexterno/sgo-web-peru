<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Cancelar reservas</li>
			</ol>
			<article id="product-list-manual">
				<h3>Cancelar Reserva</h3>
				<s:if test="mensaje != null">
					<div id="mensajeExito" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="center">
					<s:form action="cancelarReserva" theme="simple" class="form-inline"
						role="form" data-toggle="validator">
						<s:hidden name="estado" value="1" />
						<div class="form-group">
							<label>Ingresa Nº de reserva</label>
							<div class="input-group">
								<s:textfield type="number" name="numeroReserva"
									cssClass="form-control" placeholder="Ingresa nº de reserva"
									data-error="Debe llenar este campo" data-minlength="2"
									required="" />
								<span class="input-group-btn">
									<button class="btn btn-primary">Cancelar Reserva</button>
								</span>
							</div>
							<div class="help-block with-errors"></div>
						</div>
					</s:form>
				</div>
			</article>
			<%-- FIN SECCION PRINCIPAL --%>
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>