<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">B&uacute;squeda de reservas</li>
			</ol>
			<article id="product-list-manual">
				<h3>B&uacute;squeda de Reservas</h3>
				<s:if test="mensaje != null">
					<div id="mensajeExito" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="clear"></div>
				<div class="col-md-12">
					<s:form action="buscarReserva" theme="simple" class="form-inline"
						role="form" data-toggle="validator">
						<s:hidden name="estado" value="1" />
						<div class="form-group">
							<div class="clear"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Ingresa Nº de reserva</label>
									<s:textfield type="number" name="numeroReserva"
										cssClass="form-control" placeholder="Ingresa nº de reserva"
										data-minlength="2" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Ingresa DNI de cliente</label>
									<s:textfield type="number" name="rutCliente"
										cssClass="form-control" placeholder="Ingresa DNI de cliente" />
								</div>
							</div>
							<div class="col-md-4">
								<label>Ingresa estado</label> <select class="form-control"
									name="estadoReserva">
									<option value="0">Seleccione</option>
									<option value="1">Pendiente de Impresion</option>
									<option value="61">Pendiente de pago</option>
									<option value="62">Orden cancelada en SGO</option>
									<option value="63">Orden cancelada por fecha de vigencia</option>
									<!-- <option value="64">Orden cancelada desde Back Office</option> -->
									<!-- <option value="2">Boleta</option> -->
								</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="col-md-12">
							<button class="btn btn-primary">Buscar</button>
						</div>
					</s:form>
				</div>
			</article>
			<%-- FIN SECCION PRINCIPAL --%>
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>