<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li><a href="javascript:history.back();">B&uacute;squeda de
						reservas</a></li>
				<li class="active">Detalle Reserva</li>
			</ol>

			<article id="product-list-manual">
				<h3>Datos de reserva</h3>
				<div class="panel panel-default oc-details">
					<div class="panel-body">
						<dl>
							<dt>Orden de compra</dt>
							<dd>
								<s:property value="reserva.ordenCompra" />
							</dd>
						</dl>
						<dl>
							<dt>Estado</dt>
							<dd>
								<s:property value="reserva.estado" />
							</dd>
						</dl>
						<dl>
							<dt>Nombre del cliente</dt>
							<dd>
								<s:property value="reserva.nombreCliente" />
							</dd>
						</dl>
						<dl>
							<dt>Fecha de creaci&oacute;n</dt>
							<dd>
								<s:property value="reserva.fechaCreacion" />
							</dd>
						</dl>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>SKU</th>
								<th>Nombre de producto</th>
								<th>Unidades</th>
								<th>Monto</th>
								<th>Tipo de despacho</th>
								<th>Direcci&oacute;n de despacho</th>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="reserva.productos" var="det">
								<tr>
									<td><s:property value="#det.sku" /></td>
									<td><s:property value="#det.nombreProducto" /></td>
									<td><s:property value="#det.unidades" /></td>
									<td><s:property value="#det.monto" /></td>
									<td><s:property value="#det.tipoDespacho" /></td>
									<td><s:property value="#det.direccionDespacho" /></td>
								</tr>
							</s:iterator>
						</tbody>
					</table>
				</div>
			</article>
		</div>
	</div>
</div>
I
<s:include value="/commons/footer.jsp"></s:include>