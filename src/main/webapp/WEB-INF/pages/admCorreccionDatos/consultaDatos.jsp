<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>

<s:include value="/commons/header.jsp"></s:include>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>

<script type="text/javascript">

	$(function() {
		$('#resultData').DataTable({
		    dom: 'Bfrtip',
	        buttons: [
	            'csv', 'excel'
	        ],
	        paging: true});
		
		<s:if test="queryResultConsulta != null">
		$('#resultDataConsulta').DataTable({
		    dom: 'Bfrtip',
	        buttons: [
	            'csv', 'excel'
	        ],
	        paging: true});
		</s:if>
		
	});

</script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Administraci&oacute;n Correcci&oacute;n Datos</li>
			</ol>
			<article id="product-list-manual">
				<h3>Administraci&oacute;n Consulta de Datos</h3>
				
				<s:if test="mensajeError != null">
					<div id="mensajeError" class="alert alert-danger"><s:property value="mensajeError"/> </div>
				</s:if>
				<s:if test="mensajeExito != null">
					<div id="mensajeExito" class="alert alert-success"><s:property value="mensajeExito"/></div>
				</s:if>
				<div class="clear"></div>
				
		
			        <s:form theme="simple" data-toggle="validator" method="POST" action="admConsultaData">
			        <div class="row">
						<div class="col-md-3 row">
							<div class="form-inline">
								<div class="form-check">
									<label class="form-check-label" for="tipoQuery">Consulta</label>
									<input type="radio" class="form-check-input" name="tipoQuery" id="tipoQuery" value="1" checked/>
									<s:if test="update==1">
										<label class="form-check-label" for="tipoQuery2">Insert, Update, Delete</label>
										<input type="radio" class="form-check-input" name="tipoQuery" id="tipoQuery2" value="2"/>
									</s:if>
									
									
								</div>
								<button type="submit">Ejecutar</button>
							</div>
							<div class="help-block with-errors"></div>
						</div>
			        </div>
			        <div class="row">
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<textarea name="consulta" style="height: 300px; width: 1000px;"></textarea>
			        		</div>
			        	</div>
			        </div>
			     </s:form>
			     <div class="row">
			        	<div class="col">
			        		<s:if test="queryResultConsulta != null">
			        			<div class="caja">
				        			<table id="resultDataConsulta" class="table-responsive" style="width:100%; height: 100%;">
				        				<thead>
				        					<tr>
				        						<th>#</th>
				        						<s:iterator value="queryResultConsulta" var="resList" status="status">
				        							<s:if test="#status.index == 0">
				        								<s:iterator>
				        									<th><s:property value="key"/></th>
				        								</s:iterator>
				        							</s:if>
				        						</s:iterator>
				        					</tr>
				        				</thead>
				        				<tbody>
				        					<s:iterator value="queryResultConsulta" var="resList" status="status">
				        						<tr>
				        							<td>
				        								<s:property value="#status.index + 1"/>
				        							</td>
				        							<s:iterator>
				        								<td><s:property value="value"/></td>
				        							</s:iterator>
				        						</tr>
				        					</s:iterator>
				        				</tbody>
				        			</table>
			        			</div>
			        		</s:if>
			        	</div>
			        </div>
			</article>	
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>

<style type="text/css">

.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

.caja {
  border-color: blue;
  position: relative;
  overflow-x: scroll; 
  overflow-y: scroll;}

</style>
