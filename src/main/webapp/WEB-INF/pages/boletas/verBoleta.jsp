<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-9XS5');</script>
<!-- End Google Tag Manager -->
<title>Boleta, Ripley.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
/* CLIENT-SPECIFIC STYLES */
body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
img { -ms-interpolation-mode: bicubic; }

/* RESET STYLES */
img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
table { border-collapse: collapse !important; }
body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

/* iOS BLUE LINKS */
a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

tr.product[value=""], tr.product[value=""] td, tr.product[value=""] td table{
    display: none !important;
    padding:0 0 0 0 !important;
    margin:0 !important;
}

/* MEDIA QUERIES */
@media screen and (max-width: 600px) {
    .mobile-hide {
        display: none !important;
    }
    .mobile-center {
        text-align: center !important;
    }
    .mobile-table {
        width: 100% !important;
    }
    .text-center {
    	text-align: center !important;
    }
    .mobile-td{
        padding-left:15px !important;
        padding-right:15px !important;
    }
}

/* ANDROID CENTER FIX */
div[style*="margin: 16px 0;"] { margin: 0 !important; }

.detalleCompra tr:nth-of-type(odd){
  background:#f8f8f8;
}
#barcode canvas{
  display: block;
  margin-left: auto;
  margin-right: auto;
}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
<script type="text/javascript" src="http://barcode-coder.com/js/jquery-barcode-last.min.js"></script>
</head>


<body style="margin: 0 !important; padding: 0 !important; background-color: #f6f6f6; color:#444;" bgcolor="#f6f6f6">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td style="height:10px">
      </td>
    </tr>
    <tr>
      <td align="center" style="background-color: #f6f6f6;" bgcolor="#f6f6f6">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="340" class="mobile-table" style="border:solid 1px #e9e9e9;">
          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">



              <table cellspacing="0" cellpadding="0" border="0" width="100%" class="mobile-table">
                <tr class="product">
                  <td style="padding:15px 0 15px 0;">

                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                        <td align="left" width="50%" style="padding:0 15px 0 0;">
                          <img  name="Cont_0" src="https://home.ripley.cl/assets-email/img/ripley-logo.png" width="150" style="display: block; border: 0px;"/>
                        </td>
                        <td align="left" width="50%" valign="middle">
                          <p style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 13px; margin:0; color:#4a4a4a"><s:property value="razonSocial"/></p>
                          <p style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 13px; margin:0; color:#4a4a4a">R.U.T.: <s:property value="rutEmpresa"/></p>
                          <p style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 13px; margin:0; color:#4a4a4a">GIRO: <s:property value="giro"/></p>
                          <p style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 13px; margin:0; color:#4a4a4a">www.ripley.cl</p>
                        </td>
                      </tr>
                    </table>

                  </td>
                </tr>
              </table>
            </td>
          </tr>




          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
              <p style="font-size:13px; margin:0; padding:0 0 15px 0"><s:property value="direccion"/></p>
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
              <s:if test="orden.notaVenta.numeroSucursal != 39">
	              <s:if test="nombreSucursal != null && nombreSucursal !='OFF'">
		              <tr>
		              <td align="left" width="60%" style="font-size:13px; padding:0; margin:0;"><s:property value="nombreSucursal"/></td>
		              </tr>
	              </s:if>
              </s:if>
                <tr>
                  <td align="left" width="60%" style="padding:0; margin:0;">
                    <p style="font-size:13px; margin:0; padding:0">SUCURSAL: <s:property value="orden.notaVenta.numeroSucursal"/></p>
                    <p style="font-size:13px; margin:0; padding:0">FECHA EMISION: <s:date name="orden.NotaVenta.horaBoleta" format="yyyy-MM-dd"/></p>
                  </td>
                  <td align="left" width="40%" style="padding:0; margin:0;">
                    <p style="font-size:13px; margin:0; padding:0">CAJA: <s:property value="orden.notaVenta.numeroCaja"/></p>
                    <p style="font-size:13px; margin:0; padding:0">HORA: <s:date name="orden.NotaVenta.horaBoleta" format="HH:mm:ss"/></p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <!-- CODIGO DE BARRAS -->
              <div class="codigoBar" style="margin-top:20px; margin-bottom:20px; margin-left:auto; margin-right:auto"></div>
              <!-- CODIGO DE BARRAS -->
            </td>
          </tr>
          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
              <p style="font-size:13px; margin:0; padding:0 0 15px 0">NRO TRANSACCION/DOCUMENTO: <s:property value="orden.notaVenta.numeroBoleta"/></p>
              <p style="font-size:13px; margin:0; padding:0">VENDEDOR: <s:property value="orden.vendedor.nombreVendedor"/></p>
              <p style="font-size:13px; margin:0; padding:0">CODIGO REGALO: <s:property value="orden.notaVenta.codigoRegalo"/></p>
              <p style="font-size:13px; margin:0; padding:0">NOMBRE CLIENTE: <s:property value="orden.despacho.nombreCliente"/> <s:property value="orden.despacho.apellidoPatCliente"/> <s:property value="orden.despacho.apellidoMatCliente"/></p>
              <p style="font-size:13px; margin:0; padding:0">DNI CLIENTE: <s:property value="@cl.ripley.omnicanalidad.util.Util@formatoRUT2(orden.despacho.rutCliente+@cl.ripley.omnicanalidad.util.Util@getDigitoValidadorRut2(orden.despacho.rutCliente))"/></p>
              <p style="font-size:13px; margin:0; padding:0">ORDEN DE COMPRA: <s:property value="orden.notaVenta.correlativoVenta"/></p>
              <p style="font-size:13px; margin:0; padding:0">FECHA OC: <s:date name="orden.NotaVenta.horaBoleta" format="dd/MM/yyyy HH:mm:ss"/></p>

              <!-- SEPARADOR  -->
              <hr style="height:1px; background:#ddd; border:none; margin:20px -30px 20px;">
              <!-- SEPARADOR -->
            </td>
          </tr>







          <tr>
            <td align="left" style="margin:0; padding: 0 30px 30px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
                           <table cellspacing="0" cellpadding="0" border="0" width="100%" class="mobile-table" style="margin-bottom:20px">
                
                <thead style="margin:0; padding:0; border-top:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3">
                  <tr>
                    <th style="font-size:12px; margin:0; padding:0 0 1px 10px; text-align:left">C�DIGO</th>
                    <th style="font-size:12px; margin:0; padding:0 0 1px 10px; text-align:left">DESCRIPCION <br> CANTIDAD X PRECIO</th>
                    <th style="font-size:12px; margin:0; padding:0 10px 1px 0; text-align:right">VALOR</th>
                  </tr>
                </thead>
                <tbody class="detalleCompra">

				<s:iterator value="listaArticulos" var="articulo">

				  <tr>
                    <td width="25%" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left; vertical-align:top"><s:property value="#articulo.articuloVenta.codArticulo" /></td>
                    <td width="50%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:left">
                    	<s:property value="#articulo.articuloVenta.descRipley" /> <s:property value="#articulo.articuloVenta.unidades" /> X 
                    	<s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(#articulo.articuloVenta.precio)" /> 
                    </td>
                    <td width="25%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:right; vertical-align:top"><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(#articulo.articuloVenta.precioPorUnidades)" /></td>
                  </tr>
                  
                  <s:if test="#articulo.articuloVenta.montoDescuento > 0">
                 	<tr>
	                    <td width="25%" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left"></td>
	                    <td width="50%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:left">DESCUENTO</td>
	                    <td width="25%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:right">-<s:property value="#articulo.articuloVenta.montoDescuento"/></td>
                  	</tr>
                   </s:if>
                  <s:if test='#articulo.articuloVenta.codDespacho != " " || #articulo.articuloVenta.codDespacho !=0' >
                 	<tr>
                    	<td width="100%" colspan="3" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left">&#60;BT&#62; <s:property value="#articulo.articuloVenta.codDespacho"/> - [<s:property value="orden.despacho.despachoId"/>]</td>
                  	</tr>
				  </s:if>
				</s:iterator>

                </tbody>
              </table>
              <table cellspacing="0" cellpadding="0" border="0" width="100%" class="mobile-table" style="margin:0; padding:0; border-top:1px solid #b3b3b3">
                <tr class="total">
                  <td width="50%" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left; font-weight:bold">SUBTOTAL</td>
                  <td width="50%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:right; font-weight:bold"><span>$</span> <s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(subTotal)" /></td>
                </tr>
                <tr class="total">
                  <td width="50%" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left; font-weight:bold">TOTAL EXENTO</td>
                  <td width="50%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:right; font-weight:bold"><span>$</span> <s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(totalExento)" /></td>
                </tr>
                <tr class="total">
                  <td width="50%" align="left" style="font-size:13px; margin:0; padding:5px 10px; text-align:left; font-weight:bold">TOTAL</td>
                  <td width="50%" align="right" style="font-size:13px; margin:0; padding:5px 10px; text-align:right; font-weight:bold"><span>$</span> <s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(total)" /></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
             <s:if test="orden.tarjetaRegaloEmpresa.correlativoVenta != null">
            	<p style="font-size:13px; margin:0; padding:0 0 15px 0">T.R.EMPRESA        <span>$</span> <s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(total)" /></p>
              </s:if>
              <p style="font-size:13px; margin:0; padding:0 0 15px 0"><s:property value="medioPago"/></p>
              <p style="font-size:13px; margin:0; padding:0">CODIGO AUT.:<s:property value="codAutorizadorMP"/> B</p>
              <p style="font-size:13px; margin:0; padding:0">NRO UNICO: <s:property value="nroUnico"/></p>
              <p style="font-size:13px; margin:0; padding:0">VUELTO 0</p>
              <p style="font-size:13px; margin:0; padding:0"><s:date name="orden.notaVenta.horaBoleta" format="dd/MM/yyyy"/> - [1] - VENTA <s:property value="total" /></p>
              <!-- SEPARADOR -->
              <hr style="height:1px; background:#ddd; border:none; margin:20px -30px 20px;">
              <!-- SEPARADOR -->
            </td>
          </tr>



          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
              <p style="font-size:13px; margin:0; padding:0 0 15px 0; font-weight:bold">DOCUMENTOS REFERENCIADOS</p>
              <p style="font-size:13px; margin:0; padding:0">TIPO DOCUMENTO: Orden de Compra</p>
              <p style="font-size:13px; margin:0; padding:0">FOLIO: <s:property value="orden.notaVenta.folioSii"/></p>
              <p style="font-size:13px; margin:0; padding:0">FECHA:</p>
              <p style="font-size:13px; margin:0; padding:0">RAZON:</p>
              <!-- CODIGO DE BARRAS -->
              <div id="barcode" style="margin-top:20px; margin-bottom:0; margin-left:auto; margin-right:auto"></div>
              <p style="font-size:10px; margin:0; padding:0; text-align:center">Timbre Electr�nico SII</p>
              <p style="font-size:10px; margin:0; margin-bottom:30px !important; padding:0; text-align:center">Res. 46 del 2014. - Verifique documento: www.ripley.cl</p>
              <!-- CODIGO DE BARRAS -->
              <p style="font-size:13px; margin:0; margin-top:30px !important; padding:0">DIRECCION DESPACHO - [<s:property value="orden.despacho.despachoId"/>]</p>
              <p style="font-size:13px; margin:0; padding:0"><s:property value="orden.despacho.direccionDespacho"/></p>
              <p style="font-size:13px; margin:0; padding:0">REGION: <s:property value="orden.despacho.regionDespachoDes"/></p>
              <p style="font-size:13px; margin:0; padding:0">COMUNA: <s:property value="orden.despacho.comunaDespachoDes"/></p>

              <!-- <p style="font-size:11px; margin:0; margin-top:30px !important; padding:0 0 20px">RECIBI CONFORME MERCADERIA Y/O SERVICIOS - POR CUENTA DE <s:property value="razonSocial"/> -</p> -->
              <p style="font-size:11px; margin:0; margin-top:0px !important; padding:0 0">-----------------------------------------------------------------------------------</p>
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <!-- CODIGO DE BARRAS -->
              <div class="codigoBar" style="margin-top:20px; margin-bottom:20px; margin-left:auto; margin-right:auto"></div>
              <!-- CODIGO DE BARRAS -->
            </td>
          </tr>
          <tr> 
          	<td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">
				<p style="font-size: 13px; margin: 0; margin-top: 0 !important; padding: 0; font-weight: bold;">TICKET DE CAMBIO</p>
				<p style="font-size: 13px; margin: 0; margin-top: 0 !important; padding: 0;">PRESENTE ESTE CUPON</p>
				<p style="font-size: 11px; margin: 0; margin-top: 0 !important; padding: 0;">PARA CAMBIAR ESTE PRODUCTO</p>
				<p style="font-size: 13px; margin: 0; margin-top: 10px; ! important; padding: 0;">
				<s:property value="cajaTicketCambio"/>/<s:property value="sucTicketCambio"/> <s:date name="orden.NotaVenta.horaBoleta" format="dd/MM/yy"/>  <s:date name="orden.NotaVenta.horaBoleta" format="HH:mm"/> <s:property value="orden.notaVenta.numeroBoleta"/> <s:property value="orden.vendedor.nombreVendedor"/></p>
				<p style="font-size: 11px; margin: 0; margin-top: 0 !important; padding: 0; font-weight: bold;">BOLETA REF. No. <s:property value="orden.notaVenta.folioSii"/></p>
				<p style="font-size: 13px; margin: 0; margin-top: 0; ! important; padding: 0; font-weight: bold;">RIPLEY</p>
			</td>
          </tr>
          <tr>
            <td style="padding: 0 30px 5px; background-color: #ffffff; font-family: Open Sans, Helvetica, Arial, sans-serif;" bgcolor="#ffffff" class="mobile-td">       
              <p style="font-size:11px; margin:0; margin-top:0px !important; padding:0 0">-----------------------------------------------------------------------------------</p>
              <s:if test="orden.notaVenta.urlDoce != null">
            	 <!-- <p style="font-size:10px; margin:0; padding:0 0 15px 0; text-align: center;">Puedes descargar tu boleta <a href='<s:property value="orden.notaVenta.urlDoce"/>'>aqu�</a></p> -->
     		  </s:if>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


  <script src="https://rawgit.com/bkuzmic/pdf417-js/master/pdf417.js" type="text/javascript"></script>
  <script>
  $('.codigoBar').barcode({code: '<s:property value="codigoBarra"/>', crc:true}, "code11",{barWidth:2, barHeight:25})
  </script>


  <script>
  generate();

      function generate() {
        //var xmlVar = '<TED version="1.0"><DD><RE>83382700-6</RE><TD>39</TD><F>21563</F><FE>2017-08-17</FE><RR>10669776-0</RR><RSR>NO INFORMADO</RSR><MNT>9490</MNT><IT1>FREEZ R DAEWOO VERT A   FF 211V CHI</IT1><CAF version="1.0"><DA><RE>83382700-6</RE><RS>COMERCIAL ECCSA S A</RS><TD>39</TD><RNG><D>11701</D><H>21700</H></RNG><FA>2011-12-14</FA><RSAPK><M>1oGiMK6bTMLIfmzwmOk9EZYSJiaJS+Y4U1OQzjyCkwpxjzPhWUkaMkTdtTfvma9BsOaPAacOLlQY+TL/Z3YdgQ==</M><E>Aw==</E></RSAPK><IDK>100</IDK></DA><FRMA algoritmo="SHA1withRSA">FHuozKw1ETaABlK3zh4tfitYED/ZMOv6FlwyguZ9JRt6r0uBZ7Uk4hiPfv2Bdia8uR+PjKk8T02iUWfm6nLGTA==</FRMA></CAF><TSTED>2017-08-17T17:39:55</TSTED></DD><FRMT algoritmo="SHA1withRSA">rMBknWYHyCfhjiFXN/P13g4wnBbNXdFM6O9uYc2uZKZ2Y1nfdW0mecIf1UI9GKvwSFg/GOh0HODc2LiHRrNVog== </FRMT></TED>';
        var xmlVar = reemplazarXml('<s:property value="ted"/>');
    	var barcode = PDF417.getBarcodeArray();
          var bw = 1;
          var bh = 1;

          var canvas = document.createElement('canvas');
          canvas.width = bw * barcode['num_cols'];
          canvas.height = bh * barcode['num_rows'];
          document.getElementById('barcode').appendChild(canvas);

          var ctx = canvas.getContext('2d');

          var y = 0;
          for (var r = 0; r < barcode['num_rows']; ++r) {
              var x = 0;
              for (var c = 0; c < barcode['num_cols']; ++c) {
                  if (barcode['bcode'][r][c] == 1) {
                      ctx.fillRect(x, y, bw, bh);
                  }
                  x += bw;
              }
              y += bh;
          }

          PDF417.init(xmlVar);

          barcode = PDF417.getBarcodeArray();

          var bw = 1;
          var bh = 1;

          var container = document.getElementById('barcode');

          container.removeChild(container.firstChild);
          var canvas = document.createElement('canvas');
          canvas.width = bw * barcode['num_cols'];
          canvas.height = bh * barcode['num_rows'];
          container.appendChild(canvas);

          var ctx = canvas.getContext('2d');

          var y = 0;
          for (var r = 0; r < barcode['num_rows']; ++r) {
              var x = 0;
              for (var c = 0; c < barcode['num_cols']; ++c) {
                  if (barcode['bcode'][r][c] == 1) {
                      ctx.fillRect(x, y, bw, bh);
                  }
                  x += bw;
              }
              y += bh;
          }
      }
      //Se reemplazan los Tag < > " " & 
      function reemplazarXml(str){
      	var retorno = str;
      	retorno = retorno.replace(/&lt;/g,"<");
      	retorno = retorno.replace(/&gt;/g,">");
      	retorno = retorno.replace(/&amp;/g,"&");
      	retorno = retorno.replace(/&quot;/g,'"');
      	retorno = retorno.replace(/&quot;/g,'"');
      	retorno = retorno.replace(/&nbsp;/g," ");
      	retorno = retorno.replace(/&apos;/g,"'");
      	return retorno;
      }
  </script>
</body>
</html>
