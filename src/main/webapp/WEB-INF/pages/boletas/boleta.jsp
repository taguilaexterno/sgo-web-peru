<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="cl.ripley.omnicanalidad.util.Constantes" %>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src="js/boleta.js?<%=Constantes.VERSION_APP%>"></script>
		
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.table2excel.js"></script>
	
		<link rel="stylesheet" type="text/css" href="css/datatables.min.css"/> 
		<script type="text/javascript" src="js/datatables.min.js"></script>
		 <script type="text/javascript">
			 $(document).ready(
					function() {
				    	$('#tblDatos').DataTable({
							"bFilter": false,
							"scrollX": false,
							"sDom": '<"top"i>rtlp',
							"bInfo": false,
							"bPaginate": false,
							"bSortable": true,
							"bLengthChange": true,
							"bSort": true,
							"iDisplayLength": 10
				    	});
					}
				);
	$(function() {
				$("#aExcel").click(function(){
				$("#tblDatos").table2excel({
					exclude: ".noExl",
    				name: "Exportacion A Excel",
    				filename:"Confirmacion medio de pago"
				}); 
				 });
			});			
    </script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">Confirmaci&oacute;n de pago no seguro</li>
					</ol>

					<article id="product-list-manual">
						<h3>Confirmaci&oacute;n de pago de compras no seguras</h3>
						<h4>Total n&uacute;mero de ordenes: <span><s:property value="nroOrdenes"/></span></h4>
						<s:if test="hasActionMessages()">
							<div id="mensajeExito" class="alert alert-success">
								<s:actionmessage />
							</div>
						</s:if>
						<s:if test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger">
								<s:actionerror />
							</div>
						</s:if>
						<nav class="navbar">
							<div class="container-fluid row">
 								<div class="col-sm-4 col-xs-8">  
									<div class="input-group">
 										<input id="txtBuscar" type="text" class="form-control" placeholder="Buscar OC">  
										<span class="input-group-btn">
											<button id="btnBuscar" class="btn btn-primary" type="button">Buscar</button>
										</span>
										
									</div>

								</div>
								<br><br><br>
								<div class="form-group pull-right">

								<div class="filtros col-sm-2 col-xs-4">
															
									<div class="btn-group-md pull-right">
												
										<select id="filtroSel" class="btn btn-default">
											<option value="" selected>Filtrar por</option>
											<option value="03">Boleta</option>
											<option value="04">Factura</option>
											<option value="32">Recaudaci&oacute;n</option>
											<option value="13">Mercado Ripley</option>
											<option value="23">Ripley/Mercado</option>
											<option value="">Todos</option>
										</select>
									
										
									</div>
									
								</div>
								
								<div class="filtros col-sm-2 col-xs-4">
									<div class="btn-group-md ">
									
										<select id="filtroSelDes" class="btn btn-default">
											<option value="" selected>Filtrar por</option>
											<option value="0">S - Despacho Domicilio</option>
											<option value="1">AM - Despacho Domicilio</option>
											<option value="2">PM - Despacho Domicilio</option>
											<option value="6">RB - Retiro Remoto</option>
											<option value="7">RC - Retiro Remoto</option>
											<option value="8">RT - Retiro en Tienda</option>
											<option value="101">SH - Despacho Domicilio</option>
											<option value="102">ST - Site to store</option>
										</select>
									
										
									</div>
									
								</div>

 								<div class="form-group pull-right"> 
									<button class="btn btn-large btn-default" onclick="javascript:refrescar(event,<s:property value="estado"/>);"><span class="fa fa-refresh"></span> Refrescar</button>
									<s:if test="#session.nivelPermiso == 1">
										<button class="btn btn-large btn-danger" onclick="return false;" data-toggle="modal" data-target="#confirmar">Anular</button>
										<button class="btn btn-large btn-success" onclick="javascript:generar(event, <s:property value="estado"/>);">Generar DTE</button>
									</s:if>
								</div>
							</div>
								
							</div>
						</nav>
						<button class="btn btn-large btn-success" id="aExcel">Excel</button>
						<div class="table-responsive">

							<table id="tblDatos" class="table table-striped table-bordered">
								<thead>
									<tr>
										<s:if test="#session.nivelPermiso == 1">
											<th class="noExl"></th>
										</s:if>
										<s:if test="#session.nivelPermiso == 1">
											<th class="noExl">Acción</th>
										</s:if>
										<th>OC</th>
										<th>DNI del cliente</th>
										<th>Fecha</th>
										<th>Fecha de despacho</th>
										<th>Medio de Pago</th>
										<th>N° Cuotas</th>
										<th>Tipo de despacho</th>
										<th>Monto</th>
										<th>H.A.</th>
										<th>Estado OC</th>
										<th>Estado CoC</th>
										<th>Estado CS</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="ordenesCompra" var="orden">
									<tr class="<s:property value="#orden.NotaVenta.indicadorMkp + #orden.NotaVenta.tipoDoc"/>">
										<s:if test="#session.nivelPermiso == 1">
												<s:if test="#orden.NotaVenta.bloqueado == 'No'">
													<td class="noExl"><input class="mycheckbox" type="checkbox" id="<s:property value="#orden.NotaVenta.correlativoVenta"/>"></td>
												</s:if>
												<s:elseif test="#orden.NotaVenta.bloqueado == 'Si'">
													<td class="noExl"><img src="img/bloquear.png" width="15" height="15" title="<s:property value="#orden.NotaVenta.glosaBloqueo"/>"/></td>
											   </s:elseif>
										</s:if>
										<s:if test="#session.nivelPermiso == 1">
											<td class="noExl"><div class="center"><button class="btn btn-default btn-xs" 
											onclick="javascript:verDetalle(<s:property value="#orden.NotaVenta.correlativoVenta"/>,
											<s:property value="estado"/>);">Detalle</button></div></td>
										</s:if>
										<td><s:property value="#orden.NotaVenta.correlativoVenta"/></td>
										<td><s:property value="#orden.NotaVenta.rutComprador"/>-<s:property value="#orden.NotaVenta.dvComprador"/></td>
										<td><s:date name="#orden.NotaVenta.fechaCreacion" format="dd/MM/yyyy"/></td>
										<td><s:date name="#orden.Despacho.fechaDespacho" format="dd/MM/yyyy"/></td>
										<td><s:property value="#orden.NotaVenta.tipoPago"/></td>
										<td><s:property value="#orden.NotaVenta.ncuotas"/></td>
										<td><s:property value="#orden.TipoDespacho.nomTipoDespacho"/> - <s:property value="#orden.TipoDespacho.glosaDespacho"/></td>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#orden.NotaVenta.montoVenta)"/></td>
										<td><s:property value="#orden.NotaVenta.horasAdministrativas"/></td>
										<td><s:property value="#orden.NotaVenta.estado"/></td>
										<td><s:property value="#orden.NotaVenta.estado_COC"/></td>
										<td><s:property value="#orden.NotaVenta.estado_CS_DESC"/></td>
									</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>

							<s:form id="boletaForm" action="confirmacionBoleta" class="navbar-form navbar-top-actions">
								<s:hidden id="metodo" name="metodo" value=""/>
								<s:hidden id="seleccionados" name="seleccionados" value=""/>
								<s:hidden id="cajaEstado" name="cajaEstado" value="true"/>
<%--								<s:if test="#session.nivelPermiso == 1">
	 							<div class="form-group">
										<div class="checkbox">
											<label>
												<input type="checkbox" id="selectall" class="mycheckbox"/>
												Seleccionar todos
											</label>
										</div>
									</div>
								</s:if> --%>
								<div class="form-group pull-right">
									<button class="btn btn-default btn-sm" onclick="javascript:refrescar(event,<s:property value="estado"/>);"><span class="fa fa-refresh"></span> Refrescar</button>
									<s:if test="#session.nivelPermiso == 1">
										<button class="btn btn-danger btn-sm" onclick="return false;" data-toggle="modal" data-target="#confirmar">Anular</button>
										<button class="btn btn-success btn-sm" onclick="javascript:generar(event, <s:property value="estado"/>);">Generar DTE</button>
									</s:if>
								</div>
							</s:form>

					</article>
				<%-- FIN SECCION PRINCIPAL --%>
				</div>
			</div>
		</div>
	
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Confirmar Anular</h4>
				</div>
				<s:form id="boletaForm" action="confirmacionBoleta" theme="simple" role="form" data-toggle="validator">
					<div class="modal-body">
						<span>En el caso de ser tarjeta bancaria se generará una nota de crédito, y en el caso de ser Tarjeta Ripley o TRE será un rechazo.</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button id="btnEliminarSku" type="button" class="btn btn-primary" onclick="javascript:anular(event, <s:property value="estado"/>);">Anular</button>
					</div>
				</s:form>
			</div>
		</div>
		</div>
	</s:if>
		
		<s:include value="/commons/footer.jsp"></s:include>