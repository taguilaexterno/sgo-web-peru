<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src="js/administradorParametros.js"></script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href="#">SGO</a></li>
				<li class="active">Administrador de Parámetros</li>
			</ol>

			<article id="product-list-manual">
				<h3>Reglas para Administrador de Parámetros</h3>
				<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
				</s:if>
				<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
				</s:elseif>
				<s:form id="administradorParametrosForm" action="updateApagar">
					<s:hidden id="metodo" name="metodo"/>
						  	<div class="form-group pull-left">
						  		<label class="checkbox">
						  			<input id="checkApagar" type="checkbox" value="true"
						  			<s:if test="%{metodo==1}">checked</s:if>
						  			/>
						  		<s:if test="%{metodo==1}">
						  			<p class="help-block">Si desactivas esta opción la caja automática dejará de funcionar</p>
						  		</s:if>
						  		
						  		<s:if test="%{metodo==0}">
						  			<p class="help-block">Si activas esta opción la caja automática comenzará a funcionar</p>
						  		</s:if>
						  		
						  		</label>
						  	</div>
						 <br/>
							<s:if test="#session.nivelPermiso == 1">
							  	<div class="form-group pull-right">
								  	<button class="btn btn-default btn-lg" onclick="javascript:cancelar();">Cancelar</button>
								  	<button class="btn btn-primary btn-lg" onclick="return false;" data-toggle="modal" data-target="#confirmar">Guardar</button>
								</div>
							</s:if>
				</s:form>
			</article>

		</div>
	</div>
</div>

<s:if test="#session.nivelPermiso == 1">
<div id="confirmar" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Confirmar cambios</h4>
			</div>
			<div class="modal-body">
				<span>¿Está Seguro?</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" onclick="javascript:modificar();">Guardar</button>
			</div>
		</div>
	</div>
</div>
</s:if>

<s:include value="/commons/footer.jsp"></s:include>