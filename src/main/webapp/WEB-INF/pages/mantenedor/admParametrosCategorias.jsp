<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		
<script type="text/javascript">
	function entrar(idTipo){
		$('#idTipo').val(idTipo);
		$('#admParCatForm').submit();
	}
</script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href="#">SGO</a></li>
				<li class="active">Administrador de Par&aacute;metros</li>
				<li class="active">Categor&iacute;a</li>
			</ol>

			<article id="product-list-manual">
				<h3>Administrador de Par&aacute;metros</h3>
				<h5>Elija categor&iacute;a</h5>
				<s:form id="admParCatForm" action="administradorCategoriaParametro">
					<s:hidden id="idTipo" name="idTipo" />
				</s:form>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Id categor&iacute;a</th>
								<th>Categor&iacute;a</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
						<s:iterator value="categorias">
							<tr>
								<td><s:property value="propertyTypeId"/></td>
								<td><s:property value="description"/></td>
								<td><a href="javascript:entrar(<s:property value="propertyTypeId"/>)">Entrar</a></td>
							</tr>
						</s:iterator>
						</tbody>
					</table>
				</div>
			</article>

		</div>
	</div>
</div>

<s:include value="/commons/footer.jsp"></s:include>