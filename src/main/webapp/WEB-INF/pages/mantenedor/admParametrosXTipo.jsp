<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>

<script type="text/javascript">
	function modificar(idParametro){
		var nombre;
		var valor_old;

		nombre		= $('#td_nombre_'+idParametro).text();
		valor_old	= $('#td_valor_'+idParametro).text();
		console.log("idTipo: "+$('#idTipo').val());
		$("#par_cat option[value="+ $('#idTipo').val() +"]").attr("selected",true);
		$('#par_id_par').val(idParametro);
		$('#par_nombre').val(nombre);
		$('#par_valor_old').val(valor_old);
		$('#par_valor_new').val("").attr("placeholder", valor_old);
		
		$('#confirmar').modal('show');

	}
	
	function grabar(){
		$('#nombre').val($('#par_nombre').val());
		$('#valor').val($('#par_valor_new').val());
		$('#idParametro').val($('#par_id_par').val());
		$('#propertyTypeId').val($('#par_cat').val());
		
		$('#admParCatForm').submit();
	}
	
</script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href="#">SGO</a></li>
				<li class="active"><a href="javascript:history.back();">Administrador de Par&aacute;metros</a></li>
				<li class="active"><a href="javascript:history.back();">Categor&iacute;a</a></li>
				<li class="active">Par&aacute;metros</li>
			</ol>

			<article id="product-list-manual">
				<h3>Administrador de Par&aacute;metros</h3>
				<h5>Elija par&aacute;metro</h5>
				<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
				</s:if>
				<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
				</s:elseif>
				
				<s:form id="admParCatForm" action="updateParametro">
					<s:hidden id="propertyTypeId" name="parametro.propertyTypeId" value="%{idTipo}" />
					<s:hidden id="idTipo" name="idTipo" value="%{idTipo}" />
					<s:hidden id="idParametro" name="parametro.idParametro" />
					<s:hidden id="nombre" name="parametro.nombre" />
					<s:hidden id="valor" name="parametro.valor" />
				</s:form>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Id par&aacute;metro</th>
								<th>Nombre</th>
								<th style="width:30%;">Valor</th>
								<s:if test="#session.nivelPermiso == 1">
								<th>Acciones</th>
								</s:if>
							</tr>
						</thead>
						<tbody>
						<s:iterator value="parametros">
							<tr>
								<td><s:property value="idParametro"/></td>
								<td id="td_nombre_<s:property value="idParametro"/>"><s:property value="nombre"/></td>
								<td id="td_valor_<s:property value="idParametro"/>"><div style="width:250px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><s:property value="valor"/></div></td>
								<s:if test="#session.nivelPermiso == 1">
								<td><a href="javascript:modificar(<s:property value="idParametro"/>)">Modificar</a></td>
								</s:if>
							</tr>
						</s:iterator>
						</tbody>
					</table>
				</div>
			</article>

		</div>
	</div>
</div>

<div id="confirmar" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Modificar Valor</h4>
			</div>
				<div class="modal-body">
					<div class="alert alert-warning">Tener en cuenta que un cambio puede afectar el correcto funcionamiento de alguna aplicaci&oacute;n</div>
					<div class="row">
						<s:hidden id="par_id_par" name="par_id_par" />
						<div class="col-md-12">
							<div class="form-group">
								<label>Categor&iacute;a</label>
								<select class="form-control" id="par_cat">
									<s:iterator value="categorias">
										<option value="<s:property value="propertyTypeId"/>"><s:property value="description"/></option>
									</s:iterator>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" id="par_nombre" class="form-control" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Valor Anterior</label>
								<input type="text" id="par_valor_old" class="form-control" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Valor Nuevo</label>
								<input type="text" id="par_valor_new" class="form-control" placeholder="email@email.com">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<s:if test="#session.nivelPermiso == 1">
					<button id="btnModificarPar" type="button" class="btn btn-primary" onclick="grabar();">Modificar</button>
					</s:if>
				</div>

		</div>
	</div>
</div>


<s:include value="/commons/footer.jsp"></s:include>