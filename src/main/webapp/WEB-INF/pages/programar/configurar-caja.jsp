<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>

<script type="text/javascript"
	src='<s:url value="/js/configurarCaja.js"/>'></script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Configuraci&oacute;n de cajas</li>
			</ol>

			<article id="product-list-manual">
				<h3>Configuraci&oacute;n de cajas</h3>
				<s:if test="hasActionMessages()">
					<div id="mensajeExito" class="alert alert-success">
						<s:actionmessage />
					</div>
				</s:if>
				<s:if test="hasActionErrors()">
					<div id="mensajeError" class="alert alert-danger">
						<s:actionerror />
					</div>
				</s:if>

				<s:form id="configSucursalForm" theme="simple" role="form"
					data-toggle="validator">
					<div class="col-xs-12">
						<div class="form-group">
							<div class="col-xs-6">
								<label>Sucursal</label> 
								<select class="form-control" name="sucursalSelect">
									<s:iterator value="sucursalesList" var="suc" status="status">
											<option><s:property value="#suc" /></option>
									</s:iterator>
								</select>
							</div>
							<div class="col-xs-6">
								<label>Caja</label> 
								<select class="form-control" name="cajaSelect">
									<s:if test="cajasList != null">
										<s:iterator value="cajasList" var="caj" status="status">
												<option><s:property value="#caj" /></option>
										</s:iterator>
									</s:if>
								</select>
							</div>
						</div>
					</div>
				</s:form>
				
				<div class="clearfix visible-xs-block visible-ms-block visible-md-block visible-lg-block">&nbsp;</div>
				
 				<s:form action="guardarConfiguracionCierreCaja"
					id="guardarConfiguracionCierreCaja" theme="simple" role="form"
					data-toggle="validator">

					<s:if test="#session.nivelPermiso == 1">
						<s:hidden name="urlEliminarCierreCaja" value="eliminarCierreCaja.action?numeroCierre" /> 
					</s:if>

					<div class="col-xs-6">
							<div class="form-group">
								<label>Configuraci&oacute;n de tareas</label>
								<div class="responsive-table">
									<table class="table table-bordered table-striped" id="tblDatos">
										<thead>
											<tr>
												<th>Sucursal</th>
												<th>Caja</th>
												<th>Horario Cierre</th>
												<s:if test="#session.nivelPermiso == 1">
													<th>Eliminar</th>
												</s:if>
												<th style="visibility: hidden;">N&uacute;mero</th>
											</tr>
										</thead>
										<tbody>
											<s:iterator value="progList" var="prog" status="status">
												<s:if test="#prog.tipoCierre == 2">
													<tr>
														<td><div>${prog.numeroSucursal}</div></td>
														<td><div>${prog.numeroCaja}</div></td>
														<td>
															<div name="divChange">${prog.horaCierre}</div>
															<div class="form-group">
																<div class='input-group date' style="display: none;">
																	<s:textfield
																		name="%{'progList[' + #status.index + '].horaCierre'}"
																		cssClass="form-control" required="" />
																	<span class="input-group-addon"> <span
																		class="glyphicon glyphicon-time"></span>
																	</span>
																</div>
																<div class="help-block with-errors"></div>
															</div>
														</td>
														
														<s:if test="#session.nivelPermiso == 1">
															<s:url action="eliminarCierreCaja" var="urlEliminar">
																<s:param name="numeroCierre">${prog.id}</s:param>
															</s:url>
															<td><a href="${urlEliminar}&sucursalSelect=${prog.numeroSucursal}&cajaSelect=${prog.numeroCaja}"><button type="button" class="btn btn-default btn-xs">Eliminar</button></a></td>
														</s:if>
														<td style="visibility: hidden;">${prog.id}
														<s:hidden name="%{'progList[' + #status.index + '].id'}" /> 
														<s:hidden name="%{'progList[' + #status.index + '].tipoCierre'}" />
														<s:hidden name="%{'progList[' + #status.index + '].numeroCaja'}" />
								   						<s:hidden name="%{'progList[' + #status.index + '].numeroSucursal'}" />
														</td>
													</tr>
												</s:if>
											</s:iterator>
										</tbody>
									</table>
								</div>
								<s:if test="#session.nivelPermiso == 1">
									<button id="agregarHorario" type="button"
										class="btn btn-primary pull-left" data-toggle="modal"
										data-target="#add-cierre">Agregar</button>
								</s:if>
							</div>
						</div>
						<div  class="col-xs-6">
														
							<s:iterator value="progList" var="prog" status="status">
							
								<s:if test="#prog.tipoCierre == 1">
									<div id="tipoCierre_1">
									<s:hidden name="%{'progList[' + #status.index + '].tipoCierre'}" />
									<s:hidden name="%{'progList[' + #status.index + '].id'}" />
									<s:hidden name="%{'progList[' + #status.index + '].numeroCaja'}" />
								    <s:hidden name="%{'progList[' + #status.index + '].numeroSucursal'}" />
								    </div>
								    
									<div class="form-group">
										<label>Volumen</label> <input type="number" id="volumenInput"
											name="progList[${status.index}].volumen" class="form-control"
											placeholder="1000" value="${prog.volumen}"
											data-error="Debe llenar este campo" required
											<s:if test="#session.nivelPermiso != 1">disabled</s:if> />
										<div class="help-block with-errors"></div>
									</div>
									<div class="form-group">
										<label>Hora de Cierre Final</label>
										<div class='input-group date'>
											<s:if test="#session.nivelPermiso == 1">
												<s:textfield id="horaCierre"
													name="%{'progList[' + #status.index + '].horaCierre'}"
													cssClass="form-control" placeholder="23:00:00"
													data-error="Debe llenar este campo" required="" />
												<span class="input-group-addon"> <span
													class="glyphicon glyphicon-time"></span>
												</span>
											</s:if>
											<s:else>
												<s:textfield id="horaCierre"
													name="%{'progList[' + #status.index + '].horaCierre'}"
													cssClass="form-control" placeholder="23:00:00"
													data-error="Debe llenar este campo" required=""
													disabled="true" />
											</s:else>
										</div>
										<div class="help-block with-errors"></div>
									</div>
								</s:if>
							</s:iterator>
						</div>
						<s:if test="#session.nivelPermiso == 1">
							<div class="col-xs-6 pull-right">
								
								<button type="submit" class="btn btn-primary pull-right">Guardar cambios</button>
							</div>
						</s:if>
				</s:form>
			</article>
		</div>
	</div>
</div>

<s:if test="#session.nivelPermiso == 1">
	<div id="add-cierre" class="modal fade">
		<div class="modal-dialog modal-sm">
			<s:form action="agregarCierre" theme="simple" role="form"
				data-toggle="validator">
				<div id="progCierreNew">
				<s:hidden name="progCierreNew.tipoCierre" value="2" />
				<s:hidden name="progCierreNew.volumen" value="%{progList[0].volumen}" />
				<s:hidden name="progCierreNew.numeroCaja" value="%{progList[0].numeroCaja}" />
				<s:hidden name="progCierreNew.numeroSucursal" value="%{progList[0].numeroSucursal}" />	
				</div>
				<div id="tipoCierre_2"> 
				<s:iterator value="progList" var="prog" status="status">
					<s:hidden name="%{'progList[' + #status.index + '].tipoCierre'}" />
					<s:hidden name="%{'progList[' + #status.index + '].id'}" />
					<s:hidden name="%{'progList[' + #status.index + '].horaCierre'}" />
					<s:hidden name="%{'progList[' + #status.index + '].numeroCaja'}" />
					<s:hidden name="%{'progList[' + #status.index + '].numeroSucursal'}" />
				</s:iterator>
				</div>
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Agregar Horario Cierre</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label>Hora de Cierre</label>
									<div class='input-group date'>
										<s:textfield id="horaCierre" name="progCierreNew.horaCierre"
											cssClass="form-control" placeholder="23:00:00"
											data-error="Debe llenar este campo" required="" />
										<span class="input-group-addon"> <span
											class="glyphicon glyphicon-time"></span>
										</span>
									</div>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Guardar cambios</button>
					</div>
				</div>
			</s:form>
		</div>
	</div>
</s:if>


<s:include value="/commons/footer.jsp"></s:include>
