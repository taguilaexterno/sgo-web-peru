<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
<!DOCTYPE html>
<html lang="es">
	<head> 
		<title>SGO - Sistema de Gestión de Ordenes</title>
		<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8" >
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
		<link rel="stylesheet" href="css/style.css" type="text/css"/>

		<script type="text/javascript" src="js/jquery.min.js "></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- Linea recaptcha -->
		 <script src="https://www.google.com/recaptcha/api.js"></script>
		<!--[if lte IE 8]>
  			<link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<!--[if IE 9]>
		  <link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<link rel="icon" href="img/favicon.ico">

		<!--[if lt IE 9]>
    		<script type="text/javascript" src="js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body onload="myFunction()">
		<nav class="navbar">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
				    	<h1><a href='<s:url value="/"/>'>SGO</a></h1>
				    	<h2>Sistema de Gesti&oacute;n de Ordenes</h2>
				    </div>
				</div>
			</div>
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div id="login">
						<s:form id="formLogin" name="formLogin" action="login" method="post" namespace="/" theme="simple">
						<s:if test="recaptcha == 1">
						<div class="form-group">
								<label>Nombre de usuario</label>
								<s:textfield name="username" id="username" cssClass="form-control" onchange="myFunction()"></s:textfield>
							</div>
							<div class="form-group">
								<label>Contrase&ntilde;a</label>
								<s:password name="password" id="password" cssClass="form-control" onchange="myFunction()"></s:password>
							</div>
							
								 <div class="form-group">
									<div class="g-recaptcha"  data-callback="imNotARobot" data-sitekey="6Lc1P5IdAAAAAAAi0cHfHHsu8JqtVOpuzXvgNsBR"></div>								
								 </div>							 
							<div class="form-actions">
								<button name="iniciar" class="btn btn-primary" disabled>Ingresar</button>
							</div>
						 </s:if>
						 <s:if test="recaptcha != 1">
							<div class="form-group">
								<label>Nombre de usuario</label>
								<s:textfield name="username" id="username" cssClass="form-control" onchange="myFunction1()"></s:textfield>
							</div>
							<div class="form-group">
								<label>Contrase&ntilde;a</label>
								<s:password name="password" id="password" cssClass="form-control" onchange="myFunction1()"></s:password>
							</div>
															 						 
							<div class="form-actions">
								<button name="iniciar" class="btn btn-primary" disabled>Ingresar</button>
							</div>
							
							</s:if>
						</s:form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript"> 		
						
		   var imNotARobot = function() {			   				  
			  document.getElementsByName("iniciar").forEach(e => {							  	  
				  if(document.getElementById("username").value != '' && document.getElementById("password").value != ''){
					  document.getElementById("login").click();					  
					  e.disabled = false;  
				  }			     				  
			    });		 				  			  
		  };

		  function myFunction(val) {
			  
			  if(document.getElementById("username").value != '' && document.getElementById("password").value != '' && grecaptcha.getResponse()!=''){
				  document.getElementsByName("iniciar").forEach(a => {
				      a.disabled = false;
				    });  				 
			  }else{
				  document.getElementsByName("iniciar").forEach(a => {
				      a.disabled = true;
				    });  
			  }
			}		
 			function myFunction1(val) {
			  
			  if(document.getElementById("username").value != '' && document.getElementById("password").value != ''){
				  document.getElementsByName("iniciar").forEach(a => {
				      a.disabled = false;
				    });  				 
			  }else{
				  document.getElementsByName("iniciar").forEach(a => {
				      a.disabled = true;
				    });  
			  }
			}
 		</script>
	</body>
</html>