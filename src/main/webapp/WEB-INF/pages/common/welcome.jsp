<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">Bienvenido</li>
					</ol>

					<article id="product-list-manual">
						<h3>Bienvenido</h3>
						<div class="col-md-4">&nbsp;</div>
						<div class="col-md-4" style="margin: 50px 200px;">
							<p><img src="img/titbievenido.jpg" width="379" height="47" /></p>
							<p>Sr(a). <strong><s:property value="#session.usuario.perfil.nombrePerfil"/></strong> del Sistema Omnicanalidad</p>
							<p></p>
							<p>Escoja la opci&oacute;n que desee para realizar sus operaciones</p>
						</div>
					 	<div align="center"  style="margin: 50px 200px;">
						
						<s:if test="#session.usuario.perfil.nombrePerfil == 'Administrador' || #session.usuario.perfil.nombrePerfil == 'Supervisor CCR'">
						<table border="0" >
						<tr>
								<td colspan="3" width="400"><h3>Resumen de Ordenes Pendientes</h3></td>
								
							</tr>
							<tr>
								<td width="400">Ordenes Por Confirmar Medio de Pago</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.medioPago"/></td>
							</tr>
							<tr>
								<td width="400">Pendientes de Aprobacion Manual de Documentos&nbsp;</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.ordenesManuales"/></td>
							</tr>
						
							<tr>
								<td width="400">Ordenes con Rechazo Total de Stock</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.rechazoStockTotal"/></td>
							</tr>
							
							<!-- tr>
								<td width="400">Ordenes con Rechazo Parcial de Stock</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.rechazoStockParcial"/></td>
							</tr -->
							<tr>
							<td width="400">Ordenes Sin Stock</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.sinStock"/></td>
							</tr>
							<tr>
								<td width="400">Ordenes Pendientes de Validacion de Stock</td>
								<td>&nbsp;:&nbsp;</td>
								<td>&nbsp;<s:property value="panel.validacionPendienteStock"/></td>
							</tr>
							
						</table>
					</s:if>
					
						</div> 

					</article>
				<%-- FIN SECCION PRINCIPAL --%>
				</div>
			</div>
		</div>
		<s:include value="/commons/footer.jsp"></s:include>
