<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		
		<script type="text/javascript" src="js/jquery.table2excel.js"></script>
		
		<link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>
		<script type="text/javascript" src="js/datatables.min.js"></script>
		
		<script type="text/javascript" src="js/fechaNotasCreditoTBK.js"></script>
		
	 <script type="text/javascript">
            function warningFechas(){
	            var fecDesde = document.getElementById("fechaDesde").value;
	            var fecHasta = document.getElementById("fechaHasta").value;              
	            //alert("Entro al alert previa ida al action: " + fecDesde + "  xxx " + fecHasta);                       
	            var x = fecDesde.split("/");
	            var z = fecHasta.split("/");     
	                    
	            if(x[2]>z[2]){
	                 //alert("El año desde es mayor que el hasta");
	                 document.getElementById("alertaFecha").style.display='block';
	                 return false;
	            }
	            else{
	                 if(x[1]>z[1]){
	                   //alert("El mes desde es mayor que el mes hasta");
	                   document.getElementById("alertaFecha").style.display='block';
	                   return false;                                                                                                    
	                 }
	                 else{
		                 if(x[1] == z[1]){
			                 if(x[0]>z[0]){
			                       //alert("El dia desde es mayor que el dia hasta");
			                       document.getElementById("alertaFecha").style.display='block';
			                      return false;
			                   }  
		                 }
	                 }                 
	        	}  
	            document.formFechas.submit();                                           
            }
            
			$(document).ready(
				function() {
			    	$('#tableTbk').DataTable({
						"bFilter": false,
						"scrollX": false,
						"sDom": '<"top"i>rtlp',
						"bInfo": false,
						"bPaginate": false,
						"bSortable": true,
						"bLengthChange": true,
						"bSort": true,
						"iDisplayLength": 10
			    	});
				}
			);
			$(function() {
				$("#aExcel").click(function(){
				$("#tableTbk").table2excel({
					exclude: ".noExl",
    				name: "Exportacion A Excel",
    				filename:"Anulaciones Transbank"
				}); 
				 });
			});
    </script>
         
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">Nota de Crédito TBK</li>
					</ol>
        <div class="alert alert-warning" style="display:none;" id="alertaFecha">Fecha Desde debe ser inferior que fecha Hasta</div>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
				<h3>Lista Anulación Transbank</h3>
				
				<s:form action="notasCreditoTBK" theme="simple" role="form" data-toggle="validator" name="formFechas">
				<div class="clear"></div>
					<div class="col-md-5">
						<div class="form-group">
							<label>Fecha desde:</label>
								<div class="input-group date" >

										<s:if test="fechaDesde != null">
											<input id="fechaDesde" name="fechaDesde" class="datepicker" data-date-format="dd/mm/yyyy" value="${fechaDesde}">
										</s:if>
									
									<span class="fa fa-calendar" onclick="$('#fechaDesde').datepicker('show')"></span>
									
								</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">	
							<label>Fecha hasta:</label>
								<div class="input-group date" >
								    <s:if test="fechaHasta != null">
										<input id="fechaHasta" name="fechaHasta" class="datepicker" data-date-format="dd/mm/yyyy" value="${fechaHasta}"> 
									</s:if>	
									<span class="fa fa-calendar" onclick="$('#fechaHasta').datepicker('show')"></span>
								
								</div>
						</div>
					</div>
					
				
				<div class="clear"></div>
		        <div class="col-md-5">
						<div class="form-group">	
							<button type="button" class="btn btn-lg btn-primary pull-left"  onClick="warningFechas();">Buscar</button>
						</div>
					</div>
					</s:form>
					<div class="clear"></div>
					 <br> <br>
                 	<div class="form-group pull-right">

						 											
										<select id="filtroSelDesTbk" class="btn btn-default">
											<option value="" selected>Filtrar por</option>
											<option value="OK">OK</option>
											<option value="MANUAL">MANUAL</option>
											<option value="Error">Error no tipificado</option>
											<option value="entrada">Validacion de campos de entrada nulos</option>
											<option value="comercio">Codigo de comercio no existe</option>
											<option value="activo">El comercio no se encuentra activo</option>
											<option value="permitida">Operacion no permitida</option>
											<option value="encontrada">Transaccion no encontrada</option>
											<option value="permite">La transaccion no permite anulacion</option>
											<option value="autorizada">La transaccion no esta autorizada</option>
											<option value="excedido">Periodo de anulacion excedido</option>
											<option value="previamente">Transaccion anulada previamente</option>
											<option value="diponible">Monto a anular excede el saldo diponible para anular</option>
											<option value="generico">Error generico para anulaciones</option>
											<option value="autorizador">Error del autorizador</option>
										</select>
							
					</div>	
                     <br> <br>
                    
					<h4>Lista de Anulación</h4>
					<button class="btn btn-large btn-success" id="aExcel">Excel</button>
					<div class="table-responsive">
						<table class="table table-striped table-bordered" cellspacing="0" id="tableTbk">
							<thead>
								<tr>
									<th>Orden de Compra</th>
									<th>Fecha Compra</th>
									<th>Número de TRX</th>
									<th>Nota Crédito</th>
									<th>Folio Nota Crédito</th>
									<th>DNI Comprador</th>
									<th>Código Autorización</th>									
									<th>Monto Autorizado</th>
									<th>Monto Anulado</th>
									<th>Código Comercio</th>
									<th>Fecha Anulación</th>
									<th>Código Error</th>
									<th>Descripcion Error</th>
									<th>N° Intentos</th>
								</tr>
							</thead>
							<tbody>
								<s:iterator value="listaAnulacion" var="comp">
									<tr>
										<td><s:property value="#comp.ordenCompra"/></td>
										<td><s:property value="#comp.fechaCompra"/></td>
										<td><s:property value="#comp.numBoleta"/></td>
										<td><s:property value="#comp.numNotaCredito"/></td>
										<td><s:property value="#comp.folioNc"/></td>
										<td><s:property value="#comp.rutComprador"/></td>
										<td><s:property value="#comp.codigoAutorizacion"/></td>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp.montoAutorizado)"/></td>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp.montoAnulado)"/></td>
										<td><s:property value="#comp.codigoComercio"/></td>
										<td><s:property value="#comp.fechaAnualacion"/></td>
										<td><s:property value="#comp.codigoError"/></td>
										<td><s:property value="#comp.descripcionError"/></td>										
										<td><s:property value="#comp.intento"/></td>					
									</tr>
			
								</s:iterator>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%-- FIN SECCION PRINCIPAL --%>
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>
