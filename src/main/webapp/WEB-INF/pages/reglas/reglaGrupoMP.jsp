<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>	
		<script type="text/javascript" src="js/mpseguro.js"></script>
		
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href="#">SGO</a></li>
						<li class="active">Grupo Reglas pago seguro</li>
					</ol>

					<article id="product-list-manual">
						<h3>Administrador para Grupo Reglas Medio de Pago Seguro</h3>
							<s:form id="grupoMPSeguroForm" action="admGrupoMPSeguro">
							<s:hidden id="metodo" name="grupoMP.metodo" />
							<s:hidden id="estado" name="grupoMP.estado" />
							<s:hidden id="id" name="grupoMP.id" />
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Id Grupo</th>
													<th>Descripción</th>
													<s:if test="#session.nivelPermiso == 1">
														<th colspan="2">Acciones</th>
													</s:if>
												</tr>
											</thead>
											<tbody>
											<s:iterator value="grupoReglasMp">
												<tr>
													<td><s:property value="id"/></td>
													<td><s:property value="descripcion"/></td>
													<s:if test="#session.nivelPermiso == 1">
														<td>									
															<s:if test="estado==1">								
																<div class="center" id="<s:property value="id"/>" 
																onclick="setEstadoGrupo(1,<s:property value="id"/>);" data-toggle="modal" data-target="#confirmar">
																	<span class="label label-success">
																	<span class="fa fa-check"></span>
																	Activo</span>
																</div>
															</s:if>
															<s:else>
																<div class="center" id="<s:property value="id"/>" 
																onclick="setEstadoGrupo(0,<s:property value="id"/>);" data-toggle="modal" data-target="#confirmar">
																	<span class="label label-danger">
																	<span class="fa fa-check">
																	Inactivo
																	</span>
																	</span>
																</div>
															</s:else>
														</td>
													</s:if>
												</tr>
											</s:iterator>
											</tbody>
										</table>							
									</div>		
							</s:form>
					</article>	
				</div>
			</div>
		</div>
	
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">confirmar grupo regla de Pago Seguro</h4>
					</div>
					<s:form id="grupoMPSeguroForm" action="admGrupoMPSeguro" theme="simple" role="form" data-toggle="validator">
						<div class="modal-body">
							<span>¿Está Seguro?</span>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
							<button type="button" class="btn btn-primary" onclick="updateEstadoGrupo();">Guardar</button>
						</div>
					</s:form>
				</div>
			</div>
		</div>
	</s:if>
		
	<s:include value="/commons/footer.jsp"></s:include>