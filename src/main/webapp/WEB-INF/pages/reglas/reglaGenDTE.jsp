<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src="js/dteauto.js"></script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href="#">SGO</a></li>
				<li class="active">Reglas para generación de DTE automático</li>
			</ol>

			<article id="product-list-manual">
				<h3>Reglas para generación de DTE automático</h3>
				<s:form id="reglaGeneracionDTEForm" action="reglaGeneracionDTE">
				<s:hidden id="metodo" name="metodo"/>
				
				<s:hidden id="rpid" name="reglaPrecio.id"/>
				<s:hidden id="rpidvalidacion" name="reglaPrecio.idValidacion"/>
				<s:hidden id="rpmetodo" name="reglaPrecio.idParam"/>
				<s:hidden id="rpvalor" name="reglaPrecio.valor"/>
				<s:hidden id="skuid" name="reglaSku.id"/>
				<s:hidden id="skuidvalidacion" name="reglaSku.idValidacion"/>
				<s:hidden id="skumetodo" name="reglaSku.idParam"/>
				<s:hidden id="todoid" name="reglaTodoManual.id"/>
				<s:hidden id="todoidvalidacion" name="reglaTodoManual.idValidacion"/>
				<s:hidden id="todometodo" name="reglaTodoManual.idParam"/>			
				
							<div class="form-group pull-left">
						    	<label class="checkbox">
							    		<input id="checkrp" type="checkbox" name="reglaPrecio.estado" value="true"
							    		<s:if test="reglaPrecio.estado">checked="checked"</s:if>
							    		<s:if test="#session.nivelPermiso != 1">disabled</s:if>/>Reglas de precio
						    	</label>
						  	</div>
						  	<div class="form-group pull-right" data-toggle="modal" data-target="#editarfactor">
						  		<button class="btn btn-default" onclick="return false;">
						  			<s:if test="#session.nivelPermiso == 1">
						  				Editar factor
						  			</s:if>
						  			<s:else>
						  				Ver factor
						  			</s:else>
						  		</button>
						  	</div>
						  					  	
						  	<hr>
						  	<div class="form-group pull-left">
						  		<label class="checkbox">
						  			<input id="checksku" type="checkbox" name="reglaSku.estado" value="true"
						  			<s:if test="reglaSku.estado">checked="checked"</s:if>
						  			<s:if test="#session.nivelPermiso != 1">disabled</s:if>/>Listas de SKU
						  		</label>
						  	</div>
						  	<div class="form-group pull-right">
						  		<button class="btn btn-default" onclick="javascript:modificar();">
							  		<s:if test="#session.nivelPermiso == 1">
							  			Modificar
							  		</s:if>
							  		<s:else>
							  			Ver
							  		</s:else>
						  		</button>
						  	</div>
						  	<hr>
						  	<div class="form-group pull-left">
						  		<label class="checkbox">
						  			<input id="checktodo" type="checkbox" name="reglaTodoManual.estado" value="true"
						  			<s:if test="reglaTodoManual.estado">checked="checked"</s:if>
						  			<s:if test="#session.nivelPermiso != 1">disabled</s:if>/>Todo manual
						  		</label>
						  	</div>
						  	<div class="form-group pull-right">
						  		<p class="help-block">Si activas esta opción todas las ordenes pasaran a confirmación Manual DTE.</p>
						  	</div>
						  	
							<s:if test="#session.nivelPermiso == 1">
							  	<div class="form-group pull-right">
								  	<button class="btn btn-default btn-lg" onclick="javascript:cancelar();">Cancelar</button>
								  	<button class="btn btn-primary btn-lg" onclick="return false;" data-toggle="modal" data-target="#confirmar">Guardar</button>
								</div>
							</s:if>
				</s:form>
			</article>

		</div>
	</div>
</div>

<s:if test="#session.nivelPermiso == 1">
<div id="confirmar" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">confirmar Reglas de Precio</h4>
			</div>
			<div class="modal-body">
				<span>¿Está Seguro?</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" onclick="javascript:updateEstado();">Guardar</button>
			</div>
		</div>
	</div>
</div>
</s:if>

<div id="editarfactor" class="modal fade">
	<div class="modal-dialog">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Editar Regla de Precio</h4>
						</div>
						<s:form id="reglaGeneracionDTEForm" action="reglaGeneracionDTE" theme="simple" role="form" data-toggle="validator">
							<div class="modal-body">
								<div class="form-group">
									<label>Regla de Precio</label>
									<input id="inptvalor" min="0" max="1000000" type="number" step="0.01" class="form-control input-lg" 
									placeholder="<s:property value="reglaPrecio.valor"/>" required value="${reglaPrecio.valor}"
									<s:if test="#session.nivelPermiso != 1">disabled</s:if>>
									<input id="factortxt" type="text" value="" style="border-width:0;">
									<div id="bootError" class="help-block with-errors"></div>
								</div>
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
								<s:if test="#session.nivelPermiso == 1">
									<button type="button" class="btn btn-primary" onclick="javascript:updateFactor();">Guardar</button>
								</s:if>
							</div>
						</s:form>
				</div>
	</div>
</div>

<s:include value="/commons/footer.jsp"></s:include>