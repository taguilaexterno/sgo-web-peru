<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src="js/dteauto.js"></script>
		
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href="#">SGO</a></li>
						<li class="active">Productos que pasan Aprobación Manual de DTE</li>
					</ol>

					<article id="product-list-manual">
						<h3>Productos que pasan Aprobación Manual de DTE</h3>
						
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger"><s:actionerror/></div>
						</s:elseif>
						
						<s:form id="reglaGeneracionDTEForm" action="reglaGeneracionDTE">
							<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<s:if test="#session.nivelPermiso == 1">
											<th><input type="checkbox" id="selectall" class="mycheckbox"/></th>
										</s:if>
										<th>SKU</th>
										<th>Descripción</th>
										<th>Fecha Ingreso</th>
										<th>Ingresado por</th>
									</tr>
								</thead>
								<tbody>
								<s:iterator value="productosDTE">
									<tr>
										<s:if test="#session.nivelPermiso == 1">
											<td><input class="mycheckbox" type="checkbox" id="<s:property value="codigoArticulo"/>"/></td>
										</s:if>
										<td><s:property value="codigoArticulo"/></td>
										<td><s:property value="descripcion"/></td>
										<td><s:date name="fechaIngreso" format="dd/MM/yyyy"/></td>
										<td><s:property value="usuarioCreacion"/></td>
									</tr>
								</s:iterator>
								</tbody>
							</table>							
						</div>
						<s:if test="#session.nivelPermiso == 1">
						  <div class="form-group pull-left">
							  <button class="btn btn-default btn-lg" onclick="return false;" data-toggle="modal" data-target="#agregarSKU">Agregar SKU</button>
							  <button class="btn btn-default btn-lg" onclick="return false;" data-toggle="modal" data-target="#confirmar">Eliminar SKU</button>							  
						  </div>			   
					
						  <div class="form-group pull-right">
							  <button type="submit" class="btn btn-default btn-lg" onclick="return false;" data-toggle="modal" data-target="#uploadMdl">Carga Lista</button>
						  </div>
					  	</s:if>
					  
					  </s:form>	
						
						
					</article>
						
				</div>
			</div>
		</div>
		
<s:if test="#session.nivelPermiso == 1">
<div id="agregarSKU" class="modal fade">
	<div class="modal-dialog">
				<div class="modal-content" >
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Agregar SKU</h4>
							<s:form id="productoSKUForm" action="reglaDTESkuAction" theme="simple" role="form" data-toggle="validator">
							<s:hidden id="metodo" name="metodo" value=""/>
							<s:hidden id="skuArticulo" name="productoSku.codigoArticulo" value=""/>
							<s:hidden id="seleccionados" name="seleccionados" value=""/>
								<div class="modal-body">
									<div id="agregarDiv" class="form-group">
										<label>SKU</label>
									
										<div class="input-group">
											<input id="inptvalor" min="0" max="10000000000000" type="number" class="form-control input-lg" 
											placeholder="SKU" required>
											<span class="input-group-btn">
												<button id="btnConsultaSku" class="btn btn-primary" type="button" onclick="javascript: return false;">Buscar</button>
											</span>
										</div>
										<div id="bootError" class="help-block with-errors"></div>
									</div>
									<div id="divConsulta" class="form-group">
									</div>								
								</div>
							
								<div class="modal-footer">
									<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
									<button id="btnGrabarSku" type="button" class="btn btn-primary" onclick="javascript: guardar();" disabled="disabled">Guardar</button>
								</div>
						</s:form>
						</div>
				</div>
	</div>
</div>

<div id="confirmar" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">confirmar eliminar articulo SKU</h4>
			</div>
			<s:form id="productoSKUForm" action="reglaDTESkuAction" theme="simple" role="form" data-toggle="validator">
				<div class="modal-body">
					<span>¿Está Seguro?</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<button id="btnEliminarSku" type="button" class="btn btn-primary" onclick="javascript: return false;">Eliminar</button>
				</div>
			</s:form>
		</div>
	</div>
</div>

<div id="uploadMdl" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="uploadForm" action="uploadAction" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Carga Archivo</h4>
				</div>
				<div class="modal-body">
					<div id="uploadFile" class="input-group">
						<input id="btnLoadFile" type="file" name="myFile" accept=".txt" />
					</div>
					<div id="dvLoading" style="visibility: hidden;">
						<img id="imgLoading" alt="Espere por favor" src="img/loading.gif">
						<span>Por favor, Espere un momento... <span>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<button id="btncargar" type="submit" class="btn btn-default btn-lg">Carga</button>
				</div>
			</form>
		</div>
	</div>
</div>
</s:if>		
		
	<s:include value="/commons/footer.jsp"></s:include>