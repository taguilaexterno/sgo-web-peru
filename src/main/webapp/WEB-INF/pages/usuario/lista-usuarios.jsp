<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		
<script type="text/javascript" src='<s:url value="/js/listaUsuarios.js"/>'></script>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Lista de usuarios</li>
					</ol>

					<article id="product-list-manual">
						<h3>Usuarios</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger"><s:actionerror/></div>
						</s:elseif>

						<s:form action="listaUsuarios" theme="simple" cssClass="navbar-form user-navbar">
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="usuarioBean.usuario" class="form-control" placeholder="Buscar usuario">
									<div class="input-group-btn">
										<button class="btn btn-primary">Buscar</button>
									</div>
								</div>
							</div>
							<s:if test="#session.nivelPermiso == 1">
								<div class="form-group pull-right">
									<a href='<s:url action="crearUsuario"/>'><button type="button" class="btn btn-default"><span class="fa fa-plus"></span> Crear usuario</button></a>
								</div>
							</s:if>
						</s:form>

						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Usuario</th>
										<th>Email</th>
										<th>Perfil</th>
										<th>Vigencia</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="usuarios" var="usr">
										<tr>
											<td>${usr.nombre} ${usr.apePaterno} ${usr.apeMaterno}</td>
											<td>${usr.usuario}</td>
											<td>${usr.email}</td>
											<td>${usr.perfil.nombrePerfil}</td>											
											<td>${usr.vigencia}</td>																					
											<td>
												<div class="center">
													<button onclick="javascript: getUserForModal('${usr.usuario}')" type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#edit-user">
														<s:if test="#session.nivelPermiso == 1">
															Editar
														</s:if>
														<s:else>
															Ver
														</s:else>
													</button>
													<s:if test="#session.nivelPermiso == 1">
														<s:form action="actualizarUsuario" theme="simple">
															<input type="hidden" name="usuarioBean.codUsuario" value="${usr.codUsuario}"/>
															<input type="hidden" name="usuarioBean.codEstado" value="0"/>
															<button class="btn btn-danger btn-xs">Eliminar</button>
														</s:form>
													</s:if>
												</div>
											</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</article>
				</div>
			</div>
		</div>

		<div id="edit-user" class="modal fade">
			<div class="modal-dialog modal-lg">
				<s:form action="actualizarUsuario" theme="simple" role="form" data-toggle="validator">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Editar Usuario</h4>
					</div>
					<input type="hidden" id="usuarioBean_codUsuario" name="usuarioBean.codUsuario"/>
					<input type="hidden" id="usuarioBean_codEstado" name="usuarioBean.codEstado"/>
					<input type="hidden" id="txtEditFechaFin" name="usuarioBean.fecFinVigencia"/>
					
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditNombreUsuario" name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre solo debe contener letras." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditNombreUsuario" name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre"
										data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Apellido Paterno</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditApePat" name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras."/>
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditApePat" name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Apellido Materno</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditApeMat" name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditApeMat" name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Vigencia Usuario</label>
									  
									<div class="form-group">
									 
									<s:select headerKey="-1" headerValue="Seleccione" id="cmbVigencia"
										list="searchEngine" 
										name="yourSearchEngine" 
										value="defaultSearchEngine" class="form-control"/>
								     																			
									</div>																				
								<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Email</label>
									<input type="email" id="txtEditEmail" name="usuarioBean.email" class="form-control" placeholder="email@email.com" 
											data-error="Email no válido" data-minlength="2" maxlength="50" required
											<s:if test="#session.nivelPermiso != 1">disabled</s:if>/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Perfil</label>
									<select id="cmbEditPerfil" class="form-control" name="usuarioBean.codPerfil" data-error="Debe seleccionar un perfil" required=""
										<s:if test="#session.nivelPermiso != 1">disabled</s:if> >
										<option value="">Seleccione</option>
										<s:iterator value="perfiles" var="perfil">
											<option value='${perfil.id}'/>${perfil.nombrePerfil}</option>
										</s:iterator>
									</select>
								<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Contrase&ntilde;a</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:password id="txtEditPassword" name="usuarioBean.password" cssClass="form-control" placeholder="password" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:password id="txtEditPassword" name="usuarioBean.password" cssClass="form-control" placeholder="password" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Repetir contrase&ntilde;a</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:password id="txtEditPasswordValid" name="usuarioBean.passwordValid" cssClass="form-control" placeholder="password" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:password id="txtEditPasswordValid" name="usuarioBean.passwordValid" cssClass="form-control" placeholder="password" maxlength="50" disabled="true"/>
									</s:else>
								</div>								
							</div>
						</div>
						<hr>
						<h4>Habilidades del usuario</h4>
						<div class="responsive-table">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tarea</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody id="tbodyHabilidades">
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<s:if test="#session.nivelPermiso == 1">
							<button type="submit" class="btn btn-primary">Guardar cambios</button>
						</s:if>
					</div>
				</div>
				</s:form>
			</div>
		</div>
	<s:include value="/commons/footer.jsp"></s:include>