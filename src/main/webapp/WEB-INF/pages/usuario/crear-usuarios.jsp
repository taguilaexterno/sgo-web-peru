<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		
<script type="text/javascript" src='<s:url value="/js/crearUsuario.js"/>'></script>
		
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Crear usuarios</li>
					</ol>

					<article id="product-list-manual">
						<h3>Crear nuevo usuario</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger"><s:actionerror/></div>
						</s:elseif>

						<form class="navbar-form user-navbar">
							<div class="input-group">
								<input type="text" id="txtUsuarioBuscar" class="form-control" placeholder="Buscar usuario">
								<div class="input-group-btn">
									<button id="buscarUsuario" type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit-user">Buscar</button>
								</div>
							</div>
							<div class="form-group pull-right">
								<a href='<s:url action="listaUsuarios"/>'><button type="button" class="btn btn-default">Ver todos los usuarios</button></a>
							</div>
						</form>
						<hr>
						<s:form action="guardarUsuario" theme="simple" role="form" data-toggle="validator">
							<div class="form-group">
								<label>Nombre del usuario</label>
								<s:textfield name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre del usuario" data-error="Favor, completar nombre" 
										data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre solo debe contener letras." />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Apellido Paterno del usuario</label>
								<s:textfield name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno del usuario" data-error="Favor, completar apellido paterno" 
										data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Apellido Materno del usuario</label>
								<s:textfield name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno del usuario" data-error="Favor, completar apellido materno" 
										data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>E-mail</label>
								<input type="email" name="usuarioBean.email" id="guardarUsuario_usuarioBean_email" class="form-control" placeholder="E-mail" data-error="Dirección email no válida" 
										data-minlength="2" maxlength="50" required />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Perfil</label>
								<select class="form-control" name="usuarioBean.codPerfil" data-error="Debe seleccionar un perfil" required="">
									<option value="">Seleccione</option>
									<s:iterator value="perfiles" var="perfil">
										<option value='<s:property value="#perfil.id"/>'><s:property value="#perfil.nombrePerfil"/></option>
									</s:iterator>
								</select>
								<div class="help-block with-errors"></div>
								<a href='<s:url action="crearPerfil"/>'>Crear nuevo perfil</a>
							</div>
							<div class="form-group">
								<label>Nombre de usuario</label>
								<s:textfield name="usuarioBean.usuario" cssClass="form-control" placeholder="Nombre de usuario" data-error="Debe colocar un nombre de usuario" 
										data-minlength="2" maxlength="20" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre de usuario solo debe contener letras."/>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Contrase&ntilde;a</label>
								<s:password id="usuarioBean_password" name="usuarioBean.password" cssClass="form-control" placeholder="Contraseña" data-error="Debe colocar una contraseña" 
										data-minlength="2" maxlength="50" required="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres."  />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label>Confirmar contrase&ntilde;a</label>
								<s:password name="usuarioBean.passwordValid" cssClass="form-control" placeholder="Confirmar contraseña" data-match="#usuarioBean_password"
								data-match-error="La contraseña no coincide" data-error="Debe escribir la contraseña nuevamente" 
										data-minlength="2" maxlength="50" required="" />
								<div class="help-block with-errors"></div>
							</div>
							<a href='<s:url action="listaUsuarios"/>'><button type="button" class="btn btn-lg btn-default pull-left">Ver todos los usuarios</button></a>
							<s:if test="#session.nivelPermiso == 1">
								<button type="submit" class="btn btn-lg btn-primary pull-right">Crear usuario</button>
							</s:if>
						</s:form>
						
					</article>
				</div>
			</div>
		</div>

		<div id="edit-user" class="modal fade">
			<div class="modal-dialog modal-lg">
				<s:form action="actualizarUsuario" theme="simple" role="form" data-toggle="validator">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Editar Usuario</h4>
					</div>
					<input type="hidden" id="usuarioBean_codUsuario" name="usuarioBean.codUsuario"/>
					<input type="hidden" id="usuarioBean_codEstado" name="usuarioBean.codEstado"/>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditNombreUsuario" name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre solo debe contener letras." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditNombreUsuario" name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre"
										data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El nombre solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Apellido Paterno</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditApePat" name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditApePat" name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Apellido Materno</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditApeMat" name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditApeMat" name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" pattern="^[A-Za-z\-]+$" data-pattern-error="El apellido solo debe contener letras." disabled="true"/>
									</s:else>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Email</label>
									<input type="email" id="txtEditEmail" name="usuarioBean.email" class="form-control" placeholder="email@email.com" 
											data-error="Email no válido" data-minlength="2" maxlength="50" required
											<s:if test="#session.nivelPermiso != 1">disabled</s:if>/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Perfil</label>
									<select id="cmbEditPerfil" class="form-control" name="usuarioBean.codPerfil" data-error="Debe seleccionar un perfil" required=""
										<s:if test="#session.nivelPermiso != 1">disabled</s:if> >
										<option value="">Seleccione</option>
										<s:iterator value="perfiles" var="perfil">
											<option value='${perfil.id}'/>${perfil.nombrePerfil}</option>
										</s:iterator>
									</select>
								<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Contrase&ntilde;a</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:password id="txtEditPassword" name="usuarioBean.password" cssClass="form-control" placeholder="password" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres." />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:password id="txtEditPassword" name="usuarioBean.password" cssClass="form-control" placeholder="password" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-pattern-error="Debe contener un Numero, Mayuscula , Minuscula y un largo de 8 caracteres." disabled="true"/>
									</s:else>
								</div>
								<div class="form-group">
									<label>Repetir contrase&ntilde;a</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:password id="txtEditPasswordValid" name="usuarioBean.passwordValid" cssClass="form-control" placeholder="password" maxlength="50"  />
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:password id="txtEditPasswordValid" name="usuarioBean.passwordValid" cssClass="form-control" placeholder="password" maxlength="50" disabled="true"/>
									</s:else>
								</div>
							</div>
						</div>
						<hr>
						<h4>Habilidades del usuario</h4>
						<div class="responsive-table">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tarea</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody id="tbodyHabilidades">
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<s:if test="#session.nivelPermiso == 1">
							<button type="submit" class="btn btn-primary">Guardar cambios</button>
						</s:if>
					</div>
				</div>
				</s:form>
			</div>
		</div>

	<s:include value="/commons/footer.jsp"></s:include>