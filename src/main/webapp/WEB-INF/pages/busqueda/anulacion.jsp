<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Anulaci&oacute;n de Recaudaci&oacute;n manual</li>
					</ol>

					<article id="product-list-manual">
						<h3>Anulaci&oacute;n de Recaudaci&oacute;n manual</h3>
						<div class="center">
							<s:form action="buscarNC" theme="simple" class="form-inline" role="form" data-toggle="validator">
								<s:hidden name="estado" value="1" />
								<div class="form-group">
									<label>Ingresa Nº de OC</label>

									<div class="input-group">
										<s:textfield type="number" name="oc" cssClass="form-control" placeholder="Ingresa nº de OC" data-error="Debe llenar este campo" data-minlength="2" required=""/>
										<span class="input-group-btn">
											<button class="btn btn-primary">Generar</button>
										</span>
									</div>
									<div class="help-block with-errors"></div>
								</div>
							</s:form>
						</div>
					</article>
				<%-- FIN SECCION PRINCIPAL --%>
				</div>
			</div>
		</div>
		<s:include value="/commons/footer.jsp"></s:include>
