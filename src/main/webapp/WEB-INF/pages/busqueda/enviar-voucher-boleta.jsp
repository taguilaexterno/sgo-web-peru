<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
<html>
<head>
		<title>SGO - Sistema de Gestión de Ordenes</title>
		<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8" >
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="css/bootstrap-datepicker.css" type="text/css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
		<link rel="stylesheet" href="css/style.css" type="text/css"/>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<style type="text/css">
/* CLIENT-SPECIFIC STYLES */
body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
img { -ms-interpolation-mode: bicubic; }

/* RESET STYLES */
img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
table { border-collapse: collapse !important; }
body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

/* iOS BLUE LINKS */
a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

tr.product[value=""], tr.product[value=""] td, tr.product[value=""] td table{
    display: none !important;
    padding:0 0 0 0 !important;
    margin:0 !important;
}

/* MEDIA QUERIES */
@media screen and (max-width: 600px) {
    .mobile-hide {
        display: none !important;
    }
    .mobile-center {
        text-align: center !important;
    }
    .mobile-table {
        width: 100% !important;
    }
    .text-center {
        text-align: center !important;
    }
    .mobile-td{
        padding-left:15px !important;
        padding-right:15px !important;
    }
}

/* ANDROID CENTER FIX */
div[style*="margin: 16px 0;"] { margin: 0 !important; }
</style>

<!-- GO TO BUTTON GOOGLE TAG-->
<script type="application/ld+json">
    {
      "@context":       "http://schema.org",
      "@type":          "EmailMessage",
      "description":    "Sigue tu compra",
      "action": {
        "@type": "ViewAction",
        "name": "Sigue tu compra",
        "url":   "http://simple.ripley.cl/seguimiento?orderId=[Orden_compra]"
      }
    }
</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
<script>
   function downloadPDF(){
       window.location.href = document.envioBoleta.urlPDF.value;
   };
  
</script>
</head>
<body style="margin: 0 !important; padding: 0 !important; background-color: #f6f6f6; color:#444;" bgcolor="#f6f6f6">
<form name="envioBoleta">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<article id="envio-voucher-boleta">
						<s:if test="urlDoce != null">
							<input type="hidden" name="urlPDF" value="${urlDoce}">
							<script>downloadPDF();</script>
						</s:if>
						<s:elseif test="urlDoce == null">
							<input type="hidden" name="urlPDF" value="">
							<input type="hidden" name="tipoDoc" value="${tipoDoc}">
						    <s:if test="((tipoDoc == 3) || (tipoDoc == 4))">
								<!-- HIDDEN PREHEADER TEXT -->
								<div style="display: none; font-size: 1px; color: #f6f6f6; line-height: 1px; font-family: Open Sans, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">Tu compra está confirmada.</div>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
							    <tr>
							        <td align="center" style="background-color: #f6f6f6;" bgcolor="#f6f6f6">
							            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="mobile-table">
						                <tr>
						                    <td align="center" valign="top" style="font-size:0; padding: 30px 0;">
						                        <a href="http://simple.ripley.cl" target="_blank" style="color: #444444; text-decoration: none;" xt="SPCLICK" name="simple_ripley_cl"><img  name="Cont_0" src="http://home.ripley.cl/assets-email/img/ripley-logo.png" width="150" style="display: block; border: 0px;"/></a>
						                    </td>
						                </tr>
							            </table>
							        </td>
							    </tr>
							    <tr>
							        <td align="center" style="background-color: #f6f6f6;" bgcolor="#f6f6f6">
							            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="mobile-table" style="border:solid 1px #e9e9e9;"> 
						                <tr>
						                    <td align="center" style="padding: 35px 35px 20px 35px; background-color: #ffffff;" bgcolor="#ffffff">
						                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					                            <!-- CONTENIDO -->
					                            <tr>
					                                <td style="padding:0; font-family: Open Sans, Helvetica, Arial, sans-serif;">
					                                    <img src="http://home.ripley.cl/assets-email/img/check.png" alt="Icono check" style="width:80px; height:80px; display:block; margin:0 auto;"/>
					                                    <h3 style="text-align: center; font-size: 32px; line-height: 36pt; font-weight: 600; color:#70578B; margin:25px 0 50px;">Hemos confirmado tu pago!</h3>
														<s:if test="tipoDoc == 3">
						                                    <p style="font-weight:400; font-size: 16px; text-align: center; margin-bottom: 50px;">Tu Boleta Folio SII: ${folio} estará disponible en unos minutos.</strong></p>
														</s:if>                                    
														<s:if test="tipoDoc == 4">
						                                    <p style="font-weight:400; font-size: 16px; text-align: center; margin-bottom: 50px;">Tu Factura Folio SII: ${folio} estará disponible en unos minutos.</strong></p>
														</s:if>                                    
					                                    <hr style="height:1px; background:#ddd; border:none; margin:30px 0;">
					                                </td>
					                            </tr>
					                            <!-- CONTENIDO -->
						                        </table>
						                    </td>
						                </tr>
						                <tr>
						                    <td style="font-family: Open Sans, Helvetica, Arial, sans-serif; background: #F8F8F8; padding:30px; text-align: center; font-size: 14px; color:#4a4a4a; border-top:solid 1px #ddd;">
						                        <table>
					                            <tr>
					                                <td style="padding:15px;" width="50%">
					                                    <img src="http://home.ripley.cl/assets-email/img/change.png" alt="Cambio y devoluciones" style="width:40px; margin:0 0 10px;"/>
					                                    <p style="margin:0;">Revisa nuestras
														<a href="http://simple.ripley.cl/minisitios/estatico/servicio_cliente/terminos_condiciones.html#Terminos_Condiciones_12" style="color:#4a4a4a; font-weight: 600;">polcas de cambio y devoluciones.</a></p>
									                </td>
									                <td style="padding:15px;" width="50%">
									                    <img src="http://home.ripley.cl/assets-email/img/truck.png" alt="Cambio y devoluciones" style="width:40px; margin:0 0 10px;"/>
									                    <p style="margin:0;">Si tienes alguna duda revisa nuestras
														<a href="https://simple.ripley.cl/minisitios/estatico/despacho/" style="color:#4a4a4a; font-weight: 600;">Condiciones de despacho.</a></p>
									                </td>
									             </tr>
									             </table>
									          </td>
									      </tr>
										  <!-- FOOTER -->
									      <tr>
									          <td style="font-family: Open Sans, Helvetica, Arial, sans-serif; background: #4a4a4a; padding:30px; text-align: center; font-size: 14px; color:#fff;">
									             <p style="display:inline-block; vertical-align: middle; margin-top:0;">Senos <a href="https://twitter.com/ripleychile" target="_blank" alt="Twitter" style="display:inline-block; vertical-align: middle; margin:0 10px 0; text-decoration: none;" xt="SPCLICK" name="twitter_com_ripleychile"><img  name="Cont_3" src="http://home.ripley.cl/assets-email/img/twitter.png" alt="Twitter" width="32" height="32" /></a>
									             <a href="https://www.facebook.com/ripleychile/" target="_blank" alt="Facebook" style="display:inline-block; vertical-align: middle; text-decoration: none;" xt="SPCLICK" name="www_facebook_com_ripleychile_"><img  name="Cont_4" src="http://home.ripley.cl/assets-email/img/facebook.png" alt="Facebook" width="32" height="32" /></a>
									             </p>
									             <p style="margin:0 0 25px;"><a href="http://simple.ripley.cl/minisitios/estatico/servicio_cliente/" style="font-size: 16px; line-height: 20pt; font-weight:600; color:#fff; text-decoration: none;" xt="SPCLICK" name="simple_ripley_cl_minisitios_estatic">ࠎecesitas ayuda?</a> <br />Ll⮡nos al <strong>600 230 0222</strong> o escrnos a <a href="mailto:contacto@ripley.com" style="font-weight:600; text-decoration: underline; color:#fff;" xt="SPCLICK" name="mailto_contacto_ripley_com">contacto@ripley.com</a></p>
									             <p style="font-size: 12px; margin:0;">Para asegurar que recibas nuestros correos <strong>agrega esta direcci󮠡 tu lista de correos seguros.</strong></p>
									          </td>
									      </tr>
									      </table>
									        </td>
									    </tr>
									    </table>
						    </s:if>
						    <s:elseif test="((tipoDoc != 3) && (tipoDoc != 4))">
							    </p>
						        <div id="mensajeError" class="alert alert-danger">La Orden de Compra no tiene Documento Tributario generado.</div>
						    </s:elseif>
						</s:elseif>
					</article>
				 </div>
			</div>
	</div>
</form>
</body>
</html>
