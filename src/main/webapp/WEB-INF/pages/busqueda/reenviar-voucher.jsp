<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Reenviar Voucher</li>
					</ol>
					
					<article id="reenvio-voucher">
						<h3 align="center">Reenv&iacute;o Voucher</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
				</br>
						<div class="general-search">
							<s:form action="reenviarVoucher?tipoEjecucion=2" theme="simple" class="form-inline" role="form" data-toggle="validator">
								<div class="col-md-12">
									<label>Tipo de Voucher</label>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group radio">
										<label for="r1"><input type="radio" name="tipoVoucher" id="r1" value="1" checked="checked" />Recaudaci&oacute;n</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group radio">
										<label for="r2"><input type="radio" name="tipoVoucher" id="r2" value="2" />Anulaci&oacute;n</label>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">								
										<label>Ingresa Nº de OC</label>
										<input type="number" name="oc" class="form-control" placeholder="Ingresa nº de OC" data-error="Debe llenar este campo" data-minlength="1" required/>	
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>E-mail</label>
										<input type="email" name="email" class="form-control" placeholder="Ingrese E-mail" data-error="E-mail incorrecto" data-minlength="1" required/>
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-12">
								
									<button class="btn btn-primary btn-lg pull-right" type="submit">Reenviar Voucher</button>

								</div>
								<div class="clear"></div>
							</s:form>
						</div>
					</article>
							
				 </div>
			</div>
	</div>
<s:include value="/commons/footer.jsp"></s:include>
