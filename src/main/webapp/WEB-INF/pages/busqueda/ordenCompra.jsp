<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src="js/ordenCompra.js"></script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">Buscar OC</li>
					</ol>

					<article id="product-list-manual">
						<h3>Buscar OC</h3>
						


						<div class="general-search">
							<s:form id="boletaForm" action="buscarOrdenCompra" class="navbar-form navbar-top-actions">
								<s:hidden id="metodo" name="metodo" value=""/>
								<s:hidden id="seleccionados" name="seleccionados" value=""/>
								<s:hidden id="cajaEstado" name="cajaEstado" value="true"/>
								<div class="col-md-12">
									<label>Tipo de Busqueda</label>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group radio">
										<label for="r1"><input type="radio" name="tipoBuscar" id="r1" value="1" checked="checked" />Por Orden</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group radio">
										<label for="r2"><input type="radio" name="tipoBuscar" id="r2" value="2" />Por DNI</label>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">								
										<label>Ingresa Nº de OC</label>
										<input type="number" name="oc" class="form-control" placeholder="Ingresa nº de OC" data-error="Debe llenar uno de estos campos" data-minlength="1"  data-maxlength="10"/>	
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Ingresa DNI de Cliente</label>
										<input type="number" name="rut" class="form-control" placeholder="Ingresa nº de DNI" data-error="Debe llenar uno de estos campos" data-minlength="1" data-maxlength="9"/>	
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-12">
									<button id="btnBuscar" type="button" class="btn btn-primary" onclick="javascript:buscar(event);">Buscar</button>
								</div>
								<div class="clear"></div>

							</s:form>
<BR>
							<s:if test="mensaje != null">
								<div id="mensaje" class="alert alert-danger">${mensaje}</div>
							</s:if>

						</div>

						<div class="table-responsive">

							<table id="tblDatos" class="table table-striped table-bordered">
								<thead>
									<tr>
										<s:if test="#session.nivelPermiso == 1">
											<th>Acción</th>
										</s:if>
										<th>OC</th>
										<th>Fecha</th>
										<th>Fecha de despacho</th>
										<th>Tipo venta</th>
										<th>Estado</th>
										<th>Monto</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="ordenesCompra" var="orden">
									<tr class="<s:property value="#orden.NotaVenta.indicadorMkp + #orden.NotaVenta.tipoDoc"/>">
										<s:if test="#session.nivelPermiso == 1">
											<td><div class="center"><button class="btn btn-default btn-xs" 
											onclick="javascript:verDetalle(<s:property value="#orden.NotaVenta.correlativoVenta"/>,
											<s:property value="estado"/>);">Detalle</button></div></td>
										</s:if>
										<td><s:property value="#orden.NotaVenta.correlativoVenta"/></td>
										<td><s:date name="#orden.NotaVenta.fechaCreacion" format="dd/MM/yyyy"/></td>
										<td><s:date name="#orden.Despacho.fechaDespacho" format="dd/MM/yyyy"/></td>
										<td><s:property value="#orden.NotaVenta.tipoPago"/></td>
										<td><s:property value="#orden.NotaVenta.estadoDes"/></td>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#orden.NotaVenta.montoVenta)"/></td>
									</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>

					</article>
				</div>
			</div>
		</div>
	
		<s:include value="/commons/footer.jsp"></s:include>