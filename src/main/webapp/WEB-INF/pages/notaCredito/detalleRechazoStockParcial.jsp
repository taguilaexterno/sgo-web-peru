<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/detalleRechazoStockParcial.js"/>'></script>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">OC Rechazo Parcial Stock <%-- Generaci&oacute;n de NC --%></li>
					</ol>

					<article id="product-list-manual">
						<h3>OC Rechazo Parcial Stock <%-- Generaci&oacute;n de NC Manual --%></h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger"><s:actionerror/></div>
						</s:elseif>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>OC N&deg; <span>${oc}</span> | <span class="text-danger"> Monto total <span><s:property value="formatNumber(montoTotal)"/></span></span></h4>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">

<!-- Datos del Cliente -->
								<div class="panel-heading" role="tab" id="tb-datos-cliente">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-tb-datos-cliente" aria-expanded="true" aria-controls="collapse-tb-datos-cliente">Datos del Cliente <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-tb-datos-cliente" class=<%-- "panel-collapse collapse" --%>"panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-tb-datos-cliente">

								<div class="panel panel-default oc-details">
									<div class="panel-body">


									<dl>
										<dt>Correlativo ventas</dt>
										<dd><s:property value="datosCliente.correlativoVentas"/></dd>
									</dl>
									<dl>
										<dt>DNI cliente</dt>
										<dd><s:property value="datosCliente.rutCliente"/></dd>
									</dl>
									<dl>
										<dt>Nombre del cliente</dt>
										<dd><s:property value="datosCliente.nombreCliente"/></dd>
									</dl>
									<dl>
										<dt>E-mail</dt>
										<dd><s:property value="datosCliente.mail"/></dd>
									</dl>
									<dl>
										<dt>Tipo de documento</dt>
										<dd><s:property value="datosCliente.tipoDoc"/></dd>
									</dl>
									<dl>
										<dt>Tipo de orden</dt>
										<dd><s:property value="datosCliente.tipoOrden"/></dd>
									</dl>
									<s:if test="datosCliente.tipoDoc == 'Factura Electrónica'">
										<dl>
											<dt>RUC Factura</dt>
											<dd><s:property value="datosCliente.rutFactura"/></dd>
										</dl>
										<dl>
											<dt>Razón Social Factura</dt>
											<dd><s:property value="datosCliente.razonSocialFactura"/></dd>
										</dl>
									</s:if>
									<dl>
										<dt>Direcci&oacute;n</dt>
										<dd><s:property value="datosCliente.direccion"/></dd>
									</dl>
									<dl>
										<dt>Tel&eacute;fono de despacho</dt>
										<dd><s:property value="datosCliente.fonoDespacho"/></dd>
									</dl>
									<dl>
										<dt>Tel&eacute;fono del cliente</dt>
										<dd><s:property value="datosCliente.fonoCliente"/></dd>
									</dl>
								</div>
								</div>

							</div>

<!-- Datos de despacho -->
								<div class="panel-heading" role="tab" id="tb-datos-despacho">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-tb-datos-despacho" aria-expanded="true" aria-controls="collapse-tb-datos-despacho">Datos de Despacho <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-tb-datos-despacho" class=<%-- "panel-collapse collapse in" --%>"panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-tb-datos-despacho">

								<div class="panel panel-default oc-details">
									<div class="panel-body">
<br>
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>DNI despacho</th>
													<th>Nombre de despacho</th>
													<th>SKU</th>
													<th>Estado BT</th>
													<th>Direcci&oacute;n</th>
													<th>Cod. BO</th>
													<th>Regi&oacute;n</th>
													<th>Comuna</th>
													<th>CUD</th>
													<th>Art&iacute;culo</th>
													<th>Tel&eacute;fono</th>
													<th>Observaci&oacute;n</th>
													<th>DNI cliente</th>
													<th>Nombre FACT</th>
													<th>Comuna facturaci&oacute;n</th>
												</tr>
											</thead>
											<tbody>
												<s:iterator value="datosDespacho" var="desp">
													<tr>
														<td><s:property value="#desp.rutDespacho"/></td>
														<td><s:property value="#desp.nombreDespacho"/></td>
														<td><s:property value="#desp.sku"/></td>
														<td><s:property value="#desp.estadoBt"/></td>
														<td><s:property value="#desp.direccion"/></td>
														<td><s:property value="#desp.codigoBo"/></td>
														<td><s:property value="#desp.region"/></td>
														<td><s:property value="#desp.comuna"/></td>
														<td><s:property value="#desp.cud"/></td>
														<td><s:property value="#desp.articulo"/></td>
														<td><s:property value="#desp.fono"/></td>
														<td><s:property value="#desp.observacion"/></td>
														<td><s:property value="#desp.rutCliente"/></td>
														<td><s:property value="#desp.nombreFact"/></td>
														<td><s:property value="#desp.comunaFact"/></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</div>
								</div>
								</div>
							</div>

<!-- Datos de compra -->
							<div class="panel-heading" role="tab" id="tb-datos-compra">
								<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-tb-datos-compra" aria-expanded="true" aria-controls="collapse-tb-datos-compra">Datos de Compra <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
							</div>
							<div id="collapse-tb-datos-compra" class=<%-- "panel-collapse collapse in" --%>"panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-tb-datos-despacho">

							<div class="panel panel-default oc-details">
								<div class="panel-body">
<br>
							<div class="table-responsive">
								<table class="table table-bordered" style="table-layout:auto;">
									<thead>
										<tr>
											<th>Monto CAR</th>
											<th>Cuotas</th>
											<th>1er vencimiento</th>
											<th>Valor cuota</th>
											<th>Monto TRE</th>
											<th>Webpay cr&eacute;dito/d&eacute;bito</th>
											<th>Glosa GSIC</th>
											<th>Descripci&oacute;n</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="datosCompra" var="comp" begin="0" end="0">
											<tr>
												<td><s:property value="#comp.montoCar"/></td>
												<td><s:property value="#comp.cuotas"/></td>
												<td><s:property value="#comp.primerVenc"/></td>
												<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp.valorCuota)"/></td>
												<td><s:property value="#comp.montoTre"/></td>
												<td><s:property value="#comp.esWebPay"/></td>
												<td><s:property value="#comp.glosaGsic"/></td>
												<td><s:property value="#comp.descripcion"/></td>
											</tr>
										</s:iterator>						
									</tbody>
								</table>
								<br>
								<table class="table table-bordered" style="table-layout:auto;">
									<thead>
										<tr>
											<th>SKU</th>
											<th>Precio</th>
											<th>Unidades</th>
											<th>Descuento</th>
											<th>Total</th>
											<th>Despacho</th>
											<th>Fecha de despacho</th>
											<th>Cod. bodega</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="datosCompra" var="comp2">
											<tr>
												<td><s:property value="#comp2.sku"/></td>
												<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp2.precio)"/></td>
												<td><s:property value="#comp2.unidades"/></td>
												<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp2.descuento)"/></td>
												<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp2.total)"/></td>
												<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#comp2.despacho)"/></td>
												<td><s:property value="#comp2.fechaDespacho"/></td>
												<td><s:property value="#comp2.codBodega"/></td>					
											</tr>
										</s:iterator>						
									</tbody>
								</table>
							</div>
							</div>
							</div>
							</div>

<!-- NC Total -->
								<div class="panel-heading" role="tab" id="nc-total-parcial">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-nc-total-parcial" aria-expanded="true" aria-controls="collapse-nc-total-parcial">Rechazo Total Stock <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-nc-total-parcial" class=<%-- "panel-collapse collapse in" --%>"panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-nc-total-parcial">
									<s:form action="confirmarNCDetalle" theme="simple">
										<input type="hidden" name="listaArtSize" value="${listaArtSize}"/>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>SKU</th>
															<th>Descripci&oacute;n</th>
															<th>Precio U</th>
															<th>Cantidad</th>
															<th>Cant. NC</th>
														</tr>
													</thead>
													<tbody>
														<input type="hidden" name="ncTotal" value="1" />
														<input type="hidden" name="estado" value="${estado}" />
														<input type="hidden" name="oc" value="${oc}" />
														<s:iterator value="articulos" var="articulo" status="status">
															<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
															<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
															<input type="hidden" name="articulos[${status.index}].articuloVenta.esNC" value="1" />
															<tr>
																<td>${articulo.articuloVenta.codArticulo}</td>
																<td>${articulo.articuloVenta.descRipley}</td>
																<td><s:property value="formatNumber(#articulo.articuloVenta.precio)"/></td>
																<td>${articulo.articuloVenta.unidades}</td>
																<td>${articulo.articuloVenta.unidades}</td>
															</tr>
														</s:iterator>
													</tbody>
												</table>
											</div>
											<s:if test="#session.nivelPermiso == 1">
												<div class="center">
													<button class="btn btn-primary btn-lg">Confirmar</button>
												</div>
											</s:if>
										</div>
									</s:form>
								</div>
<!-- NC Parcial -->
								<div class="panel-heading" role="tab" id="nc-total-parcial">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-nc-parcial" aria-expanded="true" aria-controls="collapse-nc-parcial">Rechazo Parcial Stock <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-nc-parcial" class=<%-- "panel-collapse collapse" --%>"panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse-nc-parcial">
									<s:form id="confirmarParcial" action="confirmarNCDetalle" theme="simple">
										<input type="hidden" name="listaArtSize" value="${listaArtSize}"/>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>SKU</th>
															<th>Descripci&oacute;n</th>
															<th>Precio U</th>
															<th>Cantidad</th>
															<th>Cant. NC</th>
															<th>Selecci&oacute;n</th>
														</tr>
													</thead>
													<tbody>
														<input type="hidden" name="ncTotal" value="0" />
															<input type="hidden" name="estado" value="${estado}" />
															<input type="hidden" name="oc" value="${oc}" />
															<s:iterator value="articulos" var="articulo" status="status">
																<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
																<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
																<tr>
																	<td>${articulo.articuloVenta.codArticulo}</td>
																	<td>${articulo.articuloVenta.descRipley}</td>
																	<td><s:property value="formatNumber(#articulo.articuloVenta.precio)"/></td>
																	<td>${articulo.articuloVenta.unidades}</td>
																	<td>${articulo.articuloVenta.unidades}</td>
																	<s:if test="#session.nivelPermiso == 1">
																		<td>
																			<input type="checkbox" name="articulos[${status.index}].articuloVenta.esNC" value="1" <s:if test="#articulo.articuloVenta.estadoVenta == 'Sin Stock' || #articulo.articuloVenta.esNC == 1">checked</s:if>
																			<s:if test="#session.nivelPermiso != 1">disabled</s:if> />
																		</td>
																	</s:if>
																</tr>
															</s:iterator>
													</tbody>
												</table>
											</div>
											<s:if test="#session.nivelPermiso == 1">
												<div class="center">
													<s:if test="estado == 1 || estado == 2 || estado == 3">
														<button class="btn btn-primary btn-lg">Confirmar</button>
													</s:if>
													<s:else>
														<button id="btnMostrarConfirmacion" type="button" class="btn btn-primary btn-lg">Confirmar</button>
													</s:else>
												</div>
											</s:if>
										</div>
									</s:form>
								</div>
							
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>

<s:if test="estado != 1 || estado != 2 || estado != 3">	
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Confirmar Voucher de Recaudaci&oacute;n</h4>
					</div>
					<div class="modal-body">
						<span id="mensajeModal">No seleccion&oacute; ning&uacute;n art&iacute;culo. Esto generar&aacute; una boleta. ¿Continuar?</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button id="btnConfirmarModal" type="button" class="btn btn-primary">Confirmar</button>
					</div>
				</div>
			</div>
		</div>
	</s:if>
</s:if>
<s:include value="/commons/footer.jsp"></s:include>