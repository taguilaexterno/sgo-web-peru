<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src='<s:url value="/js/crearAcreedor.js"/>'></script>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Creaci&oacute;n Acreedor</li>
					</ol>

					<article id="product-list-manual">
						<h3>Creaci&oacute;n de Acreedor</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>Numero de orden de compra: <span>${oc}</span></h4>
						
<%-- 						<s:if test="mostrar > 0">  --%>
<!-- 							<div id="mensajeContinuar" class="alert alert-warning"> -->
<!-- 							    <p> -->
<%-- 							    	<span class="fa fa-exclamation"></span> --%>
<!-- 							    	Atenci&oacute;n: Aseg&uacute;rese que el cliente est&eacute; creado en SAP antes de generar la nota de cr&eacute;dito. -->
<%-- 									<s:form id="generarNCDD" action="generarNCDD.action" theme="simple" class="form-inline" role="form"> --%>
<!-- 											<input type="hidden" name="metodo" value="continuar"/> -->
<!-- 											<div class="form-group"> -->
<!-- 												<div class="input-group"> -->
<%-- 													<span class="input-group-btn"> --%>
<!-- 														<button class="btn btn-primary">Generar NC</button> -->
<%-- 													</span> --%>
<!-- 												</div> -->
<!-- 											</div> -->
<%-- 									</s:form> --%>
<!-- 								</p> -->
<!-- 							</div>								 -->
<%-- 						</s:if> --%>
<%-- 						<s:else>  --%>
<!-- 							<br/>  -->
<%-- 						</s:else> --%>
							<br/>				    
						<s:form action="crearAcreedor" theme="simple" class="form-inline" role="form" data-toggle="validator">
							<div class="panel-body">
								<h4>Datos del acreedor</h4>
								<input type="hidden" name="oc" value="${oc}"/>
								<div class="col-md-6">
									<div class="form-group">
										<label>DNI</label>
										<input type="text" name="creaAcreedor.rut" class="form-control" value="${acreedor.rut}-${acreedor.dv}" readonly>
										<div class="help-block with-errors"></div>
										<input type="hidden" name="acreedor.rut" value="${acreedor.rut}"/>
										<input type="hidden" name="acreedor.dv" value="${acreedor.dv}"/>
										<input type="hidden" name="creaAcreedor.numeroIF" id="numeroIF" value="${acreedor.rut}"/>
										<input type="hidden" name="creaAcreedor.numeroIFDV" id="numeroIFDV" value="${acreedor.dv}"/>
									</div>
								</div>
<%--
								<div class="col-md-4">
									<div class="form-group">
										<label>* N&deg; indentificaci&oacute;n fiscal</label>
										<div class="form-group">
											<s:textfield id="numeroIF" type="number" name="creaAcreedor.numeroIF" cssClass="form-control" data-error="Campo obligatorio, max. 8 digitos" min="0" max="99999999" required=""/>
										    <div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>&nbsp;</label>
										<div class="form-inline">
											<div class="form-group">
												<select id="numeroIFDV" class="form-control" name="creaAcreedor.numeroIFDV" data-error="Debe seleccionar una opción" required="">
													<option value="0">- 0</option>
													<option value="1">- 1</option>
													<option value="2">- 2</option>
													<option value="3">- 3</option>
													<option value="4">- 4</option>
													<option value="5">- 5</option>
													<option value="6">- 6</option>
													<option value="7">- 7</option>
													<option value="8">- 8</option>
													<option value="9">- 9</option>
													<option value="k">- K</option>
												</select>
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>
								</div>
 --%>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Nombre</label>
										<s:textfield id="nombre1" type="text" name="creaAcreedor.nombre1" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Calle</label>
										<s:textfield id="calle1" type="text" name="creaAcreedor.calle1" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Distrito</label>
										<s:textfield id="distrito" type="text" name="creaAcreedor.distrito" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Regi&oacute;n</label>
										<select id="region" class="form-control" name="creaAcreedor.region" data-error="Debe seleccionar una opción" required="">
											<option value="">Seleccione</option>
											<s:iterator value="regiones" var="region">
												<option value='<s:property value="#region.codigo"/>'>(<s:property value="#region.codigo"/>) <s:property value="#region.nombre"/></option>
											</s:iterator>
										</select>
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>E-mail</label>
										<s:textfield type="email" name="creaAcreedor.email" cssClass="form-control" data-error="Dirección email no válida" data-minlength="0" maxlength="50"/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Telefono</label>
										<s:textfield type="number" name="creaAcreedor.telefono" cssClass="form-control" min="0"/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Telefax</label>
										<s:textfield type="number" name="creaAcreedor.telefax" cssClass="form-control" min="0"/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Telex</label>
										<s:textfield type="number" name="creaAcreedor.telex" cssClass="form-control" min="0"/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<br/>
								<h4>Datos Bancarios</h4>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Titular Cuenta</label>
										<s:textfield id="titularC" type="text" name="creaAcreedor.titularC" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Clave Banco</label>
										<select id="claveB" class="form-control" name="creaAcreedor.claveB" data-error="Debe seleccionar una opción" required="">
											<option value="">Seleccione</option>
											<s:iterator value="bancos" var="banco">
												<option value='<s:property value="#banco.codigo"/>'>(<s:property value="#banco.codigo"/>) <s:property value="#banco.nombre"/></option>
											</s:iterator>
										</select>
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Cuenta Bancaria</label>
										<s:textfield id="cuentaB" type="number" name="creaAcreedor.cuentaB" cssClass="form-control" data-error="Campo obligatorio" min="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>								
								<div class="clear"></div>
								<div class="center">
									<div class="form-group">
										<div class="form-inline">
											<div class="form-group">
												<input type="hidden" id="metodo" name="metodo" value=""/>
												<button class="btn btn-primary btn-lg" 
												 onclick="javascript:cancelar();">Cancelar</button>
											</div>
											<div class="form-group">
												<button class="btn btn-primary btn-lg">Crear</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</s:form>
					</article>
				</div>
			</div>
		</div>
<s:include value="/commons/footer.jsp"></s:include>