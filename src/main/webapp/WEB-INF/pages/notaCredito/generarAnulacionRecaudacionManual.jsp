<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/selecionMkp.js"></script>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Generaci&oacute;n de NC</li>
			</ol>

			<article id="product-list-manual">
				<h3>Anulaci&oacute;n de recaudaciones Manual</h3>
				<s:if test="mensaje != null">
					<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
				</s:if>
				<s:elseif test="mensajeError != null">
					<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
				</s:elseif>
				<h4>
					OC N&deg; <span>${oc}</span> | <span class="text-danger">
						Monto total <span><s:property
								value="@cl.ripley.omnicanalidad.util.Util@formatNumber(montoTotal)" /></span>
					</span>
				</h4>
				<div class="panel panel-default oc-details">
					<div class="panel-heading">Datos del cliente</div>
					<div class="panel-body">
						<dl>
							<dt>Correlativo ventas</dt>
							<dd><s:property value="datosCliente.correlativoVentas"/></dd>
						</dl>
						<dl>
							<dt>DNI cliente</dt>
							<dd><s:property value="datosCliente.rutCliente"/></dd>
						</dl>
						<dl>
							<dt>Nombre del cliente</dt>
							<dd><s:property value="datosCliente.nombreCliente"/></dd>
						</dl>
						<dl>
							<dt>E-mail</dt>
							<dd><s:property value="datosCliente.mail"/></dd>
						</dl>
						<dl>
							<dt>Tipo de documento</dt>
							<dd><s:property value="datosCliente.tipoDoc"/></dd>
						</dl>
						<dl>
							<dt>Direcci&oacute;n</dt>
							<dd><s:property value="datosCliente.direccion"/></dd>
						</dl>
						<dl>
							<dt>Tel&eacute;fono de despacho</dt>
							<dd><s:property value="datosCliente.fonoDespacho"/></dd>
						</dl>
						<dl>
							<dt>Tel&eacute;fono del cliente</dt>
							<dd><s:property value="datosCliente.fonoCliente"/></dd>
						</dl>
					</div>
				</div>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					

					<div class="panel panel-default">
						<!--#########TOTAL######### -->
						<div class="panel-heading" role="tab" id="nc-total-parcial">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion"
									href="#collapse-nc-total-parcial" aria-expanded="true"
									aria-controls="collapse-nc-total-parcial">Anulaci&oacute;n
									de recaudaci&oacute;n total <span
									class="glyphicon glyphicon-chevron-down pull-right"></span>
								</a>
							</h4>
						</div>
						<div id="collapse-nc-total-parcial" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-nc-total-parcial">
							<s:form id="ncTotalForm" action="confirmarNCDetalle" theme="simple" data-toggle="validator">
								<input type="hidden" name="listaArtSize" value="${listaArtSize}" />
								<div class="panel-body">
									<input type="hidden" id="fpNcTotal" name="formaPago" value="${formaPago}" />
									<input type="hidden" name="ncTotal" value="1" />
									<input type="hidden" name="estado" value="${estado}" />
									<input type="hidden" name="oc" value="${oc}" />
									<div class="table-responsive">
										<s:iterator value="articulosConOrdenes" var="subOrdenes" status="statuss">
												
										<table class="table table-bordered">
												
												<thead>
													<tr>
														<th>SKU</th>
														<th>Descripci&oacute;n</th>
														<th>Precio U</th>
														<th>Cantidad</th>
														<th>Cant. NC</th>
													</tr>
												</thead>
												<tbody>
													<s:iterator value="#subOrdenes.articulos" var="articulo" status="status">
														<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
														<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
														<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.esMkp" value="${articulo.articuloVenta.esMkp}" />
														<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.esNC" value="1" />
														<tr>
															<td>${articulo.articuloVenta.codArticulo}</td>
															<td>${articulo.articuloVenta.descRipley}</td>
															<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#articulo.articuloVenta.precio)" /></td>
															<td>${articulo.articuloVenta.unidades}</td>
															<td>${articulo.articuloVenta.unidades}</td>
														</tr>
														
													</s:iterator>
												</tbody>

												</table>
											



									<s:if test="#articulo.articuloVenta.esMkp!=0">
										<div class="col-md-4">
											<div class="form-group">
												<label>Sub Orden</label> 
												<input
													name=""
													value="${subOrdenes.subOrden}" cssClass="form-control"
													placeholder="Sub Orden"
													data-error="Favor, completar ID Sub Orden"
													data-minlength="2" maxlength="30" required="" disabled />
												<input type="hidden" 
													name='infoSubOrdenIdRefunds[${statuss.index}].subOrden'
													value="${subOrdenes.subOrden}" 												
												/>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Monto</label><br /> 
												<input
													name=""
													value = "<s:property value='@cl.ripley.omnicanalidad.util.Util@formatNumber(#subOrdenes.montoTotal)' />"														
													cssClass="form-control" placeholder="Monto"
													data-error="Favor, completar Monto" data-minlength="2"
													maxlength="30" required="" disabled />
													<input type="hidden"  
													   name="articulosConOrdenes[${statuss.index}].montoTotal"
													   value="${subOrdenes.montoTotal}"
													/>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Id Refund</label> <input
													name="infoSubOrdenIdRefunds[${statuss.index}].idRefund"
													cssClass="form-control" placeholder="Id Refund"
													data-error="Favor, completar Id Refund" data-minlength="2"
													maxlength="30" />
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</s:if>
									<s:if test="#subordenesenv == null && #subOrdenes.subOrden.indexOf('-') != -1">
										<s:set var="subordenesenv" value="#subOrdenes.subOrden" />
									</s:if>
									<s:elseif test="#subOrdenes.subOrden.indexOf('-') != -1">
										<s:set var="subordenesenv" value="#subordenesenv + ',' + #subOrdenes.subOrden" />
									</s:elseif>
<%-- 									<input type="hidden" name="chkSubOrdenes" value='<s:property value="#subordenesenv"/>'> --%>
							</s:iterator>

									</div>
 									<s:if test="#session.nivelPermiso == 1"> 
											<div class="center">
												<button class="btn btn-primary btn-lg" type="submit"
												<% int esANCTotal = (Integer)request.getAttribute("esANCTotal"); 
												   if(esANCTotal == 1){  
												       out.print("disabled");
												   }
												%> 
												>Confirmar</button>
											</div>
									</s:if>

								</div>
							</s:form>
						</div>
						<!--#########PARCIAL######### -->
						<div class="panel-heading" role="tab" id="nc-total-parcial">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion"
									href="#collapse-nc-parcial" aria-expanded="true"
									aria-controls="collapse-nc-parcial">Anulaci&oacute;n de
									recaudaci&oacute;n parcial <span
									class="glyphicon glyphicon-chevron-down pull-right"></span>
								</a>
							</h4>
						</div>
						<div id="collapse-nc-parcial" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse-nc-parcial">
							<s:form id="ncParcialForm" action="confirmarNCDetalle" theme="simple" data-toggle="validator">
								<input type="hidden" name="listaArtSize" value="${listaArtSize}" />
								<div class="panel-body">
									<input type="hidden" id="fpNcParcial" name="formaPago" value="${formaPago}" />
									<input type="hidden" name="ncTotal" value="1" />
									<input type="hidden" name="estado" value="${estado}" />
									<input type="hidden" name="oc" value="${oc}" />
									<div class="table-responsive">
									<s:iterator value="articulosConOrdenes" var="subOrdenes" status="statuss">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>SKU</th>
													<th>Descripci&oacute;n</th>
													<th>Precio U</th>
													<th>Cantidad</th>
													<th>Cant. NC</th>
													<%--<th>Selecci&oacute;n</th> --%>
												</tr>
											</thead>
											<tbody>
												<s:iterator value="#subOrdenes.articulos" var="articulo" status="status">
													<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
													<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
													<input type="hidden" name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.esMkp" value="${articulo.articuloVenta.esMkp}" />
													<tr>
														<td>${articulo.articuloVenta.codArticulo}</td>
														<td>${articulo.articuloVenta.descRipley}</td>
														<td class="precio"><s:property	value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#articulo.articuloVenta.precio)" /></td>
														<td class="unidad">${articulo.articuloVenta.unidades}</td>
														<td>${articulo.articuloVenta.unidades}</td>
														<%--<td>
														<s:if test="#articulo.articuloVenta.esNC != 2 && #articulo.articuloVenta.enTransito == 0">
														<s:if test="#articulo.articuloVenta.esMkp!=0">
														<input class="mycheckbox${statuss.index}" type="checkbox"
															id = "art${statuss.index}${status.index}"
															name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.esNC"
															value="1"
																onclick="javascript:marcar('mycheckbox${statuss.index}','art${statuss.index}${status.index}',${statuss.index},${articulo.articuloVenta.precio},${articulo.articuloVenta.unidades});"
																<s:if test="#articulo.articuloVenta.estadoVenta == 'Sin Stock' || #articulo.articuloVenta.esNC == 1">checked</s:if>
																<s:if test="#session.nivelPermiso != 1">disabled</s:if> />
															
															</s:if>
															
															<s:else>
															<input class="mycheckbox${statuss.index}" type="checkbox"
																id = "art${statuss.index}${status.index}"
																name="articulosConOrdenes[${statuss.index}].articulos[${status.index}].articuloVenta.esNC"
																value="1"
																<s:if test="#articulo.articuloVenta.estadoVenta == 'Sin Stock' || #articulo.articuloVenta.esNC == 1">checked</s:if>
																<s:if test="#session.nivelPermiso != 1">disabled</s:if> />														
															</s:else>

														</s:if>
														<s:elseif test="#articulo.articuloVenta.esNC == 2">Ya Procesado</s:elseif>
														<s:else>En transito</s:else>
														</td>--%>
													</tr>
												</s:iterator>
											</tbody>
										</table>
										<s:if test="#articulo.articuloVenta.esMkp!=0">
												<div class="col-md-2">
														<div class="form-group">
															<label>Selecci&oacute;n</label></br>
															<s:if test="#subOrdenes.reversado == true">
																Reversado
															</s:if>
															<s:else>
																<input type="checkbox" 
<%-- 																		class="mycheckbox${status.index}" --%>
<%-- 																		id="chkOrden${status.index}" --%>
																		name="chkSubOrdenes"
																		value='<s:property value="#subOrdenes.subOrden"/>'/>
															</s:else>
															<div class="help-block with-errors"></div>
														</div>
													</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Sub Orden</label> 
												<input
													id="subOrdenVisible${statuss.index}"													
													value="${subOrdenes.subOrden}" class="form-control"
													placeholder="Sub Orden"
													data-error="Favor, completar ID Sub Orden"
													data-minlength="2" maxlength="30" required="" disabled />
												<input type="hidden" id="subOrdenInvisible${statuss.index}" name='infoSubOrdenIdRefunds[${statuss.index}].subOrden'
													value="${subOrdenes.subOrden}" 												
												/>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Monto</label><br /> 
												<input
													id="montoTotalVisible${statuss.index}"													
													value='<s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#subOrdenes.montoTotal)"/>'														
													class="form-control" placeholder="Monto"
													data-error="Favor, completar Monto" data-minlength="2"
													maxlength="30" required="" disabled />
													
												<input type="hidden"  
													   id="montoTotalInvisible${statuss.index}"
													   name='<s:if test="#subOrdenes.esRequired == 1">articulosConOrdenes[${statuss.index}].montoTotal</s:if>'
													   value="0"
													   />
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Id Refund</label> 
												<input
													id="idRefundVisible${statuss.index}"
													name="infoSubOrdenIdRefunds[${statuss.index}].idRefund"
													class="form-control idRefundClass" placeholder="Id Refund"
													data-error="Favor, completar Id Refund" data-minlength="2"
													maxlength="30"/>
<%-- 												<input type="hidden"   --%>
<%-- 												name="articulosConOrdenes[${statuss.index}].idRefund" --%>
<%-- 												id="idRefundInvisible${statuss.index}"></> --%>
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</s:if>
										
<%--								<s:if test="#session.nivelPermiso == 1">
										<div class="col-md-4">
											<div class="form-group">
												<div class="checkbox">
													<label>
														<input type="checkbox" id="mycheckbox${statuss.index}" 
														name="mycheckbox${statuss.index}" value="mycheckbox${statuss.index}" 
														class="mycheckboxAll" />

														Seleccionar todos
													</label>
												</div>
											</div>
										</div>
								</s:if> --%>
					</s:iterator>
									</div>
									<div class="center">
										<s:if test="#session.nivelPermiso == 1">
										<button id="confirmarParcial" class="btn btn-primary btn-lg">Confirmar</button>
										</s:if>
									</div>

								</div>
							</s:form>
						</div>

					</div>
				</div>
			</article>
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>
