<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/detalleRechazoNC.js"/>'></script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Generaci&oacute;n de NC</li>
					</ol>

					<article id="product-list-manual">
						<h3>Generaci&oacute;n de NC Manual</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>OC N&deg; <span>${oc}</span> | <span class="text-danger"> Monto total <span><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(montoTotal)"/></span></span></h4>

						<div class="panel panel-default oc-details">
							<div class="panel-heading">Datos del cliente</div>
							<div class="panel-body">
								<dl>
									<dt>Correlativo ventas</dt>
									<dd><s:property value="datosCliente.correlativoVentas"/></dd>
								</dl>
								<dl>
									<dt>DNI cliente</dt>
									<dd><s:property value="datosCliente.rutCliente"/></dd>
								</dl>
								<dl>
									<dt>Nombre del cliente</dt>
									<dd><s:property value="datosCliente.nombreCliente"/></dd>
								</dl>
								<dl>
									<dt>E-mail</dt>
									<dd><s:property value="datosCliente.mail"/></dd>
								</dl>
								<dl>
									<dt>Tipo de documento</dt>
									<dd><s:property value="datosCliente.tipoDoc"/></dd>
								</dl>
								<dl>
									<dt>Direcci&oacute;n</dt>
									<dd><s:property value="datosCliente.direccion"/></dd>
								</dl>
								<dl>
									<dt>Tel&eacute;fono de despacho</dt>
									<dd><s:property value="datosCliente.fonoDespacho"/></dd>
								</dl>
								<dl>
									<dt>Tel&eacute;fono del cliente</dt>
									<dd><s:property value="datosCliente.fonoCliente"/></dd>
								</dl>
							</div>
						</div>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="nc-total-parcial">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-nc-total-parcial" aria-expanded="true" aria-controls="collapse-nc-total-parcial">Nota Cr&eacute;dito total <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-nc-total-parcial" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse-nc-total-parcial">
									<s:form id="ncTotalForm" action="confirmarNCDetalle" theme="simple">
										<input type="hidden" name="listaArtSize" value="${listaArtSize}"/>
										<div class="panel-body">
											<input type="hidden" id="fpNcTotal" name="formaPago" value="${formaPago}" />
											<input type="hidden" name="ncTotal" value="1" />
											<input type="hidden" name="estado" value="${estado}" />
											<input type="hidden" name="oc" value="${oc}" />
											<div class="table-responsive">
										<s:iterator value="articulosConOrdenes" var="subOrdenes" status="statuss">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>SKU</th>
															<th>Descripci&oacute;n</th>
															<th>Precio U</th>
															<th>Cantidad</th>
															<th>Cant. NC</th>
														</tr>
													</thead>
													<tbody>
													<s:iterator value="#subOrdenes.articulos" var="articulo" status="status">
															<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
															<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
															<input type="hidden" name="articulos[${status.index}].articuloVenta.esNC" value="1" />
															<tr>
																<td>${articulo.articuloVenta.codArticulo}</td>
																<td>${articulo.articuloVenta.descRipley}</td>
																<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#articulo.articuloVenta.precio)"/></td>
																<td>${articulo.articuloVenta.unidades}</td>
																<td>${articulo.articuloVenta.unidades}</td>
															</tr>
														</s:iterator>
													</tbody>
												</table>
										</s:iterator>
											</div>
											<div class="center">
											<s:if test="#session.nivelPermiso == 1">
												<button class="btn btn-primary btn-lg"
												onclick="javascript:validaFormaPago(getElementById('fpNcTotal').value,getElementById('fpNcTotal').id);"
												<% int esANCTotal = (Integer)request.getAttribute("esANCTotal"); 
												   if(esANCTotal == 1){  
												       out.print("disabled");
												   }
												%> 
												>Confirmar</button>
											</s:if>
											</div>
										</div>
									</s:form>
								</div>
								<div class="panel-heading" role="tab" id="nc-total-parcial">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-nc-parcial" aria-expanded="true" aria-controls="collapse-nc-parcial">Nota Cr&eacute;dito parcial <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></h4>
								</div>
								<div id="collapse-nc-parcial" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-nc-parcial">
									<s:form id="ncParcialForm" action="confirmarNCDetalle" theme="simple">
										<input type="hidden" name="listaArtSize" value="${listaArtSize}"/>
										<div class="panel-body">
											<input type="hidden" id="fpNcParcial" name="formaPago" value="${formaPago}" />
											<input type="hidden" name="ncTotal" value="0" />
											<input type="hidden" name="estado" value="${estado}" />
											<input type="hidden" name="oc" value="${oc}" />
											<div class="table-responsive">
									<s:iterator value="articulosConOrdenes" var="subOrdenes" status="statuss">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>SKU</th>
															<th>Descripci&oacute;n</th>
															<th>Precio U</th>
															<th>Cantidad</th>
															<th>Cant. NC</th>
															<th>Selecci&oacute;n</th>
														</tr>
													</thead>
													<tbody>
															<s:iterator value="#subOrdenes.articulos" var="articulo" status="status">
																<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoVenta" value="${articulo.articuloVenta.correlativoVenta}" />
																<input type="hidden" name="articulos[${status.index}].articuloVenta.correlativoItem" value="${articulo.articuloVenta.correlativoItem}" />
																<tr>
																	<td>${articulo.articuloVenta.codArticulo}</td>
																	<td>${articulo.articuloVenta.descRipley}</td>
																	<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#articulo.articuloVenta.precio)"/></td>
																	<td>${articulo.articuloVenta.unidades}</td>
																	<td>${articulo.articuloVenta.unidades}</td>
																	<td>
																	<s:if test="#articulo.articuloVenta.esNC != 2 && #articulo.articuloVenta.enTransito == 0">
																		<input type="checkbox" name="articulos[${status.index}].articuloVenta.esNC" value="1" <s:if test="#articulo.articuloVenta.estadoVenta == 'Sin Stock' || #articulo.articuloVenta.esNC == 1">checked</s:if>
																		<s:if test="#session.nivelPermiso != 1">disabled</s:if> />
																	</s:if>	
																	<s:elseif test="#articulo.articuloVenta.esNC == 2">Ya Procesado</s:elseif>
																	<s:else>En transito</s:else>	
																	</td>
																</tr>
															</s:iterator>
													</tbody>
												</table>
											</div>
									</s:iterator>
											<div class="center">
												<s:if test="#session.nivelPermiso == 1">
												<button class="btn btn-primary btn-lg"
												onclick="javascript:validaFormaPago(getElementById('fpNcParcial').value,getElementById('fpNcParcial').id);">Confirmar</button>
												</s:if>
											</div>
										</div>
									</s:form>
								</div>
							
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
<s:include value="/commons/footer.jsp"></s:include>