<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src='<s:url value="/js/modificarAcreedor.js"/>'></script>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Modificaci&oacute;n Acreedor</li>
					</ol>

					<article id="product-list-manual">
						<h3>Modificaci&oacute;n del Acreedor</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>N&uacute;mero de orden de compra: <span>${oc}</span></h4>
						<br/>
					    
						<s:form action="modificarAcreedor" theme="simple" class="form-inline" role="form" data-toggle="validator">
							<div class="panel-body">
								<h4>Datos del acreedor</h4>
								<input type="hidden" name="oc" value="${oc}"/>
								<div class="col-md-6">
									<div class="form-group">
										<label>DNI</label>
										<input type="text" name="modAcreedor.rut" class="form-control" value="${modAcreedor.rut}" readonly>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Nombre</label>
										<s:textfield id="nombreProv" type="text" name="modAcreedor.nombreProv" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Calle</label>
										<s:textfield id="direccionProv" type="text" name="modAcreedor.direccionProv" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Distrito</label>
										<s:textfield id="distritoProv" type="text" name="modAcreedor.distritoProv" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>* Regi&oacute;n</label>
										<select id="regionProv" class="form-control" name="modAcreedor.regionProv" data-error="Debe seleccionar una opción" required="">
											<option value="">Seleccione</option>
											<s:iterator value="regiones" var="region">
											    <s:if test="%{modAcreedor.regionProv == #region.codigo}">
													<option value='<s:property value="#region.codigo"/>' selected>(<s:property value="#region.codigo"/>) <s:property value="#region.nombre"/></option>
												</s:if>	
												<s:else><option value='<s:property value="#region.codigo"/>'>(<s:property value="#region.codigo"/>) <s:property value="#region.nombre"/></option></s:else>
											</s:iterator>
										</select>
										<div class="help-block with-errors"></div>
									</div>
								</div>								
								<div class="col-md-6">
									<div class="form-group">
										<label>Telefono</label>
										<s:textfield type="number" name="modAcreedor.telefono" cssClass="form-control" min="0"/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="clear"></div>
								<br/>
								<h4>Datos Bancarios</h4>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Titular Cuenta</label>
										<s:textfield id="titular" type="text" name="modAcreedor.titular" cssClass="form-control" data-error="Campo obligatorio" data-minlength="1" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Clave Banco</label>
										<select id="claveBanco" class="form-control" name="modAcreedor.claveBanco" data-error="Debe seleccionar una opción" required="">
											<option value="">Seleccione</option>
											<s:iterator value="bancos" var="banco">
											    <s:if test="%{modAcreedor.claveBanco == #banco.codigo}">
													<option value='<s:property value="#banco.codigo"/>' selected>(<s:property value="#banco.codigo"/>) <s:property value="#banco.nombre"/></option>
												</s:if>	
												<s:else><option value='<s:property value="#banco.codigo"/>'>(<s:property value="#banco.codigo"/>) <s:property value="#banco.nombre"/></option></s:else>
											</s:iterator>
										</select>
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>* Cuenta Bancaria</label>
										<s:textfield id="cuentaBank" type="number" name="modAcreedor.cuentaBank" cssClass="form-control" data-error="Campo obligatorio" min="0" required=""/>
									    <div class="help-block with-errors"></div>
									</div>
								</div>								
								<div class="clear"></div>
								<div class="center">
									<div class="form-group">
										<div class="form-inline">
											<div class="form-group">
												<input type="hidden" id="metodo" name="metodo" value=""/>
												<button class="btn btn-primary btn-lg" 
												 onclick="javascript:cancelar();">Cancelar</button>
											</div>
											<div class="form-group">
											<s:if test="#session.nivelPermiso == 1">
												<button class="btn btn-primary btn-lg">Modificar</button>
											</s:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</s:form>
					</article>
				</div>
			</div>
		</div>
<s:include value="/commons/footer.jsp"></s:include>