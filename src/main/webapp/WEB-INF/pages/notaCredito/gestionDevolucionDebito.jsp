<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Gesti&oacute;n devoluci&oacute;n debito</li>
					</ol>

					<article id="product-list-manual">
						<h3>Gesti&oacute;n devoluci&oacute;n debito</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>Numero de orden de compra: <span>${oc}</span> | FOLIO SAP N&deg; <span>${acreedor.folioSAP}</span></h4>

						<div class="table-responsive">
						    <h4>Datos acreedor SAP</h4>
							<table id="tblDatos" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>DNI del cliente</th>
										<th>Nombre 1</th>
										<th>Calle</th>
										<th>Distrito</th>
										<th>Regi&oacute;n</th>
										<th>Tel&eacute;fono</th>
										<th>Pa&iacute;s</th>
										<th>Idioma</th>
										<th>Cuenta asociada</th>
										<th>Condiciones de pago</th>
										<th>Verif. factura doble</th>
										<th>V&iacute;as de pago</th>
										<th>Estado Bloqueo</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber2(acreedor.rut)"/>-<s:property value="acreedor.dv"/></td>
										<td><s:property value="acreedor.nombre1"/></td>
										<td><s:property value="acreedor.calle"/></td>
										<td><s:property value="acreedor.distrito"/></td>
										<td><s:property value="acreedor.nombreRegion"/><br/>(<s:property value="acreedor.codigoRegion"/>)</td>
										<td><s:property value="acreedor.telefono"/></td>
										<td><s:property value="acreedor.pais"/></td>
										<td><s:property value="acreedor.idioma"/></td>
										<td><s:property value="acreedor.cuentaAsociada"/></td>
										<td><s:property value="acreedor.condicionPago"/></td>
										<td><s:property value="acreedor.verFacturaDoble"/></td>
										<td><s:property value="acreedor.viaPago"/></td>
										<td><s:property value="acreedor.estadoBloqueo"/></td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="table-responsive">
							<h4>Datos Bancarios</h4>
							<table id="tblDatos" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Titular Cuenta</th>
										<th>Banco</th>
										<th>Cuenta bancaria</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><s:property value="acreedor.titularCuenta"/></td>
										<td><s:property value="acreedor.nombreBanco"/><br/>(<s:property value="acreedor.claveBanco"/>)</td>
										<td><s:property value="acreedor.cuentaBancaria"/></td>
									</tr>
								</tbody>
							</table>
						</div>

						<s:if test="acreedor.estadoBloqueo == ''">
							<s:form id="validarAcreedor" action="validarAcreedor.action" class="navbar-form navbar-top-actions" role="form" data-toggle="validator">
								<input type="hidden" name="modAcreedor.rut" value="${acreedor.rut}-${acreedor.dv}"/>
								<input type="hidden" name="modAcreedor.claveBanco" value="${acreedor.claveBanco}"/>
								<input type="hidden" name="modAcreedor.cuentaBank" value="${acreedor.cuentaBancaria}"/>
								<input type="hidden" name="estado" value="3"/>
								<input type="hidden" name="modAcreedor.nombreProv" value="${acreedor.nombre1}"/>
								<input type="hidden" name="modAcreedor.direccionProv" value="${acreedor.calle}"/>
								<input type="hidden" name="modAcreedor.distritoProv" value="${acreedor.distrito}"/>
								<input type="hidden" name="modAcreedor.regionProv" value="${acreedor.codigoRegion}"/>
								<input type="hidden" name="modAcreedor.telefono" value="${acreedor.telefono}"/>
								<input type="hidden" name="modAcreedor.titular" value="${acreedor.titularCuenta}"/>
								<input type="hidden" name="oc" value="${oc}"/>
								
								<div class="form-group pull-right">
									<s:if test="#session.nivelPermiso == 1">
									<button class="btn btn-success btn-sm">Modificar Acreedor</button>
									</s:if>
								</div>
							</s:form>
						</s:if>
					</article>
					
					<article id="product-list-manual">
						<div class="center">
							<s:form id="generarNCDD" action="generarNCDD.action" theme="simple" class="form-inline" role="form" data-toggle="validator">
								<input type="hidden" name="acreedor.rut" value="${acreedor.rut}"/>
								<input type="hidden" name="acreedor.dv" value="${acreedor.dv}"/>
								<input type="hidden" name="acreedor.folioSAP" value="${acreedor.folioSAP}"/>
								<input type="hidden" name="acreedor.claveBanco" value="${acreedor.claveBanco}"/>
								<input type="hidden" name="acreedor.cuentaBancaria" value="${acreedor.cuentaBancaria}"/>
								<input type="hidden" name="oc" value="${oc}"/>

								<div class="form-group">
									<label>* Ticket asociado</label>

									<div class="input-group">
										<s:textfield type="number" name="acreedor.ticketAsociado" cssClass="form-control" placeholder="Ingresar Nº ticket" data-error="Campo obligatorio" min="1" required=""/>
										<span class="input-group-btn">
										<s:if test="#session.nivelPermiso == 1">
											<button class="btn btn-primary">Generar NC Debito</button>
										</s:if>
										</span>
									</div>
									<div class="help-block with-errors"></div>
								</div>
							</s:form>
						</div>
					</article>
				</div>
			</div>
		</div>
<s:include value="/commons/footer.jsp"></s:include>