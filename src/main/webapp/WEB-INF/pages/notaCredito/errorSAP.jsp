<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		<script type="text/javascript" src='<s:url value="/js/errorSAP.js"/>'></script>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Error SAP</li>
					</ol>

					<article id="product-list-manual">
						<h3>Error</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="mensajeError != null">
							<div id="mensajeError" class="alert alert-danger">${mensajeError}</div>
						</s:elseif>
						<h4>Numero de orden de compra: <span>${oc}</span></h4>
						
						<div id="mensajeContinuar" class="alert alert-warning">
							<p>
						    	<span class="fa fa-exclamation"></span>
						    	Atenci&oacute;n: Aseg&uacute;rese que el cliente est&eacute; creado en SAP antes de generar la nota de cr&eacute;dito.
								<br/>
								<div class="center">
									<s:form id="generarNCDD" action="generarNCDD.action" theme="simple" class="form-inline " role="form">
											<input type="hidden" id="metodo" name="metodo" value="continuar"/>
											<div class="form-group">
												<div class="form-inline">
													<div class="form-group">
														<button class="btn btn-primary btn-lg" 
														 onclick="javascript:cancelar();">Cancelar</button>
													 </div>
													<div class="form-group">
														<button class="btn btn-primary">Generar NC</button>
													</div>
												</div>
											</div>
									</s:form>
								</div>
							</p>
						</div>								
					</article>
				</div>
			</div>
		</div>
<s:include value="/commons/footer.jsp"></s:include>