<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.table2excel.js"></script>		
<script type="text/javascript" src='<s:url value="/js/pendienteStock.js"/>'></script>
<link rel="stylesheet" type="text/css" href="css/datatables.min.css"/> 
		<script type="text/javascript" src="js/datatables.min.js"></script>
		 <script type="text/javascript">
			 $(document).ready(
					function() {
				    	$('#tblDatos').DataTable({
							"bFilter": false,
							"scrollX": false,
							"sDom": '<"top"i>rtlp',
							"bInfo": false,
							"bPaginate": false,
							"bSortable": true,
							"bLengthChange": true,
							"bSort": true,
							"iDisplayLength": 10
				    	});
					}
				);
				

	
		$(function() {
				$("#aExcel").click(function(){
				$("#tblDatos").table2excel({
					exclude: ".noExl",
    				name: "Exportacion A Excel",
    				filename:"Pendiente valida stock"
				}); 
				 });
			});				
				
    </script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
				<%-- INICIO SECCION PRINCIPAL --%>
					<ol class="breadcrumb">
						<li><a href='<s:url action="home" />'>Home</a></li>
						<li class="active">PENDIENTE VALIDA STOCK</li>
					</ol>

					<article id="product-list-manual">
						<h3>PENDIENTE VALIDA STOCK</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<h4>Total n&uacute;mero de ordenes: <span><s:property value="nroOrdenes"/></span></h4>
						
						<nav class="navbar">
							<div class="container-fluid row">
								<div class="col-sm-3 col-xs-8">
									<div class="input-group">
										<input id="txtBuscar" type="text" class="form-control" placeholder="Buscar OC">
										<span class="input-group-btn">
											<button id="btnBuscar" class="btn btn-primary" type="button">Buscar</button>
										</span>
									</div>
								</div>

<!-- 								<div class="filtros col-sm-2 col-xs-4"> -->
<!-- 									<div class="btn-group-md pull-right"> -->
<%-- 										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrar por <span class="caret"></span> --%>
<!-- 										</button> -->
<!-- 										<ul class="dropdown-menu"> -->
<!-- 											<li><a href="#">Boleta</a></li> -->
<!-- 											<li role="separator" class="divider"></li> -->
<!-- 											<li><a href="#">Factura</a></li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
<!-- 								</div> -->

								<div class="col-sm-7 col-xs-12">
									<div class="btn-group-md pull-right">
										<a href="javascript:window.location.reload();"><button type="button" class="btn btn-large btn-default"><span class="fa fa-refresh"></span> Refrescar</button></a>
									</div>
								</div>
							</div>
						</nav>
						<button class="btn btn-large btn-success" id="aExcel">Excel</button>
						<div class="table-responsive">
							<table id="tblDatos" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>OC</th>
										<!-- <th>Estado</th>-->
										<th>DNI del cliente</th>
										<th>Fecha</th>
										<th>Fecha de despacho</th>
										<th>Medio de Pago</th>
										<th>Monto</th>
										<th>Sucursal de retiro</th>
										<th class="noExl">Acciones</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="ordenesCompra" var="orden">
									<tr>
										<td><s:property value="#orden.notaVenta.correlativoVenta"/></td>
<!-- 
										<td>
										<s:if test="notaVenta.estado == 81">
										   81-M.P. Seguro
										</s:if>
										<s:if test="notaVenta.estado == 87">
										   87-M.P. No Seguro
										</s:if>
										</td>
-->
										<td><s:property value="#orden.notaVenta.rutComprador"/>-<s:property value="#orden.NotaVenta.dvComprador"/></td>
										<td><s:date name="#orden.notaVenta.fechaCreacion" format="dd/MM/yyyy"/></td>
										<td><s:date name="#orden.despacho.fechaDespacho" format="dd/MM/yyyy"/></td>
										<td><s:property value="#orden.notaVenta.tipoPago"/></td>
										<td><s:property value="@cl.ripley.omnicanalidad.util.Util@formatNumber(#orden.notaVenta.montoVenta)"/></td>
										<td><s:property value="#orden.despacho.direccionDespacho"/></td>
										<s:url action="confirmacionNCDetalle" var="detalleNCUrl">
											<s:param name="estado" value="%{notaVenta.estado}"/>
											<s:param name="oc" value="%{notaVenta.correlativoVenta}"/>
										</s:url>
										<td class="noExl"><div class="center"><a href="${detalleNCUrl}"><button type="button" class="btn btn-default btn-xs">Ver detalle</button></a></div></td>
									</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>

						<form class="navbar-form navbar-top-actions">
							<div class="form-group pull-right">
								<a href="javascript:window.location.reload();"><button type="button" class="btn btn-default btn-sm"><span class="fa fa-refresh"></span> Refrescar</button></a>
							</div>
						</form>

					</article>
				<%-- FIN SECCION PRINCIPAL --%>
				</div>
			</div>
		</div>
		<s:include value="/commons/footer.jsp"></s:include>
