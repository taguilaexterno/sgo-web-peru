<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<script type="text/javascript">
   function checkTodos(){
       $(".check").each(function(){
           $(this).prop('checked',true);
       });
       $(".todos").prop('checked', true);
       $(".sucOnline").prop('checked', false);
       $(".ninguno").prop('checked', false);
   }
   function checkNinguno(){
       $(".check").each(function(){
           $(this).prop('checked',false);
       });
       $(".ninguno").prop('checked', true);
       $(".todos").prop('checked', false);
       $(".sucOnline").prop('checked', false);
   }
   function checkSucOnline(){
	   $(".check").each(function(){
           $(this).prop('checked',false);
       });
       $(".ninguno").prop('checked', false);
       $(".todos").prop('checked', false);
	   document.getElementById("60 - Caja 120").checked = true;
       $(".sucOnline").prop('checked', true);
   }
   function getCajas(){
   var vars = '';
       $(".check").each(function(){
           if( $(this).prop('checked')){
           	vars= vars + $(this).val() + ',';
           }
       });
       $("#cajasConsultadas").val(vars);
   }
    </script>
<s:include value="/commons/header.jsp"></s:include>
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Reporte cierre de caja</li>
			</ol>
			<article id="product-list-manual">
				<h3>Reporte cierre de caja</h3>
				<s:if test="mensaje != null">
					<div id="mensajeError" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="clear"></div>
				
				<s:form action="reporteCierreCaja" theme="simple" class="form-inline" role="form" data-toggle="validator">
					<s:hidden id="metodo" name="metodo" value="consulta" />
					<s:hidden id="cajasConsultadas" name="cajasConsultadas" value=""/>
					<div class="form-group">
						<div class="clear"></div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Fecha inicio</label>
								<div>
									<input id="fechaInicio" name="fechaInicio" class="form-control"
										data-date-format="yyyy/mm/dd hh:ii:ss" value="${fechaInicio}"
										onclick="$('#fechaInicio').datepicker('show')" required="">
								</div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Fecha fin</label>
								<div>
									<input id="fechaFin" name="fechaFin" class="form-control"
										data-date-format="yyyy/mm/dd hh:ii:ss" value="${fechaFin}"
										onclick="$('#fechaFin').datepicker('show')" required="">
								</div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"><label><input type="checkbox" onclick="checkTodos()" class="todos"> Marcar todas las Cajas</label></div>
						<div class="col-md-3"><label><input type="checkbox" onclick="checkNinguno()" class="ninguno"> Desmarcar Cajas</label></div>
						<div class="col-md-3"><label><input type="checkbox" onclick="checkSucOnline()" class="sucOnline"> Marcar Sucursal Online</label></div>
			        </div>
			        <br/>
					<div class="row">
					    <div class="col-md-6 mb-4">
					        <div class="card tarjetaCajas scrollbar-deep-purple bordered-deep-purple thin">
					           <div class="col-md-12">
									<div class="form-group">
										<s:iterator value="cajas" var="caja">
											<div class="row">
												<div class="checkbox">
						  							<label><input name="" type="checkbox" class="check" id='<s:property value="#caja" />' value='<s:property value="#caja" />'> Sucursal N&#176; <s:property value="#caja" /></label>
												</div>
											</div>
										</s:iterator>
									</div>
								</div>
					        </div>					
					    </div>
					</div>
					<br/>			
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-primary" onclick="getCajas();">Buscar</button>	
						</div>
					</div>
				</s:form>
			</article>	
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>

<style type="text/css">

.scrollbar-deep-purple::-webkit-scrollbar-thumb {
  border-radius: 10px;
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
  background-color: #512da8; }
  

.thin::-webkit-scrollbar {
  width: 6px; }

.tarjetaCajas {
  border-color: blue;
  position: relative;
  overflow-y: scroll;
  height: 120px; }
</style>
