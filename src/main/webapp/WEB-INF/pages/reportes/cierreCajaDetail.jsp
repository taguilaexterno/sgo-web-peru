<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>
<s:include value="/commons/header.jsp"></s:include>

<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Reporte cierre de caja</li>
			</ol>
			<article id="product-list-manual">
				<h3>Reporte cierre de caja</h3>
				<s:if test="mensaje != null">
					<div id="mensajeExito" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="clear"></div>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>N&#176; Sucursal</th>
								<th>N&#176; Caja</th>
								<th>Fecha de Apertura</th>
								<th>Fecha de Cierre</th>
								<th>Descargar</th>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="reporteCorto" var="reporte">
								<tr>
									<td><s:property value="#reporte.sucursal" /></td>
									<td><s:property value="#reporte.caja" /></td>
									<td><s:property value="#reporte.fechaApertura" /></td>
									<td><s:property value="#reporte.fechaCierre" /></td>
									<td><s:form action="reporteCierreCaja" name="form" id="form_%{#reporte.idAperturaCierre}">
											<s:hidden name="metodo" value="descargar" />
											<input type="hidden" name="idAperturaCierre" value="<s:property value="#reporte.idAperturaCierre" />" />
											<input type="hidden" name="idCaja" value="<s:property value="#reporte.caja" />" />
											<input type="hidden" name="idSucursal" value="<s:property value="#reporte.sucursal" />" />
											<a href="reporteCierreCaja?metodo=descargar&idAperturaCierre=<s:property value="#reporte.idAperturaCierre" />&idCaja=<s:property value="#reporte.caja" />&idSucursal=<s:property value="#reporte.sucursal" />" class="btn btn-primary">Descargar</a>
										</s:form>
									</td>
								</tr>
							</s:iterator>
						</tbody>
					</table>
				</div>
			</article>	
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>
