<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld"%>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>

    
<script type="text/javascript">

	$(document).ready( function () {
		$.noConflict();
	    var table = $('#detalle').DataTable({
	    	<s:if test="#session.nivelPermiso == 1">
		    dom: 'Bfrtip',
	        buttons: [
	        	''
	            <% //'csv', 'excel' %>
	        ],
	        </s:if>
	        searching: false, 
	        paging: true});
	    
	} );
	
   function limpiar(){
       $("#fechaInicio").val("");
       $("#fechaFin").val("");
       $("#numeroOrden").val("");
       $("#numeroDNI").val("");
       $("#estado option[value='']").attr("selected",true);
   }
   
   function obtenerResultado(){
   
   	if($('#estado').val()=='' && $("#numeroOrden").val() == ''
   			&& ($('#fechaInicio').val() == '' || $('#fechaFin').val() == '')){
   	
   		$("div.mensajeEstado").fadeIn(500).delay(2000).slideUp( 500);
   		
   		
   	}else{
   		$('#loader').show();
   		$.ajax({
            type: "POST",
            data: {
                'fechaInicio':$("#fechaInicio").val(),
       			'fechaFin':$("#fechaFin").val(),
       			'numeroOrden':$("#numeroOrden").val(),
       			'numeroDNI':$("#numeroDNI").val(),
       			'estado':$("#estado").val(),
       			'tipo':$("#tipo").val(),
       			'metodo':'consulta',
                'action':'reporteModeloExtendido'
            },
            dateType: "json",
            success: function(data){
            	
            	$('#detalle').append('<tbody></tbody>');
            	var table = $('#detalle').DataTable();
            	table.clear().draw();
            	            	
	            $.each( JSON.parse(data.json), function( key, value ) {
	            
	            	
	            	
					$('#detalle').dataTable().fnAddData( [value.numeroOrden,
											value.fechaBoleta,
											value.tipoOrdenCompra,
											value.estado,
											value.tipoSubOrden,
											value.ordenMKP,
											value.estadoMKP,
											value.moneda,
											value.monto,
											value.despacho,
											value.descuento,
											value.tipoDescuento,
											value.tipoDocumento,
											value.documento,
											value.longonID,
											value.sucursal,
											value.ejecutivoVenta,
											value.numeroItem,
											value.codigoItem,
											value.skuItem,
											value.descripcionItem,
											value.descripcionPMM,
											value.precioitem,
											value.cantidaditem,
											value.descuentoItem,
											value.tipoDescuentoItem,
											value.regaloItem,
											value.mensajeItem,
											value.referenciaDireccion,
											value.indicadorExtraItem,
											value.bodegaitem,
											value.sucursalReserva,
											value.tipoDespachoItem,
											value.fechaDespachoItem,
											value.dniItem,
											value.rutDespacho,
											value.nombreDespacho,
											value.dpto,
											value.provincia,
											value.distrito,
											value.direccionDespacho,
											value.telefonoDespacho1,
											value.telefonoDespacho2,
											value.tipoDocumentoDespacho,
											value.rutCliente,
											value.dptoFact,
											value.provFact,
											value.distFact,
											value.direccionCliente,
											value.nombreCliente,
											value.apePatCliente,
											value.apeMatCliente,
											value.emailCliente,
											value.codigoAutorizador,
											value.tipotarjeta,
											value.binNumber,
											value.numeroTrajeta,
											value.numeroCuotas,
											value.valorCuota,
											value.vencimiento,
											value.certificacion,
											value.numeroOperacionBancaria,
											value.comprobante,
											value.ruc,
											value.razonSocial,
											value.kioskoVendedorMac,
											value.fechaTicket,
											value.numeroBoleta,
											value.ticket,
											value.caja,
											value.sucursalTiendaFisica,
											value.esNC]
        			);
											
				}); 
            	$('#loader').hide(1000);
		        
		    } ,
        });
        
   	}
   }   

</script>    
<s:include value="/commons/header.jsp"></s:include>
<div class="container">
	<div class="row">
		<s:include value="/commons/menu.jsp"></s:include>
		<div class="col-md-9">
			<%-- INICIO SECCION PRINCIPAL --%>
			<ol class="breadcrumb">
				<li><a href='<s:url action="home"/>'>Home</a></li>
				<li class="active">Reporte Modelo Extendido</li>
			</ol>
			<article id="product-list-manual">
				<h3>Reporte Modelo Extendido</h3>
				<s:if test="mensaje != null">
					<div id="mensajeError" class="alert alert-danger">${mensaje}</div>
				</s:if>
				<div class="clear"></div>
				
					<s:hidden id="metodo" name="metodo" value="consulta" />
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label><% //Fecha inicio %></label>
								<div>
									<input type="hidden" id="fechaInicio" name="fechaInicio" class="form-control"
										data-date-format="yyyy/mm/dd hh:ii:ss" value="${fechaInicio}"
										onclick="$('#fechaInicio').datepicker('show')">
								</div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><% //Fecha fin %></label>
								<div>
									<input type="hidden" id="fechaFin" name="fechaFin" class="form-control"
										data-date-format="yyyy/mm/dd hh:ii:ss" value="${fechaFin}"
										onclick="$('#fechaFin').datepicker('show')">
								</div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Nº Orden</label>
								<s:textfield type="number" id="numeroOrden" name="numeroOrden"
										cssClass="form-control" placeholder="Ingresa nº de orden"
										data-minlength="2" />
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
							<label>&nbsp;</label>
								<div><div class="row">
									<button class="btn btn-primary" onclick="javascript:limpiar();">Limpiar</button>
									<button class="btn btn-primary" onclick="javascript:obtenerResultado();">Buscar</button></div>
								</div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
			        </div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>DNI</label>
								<s:textfield type="number" id="numeroDNI" name="numeroDNI"
										cssClass="form-control" placeholder="Ingresa nº de DNI"
										data-minlength="2" />
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Estado</label> 
								<select class="form-control" name="estado" id="estado" >
									<option value="">Seleccione</option>
									<s:iterator value="estados" var="estados">
										<option value="<s:property value="#estados.key" />"><s:property value="#estados.value" /></option>
									</s:iterator>
								</select>
								<div class="mensajeEstado" style="display:none;"><label style="color: red;">Campo Obligatorio</label></div>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Tipo OC</label> 
								<select class="form-control" name="tipo" id="tipo" >
									<option value="ALL">Todos</option>
									<option value="0">0 - Solo Ripley</option>
									<option value="1">1 - Solo Marketplace</option>
									<option value="2">2 - Ripley + Marketplace</option>
								</select>
							</div>
							<div class="help-block with-errors"></div>
						</div>						
			        </div>
			        <div class="row">
			        	<div class="col-md-5">
			        	</div>
			        	<div class="col-md-5">
			        		 <div id="loader" class="loader" style="display:none;"></div>
			        	</div>
			        </div>
				      <div class="row">
				      <div class="col-md-12">
				      	<div class="caja">
				      	<table id="detalle" class="table-responsive" style="width:100%">
			        		<thead>
			           			<tr>
			                		<th>N° de Orden</th>
					                <th>Fecha/Hora Orden</th>
					                <th>Tipo OC</th>
					                <th>Estado</th>
					                <th>Tipo Sub Orden</th>
					                <th>Orden MKP</th>
					                <th>Estado MKP</th>
					                <th>Moneda</th>
					                <th>Monto</th>
					                <th>Despacho Orden</th>
					                <th>Monto Dscto.</th>
					                <th>Tipo Dscto.</th>
					                <th>Tipo Doc. Cliente</th>
					                <th>Nro. Doc. Cliente</th>
					                <th>Logon ID</th>
					                <th>Sucursal de Comercio</th>
					                <th>Cod. Vendedor</th>
					                <th>Nro. item Orden</th>
					                <th>Cod. Articulo</th>
					                <th>SKU</th>
					                <th>Descrip. Articulo</th>
					                <th>Descrip. PMM</th>
					                <th>Precio Articulo</th>
					                <th>Cantidad Item</th>
					                <th>Descuento Item</th>
					                <th>Tipo Dcto. Item</th>
					                <th>Regalo item</th>
					                <th>Mensaje item</th>
					                <th>Ref. dirección</th>
					                <th>Indicador Extra Item</th>
					                <th>Bodega Item</th>
					                <th>Sucursal Reserva</th>
					                <th>Tipo Desp. Item</th>
					                <th>Fecha Desp. Item</th>
					                <th>DNI Item</th>
					                <th>DNI Despacho</th>
					                <th>Nombre Despacho</th>
					                <th>Dpto.</th>
					                <th>Provincia</th>
					                <th>Distrito</th>
					                <th>Dirección Despacho</th>
					                <th>Teléfono Despacho 1</th>
					                <th>Teléfono Despacho 2</th>
					                <th>Tipo Doc. Despacho</th>
					                <th>DNI Cliente</th>
					                <th>Dpto. Fact.</th>
					                <th>Prov. Fact.</th>
					                <th>Dist. Fact.</th>
					                <th>Dirección Cliente</th>
					                <th>Nombre Cliente</th>
					                <th>Apellido Pat.</th>
					                <th>Apellido Mat.</th>
					                <th>Email Cliente</th>
					                <th>Cod. Autorizador</th>
					                <th>Tipo Tarjeta</th>
					                <th>BIN number</th>
					                <th>Num. Tarjeta</th>
					                <th>Num. Cuotas</th>
					                <th>Valor Cuota</th>
					                <th>Vencimiento</th>
					                <th>Certificación</th>
					                <th>Nro. Op. Bancaria</th>
					                <th>Comprobante</th>
					                <th>RUC</th>
					                <th>Razón Social</th>
					                <th>Kiosco Vendedor MAC</th>
					                <th>Fecha Ticket</th>
					                <th>Numero Boleta</th>
					                <th>Ticket</th>
					                <th>Caja</th>
					                <th>Suc. Tienda Fisica</th>
					                <th>Nota de Cr&eacute;dito</th>
					            </tr>
					        </thead>
					        <tbody>
					        </tbody>
					  	</table>
					  	</div>
					  	</div>
				      </div>
			</article>	
		</div>
	</div>
</div>
<s:include value="/commons/footer.jsp"></s:include>

<style type="text/css">

.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

.caja {
  border-color: blue;
  position: relative;
  overflow-x: scroll; 
  overflow-y: hidden;}

</style>
