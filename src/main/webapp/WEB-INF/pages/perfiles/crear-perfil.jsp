<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>

		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Crear Perfiles</li>
					</ol>

					<article id="product-list-manual">
						<h3>Crea un nuevo perfil de usuario</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:form action="guardarPerfil" theme="simple" role="form" data-toggle="validator">
							<input type="hidden" name="perfilBean.usuarioCrea" value="${session.usuario.usuario}"/>
							<div class="form-group">
								<label>Nombre del perfil</label>
								<s:textfield name="perfilBean.nombrePerfil" cssClass="form-control" placeholder="Ingresa un nombre para el perfil" 
										data-error="Debe ingresar un nombre para el perfil" data-minlength="2" required=""/>
							</div>
							<div class="form-group">
								<label>Configura los permisos</label>
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Tarea</th>
												<th>Permiso</th>
											</tr>
										</thead>
										<tbody>
											<s:iterator value="modulos" var="modulo" status="status">
												<tr>
													<input type="hidden" name="perfilBean.modulos[${status.index}].id" value="${modulo.id}"/>
													<td>${modulo.nombreModulo}</td>
													<td>
														<div class="form-group">
															<select name='perfilBean.modulos[${status.index}].permiso.id' class="form-control input-sm" 
																	data-error="Debe seleccionar un valor" required="">
																<option value="">Seleccione</option>
																<s:iterator value="permisos" var="permiso">
																	<option value="${permiso.id}">${permiso.nombrePermiso}</option>
																</s:iterator>
															</select>
															<div class="help-block with-errors"></div>
														</div>
													</td>
												</tr>
											</s:iterator>
										</tbody>
									</table>
								</div>
							</div>
							<s:if test="#session.nivelPermiso == 1">
								<button type="submit" class="btn btn-lg btn-primary pull-right">Crear perfil</button>
							</s:if>
						</s:form>
					</article>
				</div>
			</div>
		</div>

		<div id="edit-user" class="modal fade">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Editar Usuario</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre</label>
									<input type="text" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<label>Email</label>
									<input type="text" class="form-control" placeholder="email@email.com">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Perfil</label>
									<select class="form-control">
										<option>Administrador</option>
										<option>Editor</option>
									</select>
								</div>
								<div class="form-group">
									<label>Cargo</label>
									<input type="text" class="form-control" placeholder="Cargo">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Contrase&ntilde;a</label>
									<input type="password" class="form-control" placeholder="password">
								</div>
								<div class="form-group">
									<label>Repetir contrase&ntilde;a</label>
									<input type="password" class="form-control" placeholder="password">
								</div>
							</div>
						</div>
						<hr>
						<h4>Habilidades del usuario</h4>
						<div class="responsive-table">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tarea</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Administraci&oacute;n de reglas para generaci&oacute;n de DTE autom&aacute;tico</td>
										<td>
											<select class="form-control input-sm">
												<option>Activo</option>
												<option>Inactivo</option>
												<option>S&oacute;lo lectura</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<s:if test="#session.nivelPermiso == 1">
							<button type="button" class="btn btn-primary">Guardar cambios</button>
						</s:if>
					</div>
				</div>
			</div>
		</div>

	<s:include value="/commons/footer.jsp"></s:include>