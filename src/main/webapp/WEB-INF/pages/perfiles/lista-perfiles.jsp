<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
		
<script type="text/javascript" src='<s:url value="/js/listaPerfiles.js"/>'></script>


		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Lista de perfiles</li>
					</ol>

					<article id="product-list-manual">
						<h3>Perfiles de usuario</h3>
						<s:if test="mensaje != null">
							<div id="mensajeExito" class="alert alert-success">${mensaje}</div>
						</s:if>
						<s:elseif test="hasActionErrors()">
							<div id="mensajeError" class="alert alert-danger"><s:actionerror/></div>
						</s:elseif>

						<s:form action="listaPerfiles" theme="simple" cssClass="navbar-form user-navbar">
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="perfilBean.nombrePerfil" class="form-control" placeholder="Buscar perfil">
									<div class="input-group-btn">
										<button class="btn btn-primary">Buscar</button>
									</div>
								</div>
							</div>
							<s:if test="#session.nivelPermiso == 1">
								<div class="form-group pull-right">
									<a href='<s:url action="crearPerfil"/>'><button type="button" class="btn btn-default"><span class="fa fa-plus"></span> Crear nuevo perfil</button></a>
								</div>
							</s:if>
						</s:form>

						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Usuarios</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="perfilesPorUsuarioMap" var="perfil">
									<tr>
										<td>${perfil.key.nombrePerfil}</td>
										<td>
											<s:iterator value="value" var="usr">
												<a href="javascript:void(0);" class="btn btn-default btn-xs">${usr.nombre} ${usr.apePaterno}</a>
											</s:iterator>
										</td>
										<td>
											<div class="center">
												<button onclick="getPerfilForModal(${perfil.key.id})" class="btn btn-default btn-xs" data-toggle="modal" data-target="#edit-perfil">
													<s:if test="#session.nivelPermiso == 1">
														Editar
													</s:if>
													<s:else>
														Ver
													</s:else>
												</button>
											</div>
										</td>
									</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</article>
				</div>
			</div>
		</div>

		<div id="edit-perfil" class="modal fade">
			<div class="modal-dialog modal-lg">
				<s:form action="actualizarPerfil" theme="simple" role="form" data-toggle="validator">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Editar Perfil</h4>
					</div>
					<input type="hidden" id="perfilBean_codPerfil" name="perfilBean.id"/>
					<input type="hidden" name="perfilBean.usuarioUpd" value="${session.usuario.usuario}"/>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre</label>
									<s:if test="#session.nivelPermiso == 1">
										<s:textfield id="txtEditNombrePerfil" name="perfilBean.nombrePerfil" cssClass="form-control" placeholder="Nombre"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required=""/>
										<div class="help-block with-errors"></div>
									</s:if>
									<s:else>
										<s:textfield id="txtEditNombrePerfil" name="perfilBean.nombrePerfil" cssClass="form-control" placeholder="Nombre"
											data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required="" disabled="true"/>
									</s:else>
								</div>
							</div>
						</div>
						<hr>
						<h4>M&oacute;dulos</h4>
						<div class="responsive-table">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tarea</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody id="tbodyHabilidades">
									<s:iterator value="modulos" var="modulo" status="status">
										<tr>
											<input type="hidden" name="perfilBean.modulos[${status.index}].id" value="${modulo.id}"/>
											<input type="hidden" name="perfilBean.modulos[${status.index}].nombreModulo" value="${modulo.nombreModulo}"/>
											<td>${modulo.nombreModulo}</td>
											<td>
												<div class="form-group">
													<select id="perfilBean_modulos_${modulo.id}" name="perfilBean.modulos[${status.index}].permiso.id" class="form-control" data-error="Debe seleccionar un permiso" required=""
														<s:if test="#session.nivelPermiso != 1">disabled=""</s:if>>
														<option value="">Seleccione</option>
														<s:iterator value="permisos" var="perm">
															<option value="${perm.id}">${perm.nombrePermiso}</option>
														</s:iterator>
													</select>
													<div class="help-block with-errors"></div>
												</div>
											</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<s:if test="#session.nivelPermiso == 1">
							<button type="submit" class="btn btn-primary">Guardar cambios</button>
						</s:if>
					</div>
				</div>
				</s:form>
			</div>
		</div>

	<s:include value="/commons/footer.jsp"></s:include>