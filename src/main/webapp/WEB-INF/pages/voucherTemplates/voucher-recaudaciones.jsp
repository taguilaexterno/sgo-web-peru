<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/voucherRecaudacion.js"/>'></script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Configuraci&oacute;n de voucher recaudaciones.</li>
					</ol>

					<article id="product-list-manual">
						<h3>Configuraci&oacute;n de voucher recaudaciones.</h3>

						<div class="responsive-table">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Tipo de recaudaci&oacute;n</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="temps" var="temp" status="status">
										<tr>
											<td>${temp.nombre}</td>
											<td>
												<div class="row">
													<div class="col-xs-3">
														<s:if test="#temp.estado == 1">							
															<div class="estadoActual" <s:if test="#session.nivelPermiso == 1">data-toggle="modal" data-target="#confirmar"</s:if>>
																<span class="label label-success">
																	<span class="fa fa-check"></span>
																	<input type="hidden" name="estado"/>
																	<input type="hidden" name="llave"/>
																	Activo
																</span>
															</div>
														</s:if>
														<s:else>
															<div class="estadoActual" <s:if test="#session.nivelPermiso == 1">data-toggle="modal" data-target="#confirmar"</s:if>>
																<span class="label label-danger">
																	<span class="fa fa-check"></span>
																	<input type="hidden" name="estado"/>
																	<input type="hidden" name="llave"/>
																	Inactivo
																</span>
															</div>
														</s:else>
													</div>
													<div class="col-xs-2">
														<s:url action="voucherTemplatesEdit" var="editUrl">
															<s:param name="llave">${llave}</s:param>
														</s:url>
														<a href="${editUrl}">
															<button type="button" class="btn btn-default btn-xs">
																<s:if test="#session.nivelPermiso == 1">
																	Editar
																</s:if>
																<s:else>
																	Ver
																</s:else>
															</button>
														</a>
													</div>
												</div>
											</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</article>
				</div>
			</div>
		</div>
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
				<s:form action="voucherTemplatesActivar">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Confirmar Voucher de Recaudaci&oacute;n</h4>
					</div>
					<div class="modal-body">
						<span>¿Está Seguro?</span>
						
						
						<s:hidden name="llave" id="llaveVoucher"/>
						<s:hidden name="estado" id="activo"/>
						
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary"">Guardar</button>
					</div>
				</s:form>
				</div>
			</div>
		</div>
	</s:if>

	<s:include value="/commons/footer.jsp"></s:include>