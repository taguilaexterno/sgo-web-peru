<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
		<s:include value="/commons/header.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/voucherRecaudacion.js"/>'></script>
		<div class="container">
			<div class="row">
				<s:include value="/commons/menu.jsp"></s:include>
				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href='<s:url action="home"/>'>Home</a></li>
						<li class="active">Configuraci&oacute;n de voucher recaudaciones.</li>
					</ol>

					<article id="product-list-manual">
						<h3>Configuraci&oacute;n de voucher recaudaciones.</h3>

						<s:form action="guardarTemplate" theme="simple" role="form" data-toggle="validator">
							<div class="responsive-table">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Nombre</th>
											<th>Estado</th>
											<th>Url</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="temps" var="temp" status="status">
											<tr>
												<td>
													${temp.nombre}
													<s:hidden name="%{'temps[' + #status.index + '].nombre'}"/>
												</td>
												<td>
													<div class="row">
														<div class="col-xs-2">
															<s:if test="#temp.estado == 1">								
																<div class="estadoActual" <s:if test="#session.nivelPermiso == 1">data-toggle="modal" data-target="#confirmar"</s:if>>
																	<span class="label label-success">
																		<span class="fa fa-check"></span>
																		<s:hidden name="estado"/>
																		<s:hidden name="llave"/>
																		Activo
																	</span>
																</div>
															</s:if>
															<s:else>
																<div class="estadoActual" <s:if test="#session.nivelPermiso == 1">data-toggle="modal" data-target="#confirmar"</s:if>>
																	<span class="label label-danger">
																		<span class="fa fa-check"></span>
																		<s:hidden name="estado"/>
																		<s:hidden name="llave"/>
																		Inactivo
																	</span>
																</div>
															</s:else>
														</div>
													</div>
												</td>
												<td>
													<div class="form-group">
														<s:if test="#session.nivelPermiso == 1">
															<s:textfield name="%{'temps[' + #status.index + '].url'}" cssClass="form-control" data-error="Debe completar el campo" required=""/>
															<div class="help-block with-errors"></div>
														</s:if>
														<s:else>
															${temp.url}
														</s:else>
													</div>
													<s:hidden name="%{'temps[' + #status.index + '].llave'}"/>
													<s:hidden name="%{'temps[' + #status.index + '].estado'}"/>
												</td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</div>
							<s:if test="#session.nivelPermiso == 1">
								<div class="center form-actions">
								  	<button class="btn btn-primary btn-lg">Guardar</button>
								</div>
							</s:if>
						</s:form>
					</article>
				</div>
			</div>
		</div>
		
	<s:if test="#session.nivelPermiso == 1">
		<div id="confirmar" class="modal fade">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
				<s:form action="voucherTemplatesActivar">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Confirmar Voucher de Recaudaci&oacute;n</h4>
					</div>
					<div class="modal-body">
						<span>¿Está Seguro?</span>
						
						
						<s:hidden name="llave" id="llaveVoucher"/>
						<s:hidden name="estado" id="activo"/>
						
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary"">Guardar</button>
					</div>
				</s:form>
				</div>
			</div>
		</div>
	</s:if>

	<s:include value="/commons/footer.jsp"></s:include>