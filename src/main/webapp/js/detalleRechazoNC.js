$(function(){
		$('#btnConfirmarModal').on('click', function(){
			$('#confirmarParcial').submit();
		});
		
		$('#btnMostrarConfirmacion').on('click', function(){
			var listChk = $(this).parents('form:first').find('input:checkbox:checked');
			if(listChk.length == 0) {
				$('#confirmar').modal('show');
			} else {
				$('#confirmarParcial').submit();
			}
		});
		
		//recorremos todos los checkbox seleccionados con .each
		$('input[type="checkbox"]:checked').each(function() {
			var id = $(this).attr('id');
			var clase = $(this).attr('class');
			var indice = clase.replace("mycheckbox","");
			//console.log("id: "+id+" clase: "+clase+" indice: "+indice);
			marcar(clase,id,indice);
		});
		
	});

function marcar(valor,unico,indice){
	
	  if ($('#'+unico).is(':checked')){
//		  console.log('checked');
	  	    $('.'+valor).prop('checked', true);
	  	    $('#montoTotalInvisible'+indice).attr("name","articulosConOrdenes["+indice+"].montoTotal");
	  	    $('#subOrdenInvisible'+indice).attr("name","articulosConOrdenes["+indice+"].subOrden");
	  	    $('#idRefundVisible'+indice).attr("required","");
	  } else {
//		  console.log('unchecked');
	  	  	$('.'+valor).prop('checked', false);
		  	$('#montoTotalInvisible'+indice).attr("name","");
		  	$('#subOrdenInvisible'+indice).attr("name","");
		  	$('#idRefundVisible'+indice).removeAttr("required");
	  }
	
}

function validaFormaPago(valor, tipo){
	
	if(valor=='DEBITO'){
		if (tipo=='fpNcTotal'){
			document.forms['ncTotalForm'].action = "validarAcreedor.action";

		}else{
			document.forms['ncParcialForm'].action = "validarAcreedor.action";

		}
	}
	
	
}

