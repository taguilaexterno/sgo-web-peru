$(document).ready(function() {
	$('select[name=sucursalSelect]').on('change', function() {
		getCajas();
	});
		
	$('select[name=cajaSelect]').on('change', function() {
		getProgramacionCierre();
	});
	
	$('.date').datetimepicker({
        format: 'HH:mm:ss'
    });
	
});

function getCajas(){
	var sucursalSelect = $("select[name=sucursalSelect]").val();
	$.ajax({
		data: {'sucursalSelect':sucursalSelect},
		type : "get",
		url : "consultaCajas",
		beforeSend : function() {
		},
		success : function(data) {
	    	$("select[name=cajaSelect]").empty();
			a = data.cajasList;
			a.forEach(function(element) {
				$("select[name=cajaSelect]").append("<option>"+element+"</option>") ;
				});
			getProgramacionCierre();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			
		}
	});
	return;

}

function getProgramacionCierre(){
	var tabla1= "#tblDatos";
	var sucursalSelect = $("select[name=sucursalSelect]").val();
	var cajaSelect = $("select[name=cajaSelect]").val();
	$.ajax({
		data:{'sucursalSelect':sucursalSelect, 'cajaSelect': cajaSelect },
		type : "get",
		url : "consultaListProgramacionCierre",
		beforeSend : function() {
		
		},
		success : function(data) {
			$(tabla1+" tbody").empty();
			$("#tipoCierre_1").empty();
			$("#tipoCierre_2").empty();
			$("#progCierreNew").empty();
			var data2; 
			var divTipoCierre2="";
			var divTipoCierre1="";
			var urlEliminar = $("[name=urlEliminarCierreCaja]").val();
			console.log(data);
			$.each(data.progList, function(i, obj) {
		          if(obj.tipoCierre == 2){
				  data2= data2+"<tr>"+
		            "<td><div>"+obj.numeroSucursal+"</div></td>"+
		            "<td><div>"+obj.numeroCaja+"</div></td>"+
		            "<td><div name='divChange'>"+obj.horaCierre+"</div>"+
		            "<div class=\"form-group\">"
		            +"<div class=\"input-group date\" style=\"display: none;\">"
		            +"<input type=\"text\" name=\"progList["+i+"].horaCierre\" value=\""+obj.horaCierre+"\" id=\"guardarConfiguracionCierreCaja_progList_"+i+"__horaCierre\" class=\"form-control\" required=\"\">"
		            +"<span class=\"input-group-addon\"> <span class=\"glyphicon glyphicon-time\"></span>"
		            +"</span>"
		            +"</div>"
		            +"<div class=\"help-block with-errors\"></div>"
		            +"</div>";
		            +"</td>";
		            data2= data2+"<td>";
		            if(urlEliminar != undefined){
		            	data2= data2+"<a href='"+urlEliminar+"="+obj.id+"&sucursalSelect="+sucursalSelect+"&cajaSelect="+cajaSelect+"'><button type='button' class='btn btn-default btn-xs'>Eliminar</button></a>"
		            }
		            data2= data2+"</td>";
		            data2= data2+"<td style=\"visibility: hidden;\">"+obj.id+"</td>";
		            
		          }else if(obj.tipoCierre == 1){
		        	  divProgCierreNew = "<input type=\"hidden\" name=\"progCierreNew.tipoCierre\" value=\"2\" id=\"agregarCierre_progCierreNew_tipoCierre\">"
				  			+"<input type=\"hidden\" name=\"progCierreNew.volumen\" value=\""+obj.volumen+"\" id=\"agregarCierre_progCierreNew_volumen\">"
				  			+"<input type=\"hidden\" name=\"progCierreNew.numeroCaja\" value=\""+obj.numeroCaja+"\" id=\"agregarCierre_progCierreNew_numeroCaja\">"
				  			+"<input type=\"hidden\" name=\"progCierreNew.numeroSucursal\" value=\""+obj.numeroSucursal+"\" id=\"agregarCierre_progCierreNew_numeroSucursal\">";
		        	  $("#volumenInput").val(obj.volumen);
		        	  $("#horaCierre").val(obj.horaCierre);
		        	 
		          }  
		          divTipoCierre1 = divTipoCierre1 + "<input type=\"hidden\" name=\"progList["+i+"].tipoCierre\" value=\""+obj.tipoCierre+"\" id=\"guardarConfiguracionCierreCaja_progList_"+i+"__tipoCierre\">"
	        	  +"<input type=\"hidden\" name=\"progList["+i+"].id\" value=\""+obj.id+"\" id=\"guardarConfiguracionCierreCaja_progList_"+i+"__id\">"
	        	  +"<input type=\"hidden\" name=\"progList["+i+"].numeroCaja\" value=\""+obj.numeroCaja+"\" id=\"guardarConfiguracionCierreCaja_progList_"+i+"__numeroCaja\">"
	        	  +"<input type=\"hidden\" name=\"progList["+i+"].numeroSucursal\" value=\""+obj.numeroSucursal+"\" id=\"guardarConfiguracionCierreCaja_progList_"+i+"__numeroSucursal\">";
	        	 
	        	  
		          divTipoCierre2 = divTipoCierre2+"<input type=\"hidden\" name=\"progList["+i+"].tipoCierre\" value=\""+obj.tipoCierre+"\" id=\"agregarCierre_progList_"+i+"__tipoCierre\">"
		          +"<input type=\"hidden\" name=\"progList["+i+"].id\" value=\""+obj.id+"\" id=\"agregarCierre_progList_"+i+"__id\">"
		          +"<input type=\"hidden\" name=\"progList["+i+"].horaCierre\" value=\""+obj.horaCierre+"\" id=\"agregarCierre_progList_"+i+"__horaCierre\">"
		          +"<input type=\"hidden\" name=\"progList["+i+"].numeroCaja\" value=\""+obj.numeroCaja+"\" id=\"agregarCierre_progList_"+i+"__numeroCaja\">"
		          +"<input type=\"hidden\" name=\"progList["+i+"].numeroSucursal\" value=\""+obj.numeroSucursal+"\" id=\"agregarCierre_progList_"+i+"__numeroSucursal\">";     
		        });		
			$(tabla1+" tbody").append(data2);
			$("#tipoCierre_1").append(divTipoCierre1);
	        $("#tipoCierre_2").append(divTipoCierre2);
	        $("#progCierreNew").append(divProgCierreNew);
        
		},
		error : function(jqXHR, textStatus, errorThrown) {
			
		}
	});
	return;
}

