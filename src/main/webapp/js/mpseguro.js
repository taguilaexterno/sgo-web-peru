
$(document).ready(function() {
    $('#admMPSeguroForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            number: {
                validators: {
                    integer: {
                        message: 'El valor no es entero'
                    }
                }
            }
        }
    });
});

function updateValMenor(id,id_validacion) {
	var conError = $('#bootError').is(':empty');
	if(conError) {
		document.getElementById('metodo').value = 'UPDATEVALMENOR';
		document.getElementById('valor').value = document.getElementById('inptvalor').value;
		document.forms.admMPSeguroForm.submit();
	}
}

function updateEstado() {	
		document.getElementById('metodo').value = 'UPDATEESTADO';
		document.forms.admMPSeguroForm.submit();
}
	
function setEstado(estado,id,id_validacion,valor) {
			 document.getElementById('id').value = id;
			 document.getElementById('estado').value = estado;
			 document.getElementById('idValidacion').value = id_validacion;
			 document.getElementById('inptvalor').value = valor;
}

/* Grupo Reglas Medio de Pago */

function setEstadoGrupo(estado,id,id_validacion) {
	 document.getElementById('id').value = id;
	 document.getElementById('estado').value = estado;
}

function updateEstadoGrupo() {
	document.getElementById('metodo').value = 'UPDATEESTADO';
	document.forms.grupoMPSeguroForm.submit();
}