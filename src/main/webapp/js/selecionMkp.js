var util = new Util();
$(document).ready(function() {	    

    
	var montoActual=0;
	//recorremos todos los checkbox seleccionados con .each
	$('input[type="checkbox"]:checked').each(function() {
		
		var id = $(this).attr('id');
		var clase = $(this).attr('class');
		var indice = clase.replace("mycheckbox","");
		var monto =  $(this).parents("tr").find(".precio").html().replace("$","").replace(".","");
		var unidad =  $(this).parents("tr").find(".unidad").html();
//		console.log("id: "+id+" clase: "+clase+" indice: "+indice+" precio: "+monto+" unidad:"+unidad);
		montoActual = montoActual + (parseInt(monto) * parseInt(unidad));
	  	$('#montoTotalInvisible'+indice).val(montoActual);
	  	$('#montoTotalVisible'+indice).val(util.formatNumber(montoActual));
		
	});
    
});

function marcar(valor,unico,indice,monto, unidad){
	
	  if ($('#'+unico).is(':checked')){
//		  console.log('checked');
	  	    //$('.'+valor).prop('checked', true); //check a toda la suborden
		  	$('#'+unico).prop('checked', true); //check solo al producto
		  	montoActual = parseInt($('#montoTotalInvisible'+indice).val());
		  	montoActual = montoActual + (parseInt(monto) * parseInt(unidad));
		  	$('#montoTotalInvisible'+indice).val(montoActual);
		  	$('#montoTotalVisible'+indice).val(util.formatNumber(montoActual));
	  	    
		  	$('#montoTotalInvisible'+indice).attr("name","articulosConOrdenes["+indice+"].montoTotal");
	  	    $('#subOrdenInvisible'+indice).attr("name","articulosConOrdenes["+indice+"].subOrden");
	  	    $('#idRefundVisible'+indice).attr("required","");
	  } else {
//		  console.log('unchecked');
	  	  	//$('.'+valor).prop('checked', false);
		  	$('#'+unico).prop('checked', false); //check solo al producto
		  	montoActual = parseInt($('#montoTotalInvisible'+indice).val());
		  	montoActual = montoActual - (parseInt(monto) * parseInt(unidad));
		  	$('#montoTotalInvisible'+indice).val(montoActual);
		  	$('#montoTotalVisible'+indice).val(util.formatNumber(montoActual));

		  	//$('#montoTotalInvisible'+indice).attr("name","");
		  	//$('#subOrdenInvisible'+indice).attr("name","");
		  	$('#idRefundVisible'+indice).removeAttr("required");
	  }
	
}

function validaFormaPago(valor, tipo){
	
	if(valor=='DEBITO'){
		if (tipo=='fpNcTotal'){
			document.forms['ncTotalForm'].action = "validarAcreedor.action";

		}else{
			document.forms['ncParcialForm'].action = "validarAcreedor.action";

		}
	}
	
	
}