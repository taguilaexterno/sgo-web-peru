$(function() {
	$('#btnBuscar').on('click', function() {
		$('#tblDatos tbody tr').hide();
		var val = $('#txtBuscar').val();
		
		if(val == '') {
			$('#tblDatos tbody tr').show();
		} else {
			$('#tblDatos tbody tr').each(function(idx, obj) {
				$(obj).find('td:first').filter(':contains("' + val + '")').parent().show();
			});
		}
		
	});
});