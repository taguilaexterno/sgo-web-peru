function getModPorPerfHeader() {
	var tbody = $('#tbodyHabilidadesHeader');

	$.getJSON('getModulosPorPerfilJson',
			{codPerfil: $('#cmbEditPerfilHeader').val()},
			function(jsonResponse) {
				if(jsonResponse.error != null) {
					return false;
				}

				tbody.html('');

				$(jsonResponse.modulosPerfil).each(function(idx, obj){
					var tr = $('<tr>');
					var td = $('<td>').text(obj.nombreModulo);
					tr.append(td);
					td = $('<td>').text(obj.permiso.nombrePermiso);
					tr.append(td);
					tbody.append(tr);
				});

			});
}

function getUserForModalHeader(user) {
	if(user.trim() != "") {
		$.getJSON('buscarUsuarioJson',
				{usuarioUsr: user},
				function(jsonResponse) {
					if(jsonResponse.usuarioBean == null) {
						return false;
					}
					$('#usuarioBean_codUsuarioHeader').val(jsonResponse.usuarioBean.codUsuario);
					$('#usuarioBean_codEstadoHeader').val(jsonResponse.usuarioBean.codEstado);
					$('#txtEditNombreUsuarioHeader').val(jsonResponse.usuarioBean.nombre);
					$('#txtEditApePatHeader').val(jsonResponse.usuarioBean.apePaterno);
					$('#txtEditApeMatHeader').val(jsonResponse.usuarioBean.apeMaterno);
					$('#txtEditEmailHeader').val(jsonResponse.usuarioBean.email);
					$('#cmbEditPerfilHeader').val(jsonResponse.usuarioBean.perfil.id);
					$('#cmbEditPerfilHeader option').first().prop('disabled', true);

					getModPorPerfHeader();					
				});
	}
}

$(function(){

	$('#cmbEditPerfilHeader').on('change', function(){
		getModPorPerfHeader();
	});

});