
$(document).ready(function() {
	//formula actualizada
    $('#inptvalor').keyup(function() {
    	var text = '('+$('#inptvalor').val()+' * CP) > PP';
    	$('#factortxt').val(text);
    });
    
    //Seleccionar todos los Checkbox
    $("#selectall").click(function() {
  	  if ($(this).is(':checked')){
  	    $('.mycheckbox').prop('checked', true);
  	  } else {
  	  	$('.mycheckbox').prop('checked', false);
      }
	});
    
    //UploadFile
    $('#uploadForm').submit(function() {
    	$('#btncargar').attr('disabled', true);
    	$('#uploadFile').css({"visibility":"hidden"});
    	$('#dvLoading').css({"visibility":"visible"});
    });
    
    //Grabar SKU
    $('#btnGrabarSku').on('click', function() {
		document.getElementById('metodo').value = 'SAVESKUPROD';
		document.forms.productoSKUForm.submit();
    });
    
    //Eliminar SKU
    $('#btnEliminarSku').on('click', function(event) {
    	var seleccionados;	
    	
    	$('input[type=checkbox]').each(function () {
            if (this.checked && this.id != 'selectall') {
            	if (seleccionados == null) {
            		seleccionados=this.id;
            	} else{
            		seleccionados=seleccionados+','+this.id;
            	}
            }
    	});
    	
    	event.preventDefault();
		document.getElementById('metodo').value = 'DELSKUPROD';
		document.getElementById('seleccionados').value = seleccionados;
		document.forms.productoSKUForm.submit();
    });
    
    //Consulta SKU
	$('#btnConsultaSku').on('click', function() {
		$('#btnGrabarSku').attr("disabled", true);
		$('#mensajeErrorAjax').remove();

		if($('#inptvalor').val().trim() != "") {
			$.getJSON('consultaSkuJson',
			{skuProd: $('#inptvalor').val()},
			function(jsonResponse) {
				//Borrar resultado anterior
				$('#divConsulta').empty();
				
				if(jsonResponse.productoSku == 'undefined') {
					var divConsulta = $('<div id="mensajeErrorAjax" class="alert alert-danger">Error al consultar SKU</div>');
					$('#agregarDiv').append(divConsulta);
					return false;
				}
				
				$('#skuArticulo').val(jsonResponse.productoSku.codigoArticulo);
				
				var divConsulta = $('<label>Nombre</label>'+
				'<div class="input-group">'+
				'<input type="text" id="prodSkuNombre" name="productoSku.nombre" style="border-width:0px;" readonly="readonly" value="'+jsonResponse.productoSku.nombre+'"/>'+
				'</div>'+
				'<br/>'+
				'<label>Descripción Corta</label>'+
				'<div class="input-group">'+
				'<input type="text" id="prodSkuDescCorta" name="productoSku.descripcionCorta" style="border-width:0px;" readonly="readonly" value="'+jsonResponse.productoSku.descripcionCorta+'"/>'+
				'</div>');
				$('#divConsulta').append(divConsulta);
				
				$(jsonResponse.productoSku.precios).each(function(idx, obj){					
					var divConsulta = $('<br/><label>'+obj.descripcion+' : </label>  <p>' + obj.valor + '</p>');
					$('#divConsulta').append(divConsulta);
				});
				
				var divConsulta = $('<br/>'+
				'<label>Descripción</label>'+
				'<div>'+
				'	<textarea id="prodSkuDesc" name="productoSku.descripcion" style="border-width:0px;width: 100%;margin: 5px 0;padding: 3px;overflow:auto;resize:none" readonly="readonly">'+jsonResponse.productoSku.descripcion+'</textarea>'+	
				'</div>');
				
				$('#divConsulta').append(divConsulta);
//				$('#divConsulta').show();
				$('#btnGrabarSku').attr("disabled", false);
			}).error(function() {
				var divConsulta = $('<div id="mensajeErrorAjax" class="alert alert-danger">Error al consultar SKU</div>');
				$('#agregarDiv').append(divConsulta);
			});
		}
	});
});

function updateEstado() {
		document.getElementById('metodo').value = 'UPDATEESTADO';
		document.forms.reglaGeneracionDTEForm.submit();
}

function updateFactor() {
	var conError = $('#bootError').is(':empty');
	if(conError) {
	document.getElementById('metodo').value = 'UPDATEFACTOR';
		document.getElementById('rpvalor').value = document.getElementById('inptvalor').value;
		document.forms.reglaGeneracionDTEForm.submit();
	}
}

function cancelar() {
	document.getElementById('metodo').value = '';
	document.forms.reglaGeneracionDTEForm.submit();
}

function modificar() {
	document.forms['reglaGeneracionDTEForm'].action = "reglaDTESkuAction.action";
	document.forms['reglaGeneracionDTEForm'].submit();
}

