$(document).ready(function() {	    
    $("#selectall").click(function() {
  	  if ($(this).is(':checked')){
  	    $('.mycheckbox').prop('checked', true);
  	  } else {
  	  	$('.mycheckbox').prop('checked', false);
      }
	});    
});

//FILTROS
$(function() {
	//Buscar OC
	$('#btnBuscar').on('click', function() {
		$('#tblDatos tbody tr').hide();
		var val = $('#txtBuscar').val();
		
		if(val == '') {
			$('#tblDatos tbody tr').show();
		} else {
			$('#tblDatos tbody tr').each(function(idx, obj) {
				$(obj).find('td:nth-child(2)').filter(':contains("' + val + '")').parent().show();
			});
		}		
	});
});

function buscar(event) {
	event.preventDefault();
	if (document.forms['boletaForm']["tipoBuscar"].value==1) {
		if (document.forms['boletaForm']["oc"].value.length>0) {
		   buscarOC(event);
		} else {
			return false;
		}
	}
	if (document.forms['boletaForm']["tipoBuscar"].value==2) { 
		if (document.forms['boletaForm']["rut"].value.length>0) {
			buscarRUT(event);
		} else {
			return false;
		}
	}
}

function buscarOC(event) {
	event.preventDefault();
	document.forms['boletaForm'].action = "buscarOrdenCompra.action";
	document.forms['boletaForm']["metodo"].value='BUSCAROC';
	document.forms['boletaForm']["seleccionados"].value=document.forms['boletaForm']["oc"].value;
	document.forms['boletaForm'].submit();
}

function buscarRUT(event) {
	event.preventDefault();
	document.forms['boletaForm'].action = "buscarOrdenCompra.action";
	document.forms['boletaForm']["seleccionados"].value=document.forms['boletaForm']["rut"].value;
	document.forms['boletaForm']["metodo"].value='BUSCARRUT';
	document.forms['boletaForm'].submit();
}

function verDetalle(ordenCompra) {
	event.preventDefault();
	document.forms['boletaForm'].action = "buscarOrdenCompra.action";
	document.forms['boletaForm']["metodo"].value='DETALLE';
	document.forms['boletaForm']["seleccionados"].value=ordenCompra;
	document.forms['boletaForm'].submit();
}