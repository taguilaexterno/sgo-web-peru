$(function(){
		$('#btnConfirmarModal').on('click', function(){
			$('#confirmarParcial').submit();
		});
		
		$('#btnMostrarConfirmacion').on('click', function(){
			var listChk = $(this).parents('form:first').find('input:checkbox:checked');
			if(listChk.length == 0) {
				$('#confirmar').modal('show');
			} else {
				$('#confirmarParcial').submit();
			}
		});
	});