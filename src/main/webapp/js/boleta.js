$(document).ready(function() {	    
    $("#selectall").click(function() {
  	  if ($(this).is(':checked')){
  	    $('.mycheckbox').prop('checked', true);
  	  } else {
  	  	$('.mycheckbox').prop('checked', false);
      }
	});    
});

//FILTROS
$(function() {
	//Buscar OC
	$('#btnBuscar').on('click', function() {
		$('#tblDatos tbody tr').hide();
		var val = $('#txtBuscar').val();
		
		if(val == '') {
			$('#tblDatos tbody tr').show();
		} else {
			$('#tblDatos tbody tr').each(function(idx, obj) {
				$(obj).find('td:nth-child(3)').filter(':contains("' + val + '")').parent().show();
			});
		}		
	});
	//Filtro Boleta Factura
	$('#filtroSel').on('change', function() {
		$('#tblDatos tbody tr').hide();
		var val = $(this).val();
		
		if(val == '') {
			$('#tblDatos tbody tr').show();
		} else {
			$('#tblDatos tbody tr').each(function(idx, obj) {
				if($(obj).hasClass(val)) {
					$(obj).show();
				}
			});
		}	
	});
	
	
	//Filtro Tipo Despacho
	$('#filtroSelDes').on('change', function() {
		$('#tblDatos tbody tr').hide();
		var val = $(this).val();
		
		if(val == '') {
			$('#tblDatos tbody tr').show();
		} else {
			$('#tblDatos tbody tr').each(function(idx, obj) {
				var indice = $('#tblDatos th:contains("Tipo de despacho")').index();
				$(obj).find('td:nth-child(' + (indice + 1) + ')').filter(':contains("' + $('#filtroSelDes option[value=' + val + ']').text() +'")').parent().show();
			});
		}	
	});
	
});

function anular(event, estado) {
	var selecionados;	
	$('input[type=checkbox]').each(function () {
        if (this.checked && this.id != 'selectall') {
        	if (selecionados == null) {
        		selecionados=this.id;
        	} else{
        		selecionados=selecionados+','+this.id;
        	}
        }
	});
	
	event.preventDefault();
	document.forms['boletaForm'].action = "confirmacionBoleta.action?estado="+estado;
	document.forms['boletaForm']["metodo"].value='ANULAROC';
	document.forms['boletaForm']["seleccionados"].value=selecionados;
	document.forms['boletaForm'].submit();
}

function generar(event, estado) {
	var selecionados;	
	$('input[type=checkbox]').each(function () {
        if (this.checked && this.id != 'selectall') {
        	if (selecionados == null) {
        		selecionados=this.id;
        	} else{
        		selecionados=selecionados+','+this.id;
        	}
        }
	});
	
	event.preventDefault();
	document.forms['boletaForm'].action = "confirmacionBoleta.action?estado="+estado;
	document.forms['boletaForm']["metodo"].value='GENERAROC';
	document.forms['boletaForm']["seleccionados"].value=selecionados;
	document.forms['boletaForm']["cajaEstado"].value=document.getElementById('cajaEstado').value;;
	document.forms['boletaForm'].submit();
}

function refrescar(event, estado) {	
	event.preventDefault();
	document.forms['boletaForm'].action = "confirmacionBoleta.action?estado="+estado;
	document.forms['boletaForm']["metodo"].value='';
	document.forms['boletaForm'].submit();
}

function verDetalle(ordenCompra, estado) {
	event.preventDefault();
	document.forms['boletaForm'].action = "confirmacionBoleta.action?estado="+estado;
	document.forms['boletaForm']["metodo"].value='DETALLE';
	document.forms['boletaForm']["seleccionados"].value=ordenCompra;
	document.forms['boletaForm'].submit();
}