function getModPorPerf() {
		var tbody = $('#tbodyHabilidades');
		
		$.getJSON('getModulosPorPerfilJson',
				{codPerfil: $('#cmbEditPerfil').val()},
				function(jsonResponse) {
					if(jsonResponse.error != null) {
						return false;
					}
					
					tbody.html('');
					
					$(jsonResponse.modulosPerfil).each(function(idx, obj){
						var tr = $('<tr>');
						var td = $('<td>').text(obj.nombreModulo);
						tr.append(td);
						td = $('<td>').text(obj.permiso.nombrePermiso);
						tr.append(td);
						tbody.append(tr);
					});
					
				});
	}
	
	$(function(){
		
		$('#cmbEditPerfil').on('change', function(){
			getModPorPerf();
		});
	
		$('#buscarUsuario').on('click', function() {
			if($('#txtUsuarioBuscar').val().trim() != "") {
				$.getJSON('buscarUsuarioJson',
				{usuarioUsr: $('#txtUsuarioBuscar').val()},
				function(jsonResponse) {
					if(jsonResponse.usuarioBean == 'undefined') {
						return false;
					}
					$('#usuarioBean_codUsuario').val(jsonResponse.usuarioBean.codUsuario);
					$('#usuarioBean_codEstado').val(jsonResponse.usuarioBean.codEstado);
					$('#txtEditNombreUsuario').val(jsonResponse.usuarioBean.nombre);
					$('#txtEditApePat').val(jsonResponse.usuarioBean.apePaterno);
					$('#txtEditApeMat').val(jsonResponse.usuarioBean.apeMaterno);
					$('#txtEditEmail').val(jsonResponse.usuarioBean.email);
					$('#cmbEditPerfil').val(jsonResponse.usuarioBean.perfil.id);
					$('#cmbEditPerfil option').first().prop('disabled', true);
					
					getModPorPerf();					
				});
			}
		});
	});