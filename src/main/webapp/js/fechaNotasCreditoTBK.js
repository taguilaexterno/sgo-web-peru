 $( document ).ready(function() {
	

	 
		
	$('#fechaDesde').datepicker({
	    format: 'dd/mm/yyyy',
	    weekStart: 1,
	    autoclose: true,
	    startDate:  '-2y',
	    endDate: '+2y',
	    language: 'es'
	   
	});
	
//	var currentDateFrom = new Date();
//	currentDateFrom=sumarDias(currentDateFrom, -30);
//	var dayFrom = ((currentDateFrom.getDate()).toString().length == 1) ? '0' + (currentDateFrom.getDate()).toString() : (currentDateFrom.getDate()).toString();
//	var monthFrom = ((currentDateFrom.getMonth() + 1).toString().length == 1) ? '0' + (currentDateFrom.getMonth() + 1).toString() : (currentDateFrom.getMonth() + 1).toString();
//	var yearFrom = currentDateFrom.getFullYear();
//	$("#fechaDesde").val(dayFrom + '/' + monthFrom + '/' + yearFrom);
//	
//	$("#fechaDesde").datepicker("update");
	
	
	$('#fechaHasta').datepicker({
	    format: 'dd/mm/yyyy',
	    weekStart: 1,
	    autoclose: true,
	    startDate: '-2y',
	    endDate: '+2y',
	    language: 'es'
	   
	});
	
	
	
	//Filtro Tipo Despacho
	$('#filtroSelDesTbk').on('change', function() {
		$('#tableTbk tbody tr').hide();
		var val = $(this).val();
		
		if(val == '') {
			$('#tableTbk tbody tr').show();
		} else {
			$('#tableTbk tbody tr').each(function(idx, obj) {
				var indice = $('#tableTbk th:contains("Descripcion Error")').index();
				
				$(obj).find('td:nth-child(' + (indice + 1) + ')').filter(':contains("' + $('#filtroSelDesTbk option[value=' + val + ']').text() +'")').parent().show();
			});
		}	
	});
//	var currentDate = new Date();
//	var day = ((currentDate.getDate()).toString().length == 1) ? '0' + (currentDate.getDate()).toString() : (currentDate.getDate()).toString();
//	var month = ((currentDate.getMonth() + 1).toString().length == 1) ? '0' + (currentDate.getMonth() + 1).toString() : (currentDate.getMonth() + 1).toString();
//	var year = currentDate.getFullYear();
//	$("#fechaHasta").val(day + '/' + month + '/' + year);
//		
//	$("#fechaHasta").datepicker("update");
	
	
});

 
 /* Función que suma o resta días a una fecha, si el parámetro
 días es negativo restará los días*/
function sumarDias(fecha, dias){
	fecha.setDate(fecha.getDate() + dias);
	return fecha;
}


function getFechaDesde(){
	
	var currentDateFrom = new Date();
	currentDateFrom=sumarDias(currentDateFrom, -30);
	var dayFrom = ((currentDateFrom.getDate()).toString().length == 1) ? '0' + (currentDateFrom.getDate()).toString() : (currentDateFrom.getDate()).toString();
	var monthFrom = ((currentDateFrom.getMonth() + 1).toString().length == 1) ? '0' + (currentDateFrom.getMonth() + 1).toString() : (currentDateFrom.getMonth() + 1).toString();
	var yearFrom = currentDateFrom.getFullYear();
	
	var fecha=dayFrom + '/' + monthFrom + '/' + yearFrom

	alert(fecha);
	return fecha;
}

function getFechaHasta(){
	
	var currentDate = new Date();
	var day = ((currentDate.getDate()).toString().length == 1) ? '0' + (currentDate.getDate()).toString() : (currentDate.getDate()).toString();
	var month = ((currentDate.getMonth() + 1).toString().length == 1) ? '0' + (currentDate.getMonth() + 1).toString() : (currentDate.getMonth() + 1).toString();
	var year = currentDate.getFullYear();
	
	var fecha=day + '/' + month + '/' + year

	return fecha;
}


function warningFechas(){
	
	document.getElementById("alertaFecha").style.display='none';
	document.getElementById("alertaFechaUnMes").style.display='none';
	
	var fecDesde = document.getElementById("fechaDesde").value;
	var fecHasta = document.getElementById("fechaHasta").value;              
	//alert("Entro al alert previa ida al action: " + fecDesde + "  xxx " + fecHasta);                       
	var x = fecDesde.split("/");
	var z = fecHasta.split("/");     
	        
	if(x[2]>z[2]){
	     //alert("El año desde es mayor que el hasta");
	     document.getElementById("alertaFecha").style.display='block';
	     return false;
	}
	else{
	     if(x[1]>z[1]){
	       //alert("El mes desde es mayor que el mes hasta");
	       document.getElementById("alertaFecha").style.display='block';
	       return false;                                                                                                    
	     }
	     else{
	         if(x[1] == z[1]){
	             if(x[0]>z[0]){
	                   //alert("El dia desde es mayor que el dia hasta");
	                   document.getElementById("alertaFecha").style.display='block';
	                  return false;
	               }  
	         }
	     }                 
	 }  


     //Valida que el rango de Fechas sea dentro de un mes a partir de la fecha desde.
    
	var currentDate = new Date(parseInt(x[2]),parseInt(x[1]-1),parseInt(x[0]));
	currentDate=sumarDias(currentDate, +30);
	var day = ((currentDate.getDate()).toString().length == 1) ? '0' + (currentDate.getDate()).toString() : (currentDate.getDate()).toString();
	var month = ((currentDate.getMonth() + 1).toString().length == 1) ? '0' + (currentDate.getMonth() + 1).toString() : (currentDate.getMonth() + 1).toString();
	var year = currentDate.getFullYear();
	var fechaHastaPorDefecto = day + '/' + month + '/' + year;
	var fechaHastaPorDefectoSplit=fechaHastaPorDefecto.split("/");
	var fechaHastaPorDefectoDate=new Date(parseInt(fechaHastaPorDefectoSplit[2]),parseInt(fechaHastaPorDefectoSplit[1]-1),parseInt(fechaHastaPorDefectoSplit[0]));
	
    //alert(fechaDesdePorDefectoDate+"|"+fechaHastaPorDefectoDate);
	
	var fechaDesdeDate=new Date(parseInt(x[2]),parseInt(x[1]-1),parseInt(x[0]));
	var fechaHastaDate=new Date(parseInt(z[2]),parseInt(z[1]-1),parseInt(z[0]));
	
	
	if(fechaHastaDate>fechaHastaPorDefectoDate){
		document.getElementById("alertaFechaUnMes").style.display='block';
        return false;
	}
	
       document.formFechas.submit();                                           
    }