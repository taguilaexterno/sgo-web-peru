function getPerfilForModal(idPerfil) {

	$.getJSON('getPerfilByIdJson',
			{codPerfil: idPerfil},
			function(jsonResponse) {
				if(jsonResponse.perfilBean == null) {
					return false;
				}
				$('#perfilBean_codPerfil').val(jsonResponse.perfilBean.id);
				$('#txtEditNombrePerfil').val(jsonResponse.perfilBean.nombrePerfil);

				$(jsonResponse.perfilBean.modulos).each(function(idx, obj) {

					$('#perfilBean_modulos_' + obj.id).val(obj.permiso.id);

				});

			});
}
