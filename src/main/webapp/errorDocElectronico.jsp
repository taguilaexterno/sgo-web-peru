<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="icon" href="//home.ripley.cl/front/public/favicon.ico" type="image/x-icon">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-9XS5');</script>
	<!-- End Google Tag Manager -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <style media="screen">
	html * {
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}

	*, *:after, *:before {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}

	body {
		font-size: 14px;
		font-family: "Montserrat", sans-serif;
		color: #67666a;
		background-color: white;
	}

	.error{
		display: table;
		margin:30px auto 10px;
		text-align: center;
		max-width:700px;
	}
	.error img{
		max-width:225px;
		height:auto;
		display: block;
		margin:0 auto 30px;
	}
	.error h2{
		color: #e75252;
		font-size: 32px;
		font-weight: 700;
		line-height: 26pt;
		margin: 0;
	}
	.error h3{
		font-size: 24px;
		font-weight: 300;
		margin: 15px
	}
	.error p{
		line-height: 18pt;
		margin:0 0 10px;
	}
	.error .actions{
		overflow: hidden;
		display: table;
		margin:0 auto;
		padding:30px 0;
	}
	.btn-error-primary{
		height:40px;
		display: inline-block;
		padding:0 30px;
		margin:0 10px;
		font-size: 14px;
		text-decoration: none;
		line-height: 40px;
		background: #70578b;
		color:#fff;
		overflow: hidden;
		cursor:pointer;
		border-radius:6px;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
	}
	.btn-error-primary .icon > img{
		display: block;
		width:20px;
		height:20px;
		max-width: 20px;
		margin:0;
	}
	.btn-error-primary > .icon{
		display: inline-block;
		margin: 0 10px 0 0;
		vertical-align: middle;
	}
	.btn-error-primary:hover{
		background: #60467c;
	}
	.btn-error-secondary{
		height:40px;
		display: inline-block;
		padding:0 30px;
		font-size: 14px;
		margin:0 10px;
		text-decoration: none;
		line-height: 40px;
		background: #fff;
		border:solid 1px #777;
		color:#777;
		overflow: hidden;
		border-radius:6px;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
	}
	.btn-error-secondary:hover{
		border:solid 1px #70578b;
	}
	.fatal-error{
		max-width:600px;
	}
	.fatal-error h1{
		background: transparent url(../img/ripley-logo.svg) 0 0 no-repeat;
		background-size:100%;
		display: block;
		margin:0 auto 50px;
		width:200px;
		height:62px;
		overflow:hidden;
		text-indent:-9999px;
	}
	.fatal-error p{
		font-size: 18px;
		line-height: 20pt;
	}
	.fatal-error p.phone{
		font-size: 14px;
		color:#777;
		line-height: 16pt;
		margin:50px 0 0;
	}
	.error.fatal-error p.legal{
		font-size: 10px;
		display: block;
		line-height: 11pt;
	}
	@media only screen and (max-width: 500px){
		.btn-error-primary, .btn-error-secondary{
			width: 100%;
		}
		.btn-error-primary{
			margin:0 0 15px;
		}
		.btn-error-secondary{
			margin:0;
		}
	}
  </style>
	<title>Documento Tributario no encontrado. Ripley.com</title>
</head>
<body>
	<div class="error">
		<img src="https://home.ripley.cl/assets-email/img/ripley-logo.png" width="150" style="display: block; border:0px; margin:0 auto 50px"/>
		<img src="https://home.ripley.cl/error/images/error.jpg" alt="Error 404" />
		<h2>Ups! Tenemos un problema.</h2>
		<h3>El contenido de esta p�gina no se encuentra disponible.</h3>
		<p>En caso que tengas dudas te invitamos a contactar a nuestro Servicio al Cliente.</p>
		<div class="actions">
			<a href="http://simple.ripley.cl/minisitios/estatico/servicio_cliente/" class="btn-error-primary">Servicio al Cliente</a>
			<a href="https://simple.ripley.cl/" class="btn-error-secondary">Ir a Ripley.com</a>
		</div>
	</div>
</body>
</html>