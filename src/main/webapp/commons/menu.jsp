<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
				<aside id="main-sidebar" class="col-md-3">
					<button class="btn btn-primary btn-block visible-xs" data-toggle="collapse" href="#main-sidebar-list" aria-expanded="false" aria-controls="main-sidebar-list"><span class="fa fa-bars"></span> Men&uacute;</button>
					<ul id="main-sidebar-list" class="collapse">
						<s:iterator value="#session.usuario.perfil.modulos" var="modulo">
							<s:if test="#modulo.permiso.id != 3">
								<s:if test="#modulo.activo == 0">
									<s:url value="/" var="contexto"/>
									<s:if test="#modulo.url.contains('javascript')">
										<li><a href='<s:property value="#modulo.url"/>'><s:property value="#modulo.nombreModulo"/></a></li>
									</s:if>
									<s:else>
										<li><a href='<s:property value="#contexto + #modulo.url"/>'><s:property value="#modulo.nombreModulo"/></a></li>
									</s:else>
								</s:if>
								<s:else>
									<s:url value="/" var="contexto"/>
									<s:if test="#modulo.url.contains('javascript')">
										<li><a style="background: #574d95;color: #fff;" href='<s:property value="#modulo.url"/>'><s:property value="#modulo.nombreModulo"/></a></li>
									</s:if>
									<s:else>
										<li><a style="background: #574d95;color: #fff;" href='<s:property value="#contexto + #modulo.url"/>'><s:property value="#modulo.nombreModulo"/></a></li>
									</s:else>
								</s:else>
							</s:if>
				        </s:iterator>
					</ul>
				</aside>
