<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="cl.ripley.omnicanalidad.util.Constantes" %>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>SGO - Sistema de Gestión de Ordenes... Versi&oacute;n Corporativo <%=Constantes.VERSION_APP%></title>
		<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8" >
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="css/bootstrap-datepicker.css" type="text/css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
		<link rel="stylesheet" href="css/style.css" type="text/css"/>

		<script type="text/javascript" src="js/jquery.min.js "></script>
		<script type="text/javascript" src="js/moment.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/validator.min.js"></script>
		<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src='<s:url value="/js/configHeader.js"/>'></script>

		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!--[if lte IE 8]>
  			<link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<!--[if IE 9]>
		  <link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<link rel="icon" href="img/favicon.ico">

		<!--[if lt IE 9]>
    		<script type="text/javascript" src="js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
				    <h1><a href='<s:url value="/"/>'>SGO</a></h1>
				    <h2>Sistema de Gesti&oacute;n de Ordenes</h2> <br><small>Versi&oacute;n Corporativo <%=Constantes.VERSION_APP%></small>
				</div>
				<ul class="nav navbar-nav navbar-right">
			        <li class="dropdown">
			        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><s:property value="#session.usuario.nombre + ' ' + #session.usuario.apePaterno"/> <span class="caret"></span></a>
			        	<ul class="dropdown-menu">
			            	<li><a href="#" onclick="javascript: getUserForModalHeader('${session.usuario.usuario}')" data-toggle="modal" data-target="#edit-user-menu">Configuraci&oacute;n</a></li>
			            	<li role="separator" class="divider"></li>
			            	<li><a href='<s:url value="/logout" />'>Salir</a></li>
			          	</ul>
			        </li>
			    </ul>
			</div>
		</nav>
		
		<div id="edit-user-menu" class="modal fade">
			<div class="modal-dialog modal-lg">
				<s:form action="actualizarUsuario" theme="simple" role="form" data-toggle="validator">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Editar Usuario</h4>
					</div>
					<input type="hidden" id="usuarioBean_codUsuarioHeader" name="usuarioBean.codUsuario"/>
					<input type="hidden" id="usuarioBean_codEstadoHeader" name="usuarioBean.codEstado"/>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre</label>
									<s:textfield id="txtEditNombreUsuarioHeader" name="usuarioBean.nombre" cssClass="form-control" placeholder="Nombre"
										data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required=""/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Apellido Paterno</label>
									<s:textfield id="txtEditApePatHeader" name="usuarioBean.apePaterno" cssClass="form-control" placeholder="Apellido Paterno"
										data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required=""/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Apellido Materno</label>
									<s:textfield id="txtEditApeMatHeader" name="usuarioBean.apeMaterno" cssClass="form-control" placeholder="Apellido Materno"
										data-error="Debe llenar este campo" data-minlength="2" maxlength="30" required=""/>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Email</label>
									<input type="email" id="txtEditEmailHeader" name="usuarioBean.email" class="form-control" placeholder="email@email.com" 
											data-error="Email no válido" data-minlength="2" maxlength="50" required/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Perfil</label>
									<select id="cmbEditPerfilHeader" class="form-control" name="unPerfil" data-error="Debe seleccionar un perfil" required="" disabled>
										<option value="">Seleccione</option>
										<s:iterator value="#session.perfilesSession" var="perfil">
											<option value='${perfil.id}'/>${perfil.nombrePerfil}</option>
										</s:iterator>
									</select>
								<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Contrase&ntilde;a</label>
									<s:password id="txtEditPasswordHeader" name="usuarioBean.password" cssClass="form-control" placeholder="password" maxlength="50"/>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label>Repetir contrase&ntilde;a</label>
									<s:password id="txtEditPasswordValidHeader" name="usuarioBean.passwordValid" cssClass="form-control" placeholder="password" maxlength="50"/>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<hr>
						<h4>Habilidades del usuario</h4>
						<div class="responsive-table">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Tarea</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody id="tbodyHabilidadesHeader">
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Guardar cambios</button>
					</div>
				</div>
				</s:form>
			</div>
		</div>
