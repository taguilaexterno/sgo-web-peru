<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/WEB-INF/struts-tags.tld" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es">
	<head>
		<title>SGO - Sistema de Gestión de Ordenes</title>
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8" >
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
		<link rel="stylesheet" href="css/style.css" type="text/css"/>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js "></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!--[if lte IE 8]>
  			<link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<!--[if IE 9]>
		  <link rel="shortcut icon" href="img/favicon.ico">
		<![endif]-->
		<link rel="icon" href="img/favicon.ico">

		<!--[if lt IE 9]>
    		<script type="text/javascript" src="js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
				    	<h1><a href="/">SGO</a></h1>
				    	<h2>Sistema de Gesti&oacute;n de Ordenes</h2>
				    </div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class=""form-group">
					<div id="login">
		
						<div class="form-group">
							<h1 class="region size2of3">ERROR <s:property value="error.codError"/>.</h1>
						</div>
						<div class="form-group">
							<h1 class="region size2of3">Algo ha salido mal.</h1>
						</div>
		
						<div class="fields">
							<div class="region size2of3 suffix1of3">
								<h2>La causa fue:</h2>
								<ul>
									<li>${error.errorMsg}</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>