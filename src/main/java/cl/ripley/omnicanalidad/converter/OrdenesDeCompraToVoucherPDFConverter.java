package cl.ripley.omnicanalidad.converter;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.ripley.omnicanalidad.bean.ArticuloDetalleVoucherPDF;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.VoucherPDF;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.NumerosAPalabras;
import cl.ripley.omnicanalidad.util.Util;

@Component
public class OrdenesDeCompraToVoucherPDFConverter implements Converter<OrdenesDeCompra, VoucherPDF> {

	@Autowired
	@Lazy
	private ParametroDAO parametroDAO;
	
	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(Constantes.FORMATO_FECHA_DD_MM_YYYY);
	private static final DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern(Constantes.FORMATO_FECHA_YYYY_MM_DD);
	
	@Override
	public VoucherPDF convert(OrdenesDeCompra in) {
		
		VoucherPDF out = new VoucherPDF();
		out.setBarCode(Util.crearCodigoBarra(in));
		out.setBoleta(in.getNotaVenta().getFolioSii());
		out.setCaja(Long.valueOf(in.getNotaVenta().getNumeroCaja()));
		
		out.setDireccionRipley(parametroDAO.getParametro(Constantes.DIRECCION_EMISOR).getValor());
		out.setEmailCliente(in.getDespacho().getEmailCliente());
		out.setFechaHora(in.getNotaVenta().getFechaBoleta());
		
		String ruc = Constantes.VACIO;
		String razonSocialRecep = Constantes.VACIO;
		
		if(in.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA) {
			
			//ruc receptor
			//pos 17
			ruc = in.getDatoFacturacion().getRutFactura();
			
			//razon social receptor
			//pos 19
			razonSocialRecep = in.getDatoFacturacion().getRazonSocialFactura();
			
		} else {
			
			//ruc receptor
			//pos 17
			ruc = String.valueOf(in.getDespacho().getRutCliente());
			
			//razon social receptor
			//pos 19
			razonSocialRecep = in.getDespacho().getNombreDespacho();
			
		}
		
		out.setNombreCliente(razonSocialRecep);
		out.setDniCliente(ruc);
		out.setRipleyRazonSocial(parametroDAO.getParametro(Constantes.RAZON_SOCIAL_EMISOR).getValor());
		out.setRipleyRuc(parametroDAO.getParametro(Constantes.RUT_EMISOR).getValor());
		out.setSerieImpresora(parametroDAO.getParametro(Constantes.TERMINAL).getValor());
		out.setSucursal(Long.valueOf(in.getNotaVenta().getNumeroSucursal()));
		out.setTelefonoCliente(in.getDespacho().getTelefonoCliente());
		out.setTrx(in.getNotaVenta().getNumeroBoleta());
		
		BigDecimal subtotal = Util.getSubtotal(in, Constantes.NRO_CERO);
		
		out.setTotalPalabras(NumerosAPalabras.convertir(subtotal));
		
		List<ArticuloDetalleVoucherPDF> arts =	in.getArticulosVenta().stream()
												.filter(it -> it.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.NRO_CERO)
												.collect(ArrayList::new, 
														(lista, art) -> {
															ArticuloDetalleVoucherPDF obj = new ArticuloDetalleVoucherPDF();
															obj.setCantidad(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, BigDecimal.valueOf(art.getArticuloVenta().getUnidades())));
															obj.setCodigo(art.getArticuloVenta().getCodArticulo());
															obj.setDescripcion(Constantes.COSTO_DE_ENVIO.equalsIgnoreCase(art.getArticuloVenta().getDescRipley())? art.getArticuloVenta().getDescRipley() : art.getArticuloVenta().getDescRipley() + (art.getArticuloVenta().getCodDespacho()!= null ? "\n" + art.getArticuloVenta().getCodDespacho(): Constantes.VACIO));
															obj.setPrecio(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, art.getArticuloVenta().getPrecio()));
															obj.setTotal(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, BigDecimal.valueOf(art.getArticuloVenta().getUnidades())
																																					.multiply(art.getArticuloVenta().getPrecio())
																																					.subtract(art.getArticuloVenta().getMontoDescuento())));
															lista.add(obj);
														},
														List::addAll);
		
		out.setArticulos(arts);
		
		out.setSubtotal(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, subtotal));
		out.setDescuentoTotal(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, BigDecimal.ZERO));
		
		Integer totalUnidades =	in.getArticulosVenta().stream()
								.map(art -> art.getArticuloVenta().getUnidades())
								.reduce(Constantes.NRO_CERO, Integer::sum);
		
		out.setNumeroUnidades(String.valueOf(totalUnidades));
		out.setOpExonerada(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, BigDecimal.ZERO));
		out.setOpInafecta(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, BigDecimal.ZERO));
		
		BigDecimal iva =  new BigDecimal(parametroDAO.getParametro(Constantes.VALORIVA).getValor());
		BigDecimal tasa_iva = iva.divide(new BigDecimal(Constantes.NRO_CIEN));

		BigDecimal igv = subtotal.subtract(subtotal.divide(tasa_iva.add(BigDecimal.ONE), Constantes.NRO_DOS, BigDecimal.ROUND_HALF_UP));
		BigDecimal montoNetoInt = subtotal.subtract(igv);
		
		out.setOpGravada(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoNetoInt));
		out.setIgv(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, igv));
		out.setTotalPagar(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, subtotal));
		
		String[] boletaSplit = in.getNotaVenta().getFolioSii().split(Constantes.GUION);
		
		StringBuilder qrCode = new StringBuilder();
		qrCode
		.append(out.getRipleyRuc()).append(Constantes.PIPE)
		.append(String.format(Constantes.FORMATO_2_DIGITOS, in.getTipoDoc().getCodPpl())).append(Constantes.PIPE)
		.append(boletaSplit[Constantes.NRO_CERO]).append(Constantes.PIPE)
		.append(boletaSplit[Constantes.NRO_UNO]).append(Constantes.PIPE)
		.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, igv)).append(Constantes.PIPE)
		.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, subtotal)).append(Constantes.PIPE)
		.append(dtf2.format(in.getNotaVenta().getFechaBoleta().toLocalDateTime())).append(Constantes.PIPE)
		.append(in.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_BOLETA.intValue() ? Constantes.NRO_UNO : Constantes.NRO_SEIS).append(Constantes.PIPE)
		.append(in.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_BOLETA.intValue() ? String.valueOf(in.getDespacho().getRutCliente()) : in.getDatoFacturacion().getRutFactura()).append(Constantes.PIPE);
		
		
		out.setCodigoQr(qrCode.toString());
		
		out.setGlosas(new ArrayList<>(Constantes.NRO_CINCUENTA));
		
		List<String> glosas = out.getGlosas();
		
		
		//Seteo de medios de pago
		
		String formaPago = Util.getFormaPago(in);
		
		if(Constantes.FORMA_PAGO_TIENDA.equals(formaPago)) {
			
			formaPago = Constantes.FORMA_PAGO_EFECTIVO;
			
			switch(String.valueOf(in.getTarjetaBancaria().getGlosa())) {
			
			case Constantes.MP_GLOSA_RIPLEY:
				formaPago = Constantes.FORMA_PAGO_TARJETA_RIPLEY;
				break;
				
			case Constantes.MP_GLOSA_BANCARIA:
				formaPago = Constantes.FORMA_PAGO_TDC;
				break;
				
			case Constantes.MP_GLOSA_EFECTIVO:
				formaPago = Constantes.FORMA_PAGO_EFECTIVO;
				break;
				
			default:
				break;
			
			}
			
		}
		
		boolean isTB = Boolean.FALSE;
		boolean isTR = Boolean.FALSE;
		boolean isEfectivo = Boolean.FALSE;
		boolean isPaypal = Boolean.FALSE;
		boolean pagoEntrega = Boolean.FALSE;
		boolean isSafety = Boolean.FALSE;
		
//		switch(type) {
//		case MEDIO_PAGO:
			
		String panDoc = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
						&& !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago) ? 
								Util.getPan(in) : Constantes.VACIO;
		String codigoAutorizadorDoc = Constantes.VACIO;
		String montoDoc = Constantes.VACIO;
		
		String fechaPrimerVtoDoc = Constantes.VACIO;
		
		if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
			
			isTR = Boolean.TRUE;
			fechaPrimerVtoDoc = String.format("%1$td/%1$tm/%1$tY", new Date(in.getTarjetaRipley().getFechaPrimerVencto().getTime()));
			codigoAutorizadorDoc = String.valueOf(in.getTarjetaRipley().getCodigoAutorizacion());
			
		} else if(in.getTarjetaBancaria().getCorrelativoVenta() != null 
				&& in.getTarjetaBancaria().getCorrelativoVenta().longValue() != Constantes.NRO_CERO) {
			
			montoDoc = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, in.getTarjetaBancaria().getMontoTarjeta());
			
			codigoAutorizadorDoc = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
									&& !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago) ? 
											in.getTarjetaBancaria().getCodigoAutorizador() : Constantes.VACIO;
			
			if(Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)) {
				//efectivo
				isEfectivo = Boolean.TRUE;
				
			} else if(Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
				//paypal
				isPaypal = Boolean.TRUE;
				
			} else if(Constantes.FORMA_PAGO_DEBITO.equals(formaPago) || Constantes.FORMA_PAGO_TDC.equals(formaPago)) {
				// tarjeta bancaria
				isTB = Boolean.TRUE;
				
			}  else {
				
				pagoEntrega = Boolean.TRUE;
				fechaPrimerVtoDoc = Constantes.VACIO;
				
			}
			
			
		}
		
		String glosaMedioPago = "MEDIO DE PAGO: ";
		
		if(Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago) 
				&& Constantes.CANAL_SAFETYPAY.equals(in.getTarjetaBancaria().getDescripcionCanal())) {
			
			glosaMedioPago += Constantes.CANAL_SAFETYPAY;
			isSafety = Boolean.TRUE;
			
		} else {
			
			glosaMedioPago += formaPago;
			
		}
		
		glosas.add(glosaMedioPago);
		
		if(isTR) {
			
			glosas.add("N° TARJETA : " + panDoc);
			
			glosas.add("MONTO : " + montoDoc);
			
			glosas.add("COD.AUTORIZACION : " + codigoAutorizadorDoc);
			
			glosas.add("PRIMER VENCIMIENTO : " + fechaPrimerVtoDoc);
			
			glosas.add("VALOR CUOTA S/ : " + String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, in.getTarjetaRipley().getValorCuota() != null ? in.getTarjetaRipley().getValorCuota() : Constantes.VACIO));
			
			glosas.add("CUOTAS : " + in.getTarjetaRipley().getPlazo());
			
		} else if(isEfectivo || isTB) {
			
			if(isSafety) {
				
				glosas.add("CODIGO PAGO: " + codigoAutorizadorDoc);
				
			} else {
				
				String codAux = isEfectivo ? "CIP : *********" + codigoAutorizadorDoc : "N° TARJETA : " + panDoc;
				
				glosas.add(codAux);
				
			}
			
			glosas.add("MONTO : " + montoDoc);
			
			glosas.add("COD.AUTORIZACION : " + (!isSafety ? codigoAutorizadorDoc : Constantes.VACIO));
			
			glosas.add("VALOR CUOTA S/ : " + (!Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago) 
							&& !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago) ? 
							montoDoc : Constantes.VACIO));
			
		} else if(isPaypal) {
			
			glosas.add("ID PAYPAL : " + codigoAutorizadorDoc);
			
			glosas.add("MONTO : " + montoDoc);
			
			glosas.add("COD.AUTORIZACION : " + codigoAutorizadorDoc);
			
			glosas.add("VALOR CUOTA S/ : " + montoDoc);
			
			glosas.add("TIPO CAMBIO S/ : ");
			
		} else if(pagoEntrega) {
			
			glosas.add("MONTO : " + montoDoc);
			
			glosas.add("COD.AUTORIZACION : " + codigoAutorizadorDoc);
			
			glosas.add("VALOR CUOTA S/ : ");
			
		}
		
		Optional<Long> optIdentTrx = Optional.ofNullable(in.getTarjetaBancaria().getIdentificadorTransaccion());
		
		if(optIdentTrx.isPresent() && optIdentTrx.get().longValue() != Constantes.NRO_CERO) {
			
			glosas.add("ID DE TRANSACCION : " + optIdentTrx.get());
			glosas.add("_________________________________________");
			
		}
		
		glosas.add("DOCUMENTOS REFERENCIADOS");
		
		glosas.add("_________________________________________");
		
		glosas.add("TIPO DOCUMENTO: " + (in.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? Constantes.FACTURA : Constantes.BOLETA));
		
		glosas.add("ORDEN DE COMPRA: " + in.getNotaVenta().getCorrelativoVenta());
		
		glosas.add("FECHA: " + dtf.format(in.getNotaVenta().getFechaBoleta().toLocalDateTime()));
		
		glosas.add("DIRECCION DESPACHO: ");
		
		glosas.add("Dirección: " + in.getDespacho().getDireccionDespacho());
		
		glosas.add("DISTRITO: " + in.getDespacho().getComunaDespachoDes());
		
		glosas.add("PROVINCIA: " + in.getDespacho().getRegionDespachoDes());
		
		glosas.add("DEPARTAMENTO: " + in.getDespacho().getRegionDespachoDes());
		
		glosas.add(Constantes.VACIO);
		
		glosas.add("RECIBI CONFORME MERCADERIA Y/O SERVICIOS");
		
		glosas.add("- POR CUENTA DE TIENDAS POR DEPARTAMENTO RIPLEY S.A");
		
//			break;
//			
//		case PUNTOS:
		
		
		BigDecimal porcentajePuntos = new BigDecimal(parametroDAO.getParametro("PORCENTAJE_PUNTOS").getValor()).divide(new BigDecimal(Constantes.NRO_CIEN));
		
		BigDecimal montoCompra = Util.getSubtotal(in, Constantes.ES_MKP_RIPLEY);
		
		BigDecimal totalPuntos = montoCompra.multiply(porcentajePuntos);
		
		if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
			
			glosas.add("UD. GANO " + String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, totalPuntos) + " PUNTOS RIPLEY ");
			
			glosas.add("COMPRANDO CON SU TARJETA RIPLEY");
			
		} else if(in.getTarjetaBancaria().getCorrelativoVenta() != null 
				&& in.getTarjetaBancaria().getCorrelativoVenta().longValue() != Constantes.NRO_CERO) {
			
			glosas.add("UD. HUBIESE GANADO " + String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, totalPuntos) + " PUNTOS ");
			
			glosas.add("COMPRANDO CON SU TARJETA RIPLEY");
			
		}
		
		glosas.add(Constantes.VACIO);
		
		glosas.add("==========================================");
		
		glosas.add("Condiciones Cambio y Devolución");
		
		glosas.add("1. Producto nuevo y sin señales de uso.");
		
		glosas.add("2. Empaques completos y en buen estado.");
		
		glosas.add("3. Plazo de 7 días desde la entrega.");
		
		glosas.add("4. Presenta tu comprobante de compra y DNI.");
		
		glosas.add("Conoce otras condiciones en www.ripley.com.pe");
		
		glosas.add("==========================================");
		
		
		return out;
	}

}
