package cl.ripley.omnicanalidad.util;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

public class Password {
	private static final AriLog logger = new AriLog(Password.class,"RIP16-004-SGO", PlataformaType.JAVA);
	
	/* llave para encriptar datos */
	private final String secretKey = "RipleyAligare";
	
	public String encryptPassword(String pass) throws Exception{
		if(pass == null) pass = "";
        String base64EncryptedString = "";
 
        MessageDigest md = MessageDigest.getInstance(Constantes.ALGORITMO);
        byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
        SecretKey key = new SecretKeySpec(keyBytes, Constantes.ALGORITMO_KEY);
        Cipher cipher = Cipher.getInstance(Constantes.ALGORITMO_KEY);
        cipher.init(Cipher.ENCRYPT_MODE, key);
 
        byte[] plainTextBytes = pass.getBytes("utf-8");
        byte[] buf = cipher.doFinal(plainTextBytes);
        byte[] base64Bytes = Base64.encodeBase64(buf);
        base64EncryptedString = new String(base64Bytes);
        
        return base64EncryptedString;
	}
	
	public String decryptPassword(String pass){
		logger.initTrace("decryptPassword", "String pass: "+pass);
		if (pass == null) pass = "";
		
        String base64EncryptedString = "";
 
        try {
        	
            byte[] message = Base64.decodeBase64(pass.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance(Constantes.ALGORITMO);
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, Constantes.ALGORITMO_KEY);
 
            Cipher decipher = Cipher.getInstance(Constantes.ALGORITMO_KEY);
            decipher.init(Cipher.DECRYPT_MODE, key);
 
            byte[] plainText = decipher.doFinal(message);
 
            base64EncryptedString = new String(plainText, "UTF-8");
 
        } catch (Exception ex) {
        	base64EncryptedString = "";
        	logger.traceError("decryptPassword", "Error en decryptPassword: ", ex);
        }
        logger.endTrace("decryptPassword", "Finalizado", "String base64EncryptedString: "+base64EncryptedString);
        return base64EncryptedString;
	}

	
	
	
}
