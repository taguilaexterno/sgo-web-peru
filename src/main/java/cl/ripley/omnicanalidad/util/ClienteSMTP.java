package cl.ripley.omnicanalidad.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;

public class ClienteSMTP {
	
	private static final AriLog logger = new AriLog(ClienteSMTP.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	public ClienteSMTP(){
		
	}
	
	public String completarHTML(String html, HashMap<String, String> valores, HashMap<String, String> lista){
		logger.initTrace("completarHTML", "String: html"+" - HashMap<String, String>: "+((valores != null)?valores.size():Constantes.NRO_CERO));
		try {
			if(Validaciones.validarVacio(html)){
				logger.endTrace("completarHTML", "No cumple validacion de html(vacio). Finalizado", "String: null");
				return null;
			}
			if(valores == null || valores.isEmpty()){
				logger.endTrace("completarHTML", "No cumple validacion de valores(vacio). Finalizado", "String: null");
				return null;
			}
			
			Set<String> keySet = valores.keySet();
		    for (String key : keySet) {
				if (valores.get(key) != null){
				   html = html.replace("[" + key + "]", valores.get(key));
				} else {
				   logger.traceInfo("completarHTML", "Key: ["+key+"] no encontrado");
				   html = html.replace("[" + key + "]", " ");
				}
			}
		    
		    int inicioLista = html.indexOf("<lista>");
		    if(inicioLista > Constantes.NRO_MENOSUNO){
		    	StringBuilder sbLista = new StringBuilder();
		    	int finInicioLista = inicioLista + "<lista>".length();
		    	int iniFinLista = html.indexOf("</lista>");
		    	int finLista = iniFinLista+"</lista>".length();
		    	String htmlPart1 = html.substring(Constantes.NRO_CERO, inicioLista);
		    	String htmlPart2 = html.substring(finLista);
		    	String htmlLista = html.substring(finInicioLista, iniFinLista);
			    if(lista != null && !lista.isEmpty()){
			    	int idxIni = Constantes.NRO_CERO;
			    	int idxFin = Constantes.NRO_CERO;
			    	int idxf = htmlLista.length();
			    	int val = Constantes.NRO_CERO;
			    	List<String> tokens = new ArrayList<String>();
			    	String cadena = htmlLista;
			    	while(val > Constantes.NRO_MENOSUNO){
			    		idxIni = cadena.substring(Constantes.NRO_CERO, idxf).indexOf("[");
			    		idxFin = cadena.substring(Constantes.NRO_CERO, idxf).indexOf("]");
			    		if(idxIni == Constantes.NRO_MENOSUNO || idxFin == Constantes.NRO_MENOSUNO){
			    			val = Constantes.NRO_MENOSUNO;
			    		}else{
			    			 tokens.add(cadena.substring(idxIni+Constantes.NRO_UNO, idxFin));
			    			 cadena = cadena.substring(idxFin+Constantes.NRO_UNO);
			    			 idxf = cadena.length();
			    		}
			    	}
			    	if(!tokens.isEmpty()){
			    		int cantRegistros = lista.size()/tokens.size();
			    		for (int i = Constantes.NRO_CERO; i < cantRegistros; i++) {
							String newHtmlLista = htmlLista;
							for (int k = Constantes.NRO_CERO; k < tokens.size(); k++) {
								newHtmlLista = newHtmlLista.replace("[" + tokens.get(k) + "]", "[" + tokens.get(k) + (i+Constantes.NRO_UNO) + "]");
							}
							sbLista.append(newHtmlLista+System.getProperty("line.separator"));
						}
			    		Set<String> keySet2 = lista.keySet();
			    		cadena = sbLista.toString();
			    	    for (String key : keySet2) {
			    			if (lista.get(key) != null){
			    				cadena = cadena.replace("[" + key + "]", lista.get(key));
			    			} else {
			    				logger.traceInfo("completarHTML", "Key: ["+key+"] no encontrado");
			    				cadena = cadena.replace("[" + key + "]", " ");
			    			}
			    		}
			    	    html = htmlPart1+cadena+htmlPart2;
			    	}else{
			    		sbLista.append(System.getProperty("line.separator"));
			    		html = htmlPart1+sbLista.toString()+htmlPart2;
			    	}
			    }else{
			    	sbLista.append(System.getProperty("line.separator"));
			    	html = htmlPart1+sbLista.toString()+htmlPart2;
			    }
		    }
		} catch (Exception e){  
			logger.traceError("completarHTML", "Ha ocurrido un error al reemplazar las etiquetas.", e);
			throw new AligareException("ERROR");
		}
		logger.endTrace("completarHTML", "Finalizado", "String: html");
		return html;
	}

	public boolean enviarCorreo(String url, int puerto, String asunto, String cuerpo, String remitente, String destinatario, String grupo) {
		logger.initTrace("enviarCorreo", "String url: "+url+" - int puerto: "+puerto+" - String asunto: "+asunto+" - String: html"+" - String remitente: "+remitente+" - String destinatario: "+destinatario, new KeyLog("remitente", remitente), new KeyLog("destinatario", destinatario), new KeyLog("asunto", asunto));
        if(Validaciones.validarVacio(url)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de url(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(asunto)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de asunto(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(cuerpo)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de cuerpo(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(remitente)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de remitente(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(remitente)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de remitente(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(destinatario)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de destinatario(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(destinatario)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de destinatario(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        
		Properties properties = new Properties();  
        Session session;
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.debug", true);
        properties.put("mail.smtp.host", url);
        properties.put("mail.smtp.port", puerto);
        properties.put("mail.smtp.auth", "false");
  
        session = Session.getDefaultInstance(properties); 
        session.setDebug(true);
        
        try{  
            MimeMessage message = new MimeMessage(session);  
            message.setHeader("X-SP-Transact-Id", grupo);
            message.setFrom(new InternetAddress(remitente));
            InternetAddress address = new InternetAddress(destinatario);
            message.setRecipient(Message.RecipientType.TO, address);
            message.setSubject(asunto);
            //message.setContent(cuerpo, "text/html; charset=utf-8");
            message.setText(cuerpo, "ISO-8859-1", "html");
            Transport t = session.getTransport("smtp");  
            t.connect();
            t.sendMessage(message, message.getAllRecipients());  
            t.close();  
        }catch (MessagingException me){  
        	logger.traceError("enviarCorreo", "Error(MessagingException) en enviarCorreo(): ", me);
        	return false;
        }
        logger.endTrace("enviarCorreo", "Finalizado", "boolean: true");
        return true;
    }
	
	public boolean enviarCorreoConAdjunto(String url, int puerto, String asunto, String cuerpo, String remitente, String destinatario, List<File> adjuntos) {
		logger.initTrace("enviarCorreoConAdjunto", "String: "+url+" - int: "+puerto+" - String: "+asunto+" - String: html"+" - String: "+remitente+" - String: "+destinatario+" - Nro_adjuntos: "+((adjuntos != null)?adjuntos.size():0), new KeyLog("remitente", remitente), new KeyLog("destinatario", destinatario), new KeyLog("asunto", asunto));
		if(Validaciones.validarVacio(url)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de url(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(asunto)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de asunto(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(cuerpo)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de cuerpo(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(remitente)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de remitente(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(remitente)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de remitente(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(destinatario)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de destinatario(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(destinatario)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de destinatario(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        if(adjuntos == null || adjuntos.isEmpty()){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de adjuntos(vacio). Finalizado", "boolean: false");
        	return false;
        }
        
        Properties properties = new Properties();  
        Session session;
        
        properties.put("mail.smtp.host", url);
        properties.put("mail.smtp.starttls.enable", "true");  
        properties.put("mail.smtp.port", puerto);
        properties.put("mail.smtp.mail.sender", remitente);
        properties.put("mail.smtp.auth", "false");
  
        session = Session.getDefaultInstance(properties); 
        
        try{  
        	MimeMultipart mp = new MimeMultipart("related");
        	BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(cuerpo, "text/html; charset=utf-8");
            mp.addBodyPart(messageBodyPart);
            
        	for (File file : adjuntos) {
            	BodyPart messageBodyPart2 = new MimeBodyPart();
                DataSource ds = new FileDataSource(file);
                messageBodyPart2.setDataHandler(new DataHandler(ds));
                messageBodyPart2.setFileName(file.getName());
                messageBodyPart2.setDisposition(Part.ATTACHMENT);
                mp.addBodyPart(messageBodyPart2);
			}
            
            MimeMessage message = new MimeMessage(session);  
            message.setFrom(new InternetAddress((String)properties.get("mail.smtp.mail.sender")));
	        message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            message.setSubject(asunto);  
            message.setContent(mp);
            Transport t = session.getTransport("smtp");  
            t.connect();
            t.sendMessage(message, message.getAllRecipients());  
            t.close();  
        }catch (MessagingException me){  
        	logger.traceError("enviarCorreoConAdjunto", "Error(MessagingException) en enviarCorreoConAdjunto(): ", me);
        	return false;
        }
        logger.endTrace("enviarCorreoConAdjunto", "Finalizado", "boolean: true");
		return true;
	}
}
