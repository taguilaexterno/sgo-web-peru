package cl.ripley.omnicanalidad.util;

public class Test {

	public static void main(String[] args) {

		password();
	}

	@SuppressWarnings("unused")
	private static void password(){
		String password = "123456";
		
		Password encriptador = new Password();
		
		try {
			String encriptado = encriptador.encryptPassword(password);
			
			System.out.println("Password: "+password);
			System.out.println("Encriptado: "+encriptado);
			
			//V3CXxwZd8Og=
			
			String encriptada = "9B04bpyIbIs=";
			String desencriptada = encriptador.decryptPassword(encriptada);
			
			System.out.println("Encriptada: "+encriptada);
			System.out.println("Desencriptada: "+desencriptada);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
