package cl.ripley.omnicanalidad.util;

import java.math.BigDecimal;

public class Constantes {
	
	public static final String CODIGO_APP		= "SGO Web Corp";
	public static final String VERSION_APP      = "1.3.3 [Regulatorio]";
	public static final String JNDI_NODE		= "java:comp/env";
	public static final String JNDI_ME			= "jdbc/dsModeloExtendido";
	public static final String JNDI_BT			= "jdbc/dsBigTicket";
	public static final String JNDI_CLUB		= "jdbc/dsRegalos";

	public static final int STORE_ID			= 1;
	
	public final static int ESTADO_ACTIVO		= 1;
	public final static int ESTADO_INACTIVO		= 0;
	
	public static final String ALGORITMO		= "MD5";
	public static final String ALGORITMO_KEY	= "DESede";
	
	public static final int ESTADO_PENDIENTE_IMPRESION				= 1;
	
	public static final int ESTADO_PENDIENTE_APROBACION				= 8;
	public static final int ESTADO_STOCK_OK_APROBAR_MP				= 88;
	public static final int ESTADO_SIN_STOCK_NC_TOTAL				= 83;
	public static final int ESTADO_SIN_STOCK_NC_PARCIAL				= 84;
	public static final int ESTADO_SIN_STOCK_NC						= 34;
	public static final int ESTADO_PENDIENTE_STOCK_MP_NO_SEGURO		= 87;
	public static final int ESTADO_PENDIENTE_STOCK_MP_SEGURO		= 81;
	public static final int ESTADO_PENDIENTE_STOCK					= 71;
	public static final int ESTADO_APROBACION_MANUAL				= 100;
	public static final int ESTADO_RECHAZO_MANUAL					= 0;

	public static final int ESTADO_INDICADOR_MKP_RIPLEY				= 0;
	public static final int ESTADO_INDICADOR_MKP_MKP				= 1;
	public static final int ESTADO_INDICADOR_MKP_MIXTA				= 2;
	public static final int ESTADO_INDICADOR_MKP_CLUB				= 3;

	public static final int ESTADO_PENDIENTE_CYBERSOURCE_DD			= 94;
    public static final int ESTADO_PENDIENTE_CYBERSOURCE_RT			= 95;
	
	public static final String NO_REGISTRADO_DB	= "noregistradodb";
	public static final String RESTRICT			= "restringido";
	
	public static final String SUCCESS_PAGO_BOLETA			= "successPagoBoleta";
	public static final String SUCCESS_DETALLE_BOLETA		= "successDetalleBoleta";
	public static final String SUCCESS_APROBACION_BOLETA	= "successAprobacionBoleta";
	public static final String SUCCESS_SIN_STOCK_PARCIAL	= "successSinStockParcial";
	public static final String SUCCESS_SIN_STOCK_TOTAL		= "successSinStockTotal";
	public static final String SUCCESS_SIN_STOCK			= "successSinStock";
	public static final String SUCCESS_PENDIENTE_STOCK		= "successPendienteStock";
	public static final String SUCCESS_ANULACIONES			= "successAnulaciones";
	public static final String SUCCESS_NOTA_CREDITO			= "successNotaCredito";
	public static final String SUCCESS_NC_AMBAS				= "successNCAmbas";
	public static final String SUCCESS_ORDEN_COMPRA			= "successOrdenCompra";
	public static final String SUCCESS_DETALLE_OC			= "successDetalleOC";
	
	public static final String TIPO_PAGO_TBK_CREDITO	= "WPC";
	public static final String TIPO_PAGO_TBK_DEBITO		= "WPD";
	public static final String TIPO_PAGO_RIPLEY			= "CAR";
	public static final String TIPO_PAGO_TRE			= "TRE";
	public static final String TIPO_PAGO_MERCADO_PAGO	= "MP";
	
	public static final String TIPO_PAGO_TBK_CREDITO_	= "Credito";
	public static final String TIPO_PAGO_TBK_DEBITO_	= "Debito";
	public static final String TIPO_PAGO_RIPLEY_		= "T. Ripley";
	public static final String TIPO_PAGO_MERCADO_PAGO_	= "M. Pago";
	
	public static final String TBK_S	= "S";
	public static final String TBK_M	= "M";
	public static final String DETALLE_NC = "successNC";
	public static final String DETALLE_NC_ANULACION = "successNCAnulacion";
	
	
	public static final String SUCCESS_FECHAS_NOTAS_CREDITO_TBK	= "successFechasNotasCreditoTBK";
	
	public static final Integer TIPO_DOC_BOLETA = 3;
	public static final Integer TIPO_DOC_FACTURA = 1;
	
	
	public final static char CHAR_CERO	= '0';
	public final static char CHAR_SPACE	= ' ';
	
	public final static boolean VERDADERO	= true;
	public final static boolean FALSO		= false;
	
	public final static String STRING_CERO		= "0";
	public final static String STRING_UNO		= "1";
	public final static String STRING_DOS		= "2";
	public final static String STRING_TRES		= "3";
	public final static String STRING_UNOX2		= "11";
	public final static String STRING_CEROX2	= "00";
	public final static String STRING_CEROx4	= "0000";
	
	public final static String VACIO		= "";
	public final static String GUION		= "-";
	public final static String SLASH		= "/";
	public final static String PIPE			= "|";
	public final static String PORCENTAJE	= "%";
	public final static String SPACE		= " ";
	public final static String PUNTO		= ".";
	public final static String PUNTO_2		= "\\.";
	public final static String DOSPUNTOS	= ":";
	public final static String SI			= "S";
	public final static String NO			= "N";
	public final static String K			= "K";
	public final static char TAB 			= (char)9;
	
	//NUMEROS
	public final static int NRO_MENOSUNO			= -1;
	public final static int NRO_CERO				= 0;
	public final static int NRO_UNO					= 1;
	public final static int NRO_DOS					= 2;
	public final static int NRO_TRES				= 3;
	public final static int NRO_CUATRO				= 4;
	public final static int NRO_CINCO				= 5;
	public final static int NRO_SEIS				= 6;
	public final static int NRO_SIETE				= 7;
	public final static int NRO_OCHO				= 8;
	public final static int NRO_NUEVE				= 9;
	public final static int NRO_DIEZ				= 10;
	public final static int NRO_ONCE				= 11;
	public final static int NRO_DOCE				= 12;
	public final static int NRO_TRECE				= 13;
	public final static int NRO_CATORCE				= 14;
	public final static int NRO_QUINCE				= 15;
	public final static int NRO_DIECINUEVE			= 19;
	public static final int NRO_VEINTE 				= 20;
	public final static int NRO_VEINTIUNO			= 21;
	public final static int NRO_VEINTIDOS           = 22;
	public final static int NRO_VEINTINUEVE			= 29;
	public final static int NRO_TREINTA				= 30;
	public final static int NRO_TREINTADOS			= 32;
	public final static int NRO_TREINTACUATRO		= 34;
	public final static int NRO_TREINTANUEVE		= 39;
	public final static int NRO_CINCUENTA			= 50;
	public final static int NRO_NOVENTASEIS			= 96;
	public final static int NRO_NOVENTANUEVE        = 99;
	public final static int NRO_CIEN				= 100;
	public final static int NRO_CIENVEINTECUATRO	= 124;
	public final static int NRO_CIENVEINTESIETE		= 127;
	public final static int NRO_CIENOCHENTA			= 180;
	public static final String SUCCESS_BUSQUEDA_NC = "successBusquedaNC";
	public static final String SUCCESS_BUSQUEDA_ANULACION = "successBusquedaAnulacion";
	public static final String SUCCESS_BUSQUEDA_MIXTA = "successBusquedaMixta";
	
	public static final Integer ESTADO_BOLETA = 2;
	public static final Integer ESTADO_NC_PARCIAL = 4;
	public static final Integer ESTADO_ANULACION_TOTAL = 23;
	public static final Integer ESTADO_ANULACION_PARCIAL = 24;
	public static final Integer ESTADO_SIN_STOCK_TOTAL = 13;
	public static final Integer ESTADO_SIN_STOCK_PARCIAL = 14;
	public static final Integer NO_CAJA = -1;
	
	public final static String IND_MKP_RIPLEY	= "0";
	public final static String IND_MKP_MKP 		= "1";
	public final static String IND_MKP_AMBAS	= "2";
	public final static String IND_MKP_REGALO	= "3";
	public final static int ES_MKP_RIPLEY 		= 0;
	public final static int ES_MKP_MKP 			= 1;
	
	public final static String FECHA_YYYYMMDD 			= "yyyy-MM-dd";
	public final static String FECHA_DDMMYYYY			= "dd-MM-yyyy";
	public final static String FECHA_DDMMYYYY2			= "dd/MM/yyyy";
	public final static String FECHA_DDMMYY 			= "dd-MM-yy";
	public final static String FECHA_DDMMYY2 			= "ddMMyy";
	public final static String FECHA_HHMMSS 			= "HH:mm:ss";
	
	public final static String VIA_PAGO = "Abono (D)";
	public final static String CONDICION_PAGO = "pago contado (Z030)";
	public final static String IDIOMA = "Espa�ol";
	public final static String VER_FACT_DOBLE = "T";
	public final static String CUENTA_ASOCIADA = "ZDEV";
	public final static String PAIS = "CL";
	
	public static final int CAJA_ENCENDIDA 		= 1;
	public static final int CAJA_APAGADA   		= 0;
	public final static String BOL_FORMA_PAGO_TB	= "Tarjeta Bancaria";
	public final static String BOL_FORMA_PAGO_WO	= "One Click";
	public final static String BOL_FORMA_PAGO_MP	= "Mercado Pago";
	public final static String BOL_FORMA_PAGO_TRE	= "TRE";
	public final static String BOL_FORMA_PAGO_CAR	= "CAR";
	public final static String BOL_FORMA_PAGO_COM	= "COM";
	public final static String BOL_FORMA_PAGO_EFE   = "Efectivo";
	public final static String BOL_FORMA_PAGO_CT	= "CAR-TRE";
	public final static String BOL_FORMA_PAGO_TAR_RIP	= "TARJETA RIPLEY";
	public final static String BOL_FORMA_PAGO_TAR_REG_EMP	= "TARJETA REGALO EMPRESAS";
	public final static String BOL_FORMA_PAGO_TRECRIP	= "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
	public final static String BOL_VALOR_VD_S	= "S";
	public final static String BOL_VALOR_VD_D	= "D";
	public final static String BOL_VALOR_VD_C	= "C";
	public final static String BOL_VALOR_VD_N	= "N";
	public final static String BOL_VALOR_VD_M	= "M";
	public final static String BOL_JORNADA_AM	= "AM";
	public final static String BOL_JORNADA_PM	= "PM";
	public final static String BOL_TIPO_DES_RT  = "RT";
	public final static String TRAMA_PLAT_DEBITO		= "Debito";
	public final static String TRAMA_PLAT_CREDITO		= "Credito";	
	public final static String TIPO_PAGO_WEBP = "WEB";
	public final static String TARJETA_MERCADO_PAGO = "MP";
	public final static String TARJETA_ONE_CLICK = "WO";
	public final static String SIN_STOCK = "SIN STOCK";
	
	//CONSTANTES MIRAKL
	public final static int		MIRAKL_ESTADO_INGRESAR	= 1;
	public final static String	MIRAKL_API_OR02			= "OR02";
	
	public final static String TRAMA_PLAT_TRE_3			= "ACEPTO LOS CARGOS INDICADOS EN ESTE DOCUMENTO EN MI TARJETA REGALO EMPRESA.";
	public final static String TRAMA_PLAT_CAR_1_1		= "ACEPTO LOS CARGOS INDICADOS EN ESTE DOCUMENTO EN MI TARJETA Y PAGARLOS";
	public final static String TRAMA_PLAT_CAR_1_2		= "SEGÚN CONDICIONES DE CONTRATO CON CAR S.A. ASIMISMO, ACEPTO LAS CESIONES";
	public final static String TRAMA_PLAT_CAR_1_3		= "QUE CAR S.A. EFECT�E DE ESTE CR�DITO U OTROS VIGENTES CON CAR S.A";
	public final static String TRAMA_ACEPTO_PAGAR		= "ACEPTO PAGAR SEGUN CONTRATO CON EMISOR";
	
	//Estos son los estados que se validaran en el sp SGO_MANEJO_OC.PROC_OBTENER_OC para 
	// cambiar el estado de las ordenes en Confirmaci�n de pago de compras no seguras
	//public final static String ESTADOS_VALIDAR_GENERACION_DTE = "8,88,100";
	public final static String ESTADOS_VALIDAR_GENERACION_DTE = "8,88,100,94,95";
	
	//Constantes Reservas
	public static final String SUCCESS_INICIO			= "successIndex";
	public static final String SUCCESS_INICIO2			= "successIndex2";
	public static final String SUCCESS_DETAIL			= "successDetail";
	public static final String SUCCESS_LIST				= "successList";
	public static final String SUCCESS_CANCEL			= "successCancel";
	public static final String SUCCESS_EXTEND			= "successExtend";
	public static final String ERROR					= "error";
	public static final String LOGGED_OUT 				= "logout";
	public static final String COMA = ",";
	public static final String ARROBA = "@";
	public static final String URL_PAGO_CONTRA_ENTREGA_TARJETA = "URL_PAGO_CONTRA_ENTREGA_TARJETA";
	public static final String GATO = "#";
	
	//Constantes Reportes
	public static final String NRO_CAJA					= "NRO_CAJA";
	public static final String NRO_SUCURSAL				= "NRO_SUCURSAL";
	public static final String FEC_HOR_APERTURA			= "FEC_HOR_APERTURA";
	public static final String FEC_HOR_CIERRE			= "FEC_HOR_CIERRE";
	public static final String ID_APERTURA_CIERRE		= "ID_APERTURA_CIERRE";
	public static final String NO_DATA					= "No se encontraron registros para los datos consultados";
	public static final String SUCCESS_DOWNLOAD			= "successDownload";
	public static final BigDecimal NRO_CERO_B			= new BigDecimal(NRO_CERO);
	public static final int FUNCION_BOLETA				= 2;
	public static final String URL_TV_CONSULTA_SKU = "URL_TV_CONSULTA_SKU";
	public static final String SKU_HOLDER = "[sku]";
	public static final String SUCCESS_JSON				= "successJson";
	public static final String NROS_CAJAS_BOLETAS = "NROS_CAJAS_BOLETAS";
	public static final String NROS_CAJAS_NC = "NROS_CAJAS_NC";
	public static final String DIRECCION_EMISOR = "DIRECCION_EMISOR";
	public static final String FORMATO_8_CEROS_IZQ = "%1$08d";
	public static final String RAZON_SOCIAL_EMISOR = "RAZON_SOCIAL_EMISOR";
	public static final String RUT_EMISOR = "RUT_EMISOR";
	public static final String TERMINAL = "TERMINAL";
	public static final String FORMATO_2_DECIMALES = "%.2f";
	public static final String FORMATO_12_CEROS_IZQ = "%012d";
	public static final String VALORIVA = "VALORIVA";
	public static final String FORMA_PAGO_TARJETA_RIPLEY = "TARJETA RIPLEY";
	public static final String FORMA_PAGO_TIENDA = "PAGO TIENDA";
	public static final String FORMA_PAGO_EFECTIVO = "EFECTIVO";
	public static final String MP_GLOSA_RIPLEY = "RIP";
	public static final String MP_GLOSA_BANCARIA = "BAN";
	public static final String MP_GLOSA_EFECTIVO = "EFE";
	public static final String FORMA_PAGO_TDC = "TARJETA CREDITO";
	public static final String FORMA_PAGO_CENTREGA = "CONTRA ENTREGA";
	public static final String FORMA_PAGO_CENTREGA_BANCARIA = "CONTRA ENTREGA BANCARIA";
	public static final String FORMA_PAGO_PAYPAL = "PAYPAL";
	public static final String FORMA_PAGO_DEBITO = "TARJETA DEBITO";
	public static final String CANAL_SAFETYPAY = "SAFETYPAY";
	public static final String FACTURA = "FACTURA";
	public static final String BOLETA = "BOLETA";
	public static final String FORMATO_FECHA_DD_MM_YYYY = "dd-MM-uuuu";
	public static final String FORMATO_FECHA_YYYY_MM_DD = "uuuu-MM-dd";
	public static final String FORMATO_2_DIGITOS = "%02d";
	public static final String NRO_MIL = "1000";
	public static final String PUNTO_COMA = ";";
	public static final String OK = "OK";
	public static final String NOK = "NOK";
	public static final String COSTO_DE_ENVIO = "COSTO DE ENVIO";

}
