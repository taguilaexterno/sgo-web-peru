package cl.ripley.omnicanalidad.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.regex.Pattern;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;

public class Util {

	public static String cambiaCaracteres(String cadena){
		String res = Constantes.VACIO;
		if(cadena != null){
			for (int x=Constantes.NRO_CERO;x<cadena.length();x++){
				char a = cadena.charAt(x);
				int ascii = cadena.codePointAt(x);
				if(ascii <= Constantes.NRO_TREINTADOS || ascii == Constantes.NRO_TREINTACUATRO || ascii == Constantes.NRO_TREINTANUEVE || 
						ascii == Constantes.NRO_NOVENTASEIS || ascii == Constantes.NRO_CIENVEINTECUATRO || ascii == Constantes.NRO_CIENVEINTESIETE || ascii == Constantes.NRO_CIENOCHENTA){
					res = res+Constantes.SPACE;
				}else{
					res = res+a;
				}
			}
		}

		return res;
	}

	public static String rellenarString(String cadena, int largo, boolean lpad, char caracter){
		if(cadena !=null && largo > cadena.length()){
			int nro = largo - cadena.length();
			for (int i = Constantes.NRO_CERO; i < nro; i++) {
				if(lpad){
					cadena = caracter + cadena;
				}else{
					cadena = cadena + caracter;
				}
			}
		}

		return cadena;
	}

	public static String getDigitoValidadorRut(String rut){
		int cantidad = rut.length();
		if (cantidad == 0) return Constantes.VACIO;
		int factor = 2;
		int suma = 0;
		String verificador = "";
		for(int i = cantidad; i > 0; i--){ 
			if(factor > 7){ 
				factor = 2;
			}
			suma += (Integer.parseInt(rut.substring((i-1), i)))*factor;
			factor++;
		}
		verificador = String.valueOf(11 - suma%11);

		if(verificador.equals("10")){
			return Constantes.K;
		} else if (verificador.equals("11")){
			return Constantes.STRING_CERO;
		} else {
			return verificador;
		}
	}

	public static String getDigitoValidadorRut2(String rut){
		int cantidad = rut.length();
		if (cantidad == 0) return Constantes.VACIO;
		int factor = 2;
		int suma = 0;
		String verificador = "";
		for(int i = cantidad; i > 0; i--){ 
			if(factor > 7){ 
				factor = 2;
			}
			suma += (Integer.parseInt(rut.substring((i-1), i)))*factor;
			factor++;
		}
		verificador = String.valueOf(11 - suma%11);

		if(verificador.equals("10")){
			return String.valueOf(Constantes.NRO_ONCE);
		} else if (verificador.equals("11")){
			return Constantes.STRING_CEROX2;
		} else {
			return verificador;
		}
	}

	public static String formatNumber(Object number) {
		BigDecimal num = null;
		if(number instanceof String) {
			num = new BigDecimal((String) number);
		} else {
			num = (BigDecimal) number;
		}
		DecimalFormat format = new DecimalFormat("\u00A4#,##0.00");
		format.setMaximumFractionDigits(2);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("S/");
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		symbols.setMonetaryDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);
		String formateado = format.format(num != null ? num : 0);
		return formateado;
	}

	public static Integer validaIntegerCeroXNull(int numero){
		Integer dato = null;

		if (numero == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = new Integer(numero);
		}

		return dato;
	}
	
	public static Long validaLongCeroXNull(long numero){
		Long dato = null;
		
		if (numero == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = Long.valueOf(numero);
		}
		
		return dato;
	}
	/**
	 * Devuelve numero formateado en NNN.NNN sin signo Peso
	 * @param number
	 * @return
	 */
	public static String formatNumber2(Object number) {
		/*Number num = null;
		if(number instanceof String) {
			num = Integer.parseInt((String) number);
		} else {
			num = (Number) number;
		}
		DecimalFormat format = new DecimalFormat("\u00A4#,##0");
		format.setMaximumFractionDigits(0);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("");
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);

		String formateado = format.format(num != null ? num : 0);
		return formateado;*/
		BigDecimal num = null;
		if(number instanceof String) {
			num = new BigDecimal((String) number);
		} else {
			num = (BigDecimal) number;
		}
		DecimalFormat format = new DecimalFormat("\u00A4#,##0.00");
		format.setMaximumFractionDigits(2);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("S/");
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		symbols.setMonetaryDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);
		String formateado = format.format(num != null ? num : 0);
		return formateado;

	}

	/**
	 * Metodo que devuelve el RUT en formato xxx.xxx.xxx-K
	 * 
	 * @author FRiveros
	 * Oct 21, 2016
	 * @param rut
	 * @return rutFormateado
	 */
	public static String formatoRUT(String rut) {
		String rutFormateado = "";
		int longitud = 0, indice;
		String digitoVerif, rutAux, punto = Constantes.PUNTO;
		boolean termino = false;

		rut = rut.replaceAll("[.]", Constantes.VACIO);
		rut = rut.replaceAll("[-]", Constantes.VACIO);

		if(rut != null) {
			longitud = rut.length();
			rutAux = rut.substring(0,longitud - 1);
			digitoVerif = rut.substring(longitud - 1);
			rutFormateado += Constantes.GUION + digitoVerif;		
			while(!termino) {
				longitud = rutAux.length();			
				if(longitud <= 3) {
					//punto = "";
					termino = true;
					indice = 0;
				}
				else {
					indice = longitud - 3;
				}
				rutFormateado = punto + rutAux.substring(indice) + rutFormateado;
				rutAux = rutAux.substring(0,indice);
			}
			rutFormateado= rutFormateado.substring(1,rutFormateado.length());
		}		
		return rutFormateado;
	}

	/**
	 * Metodo que devuelve el RUT en formato xxxxxxxxx-K
	 * 
	 * @author Bparra
	 * Ago 14, 2017
	 * @param rut
	 * @return rutFormateado
	 */
	public static String formatoRUT2(String rut) {
		String rutFormateado = "";
		int longitud = 0;
		String digitoVerif, rutAux;

		rut = rut.replaceAll("[.]", Constantes.VACIO);
		rut = rut.replaceAll("[-]", Constantes.VACIO);

		if(rut != null) {
			longitud = rut.length();
			rutAux = rut.substring(0,longitud - 1);
			digitoVerif = rut.substring(longitud - 1);
			rutFormateado = rutAux + Constantes.GUION + digitoVerif;		

		}		
		return rutFormateado;
	}

	public static String getVacioPorNulo(String dato){
		if (dato == null)
			return Constantes.VACIO;
		else
			return dato;
	}
	public static String quitarSaltos(String cadena) {
		return cadena.replaceAll("\n", ""); 
	}

	private final static String[] UNIDADES = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
	private final static String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
			"diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
			"cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
	private final static String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
			"setecientos ", "ochocientos ", "novecientos "};


	public static String MontoEscrito(String numero, boolean mayusculas) {
		String literal = "";
		//si el numero utiliza (.) en lugar de (,) -> se reemplaza
		numero = numero.replace(".", ",");
		//si el numero no tiene parte decimal, se le agrega ,00
		if(numero.indexOf(",")==-1){
			numero = numero + ",00";
		}
		//se valida formato de entrada -> 0,00 y 999 999 999,00
		if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {
			//se divide el numero 0000000,00 -> entero y decimal
			String Num[] = numero.split(",");            
			//se convierte el numero a literal
			if (Integer.parseInt(Num[0]) == 0) {//si el valor es cero
				literal = "cero ";
			} else if (Integer.parseInt(Num[0]) > 999999) {//si es millon
				literal = getMillones(Num[0]);
			} else if (Integer.parseInt(Num[0]) > 999) {//si es miles
				literal = getMiles(Num[0]);
			} else if (Integer.parseInt(Num[0]) > 99) {//si es centena
				literal = getCentenas(Num[0]);
			} else if (Integer.parseInt(Num[0]) > 9) {//si es decena
				literal = getDecenas(Num[0]);
			} else {//sino unidades -> 9
				literal = getUnidades(Num[0]);
			}
			//devuelve el resultado en mayusculas o minusculas
			if (mayusculas) {
				return (literal).toUpperCase();
			} else {
				return (literal);
			}
		} else {//error, no se puede convertir
			return literal = null;
		}
	}

	/* funciones para convertir los numeros a literales */

	private static String getUnidades(String numero) {// 1 - 9
		//si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
		String num = numero.substring(numero.length() - 1);
		return UNIDADES[Integer.parseInt(num)];
	}

	private static String getDecenas(String num) {// 99                        
		int n = Integer.parseInt(num);
		if (n < 10) {//para casos como -> 01 - 09
			return getUnidades(num);
		} else if (n > 19) {//para 20...99
			String u = getUnidades(num);
			if (u.equals("")) { //para 20,30,40,50,60,70,80,90
				return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
			} else {
				return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
			}
		} else {//numeros entre 11 y 19
			return DECENAS[n - 10];
		}
	}

	private static String getCentenas(String num) {// 999 o 099
		if( Integer.parseInt(num)>99 ){//es centena
			if (Integer.parseInt(num) == 100) {//caso especial
				return " cien ";
			} else {
				return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getDecenas(num.substring(1));
			} 
		}else{//por Ej. 099 
			//se quita el 0 antes de convertir a decenas
			return getDecenas(Integer.parseInt(num)+"");            
		}        
	}

	private static String getMiles(String numero) {// 999 999
		//obtiene las centenas
		String c = numero.substring(numero.length() - 3);
		//obtiene los miles
		String m = numero.substring(0, numero.length() - 3);
		String n="";
		//se comprueba que miles tenga valor entero
		if (Integer.parseInt(m) > 0) {
			n = getCentenas(m);           
			return n + "mil " + getCentenas(c);
		} else {
			return "" + getCentenas(c);
		}

	}

	private static String getMillones(String numero) { //000 000 000        
		//se obtiene los miles
		String miles = numero.substring(numero.length() - 6);
		//se obtiene los millones
		String millon = numero.substring(0, numero.length() - 6);
		String n = "";
		if(millon.length()>1){
			n = getCentenas(millon) + "millones ";
		}else{
			n = getUnidades(millon) + "millon ";
		}
		return n + getMiles(miles);        
	}

	public static String crearCodigoBarra(OrdenesDeCompra orden){		
		String codigoCreado;
		String fecha = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYY2, orden.getNotaVenta().getFechaCreacion());
		String sucursal = String.format("%05d", orden.getNotaVenta().getNumeroSucursal());
		String nroCaja = String.format("%03d", orden.getNotaVenta().getNumeroCaja());
		String trx = String.format("%04d", orden.getNotaVenta().getNroTrx());

		codigoCreado = fecha + sucursal + nroCaja + trx;

		return codigoCreado;
	}

	/**Obtiene el subtotal de la Orden de Compra de acuerdo al flag esMkp.
	 * Si esMkp = 1 entonces calcula el subtotal de los artículos Marketplace.
	 * Si esMkp = 0 entonces calcula el subtotal de los artículos distintos a Marketplace.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trama
	 * @param esMkp
	 * @return
	 * @throws AligareException
	 */
	public static BigDecimal getSubtotal(OrdenesDeCompra trama, Integer esMkp) throws AligareException{

		List<ArticulosVentaOC> articuloVentas = trama.getArticulosVenta();

		BigDecimal acumulado =	articuloVentas.stream()
				.filter(articuloVentaTrama -> articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp.intValue())
				.map(articuloVentaTrama -> articuloVentaTrama.getArticuloVenta().getPrecio()
						.multiply(BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades()))
						.subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		return acumulado;
	}

	/**Obtiene el nombre de la forma de pago de la Orden de Compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trama
	 * @return
	 */
	public static String getFormaPago(OrdenesDeCompra trama) {

		String formaPago = Constantes.VACIO;

		if(trama.getTarjetaRipley().getCorrelativoVenta() != null 
				&& trama.getTarjetaRipley().getCorrelativoVenta().intValue() != Constantes.NRO_CERO) {

			formaPago = Constantes.FORMA_PAGO_TARJETA_RIPLEY;

		} else if(trama.getTarjetaBancaria().getCorrelativoVenta() != null 
				&& trama.getTarjetaBancaria().getCorrelativoVenta().intValue() != 0) {

			formaPago = trama.getTarjetaBancaria().getDescripcionFormaPago();


		}

		return formaPago;

	}
	
	/**Obtiene el PAN del medio de pago de la orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trama
	 * @return
	 */
	public static String getPan(OrdenesDeCompra trama) {
		
		String codBin = trama.getTarjetaBancaria().getBinNumber();
		String codigoAutorizadorDoc = trama.getTarjetaBancaria().getCodigoAutorizador();
		String ultDigito = trama.getTarjetaBancaria().getUltimosDigitos();
		String panDoc = Constantes.VACIO;
		String formaPago = getFormaPago(trama);
		
		if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
			
			panDoc = trama.getTarjetaRipley().getPan().toString();
			
			//verifico tarjeta clasica
			if(panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_CINCO).equals("96041")) {
				
				panDoc = panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_CINCO) + "*******" + panDoc.substring(panDoc.length() - 4);
				
			} else {
				
				panDoc = panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_SEIS) + "******" + panDoc.substring(panDoc.length() - 4);
				
			}
			
		} else if(Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)) {
			//efectivo
			String codAutoriza = codigoAutorizadorDoc.substring(codigoAutorizadorDoc.length() - Constantes.NRO_SIETE);
			panDoc = codBin + "999" + codAutoriza;
			
		} else if(Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
			//paypal
			panDoc = codBin + "******" + ultDigito;
			
		} else if(Constantes.FORMA_PAGO_TDC.equals(formaPago)
				|| Constantes.FORMA_PAGO_DEBITO.equals(formaPago)) {
			// tarjeta bancaria
			panDoc = codBin + "******" + ultDigito;
			
		}
		
		return panDoc;
		
		
	}
	
	public static String formatNumber3(Object number) {
		BigDecimal num = null;
		if(number instanceof String) {
			num = new BigDecimal((String) number);
		} else {
			num = (BigDecimal) number;
		}
		DecimalFormat format = new DecimalFormat("#,##0.00");
		format.setMaximumFractionDigits(2);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//		symbols.setCurrencySymbol("S/");
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		symbols.setMonetaryDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);
		String formateado = format.format(num != null ? num : 0);
		return formateado;
	}

}
