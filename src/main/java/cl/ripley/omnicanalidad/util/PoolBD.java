package cl.ripley.omnicanalidad.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;

/**Clase para iniciar y obtener conexiones a las distintas bases de datos del aplicativo SGO Web.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
public class PoolBD {
	
	private static final AriLog logger = new AriLog(PoolBD.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private DataSource dataSource;
	private DataSource DTSR_BT = null;
	private DataSource DTSR_CLUB = null;
	
	    	
	/**Inicia todos los Data Sources del aplicativo.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @throws AligareException
	 */
	public void openDS() throws AligareException {
    	initDSME();
    	initDSBT();
    	initCLUB();
    }
    
    /**Cierra todos los Data Sources del aplicativo
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @throws AligareException
     */
    public void closeDS() throws AligareException{
    	closeDSME();
    	closeDSBT();
    	closeCLUB();
    }
            
    /**Inicia el Data Source del Modelo Extendido
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @throws AligareException
     */
    private void initDSME() throws AligareException{
    	logger.initTrace("initDSME", null, new KeyLog("BD", "Modelo Extendido"));
//        try {
//        	Context initContext = new InitialContext();
//			Context envContext  = (Context)initContext.lookup(Constantes.JNDI_NODE);
//			DataSource basicDataSource = (DataSource)envContext.lookup(Constantes.JNDI_ME);
//            DTSR_ME = basicDataSource;
//        } catch (Exception ex) { 
//        	logger.traceError("initDSME", ex);
//            throw new AligareException("Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos.");
//        }
        
        logger.endTrace("initDSME","Finalizado", null);
    }
    
    /**Cierra el Data Source del Modelo Extendido.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     */
    private void closeDSME(){
//    	DTSR_ME = null;
	}
    
    private void initDSBT() throws AligareException{
    	logger.initTrace("initDSBT", null, new KeyLog("BD", "Big Ticket"));
//        try {
//        	Context initContext = new InitialContext();
//			Context envContext  = (Context)initContext.lookup(Constantes.JNDI_NODE);
//			DataSource basicDataSource = (DataSource)envContext.lookup(Constantes.JNDI_BT);
//			DTSR_BT = basicDataSource;
//        } catch (Exception ex) { 
//        	logger.traceError("initDSBT", ex);
//            throw new AligareException("Imposible realizar conexion a 'BIG TICKET' en estos momentos.");
//        }
        
        logger.endTrace("initDSBT","Finalizado", null);
    }
    
    
    private void closeDSBT(){
    	DTSR_BT = null;
	}
    
    
    private void initCLUB() throws AligareException{
    	logger.initTrace("initCLUB", null, new KeyLog("BD", "Modelo Extendido"));
//        try {
//        	Context initContext = new InitialContext();
//			Context envContext  = (Context)initContext.lookup(Constantes.JNDI_NODE);
//			DataSource basicDataSource = (DataSource)envContext.lookup(Constantes.JNDI_CLUB);
//            DTSR_CLUB = basicDataSource;
//        } catch (Exception ex) { 
//        	logger.traceError("initCLUB", ex);
//            throw new AligareException("Imposible realizar conexion a 'CLUB' en estos momentos.");
//        }
        
        logger.endTrace("initCLUB","Finalizado", null);
    }
    
    private void closeCLUB(){
    	DTSR_CLUB = null;
	}
        
    /**Obtiene la conexión de base de datos del Modelo Extendido.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @return
     */
    public Connection getConnection() {
    	logger.initTrace("getConnection", null);
    	Connection conn = null;
    	try {
//			if (DTSR_ME == null){
//		            initDSME();
//		    }
			conn = dataSource.getConnection();
    	} catch (Exception ex) {
    		logger.traceError("getConnection", ex.getMessage(), ex);
    		return null;
        }
    	
        logger.endTrace("getConnection","Finalizado", "Connection: "+((conn != null)?"Si":"No"));
        return conn;
    }
    
    public Connection getConnectionBT() {
    	logger.initTrace("getConnectionBT", null);
    	Connection conn = null;
    	try {
			if (DTSR_BT == null){
		            initDSBT();
		    }
			conn = DTSR_BT.getConnection();
    	} catch (Exception ex) {
    		logger.traceError("getConnectionBT", ex.getMessage(), ex);
    		return null;
        }
    	
        logger.endTrace("getConnectionBT","Finalizado", "Connection: "+((conn != null)?"Si":"No"));
        return conn;
    }
    
    public Connection getConnectionCLUB() {
    	logger.initTrace("getConnectionCLUB", null);
    	Connection conn = null;
    	try {
			if (DTSR_CLUB == null){
		        initCLUB();
		    }
			conn = DTSR_CLUB.getConnection();
    	} catch (Exception ex) {
    		logger.traceError("getConnectionCLUB", ex.getMessage(), ex);
    		return null;
        }
    	
        logger.endTrace("getConnectionCLUB","Finalizado", "Connection: "+((conn != null)?"Si":"No"));
        return conn;
    }
    
    
    /**Cierra la conexión de base de datos pasada por parámetro.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param conn
     * @throws AligareException
     */
    public void closeConnection(Connection conn) throws AligareException{
    	try {
			conn.close();
		} catch (Exception e) {
			logger.traceError("closeConnection", "Error al intentar cerrar conexion.", e);
		}
    }
    
    /**Cierra la conexión de base de datos pasada por parámetros junto al ResultSet y al CallableStatement asociados a la conexión.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param conn
     * @param rs
     * @param cs
     * @throws AligareException
     */
    public void closeConnection(Connection conn, ResultSet rs, CallableStatement cs) throws AligareException{
    	try {
    		if(rs != null) {
    			rs.close();
    		}
    		
    		if(cs != null) {
    			cs.close();
    		}
    		
    		if(conn != null) {
    			conn.close();
    		}
		} catch (Exception e) {
			logger.traceError("closeConnection", "Error al intentar cerrar conexion.", e);
		}
    }
    
    /**Cierra la conexión de base de datos pasada por parámetros junto al ResultSet y al PreparedStatement asociados a la conexión.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 22-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param conn
     * @param rs
     * @param ps
     * @throws AligareException
     */
    public void closeConnection(Connection conn, ResultSet rs, PreparedStatement ps) throws AligareException{
    	try {
    		if(rs != null) {
    			rs.close();
    		}
    		
    		if(ps != null) {
    			ps.close();
    		}
    		
    		if(conn != null) {
    			conn.close();
    		}
		} catch (Exception e) {
			logger.traceError("closeConnection", "Error al intentar cerrar conexion.", e);
		}
    }
    
}
