package cl.ripley.omnicanalidad.soap;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import cl.aligare.ara.util.AligareException;

public class SoapAction {
	private String URL = null;
	private int timeout;
	private boolean trustLoaded = false;
	private String username = null;
	private String password = null;
	private final String retorno = "RETORNO";
	private final String codigo = "CODRE";
	private final String mensaje = "DESRE";
	private final String datos = "DATOS";

	/**
	 * Constructor 
	 * @param URL  url de conexion
	 * @param useSecurity  usa seguridad o no 
	 */
	public SoapAction(String URL, String username, String password) {
		this.URL = URL;
		this.username = username;
		this.password = password;
		timeout = 60000;
		sslBridge();
	}

	private void sslBridge(){
		if (!trustLoaded) {
			 try {            
		            TrustManager[] trustAllCerts = new TrustManager[] {
		                new X509TrustManager() {
		                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		                        return null;
		                    }
		                    public void checkClientTrusted(
		                        java.security.cert.X509Certificate[] certs, String authType) {
		                    }
		                    public void checkServerTrusted(
		                        java.security.cert.X509Certificate[] certs, String authType) {
		                    }
		                }
		            };
		    
		            SSLContext sc = SSLContext.getInstance("SSL");
		            sc.init(null, trustAllCerts, new java.security.SecureRandom());
		            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		        } 
		        catch (Exception e) {
		            e.printStackTrace();
		        }            
	 
			 trustLoaded = true;
		}
	}

	public String performAction(String request, String username, String password) throws AligareException {
		URL                aURL;
		HttpURLConnection aC;
        OutputStreamWriter oS;
		StringBuffer       response = null;
		//boolean            isException = false;
		try{
			aURL = new URL( URL );
	        aC = (HttpURLConnection) aURL.openConnection();
	        aC.setDoOutput( true );
	        aC.setDoInput( true );
	        aC.setRequestMethod( "POST" );
	        aC.setUseCaches( false );
	        aC.setAllowUserInteraction( false );
	        aC.setRequestProperty( "Content-Length", Integer.toString( request.length() ) );  
	        aC.setRequestProperty( "Content-Type", "text/xml; charset=utf-8" );
	        //Autenticacion
	        String authString = username + ":" + password;
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			aC.setRequestProperty("Authorization", "Basic " + authStringEnc);
	        aC.setConnectTimeout(timeout);
	        aC.setReadTimeout(timeout);
	        // Escribir mensaje
	        oS = new OutputStreamWriter( aC.getOutputStream() );
	        oS.write( request );
	        oS.flush();
	        oS.close();
	        // Leer respuesta
	        InputStream iS = null;
	        try {
	        	iS = aC.getInputStream();
	        } catch (IOException ex ) {
	        	throw new AligareException(ex);
	        	//isException = true;
	        	//iS = aC.getErrorStream();
	        }
	        response = new StringBuffer("");
	        int c;
	        while ((c = iS.read()) != -1){ 
	        	response.append((char)c);
	        }
	        iS.close();
	      
	        // Si el web service retorno error genero excepcion con el string retornado
	       /* if ( isException ){ 
	        	throw new AligareException(getValueToken("RETORNO", response.toString()));
	        }*/
		}catch(Exception ex){
			throw new AligareException(ex);
		}
        return response.toString();
	}
	
	public String getValueToken(String token, String xml) {
		 String value = null;
		 String ini = "<" + token;
		 String fin = "</" + token + ">";	
		 int posini;
		 int posfin;
	
		 posini = xml.indexOf(ini) + ini.length();
		 posini = xml.indexOf(">", posini) + 1;
		 posfin = xml.indexOf(fin);
		
		 if ( posini < 0 || posfin < 0 || posini > posfin )
			return "";
			
		 value = xml.substring(posini, posfin);
				
	 	 return value;		
	 }
	

	/**
	 * @return the strUrlEndPoint
	 */
	public String getStrUrlEndPoint() {
		return URL;
	}

	/**
	 * @param strUrlEndPoint the strUrlEndPoint to set
	 */
	public void setStrUrlEndPoint(String strUrlEndPoint) {
		this.URL = strUrlEndPoint;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public boolean isTrustLoaded() {
		return trustLoaded;
	}

	public void setTrustLoaded(boolean trustLoaded) {
		this.trustLoaded = trustLoaded;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRetorno() {
		return retorno;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public String getDatos() {
		return datos;
	}
	
}
