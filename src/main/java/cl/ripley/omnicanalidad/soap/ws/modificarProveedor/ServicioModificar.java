package cl.ripley.omnicanalidad.soap.ws.modificarProveedor;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ResultadoSAPModificacion;
import cl.ripley.omnicanalidad.soap.SoapAction;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Validaciones;

public class ServicioModificar extends SoapAction{
	
	private static final AriLog logger = new AriLog(ServicioModificar.class,"RIP16-004-SGO", PlataformaType.JAVA);

	private StringBuilder requestIni = new StringBuilder();
	private StringBuilder requestFin = new StringBuilder();
	private final String rutIni = "<RUTPR>";
	private final String rutFin = "</RUTPR>";
	private final String dirIni = "<DIRECCION>";
	private final String nomIni = "<NOMBRE1>";
	private final String nomFin = "</NOMBRE1>";
	private final String calleIni = "<CALLE>";
	private final String calleFin = "</CALLE>";
	private final String distIni = "<DISTR>";
	private final String distFin = "</DISTR>";
	private final String regIni = "<REGION>";
	private final String regFin = "</REGION>";
	private final String dirFin = "</DIRECCION>";
	private final String comIni = "<COMUNICACION>";
	private final String telIni = "<TELEF>";
	private final String telFin = "</TELEF>";
	private final String comFin = "</COMUNICACION>";
	private final String banIni = "<BANCO>";
	private final String claveIni = "<CLABA>";
	private final String claveFin = "</CLABA>";
	private final String cuentaIni = "<CUBAN>";
	private final String cuentaFin = "</CUBAN>";
	private final String tituIni = "<TITCU>";
	private final String tituFin = "</TITCU>";
	private final String banFin = "</BANCO>";
	
	public ServicioModificar(String url, String username, String password){
		super(url, username, password);
		requestIni.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ripley.cl:modificacion_proveedor:sgo\">");
		requestIni.append("<soapenv:Header/>");
		requestIni.append("<soapenv:Body>");
		requestIni.append("<urn:mt_req>");
		requestIni.append("<DATOS>");
		requestFin.append("</DATOS>");
		requestFin.append("</urn:mt_req>");
		requestFin.append("</soapenv:Body>");
		requestFin.append("</soapenv:Envelope>");
	}
	
	private String generarRequest(ResultadoSAPModificacion obj){
		return (requestIni.toString()+
				  (rutIni+obj.getRut()+rutFin)+
				  (dirIni+(
				    (nomIni+obj.getNombreProv()+nomFin)+
				    (calleIni+obj.getDireccionProv()+calleFin)+
				    (distIni+obj.getDistritoProv()+distFin)+
				    (regIni+obj.getRegionProv()+regFin)
				  )+dirFin)+
				  ((!Validaciones.validarVacio(obj.getTelefono()))?
				  (comIni+(
				    (telIni+obj.getTelefono()+telFin)
				  )+comFin):Constantes.VACIO)+
				  (banIni+(
				    (claveIni+obj.getClaveBanco()+claveFin)+
				    (cuentaIni+obj.getCuentaBank()+cuentaFin)+
				    (tituIni+obj.getTitular()+tituFin)
				  )+banFin)+
			   requestFin.toString());
	}
	
	public ResultadoSAPModificacion modificarProveedor(ResultadoSAPModificacion obj){
		logger.initTrace("modificarProveedor", "ResultadoSAPModificacion obj: "+obj.toString(), new KeyLog("RUT",obj.getRut()));
	    try {
	    	String request = generarRequest(obj);
	    	logger.traceInfo("modificarProveedor", "request: "+request);
	    	String resultado = performAction(request, getUsername(), getPassword());
	    	logger.traceInfo("modificarProveedor", "response: "+resultado);
	    	
	    	String salidaRetorno = getValueToken(getRetorno(), resultado);
	    	String cod = getValueToken(getCodigo(), salidaRetorno);
	    	obj.setCodigo((cod != null)?Integer.parseInt(cod):Constantes.NRO_MENOSUNO);
			obj.setMensaje(getValueToken(getMensaje(), salidaRetorno));
	    	
		} catch (Exception e) {
			logger.traceError("modificarProveedor", "Se ha producido un error al llamar el servicio de Modificar Proveedor: ", e);
			obj.setCodigo(Constantes.NRO_MENOSUNO);
			obj.setMensaje("Error en la ejecucion");
		}
	    logger.endTrace("modificarProveedor", "Finalizado", "ResultadoSAPModificacion obj: "+obj.toString());
		return obj;
	}
}
