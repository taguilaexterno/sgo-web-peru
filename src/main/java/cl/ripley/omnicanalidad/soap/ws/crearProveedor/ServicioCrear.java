package cl.ripley.omnicanalidad.soap.ws.crearProveedor;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ResultadoSAPCreacion;
import cl.ripley.omnicanalidad.soap.SoapAction;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Validaciones;

public class ServicioCrear extends SoapAction{
	
	private static final AriLog logger = new AriLog(ServicioCrear.class,"RIP16-004-SGO", PlataformaType.JAVA);

	private StringBuilder requestIni = new StringBuilder();
	private StringBuilder requestFin = new StringBuilder();
	private final String rutIni = "<RUTPR>";
	private final String rutFin = "</RUTPR>";
	private final String dirIni = "<DIRECCION>";
	private final String nomIni = "<NOMB1>";
	private final String nomFin = "</NOMB1>";
	private final String calleIni = "<CALL1>";
	private final String calleFin = "</CALL1>";
	private final String distIni = "<DISTR>";
	private final String distFin = "</DISTR>";
	private final String regIni = "<REGIO>";
	private final String regFin = "</REGIO>";
	private final String dirFin = "</DIRECCION>";
	private final String comIni = "<COMUNICACION>";
	private final String telexIni = "<TELEX>";
	private final String telexFin = "</TELEX>";
	private final String telfaIni = "<TELFA>";
	private final String telfaFin = "</TELFA>";
	private final String telefIni = "<TELEF>";
	private final String telefFin = "</TELEF>";
	private final String emailIni = "<EMAIL>";
	private final String emailFin = "</EMAIL>";
	private final String comFin = "</COMUNICACION>";
	private final String controlIni = "<CONTROL>";
	private final String numifIni = "<NUMIF>";
	private final String numifFin = "</NUMIF>";
	private final String controlFin = "</CONTROL>";
	private final String socIni = "<SOCIEDAD>";
	private final String banIni = "<BANCO>";
	private final String claveIni = "<CLABA>";
	private final String claveFin = "</CLABA>";
	private final String cuentaIni = "<CUBAN>";
	private final String cuentaFin = "</CUBAN>";
	private final String tituIni = "<TITCU>";
	private final String tituFin = "</TITCU>";
	private final String banFin = "</BANCO>";
	private final String socFin = "</SOCIEDAD>";
	
	public ServicioCrear(String url, String username, String password){
		super(url, username, password);
		requestIni.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ripley.cl:creacion_proveedor:sgo\">");
		requestIni.append("<soapenv:Header/>");
		requestIni.append("<soapenv:Body>");
		requestIni.append("<urn:mt_req>");
		requestIni.append("<DATOS>");
		requestFin.append("</DATOS>");
		requestFin.append("</urn:mt_req>");
		requestFin.append("</soapenv:Body>");
		requestFin.append("</soapenv:Envelope>");
	}
	
	private String generarRequest(ResultadoSAPCreacion obj){
		return (requestIni.toString()+
				  (rutIni+obj.getRut()+rutFin)+
				  (dirIni+(
				    (nomIni+obj.getNombre1()+nomFin)+
				    (calleIni+obj.getCalle1()+calleFin)+
				    (distIni+obj.getDistrito()+distFin)+
				    (regIni+obj.getRegion()+regFin)
				  )+dirFin)+
				  ((!Validaciones.validarVacio(obj.getTelefono()) || !Validaciones.validarVacio(obj.getTelefax()) ||
					!Validaciones.validarVacio(obj.getTelex()) || !Validaciones.validarVacio(obj.getEmail()))?
				  (comIni+(
				    ((!Validaciones.validarVacio(obj.getTelex()))?(telexIni+obj.getTelex()+telexFin):Constantes.VACIO)+
				    ((!Validaciones.validarVacio(obj.getTelefax()))?(telfaIni+obj.getTelefax()+telfaFin):Constantes.VACIO)+
				    ((!Validaciones.validarVacio(obj.getTelefono()))?(telefIni+obj.getTelefono()+telefFin):Constantes.VACIO)+
				    ((!Validaciones.validarVacio(obj.getEmail()))?(emailIni+obj.getEmail()+emailFin):Constantes.VACIO)
				  )+comFin):Constantes.VACIO)+
				  (controlIni+(
				    (numifIni+obj.getNumeroIF()+"-"+obj.getNumeroIFDV()+numifFin)
				  )+controlFin)+
				  (socIni+(
					  (banIni+(
					    (claveIni+obj.getClaveB()+claveFin)+
					    (cuentaIni+obj.getCuentaB()+cuentaFin)+
					    (tituIni+obj.getTitularC()+tituFin)
					  )+banFin)
				  )+socFin)+
			   requestFin.toString());
	}
	
	public ResultadoSAPCreacion crearProveedor(ResultadoSAPCreacion obj){
		logger.initTrace("crearProveedor", "ResultadoSAPCreacion obj: "+obj.toString(), new KeyLog("RUT",obj.getRut()));
	    try {
	    	String request = generarRequest(obj);
	    	logger.traceInfo("crearProveedor", "request: "+request);
	    	String resultado = performAction(request, getUsername(), getPassword());
	    	logger.traceInfo("crearProveedor", "response: "+resultado);
	    	
	    	String salidaRetorno = getValueToken(getRetorno(), resultado);
	    	String cod = getValueToken(getCodigo(), salidaRetorno);
	    	obj.setCodigo((cod != null)?Integer.parseInt(cod):Constantes.NRO_MENOSUNO);
			obj.setMensaje(getValueToken(getMensaje(), salidaRetorno));
	    	
		} catch (Exception e) {
			logger.traceError("crearProveedor", "Se ha producido un error al llamar el servicio de Crear Proveedor: ", e);
			obj.setCodigo(Constantes.NRO_MENOSUNO);
			obj.setMensaje("Error en la ejecucion");
		}
	    logger.endTrace("crearProveedor", "Finalizado", "ResultadoSAPCreacion obj: "+obj.toString());
		return obj;
	}
}
