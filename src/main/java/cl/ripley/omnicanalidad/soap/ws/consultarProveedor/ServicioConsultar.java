package cl.ripley.omnicanalidad.soap.ws.consultarProveedor;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ResultadoSAPConsulta;
import cl.ripley.omnicanalidad.soap.SoapAction;
import cl.ripley.omnicanalidad.util.Constantes;

public class ServicioConsultar extends SoapAction{
	
	private static final AriLog logger = new AriLog(ServicioConsultar.class,"RIP16-004-SGO", PlataformaType.JAVA);

	private StringBuilder requestIni = new StringBuilder();
	private StringBuilder requestFin = new StringBuilder();
	private final String proveedor = "PROVEEDOR";
	private final String folio = "FOLIO";
	private final String nombre = "NOMBRE1";
	private final String distrito = "DISTR";
	private final String region = "REGION";
	private final String calle = "CALLE";
	private final String telefono = "TELEF";
	private final String clave = "CLABA";
	private final String cuenta = "CUBAN";
	private final String titular = "TITCU";
	private final String estado = "ESTBL";
	
	
	
	public ServicioConsultar(String url, String username, String password){
		super(url, username, password);
		requestIni.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:ripley.cl:consulta_proveedor:sgo\">");
		requestIni.append("<soapenv:Header/>");
		requestIni.append("<soapenv:Body>");
		requestIni.append("<urn:mt_req>");
		requestIni.append("<DATOS>");
		requestIni.append("<RUTPR>");
		
		requestFin.append("</RUTPR>");
		requestFin.append("</DATOS>");
		requestFin.append("</urn:mt_req>");
		requestFin.append("</soapenv:Body>");
		requestFin.append("</soapenv:Envelope>");
	}
	
	private String generarRequest(String rut){
		return (requestIni.toString() + rut + requestFin.toString());
	}
	
	public ResultadoSAPConsulta consultarProveedor(String rut){
		logger.initTrace("consultarProveedor", "String rut: "+rut, new KeyLog("RUT",rut));
		ResultadoSAPConsulta res = new ResultadoSAPConsulta();
	    try {
	    	String request = generarRequest(rut);
	    	logger.traceInfo("consultarProveedor", "request: "+request);
	    	String resultado = performAction(request, getUsername(), getPassword());
	    	logger.traceInfo("consultarProveedor", "response: "+resultado);
	    	
	    	String salidaRetorno = getValueToken(getRetorno(), resultado);
	    	String cod = getValueToken(getCodigo(), salidaRetorno);
	    	res.setCodigo((cod != null)?Integer.parseInt(cod):Constantes.NRO_MENOSUNO);
			res.setMensaje(getValueToken(getMensaje(), salidaRetorno));
			if(res.getCodigo() == Constantes.NRO_CERO){
				String salidaProveedor = getValueToken(proveedor, getValueToken(getDatos(), resultado));
				res.setFolio(getValueToken(folio, salidaProveedor));
				res.setClaveBanco(getValueToken(clave, salidaProveedor));
				res.setCuentaBank(getValueToken(cuenta, salidaProveedor));
				res.setEstadoBlock(getValueToken(estado, salidaProveedor));
				res.setTitularCta(getValueToken(titular, salidaProveedor));
				res.setCalle(getValueToken(calle, salidaProveedor));
				res.setDistrito(getValueToken(distrito, salidaProveedor));
				res.setNombre1(getValueToken(nombre, salidaProveedor));
				res.setRegion(getValueToken(region, salidaProveedor));
				res.setTelefono(getValueToken(telefono, salidaProveedor));
			}
	    	
		} catch (Exception e) {
			logger.traceError("consultarProveedor", "Se ha producido un error al llamar el servicio de Consulta Proveedor: ", e);
			res.setCodigo(Constantes.NRO_MENOSUNO);
			res.setMensaje("Error en la ejecucion");
		}
	    logger.endTrace("consultarProveedor", "Finalizado", "ResultadoSAPConsulta obj: "+res.toString());
		return res;
	}
}
