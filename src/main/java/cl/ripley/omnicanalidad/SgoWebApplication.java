package cl.ripley.omnicanalidad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SgoWebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SgoWebApplication.class, args);
	}
	
}
