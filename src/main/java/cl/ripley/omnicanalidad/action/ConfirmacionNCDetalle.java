package cl.ripley.omnicanalidad.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.DetalleCompra;
import cl.ripley.omnicanalidad.bean.DetalleDespacho;
import cl.ripley.omnicanalidad.bean.DetalleOC;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.InfoSubOrdenIdRefund;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ConfirmacionNCDetalle extends ActionSupport implements UsuarioHabilitado, SessionAware {


	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(ConfirmacionNCDetalle.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	private static final String SUCCESS_BUSQUEDA = "successBusqueda";

	@Autowired
	private OrdenCompraDAO ordenCompraDAO;
//	private BigTicketDAO bigTicketDAO = new BigTicketDAOImpl();
	private Usuario usuario;
	private List<ListaAgrupaSubOrdenes> articulosConOrdenes;
	private List<ArticulosVentaOC> articulos;
	private DetalleCliente datosCliente;
	private List<DetalleDespacho> datosDespacho;
	private List<DetalleCompra> datosCompra;
	private Long oc;
	private Long estado;
	private Error error;
	private BigDecimal montoTotal;
	private Integer ncTotal;
	private Integer listaArtSize;
	private String mensaje;
	private Map<String, Object> session;
	private String mensajeError;
	private String formaPago;
	private Integer esANCTotal;
	private String chkSubOrdenes;
	private List<InfoSubOrdenIdRefund> infoSubOrdenIdRefunds;

	public Integer getEsANCTotal() {
		return esANCTotal;
	}

	public void setEsANCTotal(Integer esANCTotal) {
		this.esANCTotal = esANCTotal;
	}
		
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;

	}

	public List<ArticulosVentaOC> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<ArticulosVentaOC> articulos) {
		this.articulos = articulos;
	}

	public List<ListaAgrupaSubOrdenes> getArticulosConOrdenes() {
		return articulosConOrdenes;
	}

	public void setArticulosConOrdenes(
			List<ListaAgrupaSubOrdenes> articulosConOrdenes) {
		this.articulosConOrdenes = articulosConOrdenes;
	}

	public Long getOc() {
		return oc;
	}

	public void setOc(Long oc) {
		this.oc = oc;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Integer getNcTotal() {
		return ncTotal;
	}

	public void setNcTotal(Integer ncTotal) {
		this.ncTotal = ncTotal;
	}

	public Integer getListaArtSize() {
		return listaArtSize;
	}

	public void setListaArtSize(Integer listaArtSize) {
		this.listaArtSize = listaArtSize;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<DetalleDespacho> getDatosDespacho() {
		return datosDespacho;
	}

	public void setDatosDespacho(List<DetalleDespacho> datosDespacho) {
		this.datosDespacho = datosDespacho;
	}

	public List<DetalleCompra> getDatosCompra() {
		return datosCompra;
	}

	public void setDatosCompra(List<DetalleCompra> datosCompra) {
		this.datosCompra = datosCompra;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public DetalleCliente getDatosCliente() {
		return datosCliente;
	}

	public void setDatosCliente(DetalleCliente datosCliente) {
		this.datosCliente = datosCliente;
	}

	public String getChkSubOrdenes() {
		return chkSubOrdenes;
	}

	public void setChkSubOrdenes(String chkSubOrdenes) {
		this.chkSubOrdenes = chkSubOrdenes;
	}
	
	public List<InfoSubOrdenIdRefund> getInfoSubOrdenIdRefunds() {
		return infoSubOrdenIdRefunds;
	}

	public void setInfoSubOrdenIdRefunds(List<InfoSubOrdenIdRefund> infoSubOrdenIdRefunds) {
		this.infoSubOrdenIdRefunds = infoSubOrdenIdRefunds;
	}

	public String confirmacionNCDetalle() {
		logger.initTrace("confirmacionNCDetalle", "Ingreso a metodo");
		
		String result = null;
		esANCTotal = Constantes.NRO_CERO;
		
		articulos = ordenCompraDAO.getArticulosByOC(oc);
		
		boolean esTotal1 =	articulos.stream()
							.anyMatch(obj -> obj.getArticuloVenta().getEnTransito() != null && obj.getArticuloVenta().getEnTransito().intValue() == Constantes.NRO_UNO);
		
		if(esTotal1) {
			
			esANCTotal = Constantes.NRO_UNO;
			
		} else {
			
			boolean esTotal2 =	articulos.stream()
					.anyMatch(obj -> obj.getArticuloVenta().getEsNC() == Constantes.NRO_DOS);
			
			if(esTotal2) {
				
				esANCTotal = Constantes.NRO_DOS;
				
			}
			
		}

		listaArtSize = articulos.size();
		datosCliente = ordenCompraDAO.getDatosCliente(oc);
		DetalleOC detalleOC = ordenCompraDAO.obtenerDetalleOC(Integer.parseInt(oc.toString()));
		datosDespacho = detalleOC.getDatosDespacho();
		datosCompra = detalleOC.getDatosCompra();
		
		montoTotal =	articulos.stream()
						.map(it -> it.getArticuloVenta().getPrecio().multiply(BigDecimal.valueOf(it.getArticuloVenta().getUnidades())).subtract(it.getArticuloVenta().getMontoDescuento().abs()))
						.reduce(BigDecimal.ZERO, BigDecimal::add);
						
		mensajeError = (String) session.get("mensajeError");
		session.remove("mensajeError");
		if(estado == 0) {
			result = Constantes.DETALLE_NC;
			esANCTotal = Constantes.NRO_CERO;
//			articulosConOrdenes = bigTicketDAO.getArtMarcadosConDespachoSubOrdenes(ordenCompraDAO.getSubOrdenesByOC(oc));
			articulosConOrdenes = ordenCompraDAO.getSubOrdenesByOC(oc);
			boolean esNCTotal = articulosConOrdenes.stream()
					.flatMap(item -> item.getArticulos().stream())
					.anyMatch(item -> item.getArticuloVenta().getEsNC() == Constantes.NRO_DOS);

			if(esNCTotal) {
				
				mensajeError = "No es posible hacer nota de crédito total debido a que un producto ya está procesado.";
				esANCTotal = Constantes.NRO_UNO;
				
			}
			formaPago = ordenCompraDAO.obtenerFormaPago(oc);

		} else if(estado == 1) {
			result = Constantes.DETALLE_NC_ANULACION;
			//articulosConOrdenes = ordenCompraDAO.getSubOrdenesByOC(oc);
			esANCTotal = Constantes.NRO_CERO;
//			articulosConOrdenes = bigTicketDAO.getArtMarcadosConDespachoSubOrdenes(ordenCompraDAO.getSubOrdenesByOC(oc));
			articulosConOrdenes = ordenCompraDAO.getSubOrdenesByOC(oc);
			boolean esNCTotal = articulosConOrdenes.stream()
					.flatMap(item -> item.getArticulos().stream())
					.anyMatch(item -> item.getArticuloVenta().getEsNC() == Constantes.NRO_DOS);
			
			boolean esNCTotal2 =	articulosConOrdenes.stream()
									.flatMap(item -> item.getArticulos().stream())
									.anyMatch(item -> item.getArticuloVenta().getEsNC() == Constantes.NRO_UNO);

			if(esNCTotal) {
				
				mensajeError = "No es posible hacer nota de crédito total debido a que un producto ya está procesado.";
				esANCTotal = Constantes.NRO_UNO;
				
			} else if(esNCTotal2) {
				
				mensajeError = "No es posible hacer nota de crédito total debido a que un producto está en tránsito.";
				esANCTotal = Constantes.NRO_UNO;
				
			}
			formaPago = ordenCompraDAO.obtenerFormaPago(oc);
		} else if(estado == 83) {
			result = Constantes.SUCCESS_SIN_STOCK_TOTAL;
		} else if(estado == 84) {
			result = Constantes.SUCCESS_SIN_STOCK_PARCIAL;
		} else if(estado == 81) {
			result = Constantes.SUCCESS_PENDIENTE_STOCK;
		} else if(estado == 87) {
			result = Constantes.SUCCESS_PENDIENTE_STOCK;
		} else if(estado == 2) {
			result = Constantes.SUCCESS_NC_AMBAS;
			esANCTotal = Constantes.NRO_CERO;
			articulosConOrdenes = ordenCompraDAO.getSubOrdenesByOC(oc);
			
			//Se realiza functional Programming para hacer lo mismo con las características de java 8
			
			boolean esNCTotal = articulosConOrdenes.stream()
								.flatMap(item -> item.getArticulos().stream())
								.anyMatch(item -> item.getArticuloVenta().getEsNC() == Constantes.NRO_DOS);
			
			if(esNCTotal) {
				
				mensajeError = "No es posible hacer nota de crédito total debido a que un producto ya está procesado.";
				esANCTotal = Constantes.NRO_UNO;
				
			} else {
				
				boolean esNCTotal2 =	articulosConOrdenes.stream()
						.flatMap(item -> item.getArticulos().stream())
						.anyMatch(item -> item.getArticuloVenta().getEsNC() == Constantes.NRO_UNO);
				
				if(esNCTotal2) {
					
					mensajeError = "No es posible hacer nota de crédito total debido a que un producto está en tránsito.";
					esANCTotal = Constantes.NRO_UNO;
					
				}
				
			}
			
			
			formaPago = ordenCompraDAO.obtenerFormaPago(oc);
		} else {
			error = new Error();
			error.setCodError(-1);
			error.setErrorMsg("Vista no encontrada");
			logger.traceInfo("confirmacionNCDetalle", error.getErrorMsg());
			result = ERROR;
		}
		
		logger.endTrace("confirmacionNCDetalle", "Finalizado", "Tamaño lista articulos: " + articulos.size());
		return result;
	}

	public String confirmarNCDetalle() {
		logger.initTrace("confirmarNCDetalle", "Ingreso a metodo");
		
		List<String> articulosSelected = new ArrayList<String>();
		logger.traceInfo("confirmarNCDetalle", "infoSubOrdenIdRefunds: " + infoSubOrdenIdRefunds);
		if(articulosConOrdenes != null && !articulosConOrdenes.isEmpty() && (estado == 1 || estado == 2)) {
			if(chkSubOrdenes == null) {
				mensajeError = "No se ha seleccionado sub-órdenes para anular.";
				session.put("mensajeError", mensajeError);
				return SUCCESS_BUSQUEDA;
			}
			articulosConOrdenes = ordenCompraDAO.getSubOrdenesByOC(oc);
			//Los articulos seleccionados son todos los de las subordenes seleccionadas 
			//y además los marco con el flag esNC en 1 para no cambiar tanto la lógica
			articulosSelected =	articulosConOrdenes.stream()
								.filter(it -> Stream.of(chkSubOrdenes.split(Constantes.COMA)).anyMatch(it2 -> it.getSubOrden().equals(it2)))
								.flatMap(it -> it.getArticulos().stream())
								.collect(ArrayList<String>::new, 
										(list, it) -> {
											it.getArticuloVenta().setEsNC(Constantes.NRO_UNO);
											list.add(String.valueOf(it.getArticuloVenta().getCorrelativoItem()));
										}, 
										List::addAll);
			
		}else{
			articulosSelected =	articulos.stream()
								.filter(art -> art.getArticuloVenta().getEsNC() != null && art.getArticuloVenta().getEsNC().intValue() == 1)
								.map(art -> String.valueOf(art.getArticuloVenta().getCorrelativoItem()))
								.collect(Collectors.toCollection(ArrayList<String>::new));
		}
//		
//		if((estado == 0 || estado == 1 || estado == 2) && articulosSelected.isEmpty()) {
//			mensajeError = "Debe seleccionar al menos un artículo.";
//			session.put("mensajeError", mensajeError);
//			return SUCCESS_BUSQUEDA;
//		}
		
		if((estado == 0 || estado == 1 || estado == 2) && articulosSelected.isEmpty()) {
			mensajeError = "No se ha seleccionado órdenes para anular.";
			session.put("mensajeError", mensajeError);
			return SUCCESS_BUSQUEDA;
		}
		
		String correlativosArticulos =	articulosSelected.stream()
										.reduce(Constantes.VACIO, (actual, siguiente) -> actual += siguiente + Constantes.COMA)
										.replaceAll(Constantes.COMA + "$", Constantes.VACIO);

		Optional<List<String>> optListSelected = Optional.of(articulosSelected);
		
		//Llamo los articulos para sacar los correlativos
		List<ArticulosVentaOC> articulos2 = ordenCompraDAO.getArticulosByOC(oc);
		
		//Saco los articulos no seleccionados
		List<String> articulosNotSelected =	articulos2.stream()
											.filter(it -> optListSelected.get().stream().noneMatch(it2 -> it.getArticuloVenta().getCorrelativoItem().intValue() == Integer.parseInt(it2)))
											.map(it -> String.valueOf(it.getArticuloVenta().getCorrelativoItem()))
											.collect(Collectors.toCollection(ArrayList<String>::new));
		
		boolean resultado = Boolean.TRUE;

		//Si los correlativos est� vacio es porque no se selecciona ninguno y debo poner la marca de los articulos con un 0
		if("".equals(correlativosArticulos.trim())) {

			correlativosArticulos =	articulos2.stream()
									.map(it -> String.valueOf(it.getArticuloVenta().getCorrelativoItem()))
									.reduce(Constantes.VACIO, (actual, siguiente) -> actual += siguiente + Constantes.COMA)
									.replaceAll(Constantes.COMA + "$", Constantes.VACIO);
			
			resultado = ordenCompraDAO.updateEstadoNcArticulos(correlativosArticulos, oc, 0L);

			if(!resultado) {
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("No se pudo completar la operaci�n");
				logger.traceInfo("confirmarNCDetalle", "updateEstadoNcArticulos: " + error.getErrorMsg());
				return ERROR;
			}
			
		} else {
			//Actualizo los articulos seleccionados
			resultado = ordenCompraDAO.updateEstadoNcArticulos(correlativosArticulos, oc, 1L);

			if(!resultado) {
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("No se pudo completar la operación");
				logger.traceInfo("confirmarNCDetalle", "updateEstadoNcArticulos: " + error.getErrorMsg());
				return ERROR;
			}
			
			// NUEVO ingreso de refund
			if(articulosConOrdenes != null && !articulosConOrdenes.isEmpty() && (estado == 1 || estado == 2)) {
				List<ListaAgrupaSubOrdenes> articulosWithRefund = new ArrayList<ListaAgrupaSubOrdenes>();
				for(ListaAgrupaSubOrdenes art:articulosConOrdenes) {
					if(art != null) {
						if(infoSubOrdenIdRefunds!=null) {
							for(InfoSubOrdenIdRefund i:infoSubOrdenIdRefunds) {
								if(i != null && i.getSubOrden().equalsIgnoreCase(art.getSubOrden()) && i.getIdRefund() != null) {
									art.setIdRefund(i.getIdRefund());
									articulosWithRefund.add(art);
									break;
								}
							}
						}else {
							logger.traceInfo("confirmarNCDetalle", "infoSubOrdenIdRefunds: null, esto sucede si es una orden distinta a indicador_MKP=1 o 2, en caso contrario hay que validar la data.");
						}
					
					}
				}
				if(!articulosWithRefund.isEmpty()) {
					articulosConOrdenes.clear();
					articulosConOrdenes = articulosWithRefund;
				}
				
			
				boolean callMKPRefund =	articulosConOrdenes.stream()
										.anyMatch(obj -> obj.getSubOrden() != null && !Constantes.VACIO.equalsIgnoreCase(obj.getSubOrden())
															&& obj.getMontoTotal().compareTo(new BigDecimal(Constantes.NRO_MENOSUNO))== Constantes.NRO_UNO 
															&& obj.getIdRefund() != null && obj.getIdRefund().intValue() > Constantes.NRO_MENOSUNO);
//				for (ListaAgrupaSubOrdenes obj : articulosConOrdenes) {
//					if(obj.getSubOrden() != null && !Constantes.VACIO.equalsIgnoreCase(obj.getSubOrden())
//					   && obj.getMontoTotal() > -1 && obj.getIdRefund() != null && obj.getIdRefund().intValue() > -1){
//						callMKPRefund = true;
//						break;
//					}
//				}
				
				if(callMKPRefund){
					resultado = ordenCompraDAO.updateRefundMKP(articulosConOrdenes, oc, usuario.getUsuario());
				    
					if(!resultado) {
						error = new Error();
						error.setCodError(1);
						error.setErrorMsg("No se pudo completar la operación");
						logger.traceInfo("confirmarNCDetalle", "updateRefundMKP: " + error.getErrorMsg());
						return ERROR;
					}
				}				
			}
			
			//Actualizo los articulos no seleccionados
			correlativosArticulos = "";
			
			correlativosArticulos =	articulosNotSelected.stream()
									.reduce(Constantes.VACIO, (actual, siguiente) -> actual += siguiente + Constantes.COMA)
									.replaceAll(Constantes.COMA + "$", Constantes.VACIO);
			
			if(!"".equals(correlativosArticulos)) {
				
				resultado = ordenCompraDAO.updateEstadoNcArticulos(correlativosArticulos, oc, 0L);
				
				if(!resultado) {
					error = new Error();
					error.setCodError(1);
					error.setErrorMsg("No se pudo completar la operaci�n");
					logger.traceInfo("confirmarNCDetalle", "updateEstadoNcArticulos: " + error.getErrorMsg());
					return ERROR;
				}
				
			}
			
		}
		try{
			Integer estadoNotaVenta = articulosSelected.size() == 0 ? 1 : 0;
			boolean isCajaNull = false;
			if(estadoNotaVenta == 0 && (estado == 0 || estado == 1 || estado == 2 || ((estado == 83) && ordenCompraDAO.esRechazo(oc)) )) {
//			if(estado == 0 || estado == 1 || estado == 2 || ((estado == 83) && ordenCompraDAO.esRechazo(oc)) ) {
				//24 nc/anulacion Parcial; 23 nc/anulacion Total
				estadoNotaVenta = articulosSelected.size() < listaArtSize.intValue() ? Constantes.ESTADO_ANULACION_PARCIAL : Constantes.ESTADO_ANULACION_TOTAL;
			} else if(estadoNotaVenta == 0 && (estado == 83 || estado == 84)) {
				//13 sin stock total; 14 sin stock parcial
				estadoNotaVenta = articulosSelected.size() < listaArtSize.intValue() ? Constantes.ESTADO_SIN_STOCK_PARCIAL : Constantes.ESTADO_SIN_STOCK_TOTAL;
				isCajaNull= true;
			}
			logger.traceInfo("confirmarNCDetalle", "nuevo estado nota venta: " + estadoNotaVenta);
	
			if(estadoNotaVenta == 1 && (estado == 83 || estado == 84)) {
				isCajaNull= true;
			}
			resultado = ordenCompraDAO.updateEstadoOrdenes(oc.toString(), estadoNotaVenta, -1, isCajaNull, usuario.getUsuario(), null);
			}catch(Exception e){
				resultado = Constantes.FALSO;
			}
		if(!resultado) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo completar la operación");
			logger.traceInfo("confirmarNCDetalle", "updateEstadoOrdenes: " + error.getErrorMsg());
			return ERROR;
		}

		mensaje = "Operación exitosa.";
		session.put("mensaje", mensaje);

		logger.endTrace("confirmarNCDetalle", "Finalizado", null);
		return SUCCESS;
	}

	public String formatNumber(Object number) {
		logger.initTrace("formatNumber", "number=" + number);
		DecimalFormat format = new DecimalFormat("\u00A4#,##0");
		format.setMaximumFractionDigits(0);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("S/");
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		symbols.setMonetaryDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);

		String formateado = format.format(number);
		logger.endTrace("formatNumber", "Finalizado", "formateado=" + formateado);
		return formateado;
	}

}
