package cl.ripley.omnicanalidad.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ContadorPannel;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.impl.PannelCounterDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de la página de bienvenida de la aplicación.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Welcome extends ActionSupport implements SessionAware, UsuarioHabilitado {
	
	private static final long serialVersionUID = -1281913488611056263L;
	private static final AriLog logger = new AriLog(Welcome.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	@SuppressWarnings("rawtypes")
	private Map session;
	private Usuario usuario;
	private ContadorPannel panel;
	
	@Autowired
	private PannelCounterDAOImpl contadores;
	
	/**Método que controla las acciones del WelcomeAction de Struts de la aplicación.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String rsWelcome = ERROR;
				
		try{			
			Usuario user = (Usuario) session.get("usuario");
			if(user != null){
				int perfil = user.getCodPerfil().intValue();
				int propTipoUser = 1;
									
				rsWelcome = SUCCESS;
				if(perfil == propTipoUser){
					setUsuario(user);
					logger.traceInfo("execute", "["+user.getUsuario()+"] ingresa con perfil Administrador.");
				}else{
					setUsuario(user);
					logger.traceInfo("execute", "["+user.getUsuario()+"] ingresa con perfil Restringido.");
				}
			}
		}catch(Exception e){
			logger.traceError("Error al cargar menu de usuario", e);
			rsWelcome = ERROR;
		}
		logger.endTrace("execute", "Finalizado", "String: "+rsWelcome);
		panel = contadores.cuentaOrdenesPendientes(7);
		setPanel(panel);
		return rsWelcome;
	}

	@SuppressWarnings("rawtypes")
	public Map getSession() {
		return session;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map session) {
		this.session = session;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ContadorPannel getPanel() {
		return panel;
	}

	public void setPanel(ContadorPannel panel) {
		this.panel = panel;
	}

}
