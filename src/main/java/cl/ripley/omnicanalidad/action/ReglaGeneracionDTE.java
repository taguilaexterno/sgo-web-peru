package cl.ripley.omnicanalidad.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.ProductosDTE;
import cl.ripley.omnicanalidad.bean.ReglaPrecio;
import cl.ripley.omnicanalidad.bean.ReglaSku;
import cl.ripley.omnicanalidad.bean.ReglaTodoManual;
import cl.ripley.omnicanalidad.bean.ReglasDTEAuto;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.ReglasDTEAutoDAO;
import cl.ripley.omnicanalidad.dao.SKURestDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador Regla Generación DTE.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReglaGeneracionDTE extends ActionSupport implements UsuarioHabilitado, SessionAware {
	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(ReglaGeneracionDTE.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	private static final String UPDATEESTADO = "UPDATEESTADO";
	private static final String UPDATEFACTOR = "UPDATEFACTOR";
	private static final String SAVESKUPROD = "SAVESKUPROD";
	private static final String DELSKUPROD = "DELSKUPROD";
	
	@Autowired
	private ReglasDTEAutoDAO reglasDTEAutoDao;
	
	@Autowired
	private ReglasDTEAutoDAO productosDTEAutoDao;
	
	@Autowired
	private SKURestDAO SkuDTEAutoDao;
	
	@Autowired
	private ParametroDAO parametroDAO;

	private ReglaPrecio reglaPrecio;
	private ReglaSku reglaSku;
	private ReglaTodoManual reglaTodoManual;
	private String metodo;
	private ArrayList<ProductosDTE> productosDTE;
	private ProductosDTE productoSku;
	@SuppressWarnings("unused")
	private Usuario usuario;
	private String skuProd;
	private String seleccionados;
	
	//Mensajes
	private String mensaje;
	private Error error;
	
	//Subir archivo
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private String destPath;
	
	//scope
    private Map<String, Object> request;
	@SuppressWarnings("rawtypes")
	private Map session;
	

	public Map<String, Object> getRequest() {
		return request;
	}
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}
	
	public File getMyFile() {
		return myFile;
	}
	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}
	public String getMyFileContentType() {
		return myFileContentType;
	}
	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}
	public String getMyFileFileName() {
		return myFileFileName;
	}
	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	public String getDestPath() {
		return destPath;
	}
	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public ReglaPrecio getReglaPrecio() {
		return reglaPrecio;
	}
	public void setReglaPrecio(ReglaPrecio reglaPrecio) {
		this.reglaPrecio = reglaPrecio;
	}
	public ReglaSku getReglaSku() {
		return reglaSku;
	}
	public void setReglaSku(ReglaSku reglaSku) {
		this.reglaSku = reglaSku;
	}
	public ReglaTodoManual getReglaTodoManual() {
		return reglaTodoManual;
	}
	public void setReglaTodoManual(ReglaTodoManual reglaTodoManual) {
		this.reglaTodoManual = reglaTodoManual;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	public ArrayList<ProductosDTE> getProductosDTE() {
		return productosDTE;
	}
	public void setProductosDTE(ArrayList<ProductosDTE> productosDTE) {
		this.productosDTE = productosDTE;
	}
	public ProductosDTE getProductoSku() {
		return productoSku;
	}
	public void setProductoSku(ProductosDTE productoSku) {
		this.productoSku = productoSku;
	}
	public String getSkuProd() {
		return skuProd;
	}
	public void setSkuProd(String skuProd) {
		this.skuProd = skuProd;
	}
	@SuppressWarnings("rawtypes")
	public Map getSession() {
		return session;
	}
	@SuppressWarnings("rawtypes")
	public void setSession(Map session) {
		this.session = session;
	}
	public String getSeleccionados() {
		return seleccionados;
	}
	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	/**
	 * Reglas para generaci�n de DTE autom�tico
	 * @return
	 */
	public String reglaGeneracionDTE() {
		logger.initTrace("execute", null);
		String respuesta = SUCCESS;
		
		logger.traceInfo("Regla generacion DTE", "Ingreso metodo");
		
		/*
		 * Update estado, valor menor
		 */
		if (metodo!=null) {
		logger.traceInfo("Regla generacion DTE. Metodo : " + metodo, "inicio metodo");
		
			if (metodo != null && metodo.equalsIgnoreCase(UPDATEESTADO)) {
				boolean update = reglasDTEAutoDao.updateEstadoRegla(reglaPrecio, reglaSku, reglaTodoManual);
				logger.traceInfo("DTE Automatico update estado : " + update, "fin dao");
			} else if (metodo != null && metodo.equalsIgnoreCase(UPDATEFACTOR)) {
				boolean update = reglasDTEAutoDao.updateFactorCP(reglaPrecio.getValor(),reglaPrecio.getId(),reglaPrecio.getIdValidacion());
				logger.traceInfo("DTE Automatico update factor : " + update, "fin dao");
			}
		}
		
		ReglasDTEAuto reglasDTEAuto = reglasDTEAutoDao.getReglasDTEAuto();		
		reglaPrecio = reglasDTEAuto.getReglaPrecio();
		reglaSku = reglasDTEAuto.getReglaSku();
		reglaTodoManual = reglasDTEAuto.getReglaTodoManual();

		logger.endTrace("execute", "Finalizado", "reglaGeneracionDTE Respuesta: "+respuesta);
		return respuesta;	
	}
	
	/**
	 * Modificar Productos que pasan aprobacion manual de DTE
	 * @return
	 */
	public String reglaDTESku() {
		logger.initTrace("reglaDTESku", null);
		String respuesta = SUCCESS;
		
		if (metodo != null && metodo.equalsIgnoreCase(SAVESKUPROD)) {
			Usuario u = (Usuario) session.get("usuario");
			productoSku.setCodUsuarioCreacion(u.getCodUsuario());
			
			boolean save = productosDTEAutoDao.saveSkuProd(productoSku);
			
			if (!save) {
//				respuesta=ERROR;
				logger.traceInfo("reglaDTESku. generando mensaje de error : Error al agregar SKU.", "metodo reglaDTESku");
				addActionError("Error al agregar SKU.");
			} else {
				mensaje = "SKU agregado con �xito.";
			}			
			
			logger.traceInfo("DTE Automatico save sku producto : " + save, "fin dao");
		} else if (metodo != null && metodo.equalsIgnoreCase(DELSKUPROD)) {
			logger.traceInfo("DTE Automatico delete sku productos : " + seleccionados, "inicio metodo");
			boolean del = false;
			
			logger.traceInfo("DTE Automatico delete sku productos", "Llamada dao");
			del = productosDTEAutoDao.delSkuProd(seleccionados);	

			if (!del) {
				addActionError("Error al eliminar SKU.");
			} else {
				mensaje = "SKU eliminado con éxito.";
			}	
			
			logger.traceInfo("DTE Automatico delete sku producto : " + del, "fin dao");
		}
		
		logger.traceInfo("Regla generacion DTE", "Ingreso metodo");
		productosDTE = productosDTEAutoDao.getProductosDTE();
		
		logger.endTrace("execute", "Finalizado", "reglaDTESku Respuesta: "+respuesta);
		return respuesta;	
	}
	
	/**
	 * Agregar Producto
	 * @return
	 */
	public String consultaSkuJson() {
		logger.initTrace("addProductoSku", null);
		String respuesta = SUCCESS;
		
		Parametro param = parametroDAO.getParametro(Constantes.URL_TV_CONSULTA_SKU);
    	String urlTv = param.getValor();
		
		logger.traceInfo("consultaSkuJson. skuProd: " + skuProd, "Ingreso metodo");
		productoSku = SkuDTEAutoDao.ConsultaSKURest(skuProd, urlTv);
		
		if (productoSku == null) {
			logger.traceInfo("consultaSkuJson. generando mensaje de error : Error al consultar SKU", "metodo consultaSkuJson");
			respuesta=ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Error al consultar SKU.");
			return ERROR;
		}
		
		logger.endTrace("execute", "Finalizado", "addProductoSku Respuesta: "+respuesta);
		
		return respuesta;	
	}
	
	/**
	 * Subir archivo carga masiva de SKU
	 * @return
	 */
	public String uploadFile() {
		logger.initTrace("uploadFile", null);
		String respuesta = SUCCESS;
		int usuarioCreacion;
		String linea;
		
		try {
			
		if (myFile != null) {
			logger.traceInfo("uploadFile. Path Archivo: " + myFile.getAbsolutePath(), "Ingreso metodo");
		} else {
			logger.traceInfo("uploadFile. Path Archivo no seleccionado", "Ingreso metodo");
			addActionError("Debe seleccionar archivo de carga.");
			return respuesta;
		}
		
		//Obtener usuario creacion
		Usuario u = (Usuario) session.get("usuario");
		usuarioCreacion=u.getCodUsuario();
		
		FileReader fr = new FileReader(myFile);
	    BufferedReader br = new BufferedReader(fr);
	    String skuErrorStr = null;
	    String aux = "";
	    boolean skuErrorFlag = false;
	    while((linea = br.readLine())!=null) {
	    	//validar que sea numerico el sku
	    	long sku = Long.parseLong(linea);
	    	logger.traceInfo("DTE save sku producto. Sku Valido : " + sku, "validacion SKU");
	    	
	    	Parametro param = parametroDAO.getParametro(Constantes.URL_TV_CONSULTA_SKU);
	    	String urlTv = param.getValor();
	    	
	    	//obtener descripcion producto REST
	    	productoSku = SkuDTEAutoDao.ConsultaSKURest(String.valueOf(sku), urlTv);
	    	
			if (productoSku == null) {
				aux = skuErrorStr==null?" ":skuErrorStr+", ";
				skuErrorStr=aux+sku;
				skuErrorFlag=true;
			} else {
		    	productoSku.setCodUsuarioCreacion(usuarioCreacion);
				boolean save = productosDTEAutoDao.saveSkuProd(productoSku);
				logger.traceInfo("DTE save sku producto : " + save, "fin dao");
			}

		}
	    //Cerrar buffer
	    br.close();
	    
	    //En caso de que caiga algun sku se muestra mensaje de error con el codigo articulo
	    if (skuErrorFlag) {
	    	logger.traceInfo("ReglaGeneracionDTE. Error al consultar los siguientes SKU :" + skuErrorStr, "metodo uploadFile");
	    	addActionError("Error al consultar los siguientes SKU :" + skuErrorStr);
	    }
		
		} catch (Exception e) {
			logger.traceInfo("ERROR. lectura archivo", "procesar archivo sku : " + e.getMessage());
			addActionError("Error al cargar archivo.");
			return respuesta;
		} finally {			
		    //Se realiza nuevamente la busqueda de SKU
			productosDTE = productosDTEAutoDao.getProductosDTE();
		}
		
     return respuesta;
	}

}
