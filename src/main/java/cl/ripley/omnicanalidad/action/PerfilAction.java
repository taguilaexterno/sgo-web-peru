package cl.ripley.omnicanalidad.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Perfil;
import cl.ripley.omnicanalidad.bean.Permiso;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ModulosDAO;
import cl.ripley.omnicanalidad.dao.PerfilesDAO;
import cl.ripley.omnicanalidad.dao.PermisosDAO;
import cl.ripley.omnicanalidad.dao.UsuarioDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Action para administracion de perfiles
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 09-08-2016
 * Versiones:
 * <ul>
 *  <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PerfilAction extends ActionSupport implements UsuarioHabilitado, SessionAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AriLog logger = new AriLog(PerfilAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private ModulosDAO modulosDAO;

	@Autowired
	private PermisosDAO permisosDAO;

	@Autowired
	private PerfilesDAO perfilesDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	private Perfil perfilBean;

	private List<Modulo> modulos;

	private List<Permiso> permisos;

	private Error error;

	private String mensaje;

	private Map<Perfil, List<Usuario>> perfilesPorUsuarioMap;

	private Long codPerfil;
	
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	private Map<String, Object> session;
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		
	}

	public Perfil getPerfilBean() {
		return perfilBean;
	}

	public void setPerfilBean(Perfil perfilBean) {
		this.perfilBean = perfilBean;
	}

	public List<Modulo> getModulos() {
		return modulos;
	}

	public void setModulos(List<Modulo> modulos) {
		this.modulos = modulos;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public List<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Map<Perfil, List<Usuario>> getPerfilesPorUsuarioMap() {
		return perfilesPorUsuarioMap;
	}

	public void setPerfilesPorUsuarioMap(Map<Perfil, List<Usuario>> perfilesPorUsuarioMap) {
		this.perfilesPorUsuarioMap = perfilesPorUsuarioMap;
	}

	public Long getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(Long codPerfil) {
		this.codPerfil = codPerfil;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	/**Carga formulario para crear perfil
	 * Versiones:
	 * <ul>
	 * <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	public String cargarForm() {
		logger.initTrace("cargarForm", "Ingreso a metodo");

		//obtengo modulos para cargar en la pagina
		modulos = modulosDAO.getModulos();

		if(modulos == null || modulos.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontraron m�dulos");
			logger.traceInfo("cargarForm", error.getErrorMsg());
			return ERROR;
		}

		permisos = permisosDAO.getPermisos();

		if(permisos == null || permisos.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontraron permisos");
			logger.traceInfo("cargarForm", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("cargarForm", "Finalizado", SUCCESS);
		return SUCCESS;
	}

	/**Guarda nuevo perfil
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	public String guardarPerfil() {
		logger.initTrace("guardarPerfil", "Ingreso a metodo");

		//Guardo perfil nuevo
		boolean result = perfilesDAO.guardarPerfilNuevo(perfilBean);

		if(!result) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Problema al guardar perfil.");
			return ERROR;
		}

		mensaje = "Perfil creado con �xito.";
		session.put("mensaje", mensaje);

		logger.endTrace("guardarPerfil", "Finalizado", SUCCESS);
		return SUCCESS;
	}

	/**Obtiene lista de perfiles
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	public String listaPerfiles() {
		logger.initTrace("listaPerfiles", "Ingreso a metodo");

		//Obtengo perfiles del sistema
		List<Perfil> perfiles = null;

		if(perfilBean != null && perfilBean.getNombrePerfil() != null && !"".equals(perfilBean.getNombrePerfil())) {
			perfiles = perfilesDAO.getPerfilByNombrePerfil(perfilBean.getNombrePerfil());
		} else {
			perfiles = perfilesDAO.getPerfiles();
		}

		if(perfiles == null || perfiles.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encotnraron perfiles");
			return ERROR;
		}

		perfilesPorUsuarioMap = new LinkedHashMap<Perfil, List<Usuario>>();

		//Recorro los perfiles para obtener los usuarios por cada perfil
		for(Perfil perf : perfiles) {
			perfilesPorUsuarioMap.put(perf, usuarioDAO.getUsuarios(perf.getId()));
		}


		//Busco modulos para editar el perfil
		modulos = modulosDAO.getModulos();

		if(modulos == null || modulos.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encotnraron modulos");
			return ERROR;
		}

		//Obtengo permisos del sistema
		permisos = permisosDAO.getPermisos();

		if(permisos == null || permisos.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encotnraron permisos");
			return ERROR;
		}
		
		mensaje = (String) session.get("mensaje");
		session.remove("mensaje");

		logger.endTrace("listaPerfiles", "Finalizado", SUCCESS);

		return SUCCESS;
	}

	/**Obtiene perfil por id para retornar objeto como JSON
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	public String getPerfilByIdJson() {
		logger.initTrace("getPerfilByIdJson", "Ingreso a metodo; codPerfil=" + codPerfil);

		perfilBean = perfilesDAO.getPerfilById(codPerfil);

		if(perfilBean == null) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontr� el perfil buscado");
			return ERROR;
		}

		logger.endTrace("getPerfilByIdJson", "Finalizado", "Perfil: " + perfilBean);

		return SUCCESS;
	}

	/**Actualiza Perfil
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	public String actualizarPerfil() {
		logger.initTrace("actualizarPerfil", "Ingreso a metodo; Perfil: " + perfilBean);

		
		boolean result = Boolean.TRUE;
		
		result = perfilesDAO.updPerfil(perfilBean);
		
		if(!result) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Problema al actualizar perfil " + perfilBean.getNombrePerfil());
			logger.traceInfo("actualizarPerfil", error.getErrorMsg());
			return ERROR;
		}
		
		for(Modulo mod : perfilBean.getModulos()) {
			result = modulosDAO.updPermisosModulosByPerfil(perfilBean.getId(), mod.getId(), mod.getPermiso().getId());
			
			if(!result) {
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("Problema al actualizar perfil " + perfilBean.getNombrePerfil() + " -> Modulo: " + mod.getNombreModulo());
				logger.traceInfo("actualizarPerfil", error.getErrorMsg());
				return ERROR;
			}
		}
		
		mensaje = "Perfil " + perfilBean.getNombrePerfil() + " actualizado con �xito.";
		
		perfilBean.setNombrePerfil(null);
		
		listaPerfiles();

		logger.endTrace("actualizarPerfil", "Finalizado", "Perfil: " + perfilBean.getNombrePerfil());

		return SUCCESS;
	}

}
