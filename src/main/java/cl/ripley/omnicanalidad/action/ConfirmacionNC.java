package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ConfirmacionNC extends ActionSupport implements SessionAware, UsuarioHabilitado {
	private static final AriLog logger = new AriLog(ConfirmacionNC.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings("rawtypes")
	private Map session;
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	private int estado;
	private int nroOrdenes;
	
	private List<OrdenesDeCompra> ordenesCompra;
	
	private Long oc;
	private String mensaje;

	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = ERROR;
		Error errorBean = new Error();
		
		logger.traceInfo("execute", "Estado consultado: "+estado);
		
		String[] cajasBoletas = parametroDao.getParametro(Constantes.NROS_CAJAS_BOLETAS).getValor().split(Constantes.COMA);
		String[] cajasNc = parametroDao.getParametro(Constantes.NROS_CAJAS_NC).getValor().split(Constantes.COMA);
		Integer sucursal = new Integer(parametroDao.getParametro("SUCURSAL").getValor());
		
		if (estado == Constantes.ESTADO_SIN_STOCK_NC_TOTAL){
			ordenesCompra = ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_TOTAL),null, oc);
			
			ordenesCompra =	Stream.of(cajasNc)
							.collect(() -> ordenesCompra,
									(ordenes, valor) -> ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_TOTAL),null, oc)),
									List::addAll);
			
//			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc));
//			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(nroCajaNC, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc));
			respuesta = Constantes.SUCCESS_SIN_STOCK_TOTAL;
		} else if (estado == Constantes.ESTADO_SIN_STOCK_NC_PARCIAL){
			ordenesCompra = ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc);
			
			ordenesCompra =	Stream.of(cajasNc)
							.collect(() -> ordenesCompra,
									(ordenes, valor) -> ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc)),
									List::addAll);
			
			respuesta = Constantes.SUCCESS_SIN_STOCK_PARCIAL;
		} else
		if (estado == Constantes.ESTADO_SIN_STOCK_NC){
			ordenesCompra = ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_TOTAL),null, oc);
			
			ordenesCompra =	Stream.of(cajasNc)
							.collect(() -> ordenesCompra,
									(ordenes, valor) -> {
										ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_TOTAL),null, oc));
										ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc));
									},
									List::addAll);
			
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_SIN_STOCK_NC_PARCIAL),null, oc));
			respuesta = Constantes.SUCCESS_SIN_STOCK;
		} else
		if (estado == Constantes.ESTADO_PENDIENTE_STOCK){
			ordenesCompra = ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_PENDIENTE_STOCK_MP_NO_SEGURO),null, oc);
			
			ordenesCompra =	Stream.of(cajasBoletas)
							.collect(() -> ordenesCompra,
									(ordenes, valor) -> {
										ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_PENDIENTE_STOCK_MP_NO_SEGURO),null, oc));
										ordenes.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, new Integer(Constantes.ESTADO_PENDIENTE_STOCK_MP_SEGURO),null, oc));
									},
									List::addAll);
			
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_PENDIENTE_STOCK_MP_SEGURO),null, oc));

			respuesta = Constantes.SUCCESS_PENDIENTE_STOCK;
		} else
		
		//SUCCESS_NOTA_CREDITO
		if (estado == Constantes.ESTADO_INDICADOR_MKP_RIPLEY){
			
			ordenesCompra =	Stream.of(cajasBoletas)
							.collect(ArrayList<OrdenesDeCompra>::new,
									(ordenes, valor) -> ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, Integer.valueOf(Constantes.ESTADO_RECHAZO_MANUAL), Integer.valueOf(Constantes.ESTADO_INDICADOR_MKP_RIPLEY), oc),
									List::addAll);
			
			respuesta = Constantes.SUCCESS_NOTA_CREDITO;
		} else
		//SUCCESS_ANULACIONES
		if (estado == Constantes.ESTADO_INDICADOR_MKP_MKP){
			
			ordenesCompra =	Stream.of(cajasBoletas)
							.collect(ArrayList<OrdenesDeCompra>::new,
									(ordenes, valor) -> ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, Integer.valueOf(Constantes.ESTADO_RECHAZO_MANUAL), Integer.valueOf(Constantes.ESTADO_INDICADOR_MKP_MKP), oc),
									List::addAll);
			
//			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(nroCaja, sucursal, new Integer(Constantes.ESTADO_RECHAZO_MANUAL),new Integer(Constantes.ESTADO_INDICADOR_MKP_CLUB), oc));
			
			respuesta = Constantes.SUCCESS_ANULACIONES;
		} else
		//SUCCESS_NC_AMBAS
		if (estado == Constantes.ESTADO_INDICADOR_MKP_MIXTA){
			
			ordenesCompra =	Stream.of(cajasBoletas)
							.collect(ArrayList<OrdenesDeCompra>::new,
									(ordenes, valor) -> ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(valor), sucursal, Integer.valueOf(Constantes.ESTADO_RECHAZO_MANUAL), Integer.valueOf(Constantes.ESTADO_INDICADOR_MKP_MIXTA), oc),
									List::addAll);
			
			
			respuesta = Constantes.SUCCESS_NC_AMBAS;
		}
		
		else {
			errorBean.setCodError(2);
			errorBean.setErrorMsg("OPCION DE MENU INEXISTENTE");
			logger.traceInfo("execute", "Estado inexistente, estado consultado: "+estado);
			respuesta = ERROR;
		}

		setOrdenesCompra(ordenesCompra);
		
		nroOrdenes = ordenesCompra.size();
		
		mensaje = (String) session.get("mensaje");
		session.remove("mensaje");
		
		logger.endTrace("execute", "Finalizado", "ordenesCompra: "+ordenesCompra.size()+" Respuesta: "+respuesta);
		return respuesta;
	}
	
	public String buscarNC() {
		logger.initTrace("buscarNC", null);
		String respuesta = ERROR;
		Error errorBean = new Error();
		
		logger.traceInfo("buscarNC", "Estado consultado: "+estado);
		
		Integer sucursal = new Integer(parametroDao.getParametro("SUCURSAL").getValor());

				
		ordenesCompra = new ArrayList<OrdenesDeCompra>(20);
		
		//SUCCESS_NOTA_CREDITO
		if (estado == Constantes.ESTADO_INDICADOR_MKP_RIPLEY){
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_BOLETA,new Integer(Constantes.ESTADO_INDICADOR_MKP_RIPLEY), oc));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_NC_PARCIAL,new Integer(Constantes.ESTADO_INDICADOR_MKP_RIPLEY), oc));
			
			respuesta = Constantes.SUCCESS_NOTA_CREDITO;
		} else
		//SUCCESS_ANULACIONES
		if (estado == Constantes.ESTADO_INDICADOR_MKP_MKP){
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_BOLETA,new Integer(Constantes.ESTADO_INDICADOR_MKP_MKP), oc));
//			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_BOLETA,new Integer(Constantes.ESTADO_INDICADOR_MKP_CLUB), oc));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_NC_PARCIAL,new Integer(Constantes.ESTADO_INDICADOR_MKP_MKP), oc));
//			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_NC_PARCIAL,new Integer(Constantes.ESTADO_INDICADOR_MKP_CLUB), oc));
			
			respuesta = Constantes.SUCCESS_ANULACIONES;
		} else
		//SUCCESS_NC_AMBAS
		if (estado == Constantes.ESTADO_INDICADOR_MKP_MIXTA){
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_BOLETA,new Integer(Constantes.ESTADO_INDICADOR_MKP_MIXTA), oc));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(Constantes.NO_CAJA, sucursal, Constantes.ESTADO_NC_PARCIAL,new Integer(Constantes.ESTADO_INDICADOR_MKP_MIXTA), oc));
			
			respuesta = Constantes.SUCCESS_NC_AMBAS;
		}
		
		else {
			errorBean.setCodError(2);
			errorBean.setErrorMsg("OPCION DE MENU INEXISTENTE");
			logger.traceInfo("buscarNC", "Estado inexistente, estado consultado: "+estado);
			respuesta = ERROR;
		}

		setOrdenesCompra(ordenesCompra);
		
		nroOrdenes = ordenesCompra.size();
		
		mensaje = (String) session.get("mensaje");
		session.remove("mensaje");
		
		logger.endTrace("buscarNC", "Finalizado", "ordenesCompra: "+ordenesCompra.size()+" Respuesta: "+respuesta);
		return respuesta;
		
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<OrdenesDeCompra> getOrdenesCompra() {
		return ordenesCompra;
	}

	public void setOrdenesCompra(List<OrdenesDeCompra> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getNroOrdenes() {
		return nroOrdenes;
	}

	public void setNroOrdenes(int nroOrdenes) {
		this.nroOrdenes = nroOrdenes;
	}

	public Long getOc() {
		return oc;
	}

	public void setOc(Long oc) {
		this.oc = oc;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
