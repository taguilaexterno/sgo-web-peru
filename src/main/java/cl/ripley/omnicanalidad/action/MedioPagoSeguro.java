package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ReglasMPSeguro;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ReglasMPSeguroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de Medio de Pago Seguro
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MedioPagoSeguro extends ActionSupport implements UsuarioHabilitado {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(MedioPagoSeguro.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReglasMPSeguroDAO reglasMpSeguroDao;
	
	private ArrayList<ReglasMPSeguro> reglasMpSeguro;
	private ReglasMPSeguro reglaMP;
	private final String UPDATEESTADO = "UPDATEESTADO";
	private final String UPDATEVALMENOR = "UPDATEVALMENOR";
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		
	}
	
	public ArrayList<ReglasMPSeguro> getReglasMpSeguro() {
		return reglasMpSeguro;
	}

	public void setReglasMpSeguro(ArrayList<ReglasMPSeguro> reglasMpSeguro) {
		this.reglasMpSeguro = reglasMpSeguro;
	}
	
	public ReglasMPSeguro getReglaMP() {
		return reglaMP;
	}

	public void setReglaMP(ReglasMPSeguro reglaMP) {
		this.reglaMP = reglaMP;
	}

	
	/**Carga administrador de medios de pagos seguros.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String admMPSeguro() {
		
		logger.initTrace("execute", null);
		String respuesta = SUCCESS;
		
		logger.traceInfo("Medio Pago Seguro", "Ingreso metodo");
		

		/*
		 * Update estado, valor menor
		 */
		if (reglaMP!=null) {
		logger.traceInfo("Medio Pago Seguro. Metodo : " + reglaMP.getMetodo(), "inicio metodo");
		
			if (reglaMP != null && reglaMP.getMetodo().equalsIgnoreCase(UPDATEESTADO)) {
				int estado = reglaMP.getEstado()==Constantes.ESTADO_ACTIVO?Constantes.ESTADO_INACTIVO:Constantes.ESTADO_ACTIVO;
				logger.traceInfo("Regla MP : id= " + reglaMP.getId() + " idvalid= " + reglaMP.getIdValidacion() + " estado= " + estado, "inicio");
				boolean update = reglasMpSeguroDao.updateEstado(reglaMP.getId(), reglaMP.getIdValidacion(), estado);
				logger.traceInfo("Medio Pago Seguro update estado : " + update, "fin dao");
			} else if (reglaMP != null && reglaMP.getMetodo().equalsIgnoreCase(UPDATEVALMENOR)) {
				boolean update = true;
				reglasMpSeguroDao.updateValMenor(reglaMP.getId(), reglaMP.getIdValidacion(), reglaMP.getValor());
				logger.traceInfo("Medio Pago Seguro update valor menor : " + update, "fin dao");
			}
		}
		
		reglasMpSeguro = reglasMpSeguroDao.getReglasMPSeguro();
			
		
		logger.endTrace("execute", "Finalizado", "reglasMpSeguro: "+reglasMpSeguro.size()+" Respuesta: "+respuesta);
		return respuesta;
	}

}
