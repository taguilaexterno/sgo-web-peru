package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EnviarVoucherBoletaAction extends ActionSupport implements UsuarioHabilitado, SessionAware{
	
	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(EnviarVoucherBoletaAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	private Usuario usuario;
	private int tipoEjecucion;
	private String mensaje;
	private Map<String, Object> session;
	private String mensajeError;

	private Error error;
	
	private int oc;
	private int estado;
	private String datoBoleta;
	private String urlDoce;
	private String folio;
	private int tipoDoc;
	private int sucursal;

	public int getSucursal() {
		return sucursal;
	}

	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
/*
	public Map<String, Object> getSession() {
		return session;
	}
*/
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public int getOc() {
		return oc;
	}

	public void setOc(int oc) {
		this.oc = oc;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public int getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(int tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getDatoBoleta() {
		return datoBoleta;
	}

	public void setDatoBoleta(String datoBoleta) {
		this.datoBoleta = datoBoleta;
	}

	public String getUrlDoce() {
		return urlDoce;
	}

	public void seturlDoce(String urlDoce) {
		this.urlDoce = urlDoce;
	}

	public int getTipoEjecucion() {
		return tipoEjecucion;
	}

	public void setTipoEjecucion(int tipoEjecucion) {
		this.tipoEjecucion = tipoEjecucion;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String execute() {
		logger.initTrace("execute", "Ingresa EnviarVoucherBoletaAction");
		String result = ERROR;

        if (datoBoleta.isEmpty() ) {
			mensajeError = "OC no existe";
			session.put("mensajeError", mensajeError);
			result = SUCCESS;
			return result;        	
        }
		if (datoBoleta.length() != 20) {
			mensajeError = "OC no existe";
			session.put("mensajeError", mensajeError);
			result = SUCCESS;
			return result;
		}

		oc = Integer.parseInt(datoBoleta.substring(6,16));
		sucursal = Integer.parseInt(datoBoleta.substring(0,3));
		estado = Integer.parseInt(datoBoleta.substring(18,19));
		logger.traceInfo("execute", "Prueba OC: "+oc+" Estado: "+estado + " Sucursal:" + sucursal);
		
		switch(tipoEjecucion) {
		case 1:
			result = SUCCESS;
			break;
		case 2:
			logger.traceInfo("execute", "Desde el formulario: VoucherBoleta OC: "+oc+" ");
			
			List<OrdenesDeCompra> oCompraAux = new ArrayList<OrdenesDeCompra>();
			OrdenesDeCompra oCompra = null;
			
			oCompraAux.addAll(ordenesCompraDAO.obtenerOrdenes(-1, sucursal, estado, null, new Long(oc)));

			logger.traceInfo("EnviarVoucherBoletaAction", "Size: "+oCompraAux.size());
			if (oCompraAux != null && oCompraAux.size() > 0) {
				oCompra = oCompraAux.get(0);
			    tipoDoc= oCompra.getNotaVenta().getTipoDoc();
			    folio= oCompra.getNotaVenta().getFolioSii();
				urlDoce= oCompra.getNotaVenta().getUrlDoce();
				logger.traceInfo("EnviarVoucherBoletaAction", "Tipo Doc: "+tipoDoc);
			}

			
			if(oCompra == null || oCompra.getNotaVenta().getCorrelativoVenta() == null || oCompra.getNotaVenta().getUrlDoce() == null) {
				mensajeError = "Boleta no existe OC: "+ oc+ " Estado: "+estado;
				session.put("mensajeError", mensajeError);
				result = SUCCESS;
				break;
			} else {
				mensaje = "La Boleta se descarga en forma exitosa URL: "+oCompra.getNotaVenta().getUrlDoce();
				session.put("mensaje", mensaje);

			}
			
			result = SUCCESS;
			break;

		default:
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Vista no definida");
			break;
		}
		
		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		
		return result;

	}
		
}
