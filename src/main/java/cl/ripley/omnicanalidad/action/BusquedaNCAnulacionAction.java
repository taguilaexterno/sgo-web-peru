package cl.ripley.omnicanalidad.action;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de búsqueda NC Anulación.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BusquedaNCAnulacionAction extends ActionSupport implements UsuarioHabilitado {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(BusquedaNCAnulacionAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	private Usuario usuario;
	
	private Long estado;
	
	private Error error;
	
	public String execute() {
		logger.initTrace("execute", "Long estado: " + estado);
		
		String result = ERROR;
		
		switch(estado.intValue()) {
		case Constantes.ESTADO_INDICADOR_MKP_RIPLEY:
			result = Constantes.SUCCESS_BUSQUEDA_NC;
			break;
		case Constantes.ESTADO_INDICADOR_MKP_MKP:
			result = Constantes.SUCCESS_BUSQUEDA_ANULACION;
			break;
		case Constantes.ESTADO_INDICADOR_MKP_MIXTA:
			result = Constantes.SUCCESS_BUSQUEDA_MIXTA;
			break;
		default:
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Vista no definida");
			break;
		}
		
		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		
		return result;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}
	
	

}
