package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.StrBuilder;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.ClienteClub;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ClubDAO;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.util.ClienteSMTP;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.Util;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReenviarVoucherAction extends ActionSupport implements UsuarioHabilitado, SessionAware{
	
	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(ReenviarVoucherAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	@Autowired
	private VoucherTemplateDAO templateDao;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private ClubDAO clubDAO;
	
	private Usuario usuario;
	private int tipoEjecucion;
	private String mensaje;
	private Map<String, Object> session;
	private String mensajeError;

	private Error error;
	
	private int oc;
	private String email;
	private int tipoVoucher;
	
	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
/*
	public Map<String, Object> getSession() {
		return session;
	}
*/
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public int getOc() {
		return oc;
	}

	public void setOc(int oc) {
		this.oc = oc;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTipoEjecucion() {
		return tipoEjecucion;
	}

	public void setTipoEjecucion(int tipoEjecucion) {
		this.tipoEjecucion = tipoEjecucion;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public int getTipoVoucher() {
		return tipoVoucher;
	}

	public void setTipoVoucher(int tipoVoucher) {
		this.tipoVoucher = tipoVoucher;
	}

	public String execute() {
		logger.initTrace("execute", "Ingreso ReenviarVoucherAction");
		String result = ERROR;
		
		switch(tipoEjecucion) {
		case 1:
			result = SUCCESS;
			break;
		case 2:
			logger.traceInfo("execute", "Desde el formulario: TipoVoucher: "+tipoVoucher+" OC: "+oc+" E-Mail: "+email);
			
			List<OrdenesDeCompra> oCompraAux = new ArrayList<OrdenesDeCompra>();
			OrdenesDeCompra oCompra = null;
			
			if (tipoVoucher == 1){
				for (int i=2 ; i<=4; i++) {
					oCompraAux.addAll(ordenesCompraDAO.obtenerOrdenes(-1, 39, i, null, new Long(oc))); //-1 todas las cajas
				}
			} else {
				for (int i=3 ; i<=4; i++) {
					oCompraAux.addAll(ordenesCompraDAO.obtenerOrdenes(-1, 39, i, null, new Long(oc))); //-1 todas las cajas
				}
			}

			logger.traceInfo("ReenviarVoucherAction", "Size: "+oCompraAux.size());
			if (oCompraAux != null && oCompraAux.size() > 0) {
				oCompra = oCompraAux.get(0);
			}

			if(oCompra == null || oCompra.getNotaVenta().getCorrelativoVenta() == null) {
				addActionError("Error, OC no existe");
				mensajeError = "OC no existe";
				session.put("mensajeError", mensajeError);
				result = SUCCESS;
				break;
			} else {
				//Se obtuvo OC, se buscan los articulos
				List<ArticulosVentaOC> articulos = ordenesCompraDAO.getArticulosByOC(new Long(oc));
				logger.traceInfo("ReenviarVoucherAction", "articulosOC.size() : " + articulos.size());
				oCompra.setArticulosVenta(articulos);
				
				List<IdentificadorMarketplace> identificadorMarketplace = ordenesCompraDAO.obtenerIdentificadorMarketplace(oc);
				logger.traceInfo("ReenviarVoucherAction", "identificadorMarketplace.size() : " + identificadorMarketplace.size());
				oCompra.setIdentificadorMarketplace(identificadorMarketplace);
				
				//Generar Voucher
				/*
				if (tipoVoucher == 1){
					if (enviarEmailRecaudacion(oCompra, articulos, oCompra.getNotaVenta().getCorrelativoBoleta().toString(), email)){
						mensaje = "Correo de recaudaci�n enviado en forma exitosa.";
						session.put("mensaje", mensaje);
											
					}else{
						mensajeError = "Error al env�ar correo de recaudaci�n. Favor verificar.";
						session.put("mensajeError", mensajeError);
					}
				} else {
					if (enviarEmailAnulacion(oCompra, articulos, oCompra.getNotaVenta().getCorrelativoBoleta().toString(), email)){
						mensaje = "Correo de anulaci�n enviado en forma exitosa.<br>Tener en cuenta que solo se env�a la �ltima nota de credito realizada a la OC.";
						session.put("mensaje", mensaje);
											
					}else{
						mensajeError = "Error al env�ar correo de anulaci�n. Favor verificar.";
						session.put("mensajeError", mensajeError);
					}
				}
				*/
				if (enviarEmail(oCompra, email, tipoVoucher)){
					if (tipoVoucher == 1){
						mensaje = "Correo de recaudaci�n enviado en forma exitosa.";
					} else {
						mensaje = "Correo de anulaci�n enviado en forma exitosa.<br>Tener en cuenta que solo se env�a la �ltima nota de credito realizada a la OC.";
					}
					session.put("mensaje", mensaje);
				} else {
					if (tipoVoucher == 1){
						mensajeError = "Error al env�ar correo de recaudaci�n. Favor verificar.";
					} else {
						mensajeError = "Error al env�ar correo de anulaci�n. Favor verificar.";
					}
					session.put("mensajeError", mensajeError);
				}
			}
			
			result = SUCCESS;
			break;

		default:
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Vista no definida");
			break;
		}
		
		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		
		return result;

	}
	
	public boolean enviarEmail(OrdenesDeCompra trama, String email, int tipoVoucher) {
		boolean resultado = false;
		TemplateVoucher template = new TemplateVoucher();
		if (tipoVoucher == 1){
			if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
				template = templateDao.getTemplateByLlave("VOUCHER_RECA");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmail", "Correo llave 'VOUCHER_RECA' no se encontr� o est� desactivado.");
					return false;
				}
				resultado = enviarEmailRecaudacionCDF(trama, email, template);
			} else {
				template = templateDao.getTemplateByLlave("VOUCHER_MKP");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmail", "Correo llave 'VOUCHER_MKP' no se encontr� o est� desactivado.");
					return false;
				}
				resultado = enviarEmailRecaudacionMKP(trama, email, template);
			}
		} else {
			if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
				template = templateDao.getTemplateByLlave("VOUCHER_ANULA");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmail", "Correo llave 'VOUCHER_ANULA' no se encontr� o est� desactivado.");
					return false;
				}
				resultado = enviarEmailAnulacionCDF(trama, email, template);
			} else {
				template = templateDao.getTemplateByLlave("ANULACION_MKP");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmail", "Correo llave 'ANULACION_MKP' no se encontr� o est� desactivado.");
					return false;
				}
				resultado = enviarEmailAnulacionMKP(trama, email, template);
			}
		}
		return resultado;
	}	

	private StrBuilder getCodigoUnicoGrilla(String sucursal, String caja, int nroTransaccion){
		StrBuilder respuesta = new StrBuilder();
		
		respuesta.append("NU");
		String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String fecha = FechaUtil.getCurrentDateTimeSimpleString();
		//String fecha = cajaVirtual.sysdateFromDual(Constantes.FECHA_YYMMDD_HHMISS);
		String fechaArr[] = fecha.split(Constantes.SPACE);
		String codigoCaja = Util.rellenarString(caja, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String transaccion = Util.rellenarString(nroTransaccion+Constantes.VACIO, Constantes.NRO_SEIS, Constantes.VERDADERO, Constantes.CHAR_CERO);
		
		respuesta.append(codigoSucursal).append(Constantes.GUION).append(codigoCaja).append(Constantes.GUION).append(transaccion);
		respuesta.append(Constantes.GUION).append(fechaArr[0]).append(Constantes.GUION).append(fechaArr[1]);
		
		return respuesta;
	}

	private StrBuilder getGlosaAcepto(OrdenesDeCompra trama){
		StrBuilder respuesta = new StrBuilder();
		
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			respuesta.append(Constantes.TRAMA_PLAT_TRE_3).appendNewLine();
		}
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_1).appendNewLine();
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_2).appendNewLine();
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_3).appendNewLine();
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
/*
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
*/
			respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR).appendNewLine();
		}
		
		return respuesta;
	}
	
	public boolean enviarEmailRecaudacion(OrdenesDeCompra ordenesDeCompra, List<ArticulosVentaOC> articulosOC, String nroVoucher, String email) {
		boolean resultado = false;

		logger.traceInfo("enviarEmailRecaudacion", "Inicio Metodo", 
				new KeyLog("OC: ",ordenesDeCompra.getNotaVenta().getCorrelativoVenta().toString()),
				new KeyLog("Mail: ", email));
		
		TemplateVoucher template = new TemplateVoucher();
		ClienteClub clienteClub=null;
		try {

			if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
				template = templateDao.getTemplateByLlave("VOUCHER_RECA");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmailRecaudacion", "Correo llave 'VOUCHER_RECA' no se encontr� o est� desactivado.");
					return false;
				}
			} else {
				template = templateDao.getTemplateByLlave("VOUCHER_MKP");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmailRecaudacion", "Correo llave 'VOUCHER_MKP' no se encontr� o est� desactivado.");
					return false;
				}
			}
			
			/*
			if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
				template = templateDao.getTemplateByLlave("VOUCHER_RECA");
			} else {
				template = templateDao.getTemplateByLlave("VOUCHER_MKP");
			}
			if (template.getLlave() == null){
				logger.traceInfo("enviarEmailRecaudacion", "template.getLlave == null"); 
				return false;
			}
			*/
			
			String asunto = "VOUCHER RECAUDACION OC: "+ordenesDeCompra.getNotaVenta().getCorrelativoVenta();
			
			String destinatario = email;
	
			String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();
			String nombreCliente = ordenesDeCompra.getDespacho().getNombreCliente();
	
			
			String tipoTransaccion = "";
			if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
				tipoTransaccion = "Marketplace";
			} else if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
				tipoTransaccion = "Club de Regalos";
			}
			//String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
			String fechaEmision = ordenesDeCompra.getNotaVenta().getFechaCreacion().toString();
			String rutCliente = Util.formatoRUT(ordenesDeCompra.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(ordenesDeCompra.getDespacho().getRutCliente().toString()));
			String nroEvento = (ordenesDeCompra.getNotaVenta().getCodigoRegalo() != null)?ordenesDeCompra.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;
	
			String rutRecaudacion = "";
			
			//logger.traceInfo("enviarEmailRecaudacion", "=====>>>codigo regalo: "+ordenesDeCompra.getNotaVenta().getCodigoRegalo().toString());
			if (ordenesDeCompra.getNotaVenta().getCodigoRegalo() != null){
				//logger.traceInfo("enviarEmailRecaudacion", "entr�!!!");

				clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
				if (clienteClub != null){
					rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
				}
			}
	
			String rutEvento = rutRecaudacion;
			String medioPago = "";
			
			if (ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta() != null){
				medioPago = "TARJETA RIPLEY";
			}
			if (ordenesDeCompra.getTarjetaBancaria().getCorrelativoVenta() != null){
				if (ordenesDeCompra.getTarjetaBancaria().getMedioAcceso() != null && ordenesDeCompra.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
					medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
				} else if (ordenesDeCompra.getTarjetaBancaria().getMedioAcceso() != null && ordenesDeCompra.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
					medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
				} else {
					medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
				}
				
				if (ordenesDeCompra.getTarjetaBancaria().getVd() != null && ordenesDeCompra.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
					medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
				} else {
					medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
				}
	
				medioPago = medioPago.toUpperCase();
			}
			if (ordenesDeCompra.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
				medioPago = "TARJETA REGALO EMPRESAS";
			}
			if(ordenesDeCompra.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta()!=null){
				medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
			}

			String glosaFinanciera = "";
	
			StrBuilder respuesta = new StrBuilder();
			if (ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta() != null){
				glosaFinanciera = ordenesDeCompra.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
				glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
				String glosas[] = glosaFinanciera.split("@@");
				for (int g = 0; g < glosas.length; g++){
					respuesta.append(glosas[g]).appendNewLine();
				}
				glosaFinanciera = respuesta.toString();
			}
		
		
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Tipo_transaccion", tipoTransaccion);
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			valores.put("Orden_compra", String.valueOf(ordenesDeCompra.getNotaVenta().getCorrelativoVenta()));
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Rut", rutCliente);
			valores.put("Medio_Pago", medioPago);
			valores.put("Glosa_financiera", glosaFinanciera);
			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			
			if (articulosOC.size() > 0){
				for (int i = 0;i < articulosOC.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articulosOC.get(i);
					if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getArticuloVenta().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}
					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));

			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);			
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), 
					asunto, html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			logger.traceError("enviarEmailRecaudacion Error en ReenviarVoucherAction: ", e);
			resultado = false;
		}
		logger.endTrace("enviarEmailRecaudacion", "Resultado de envio correo: ", ""+resultado);
		return resultado;
	}

	public boolean enviarEmailAnulacion(OrdenesDeCompra ordenesDeCompra, List<ArticulosVentaOC> articulosOC, String nroVoucher, String email) {
		boolean resultado = false;

		logger.traceInfo("enviarEmailAnulacion", "Inicio Metodo", 
				new KeyLog("OC: ",ordenesDeCompra.getNotaVenta().getCorrelativoVenta().toString()),
				new KeyLog("Mail: ", email));
		
		TemplateVoucher template = new TemplateVoucher();
		ClienteClub clienteClub=null;
		try {
			
			if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
				template = templateDao.getTemplateByLlave("VOUCHER_ANULA");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmailAnulacion", "Correo llave 'VOUCHER_ANULA' no se encontr� o est� desactivado.");
					return false;
				}
			} else {
				template = templateDao.getTemplateByLlave("ANULACION_MKP");
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmailAnulacion", "Correo llave 'ANULACION_MKP' no se encontr� o est� desactivado.");
					return false;
				}
			}

			String asunto = "VOUCHER ANULACION OC: "+ordenesDeCompra.getNotaVenta().getCorrelativoVenta();
			
			String destinatario = email;
	
			String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();
			String nombreCliente = ordenesDeCompra.getDespacho().getNombreCliente();
	
			
			String tipoTransaccion = "";
			if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
				tipoTransaccion = "Marketplace";
			} else if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
				tipoTransaccion = "Club de Regalos";
			}
//			String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
			String fechaEmision = ordenesDeCompra.getNotaVenta().getFecNotaCredito().toString();
			String rutCliente = Util.formatoRUT(ordenesDeCompra.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(ordenesDeCompra.getDespacho().getRutCliente().toString()));
			String nroEvento = (ordenesDeCompra.getNotaVenta().getCodigoRegalo() != null)?ordenesDeCompra.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;
	
			String rutRecaudacion = "";

			if (ordenesDeCompra.getNotaVenta().getCodigoRegalo() != null){
				clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
				if (clienteClub != null){
					rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
				}
			}
	
			String rutEvento = rutRecaudacion;
			String medioPago = "";
			
			if (ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta() != null){
				medioPago = "TARJETA RIPLEY";
			}
			if (ordenesDeCompra.getTarjetaBancaria().getCorrelativoVenta() != null){
				if (ordenesDeCompra.getTarjetaBancaria().getMedioAcceso() != null && ordenesDeCompra.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
					medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
				} else if (ordenesDeCompra.getTarjetaBancaria().getMedioAcceso() != null && ordenesDeCompra.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
					medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
				} else {
					medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
				}
				
				if (ordenesDeCompra.getTarjetaBancaria().getVd() != null && ordenesDeCompra.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
					medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
				} else {
					medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
				}
	
				medioPago = medioPago.toUpperCase();
			}
			if (ordenesDeCompra.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
				medioPago = "TARJETA REGALO EMPRESAS";
			}
			if(ordenesDeCompra.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta()!=null){
				medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
			}

			String glosaFinanciera = "";
	
			StrBuilder respuesta = new StrBuilder();
			if (ordenesDeCompra.getTarjetaRipley().getCorrelativoVenta() != null){
				glosaFinanciera = ordenesDeCompra.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
				glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
				String glosas[] = glosaFinanciera.split("@@");
				for (int g = 0; g < glosas.length; g++){
					respuesta.append(glosas[g]).appendNewLine();
				}
				glosaFinanciera = respuesta.toString();
			}
		
		
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Tipo_transaccion", tipoTransaccion);
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			valores.put("Orden_compra", String.valueOf(ordenesDeCompra.getNotaVenta().getCorrelativoVenta()));
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Rut", rutCliente);
			valores.put("Medio_Pago", medioPago);
			valores.put("Glosa_financiera", glosaFinanciera);
			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			
			if (articulosOC.size() > 0){
				for (int i = 0;i < articulosOC.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articulosOC.get(i);
					if (ordenesDeCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getArticuloVenta().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}
					
					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_DOS){ // si no es nota de credito, me lo salto
						continue;
					}
					
					if (ordenesDeCompra.getNotaVenta().getCorrelativoNotaCredito() != null && articuloVentaTrama.getArticuloVenta().getCorrelativoNotaCredito() != null){
						if (ordenesDeCompra.getNotaVenta().getCorrelativoNotaCredito().intValue() != articuloVentaTrama.getArticuloVenta().getCorrelativoNotaCredito().intValue()){ // si el correlativo de NC no coincide, me lo salto
							continue;
						}
					}

					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));

			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);			
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), 
					asunto, html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			logger.traceError("enviarEmailAnulacion Error en ReenviarVoucherAction: ", e);
			resultado = false;
		}
		logger.endTrace("enviarEmailAnulacion", "Resultado de envio correo: ", ""+resultado);
		return resultado;
	}
	
	public boolean enviarEmailRecaudacionCDF(OrdenesDeCompra trama, String email, TemplateVoucher template) {
		boolean resultado = false;
		ClienteClub clienteClub = new ClienteClub();

		String destinatario = email;
		String nroVoucher = String.valueOf(trama.getNotaVenta().getCorrelativoBoleta());
		String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();
		String nombreCliente = trama.getDespacho().getNombreCliente();
		String tipoTransaccion = "";
		if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
			tipoTransaccion = "Marketplace";
		} else if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
			tipoTransaccion = "Club de Regalos";
		}

		//String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
		String fechaEmision = trama.getNotaVenta().getFechaCreacion().toString();
		String rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;

		String rutRecaudacion = "";
		logger.traceInfo("enviarEmailRecaudacion", "Cod Regalo1: "+trama.getNotaVenta().getCodigoRegalo());
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			logger.traceInfo("enviarEmailRecaudacion", "Cod Regalo2: "+trama.getNotaVenta().getCodigoRegalo());
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
				logger.traceInfo("enviarEmailRecaudacion", "Rut Recaudacion: "+rutRecaudacion);
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}
		
		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
			glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
			String glosas[] = glosaFinanciera.split("@@");
			for (int g = 0; g < glosas.length; g++){
				respuesta.append(glosas[g]).appendNewLine();
			}
			glosaFinanciera = respuesta.toString();
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Tipo_transaccion", tipoTransaccion);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Rut", rutCliente);
			valores.put("Medio_Pago", medioPago);
			valores.put("Glosa_financiera", glosaFinanciera);
			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			List<ArticulosVentaOC> articuloVentas = trama.getArticulosVenta();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}
					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), "VOUCHER RECAUDACION OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}

	public boolean enviarEmailRecaudacionMKP(OrdenesDeCompra trama, String email, TemplateVoucher template) {
		boolean resultado = false;
		ClienteClub clienteClub = new ClienteClub();
		
		String destinatario = email;
		String nroVoucher = String.valueOf(trama.getNotaVenta().getCorrelativoBoleta());
		String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();
		String sucursal = parametroDao.getParametro("CODIGO_SUCURSAL").getValor();
		String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CINCO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroCaja =parametroDao.getParametro("NRO_CAJA").getValor();
		String codigoCaja = Util.rellenarString(nroCaja, Constantes.NRO_TRES, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroVoucherX10 = Util.rellenarString(nroVoucher, Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nombreCliente = trama.getDespacho().getNombreCliente() + Constantes.SPACE + trama.getDespacho().getApellidoPatCliente();

		//String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
		String fechaEmision = trama.getNotaVenta().getFechaHoraCreacion().toString();
		String rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;

		String codBoTipoDoc = trama.getTipoDoc().getCodBo().toString();
		String descTipoDoc  = trama.getTipoDoc().getDescTipodoc();

		String rutRecaudacion = "";
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		String titularMedioPago = "";
		String nroConvenioMP = "";
		String codAutorizadorMP = "";
		
		String nroUnico = getCodigoUnicoGrilla(sucursal, nroCaja, Integer.parseInt(nroVoucher)).toString();
		String glosaAcepto = getGlosaAcepto(trama).toString();
		
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
			codAutorizadorMP = trama.getTarjetaRipley().getCodigoAutorizacion().toString();
			titularMedioPago = "RUT CLIENTE: "+trama.getTarjetaRipley().getRutTitular();
			titularMedioPago = titularMedioPago + Constantes.SPACE + Constantes.GUION + Constantes.SPACE;
			titularMedioPago = titularMedioPago + "NOMBRE TIT.: ";
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getNombreCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoPatCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoMatCliente());
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
			codAutorizadorMP = trama.getTarjetaBancaria().getCodigoAutorizador();
			if (trama.getTarjetaBancaria().getCodigoConvenio() != null){
				nroConvenioMP = trama.getTarjetaBancaria().getCodigoConvenio().toString();
			}
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
			codAutorizadorMP = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador().toString();
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}
		
		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
			glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
			String glosas[] = glosaFinanciera.split("@@");
			for (int g = 0; g < glosas.length; g++){
				respuesta.append(glosas[g]).appendNewLine();
			}
			glosaFinanciera = respuesta.toString();
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_hora_emision", fechaEmision);
			
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			
			valores.put("Sucursal", codigoSucursal);
			valores.put("Caja", codigoCaja);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Nro_voucher_x10", nroVoucherX10);
			valores.put("Rut", rutCliente);
			
			valores.put("CodBO_Tipo_Doc", codBoTipoDoc);
			valores.put("Desc_Tipo_Doc", descTipoDoc);
			
			valores.put("Medio_Pago", medioPago);
			valores.put("Datos_titular_TRipley", titularMedioPago);
			valores.put("Nro_Convenio", nroConvenioMP);
			valores.put("Nro_autorizador", codAutorizadorMP);
			valores.put("Nro_unico", nroUnico);
			valores.put("Glosa_financiera", glosaFinanciera);
			valores.put("Glosa_Acepto", glosaAcepto);

			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			String descuentoS	= "";
			int descuentoI		= 0;
			List<ArticulosVentaOC> articuloVentas = trama.getArticulosVenta();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}
					descuentoS	= "";
					descuentoI = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue();
					if (descuentoI > 0){
						descuentoS = "-"+descuentoI;
					}
					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					lista.put("Descuento"+contador, descuentoS);
					
					lista.put("Sub_Orden"+contador, articuloVentaTrama.getIdentificadorMarketplace().getOrdenMkp());
					lista.put("Rut_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRutProveedor());
					lista.put("Razon_social_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRazonSocial());
					
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), "VOUCHER RECAUDACION MKP OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}

	public boolean enviarEmailAnulacionCDF(OrdenesDeCompra trama, String email, TemplateVoucher template) {
		boolean resultado = false;
		ClienteClub clienteClub = new ClienteClub();

		String destinatario = email;
		String nroVoucher = String.valueOf(trama.getNotaVenta().getCorrelativoNotaCredito());

		String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();;
		String nombreCliente = trama.getDespacho().getNombreCliente();
		String tipoTransaccion = "";
		if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
			tipoTransaccion = "Marketplace";
		} else if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
			tipoTransaccion = "Club de regalos";
		}
		
		//String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
		String fechaEmision = trama.getNotaVenta().getFecNotaCredito().toString();
		String rutCliente = "";
		if (trama.getDespacho().getRutCliente() != null){
			rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		}
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;
		
		String rutRecaudacion = "";
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}

		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
			glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
			String glosas[] = glosaFinanciera.split("@@");
			for (int g = 0; g < glosas.length; g++){
				respuesta.append(glosas[g]).appendNewLine();
			}
			glosaFinanciera = respuesta.toString();
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Tipo_transaccion", tipoTransaccion);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Rut", rutCliente);
			valores.put("Medio_Pago", medioPago);
			valores.put("Glosa_financiera", glosaFinanciera);
			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			List<ArticulosVentaOC> articuloVentas = trama.getArticulosVenta();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}

					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_DOS){ // si no es nota de credito, me lo salto
						continue;
					}

					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), "VOUCHER ANULACION OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}

	public boolean enviarEmailAnulacionMKP(OrdenesDeCompra trama, String email, TemplateVoucher template) {
		boolean resultado = false;
		ClienteClub clienteClub = new ClienteClub();
		
		String destinatario = email;
		String nroVoucher = String.valueOf(trama.getNotaVenta().getCorrelativoNotaCredito());

		String urlCloudFront = parametroDao.getParametro("URL_CLOUDFRONT").getValor();
		String sucursal = parametroDao.getParametro("CODIGO_SUCURSAL").getValor();
		String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CINCO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroCaja = parametroDao.getParametro("NRO_CAJA").getValor();
		String codigoCaja = Util.rellenarString(nroCaja, Constantes.NRO_TRES, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroVoucherX10 = Util.rellenarString(nroVoucher, Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nombreCliente = trama.getDespacho().getNombreCliente() + Constantes.SPACE + trama.getDespacho().getApellidoPatCliente();
		String direccionCliente = trama.getDespacho().getDireccionCliente();

		//String fechaEmision = FechaUtil.getCurrentDateYYYYMMDDString();
		String fechaEmision = trama.getNotaVenta().getFecNotaCredito().toString();
		String rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;

		String codBoTipoDoc = trama.getTipoDoc().getCodBo().toString();
		String descTipoDoc  = trama.getTipoDoc().getDescTipodoc();

		String rutRecaudacion = "";
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		String titularMedioPago = "";
		String nroConvenioMP = "";
		String codAutorizadorMP = "";
		
		String nroUnico = getCodigoUnicoGrilla(sucursal, nroCaja, Integer.parseInt(nroVoucher)).toString();
		String glosaAcepto = getGlosaAcepto(trama).toString();
		
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
			codAutorizadorMP = trama.getTarjetaRipley().getCodigoAutorizacion().toString();
			titularMedioPago = "RUT CLIENTE: "+trama.getTarjetaRipley().getRutTitular();
			titularMedioPago = titularMedioPago + Constantes.SPACE + Constantes.GUION + Constantes.SPACE;
			titularMedioPago = titularMedioPago + "NOMBRE TIT.: ";
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getNombreCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoPatCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoMatCliente());
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
			codAutorizadorMP = trama.getTarjetaBancaria().getCodigoAutorizador();
			if (trama.getTarjetaBancaria().getCodigoConvenio() != null){
				nroConvenioMP = trama.getTarjetaBancaria().getCodigoConvenio().toString();
			}
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
			codAutorizadorMP = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador().toString();
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}
		
		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
			glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
			String glosas[] = glosaFinanciera.split("@@");
			for (int g = 0; g < glosas.length; g++){
				respuesta.append(glosas[g]).appendNewLine();
			}
			glosaFinanciera = respuesta.toString();
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_hora_emision", fechaEmision);
			
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			
			valores.put("Sucursal", codigoSucursal);
			valores.put("Caja", codigoCaja);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Nro_voucher_x10", nroVoucherX10);
			valores.put("Rut", rutCliente);
			valores.put("DireccionCliente", direccionCliente);
			
			valores.put("CodBO_Tipo_Doc", codBoTipoDoc);
			valores.put("Desc_Tipo_Doc", descTipoDoc);
			
			valores.put("Medio_Pago", medioPago);
			valores.put("Datos_titular_TRipley", titularMedioPago);
			valores.put("Nro_Convenio", nroConvenioMP);
			valores.put("Nro_autorizador", codAutorizadorMP);
			valores.put("Nro_unico", nroUnico);
			valores.put("Glosa_financiera", glosaFinanciera);
			valores.put("Glosa_Acepto", glosaAcepto);

			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			String descuentoS	= "";
			int descuentoI		= 0;

			List<ArticulosVentaOC> articuloVentas = trama.getArticulosVenta();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticulosVentaOC articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}

					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_DOS){ // si no es nota de credito, me lo salto
						continue;
					}
					
					descuentoS	= "";
					descuentoI = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue();
					if (descuentoI > 0){
						descuentoS = "-"+descuentoI;
					}

					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					lista.put("Descuento"+contador, descuentoS);

					lista.put("Sub_Orden"+contador, articuloVentaTrama.getIdentificadorMarketplace().getOrdenMkp());
					lista.put("Rut_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRutProveedor());
					lista.put("Razon_social_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRazonSocial());
					
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(parametroDao.getParametro("SILVERPOP_HOST").getValor(), Integer.parseInt(parametroDao.getParametro("PORT").getValor()), "VOUCHER ANULACION MKP OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, parametroDao.getParametro("REMITENTE").getValor(), destinatario, parametroDao.getParametro("SILVERPOP_GROUP").getValor());

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}
	
}
