package cl.ripley.omnicanalidad.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.UsuarioDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Password;

/**Controlador de inicio de sesión de Usuario.
 *
 * @author Jose Matias Ortuzar (Aligare). 
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Login extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 5875637172746457775L;
	private static final AriLog logger = new AriLog(Login.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Autowired
	private ParametroDAO parametroDAO;
	
	@SuppressWarnings("rawtypes")
	private Map session;
	
	private Error error;

	private String username;
	private String password;
	
	private Usuario usuario = new Usuario();
	
	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map session) {
		this.session = session;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	/**
	 * M�todo que controla las acciones de la clase LoginAction de la aplicaci�n.
	 */
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = SUCCESS;
		Error errorBean = new Error();
		usuario = usuarioDAO.buscarUsuario(username);
		boolean verificado = Boolean.FALSE;
		
		ActionContext context = ActionContext.getContext();  
		
		if(parametroDAO.getParamByName("RECAPTCHA_ON_OFF")==1) {
			verificado = VerificarRecaptcha.verificar(context.getParameters().get("g-recaptcha-response").toString());	
		}else {
			verificado = Boolean.TRUE;
		}
		
		Password encriptador = new Password();
		String encriptado = encriptador.encryptPassword(password);
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		System.out.println(formatter.format(date));
				
		boolean usuario_vigente = false;
		if(usuario != null) {
			if(usuario.getFecFinVigencia()!=null) {
				if(date.before(formatter.parse(usuario.getFecFinVigencia()))) {
					usuario_vigente = true;
				}
			}
		}
		
		if (usuario != null && verificado && usuario_vigente){
			logger.traceInfo("execute", "["+username+"] es un usuario registrado en la base de datos.");						
			
			if (usuario.getPassword().equals(encriptado)){
				logger.traceInfo("execute", "["+username+"] password correcto.");
				session.put("usuario",usuario);
				respuesta = SUCCESS;
				
				//Se agrega menu de acuerdo al perfil del usuario
				usuario = usuarioDAO.getUsuarioConPerfil(usuario);
				
				Modulo m = null;
				
				Optional<Modulo> opt =	usuario.getPerfil().getModulos().stream()
										.filter(modulo -> modulo.getUrl().equals(Constantes.ARROBA))
										.findFirst();
				
				m = opt.isPresent() ? opt.get() : null;
				
				if(m != null) {
					Parametro param = parametroDAO.getParametro(Constantes.URL_PAGO_CONTRA_ENTREGA_TARJETA);
					
					if(param != null && param.getValor() != null) {
						StringBuilder b = new StringBuilder();
						b.append("javascript:window.open(\"")
						.append(param.getValor())
						.append("\")");
						String url = b.toString();
						logger.traceInfo("execute", "URL Pago contra entrega tarjeta: " + url);
						m.setUrl(url);
						
					} else {
						
						m.setUrl(Constantes.GATO);
						
					}
					
				}
				
				
			}else {
				logger.traceInfo("execute", "Ocurrió un error al iniciar sesión.");
				errorBean.setCodError(1);
				errorBean.setErrorMsg("Ocurrió un error al iniciar sesión.");
				setError(errorBean);
				respuesta = ERROR;
			}
		}else {
			if(!verificado) {
				logger.traceInfo("execute", "["+username+"] debe ingresar el recaptcha.");
				errorBean.setCodError(1);
				errorBean.setErrorMsg("["+username+"] debe ingresar el recaptcha");
				setError(errorBean);
				respuesta = ERROR;
			}else {
				if(!usuario_vigente && usuario != null && usuario.getPassword().equals(encriptado)) {
					logger.traceInfo("execute", "Usuario con fecha de vigencia expirada.");
					errorBean.setCodError(1);
					errorBean.setErrorMsg("Usuario con fecha de vigencia expirada.");
					setError(errorBean);
					respuesta = ERROR;
				}else {
					logger.traceInfo("execute", "Ocurrió un error al iniciar sesión.");
					errorBean.setCodError(1);
					errorBean.setErrorMsg("Ocurrió un error al iniciar sesión.");
					setError(errorBean);
					respuesta = ERROR;
				}
				
			}
		}
		logger.endTrace("execute", "Finalizado", "String: "+respuesta);
		return respuesta;
	}
}
