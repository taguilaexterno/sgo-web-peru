package cl.ripley.omnicanalidad.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ModulosDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Action para manejar modulos.
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 11-08-2016
 * Versiones:
 * <ul>
 *  <li>11-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ModulosAction extends ActionSupport implements UsuarioHabilitado {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final AriLog logger = new AriLog(ModulosAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ModulosDAO modulosDAO;
	
	private Error error;
	
	private List<Modulo> modulosPerfil;
	
	private Long codPerfil;
	
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		
	}
	
	public List<Modulo> getModulosPerfil() {
		return modulosPerfil;
	}

	public void setModulosPerfil(List<Modulo> modulosPerfil) {
		this.modulosPerfil = modulosPerfil;
	}

	public Long getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(Long codPerfil) {
		this.codPerfil = codPerfil;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	/**Obtiene módulos por perfil
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String getModulosPorPerfilJson() {
		logger.initTrace("getModulosPorPerfilJson", "Ingreso metodo; codPerfil=" + codPerfil);
		
		modulosPerfil = modulosDAO.getModulosByPerfil(codPerfil);
		
		if(modulosPerfil == null || modulosPerfil.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encuentra lista de modulos.");
			return ERROR;
		}
		
		return SUCCESS; 
	}

}
