package cl.ripley.omnicanalidad.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Anulacion;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.TbkDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NotasCreditoTBK extends ActionSupport implements SessionAware,
		UsuarioHabilitado {
	private static final AriLog logger = new AriLog(ConfirmacionBoletas.class,
			Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private TbkDAO tbkDao;

	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings({ "rawtypes", "unused" })
	private Map session;
	private Usuario usuario;

	private int estado;
	private int nroOrdenes;
	private String metodo;
	private String seleccionados;
	private final String ANULAROC = "ANULAROC";
	private final String GENERAROC = "GENERAROC";
	private final String DETALLE = "DETALLE";
	private boolean cajaEstado;

	private String fechaDesde;
	private String fechaHasta;

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public List<Anulacion> getListaAnulacion() {
		return listaAnulacion;
	}

	public void setListaAnulacion(List<Anulacion> listaAnulacion) {
		this.listaAnulacion = listaAnulacion;
	}

	private List<Anulacion> listaAnulacion;

	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = ERROR;
		Error errorBean = new Error();
		Integer dias = new Integer(parametroDao.getParametro("DIAS_CONSULTA_TBK").getValor());

		Calendar calendar = Calendar.getInstance();
		java.sql.Date fechaDesdeCal = null;
		java.sql.Date fechaHastaCal = null;

		if (fechaDesde == null) {
			calendar.add(Calendar.DAY_OF_MONTH, - dias);
			fechaDesdeCal = new java.sql.Date(calendar.getTime().getTime());
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			fechaDesde = df.format(fechaDesdeCal);

		} else {

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date parsed = sdf.parse(fechaDesde);

			fechaDesdeCal = new java.sql.Date(parsed.getTime());

			// int year = Integer.parseInt(fechaDesde.substring(6));
			// int month = Integer.parseInt(fechaDesde.substring(3, 5));
			// int day = Integer.parseInt(fechaDesde.substring(0,2));
			//
			// calendar.set(Calendar.YEAR, year);
			// calendar.set(Calendar.MONTH, month);
			// calendar.set(Calendar.DAY_OF_MONTH, day);
			//
			// fechaDesdeCal = new java.sql.Date(calendar.getTime().getTime());

			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			fechaDesde = df.format(fechaDesdeCal);
		}

		calendar = Calendar.getInstance();
		if (fechaHasta == null) {
			fechaHastaCal = new java.sql.Date(calendar.getTime().getTime());
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			fechaHasta = df.format(fechaHastaCal);
		} else {

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date parsed = sdf.parse(fechaHasta);

			fechaHastaCal = new java.sql.Date(parsed.getTime());

			// int year = Integer.parseInt(fechaHasta.substring(6));
			// int month = Integer.parseInt(fechaHasta.substring(3, 5));
			// int day = Integer.parseInt(fechaHasta.substring(0,2));
			//
			// calendar.set(Calendar.YEAR, year);
			// calendar.set(Calendar.MONTH, month);
			// calendar.set(Calendar.DAY_OF_MONTH, day);
			//
			// fechaHastaCal = new java.sql.Date(calendar.getTime().getTime());

			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			fechaHasta = df.format(fechaHastaCal);
		}

		listaAnulacion = tbkDao.getAnulaTBKPorFecha(fechaDesdeCal,
				fechaHastaCal);

		respuesta = Constantes.SUCCESS_FECHAS_NOTAS_CREDITO_TBK;

		return respuesta;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getNroOrdenes() {
		return nroOrdenes;
	}

	public void setNroOrdenes(int nroOrdenes) {
		this.nroOrdenes = nroOrdenes;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getSeleccionados() {
		return seleccionados;
	}

	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}

	public boolean isCajaEstado() {
		return cajaEstado;
	}

	public void setCajaEstado(boolean cajaEstado) {
		this.cajaEstado = cajaEstado;
	}
}
