package cl.ripley.omnicanalidad.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Linea;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.bean.VoucherPDF;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.Util;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**Controlador de Voucher Boleta.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VoucherBoletaAction extends ActionSupport implements UsuarioHabilitado, SessionAware{
	
	
	private static final String VERBOLETA = "verBoleta";
	private static final String VERFACTURA = "verFactura";
	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(VoucherBoletaAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	@Autowired
	private ParametroDAO parametroDAO;
	
	@Autowired
	private ConversionService conversionService;
	
	@Autowired
	private JasperExportManager jasperExportManager;
	
	@Autowired
	private JasperFillManager jasperFillManager;
	
	private Usuario usuario;
	private int tipoEjecucion;
	private String mensaje;
	private Map<String, Object> session;
	private String mensajeError;

	private Error error;
	
	
	private Long oc;
	private int estado;
	private String datoBoleta;
	private String urlDoce;
	private String folio;
	private int tipoDoc;
	
	
	
	private OrdenesDeCompra orden;
	private List<ArticulosVentaOC> listaArticulos;	
	private String codigoBarra;
	private String razonSocial;
	private String giro;
	private String rutEmpresa;
	private String direccion;
	private String nombreSucursal;
	private String ciudad;
	private String paginaWeb;
	private String comunaOrigen;

	private String subTotal;
	private String totalExento;
	private String totalNeto;
	private String iva;
	private String total;
	private String medioPago;
	private String codAutorizadorMP;
	private String nroConvenioMP;
	private String nroUnico;
	private int sucursal;
	private String ted;
	private String montoEscrito;
	
	private String cajaTicketCambio="0";
	private String sucTicketCambio="0";
	
	private List<Linea> lineas;	
	
	
	private InputStream fileStream;
	private String fileName;

	public String execute() {
		logger.initTrace("execute", "Ingrestt VoucherBoletaAction");
		String result = ERROR;

        if (datoBoleta.isEmpty() || datoBoleta.length() != 20 ) {
        	logger.traceInfo("execute", "OC no existe: "+oc);
			mensajeError = "OC no existe";
			session.put("mensajeError", mensajeError);
			result = ERROR;
			return result;        	
        }

		oc = Long.valueOf(datoBoleta.substring(6,16));
		sucursal = Integer.parseInt(datoBoleta.substring(0,3));
		estado = Integer.parseInt(datoBoleta.substring(18,19));
		logger.traceInfo("execute", "Numero de OC: "+oc+" Estado: "+estado + " Sucursal:" + sucursal);
		
		switch(tipoEjecucion) {
		case 1:
			result = SUCCESS;
			break;
		case 2:
			logger.traceInfo("execute", "Desde el formulario: VoucherBoleta OC: "+oc+" ");
			
			List<OrdenesDeCompra> oCompraAux = new ArrayList<OrdenesDeCompra>();
			OrdenesDeCompra oCompra = null;
			
			oCompraAux.addAll(ordenesCompraDAO.obtenerOrdenes(-1, sucursal, estado, null, new Long(oc)));

			logger.traceInfo("VoucherBoletaAction", "Size: "+oCompraAux.size());
			if (oCompraAux != null && oCompraAux.size() > 0) {
				oCompra = oCompraAux.get(0);
			    tipoDoc= oCompra.getNotaVenta().getTipoDoc();
			    folio= oCompra.getNotaVenta().getFolioSii();
				urlDoce= oCompra.getNotaVenta().getUrlDoce();
				logger.traceInfo("VoucherBoletaAction", "Tipo Doc: "+tipoDoc);
			}

			
			if(oCompra == null || oCompra.getNotaVenta().getCorrelativoVenta() == null || oCompra.getNotaVenta().getUrlDoce() == null) {
				mensajeError = "Boleta no existe OC: "+ oc+ " Estado: "+estado;
				session.put("mensajeError", mensajeError);
				result = SUCCESS;
				break;
			} else {
				mensaje = "La Boleta se descarga en forma exitosa URL: "+oCompra.getNotaVenta().getUrlDoce();
				session.put("mensaje", mensaje);

			}
			
			result = SUCCESS;
			break;
		case 3:	
			//Datos para Boleta
			
			//Carga de Info BD
			orden = obtenerDatosTramaBoleta(oc);
			listaArticulos = obtenerArticulosTramaBoleta(oc);
			if (orden.getNotaVenta()== null || orden.getNotaVenta().getFechaCreacion()==null 
					|| orden.getNotaVenta().getCorrelativoVenta() == null || listaArticulos == null){
				result = ERROR;
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("Error en la obtenci�n de la informaci�n de la Boleta");	
				return result;
			}
			
			if (orden.getNotaVenta().getTipoDoc()==Constantes.TIPO_DOC_BOLETA){
				result = VERBOLETA;
			}
			else if(orden.getNotaVenta().getTipoDoc()==Constantes.TIPO_DOC_FACTURA){
				result = VERFACTURA;
			}
			cajaTicketCambio = String.format("%05d", orden.getNotaVenta().getNumeroCaja());
			sucTicketCambio = String.format("%03d", orden.getNotaVenta().getNumeroSucursal());
			codigoBarra = crearCodigoBarra(orden);	
			razonSocial = parametroDAO.getParametro("RAZON_SOCIAL_EMISOR").getValor();
			rutEmpresa = Util.formatoRUT(parametroDAO.getParametro("RUT_EMISOR").getValor());
			giro = parametroDAO.getParametro("GIRO_EMISOR").getValor();
			direccion = parametroDAO.getParametro("DIRECCION_ORIGEN").getValor();
			nombreSucursal = parametroDAO.getParametro("TRAMA_DIRECCION_SUCURSAL_"+orden.getNotaVenta().getNumeroSucursal()).getValor();
			ciudad = parametroDAO.getParametro("CIUDAD_ORIGEN").getValor();
			comunaOrigen = parametroDAO.getParametro("COMUNA_ORIGEN").getValor();
			paginaWeb = parametroDAO.getParametro("PAGINA_WEB").getValor();
			subTotal = obtenerSubTotalBoleta(listaArticulos);
			totalExento = obtenerTotalExento();
			total = obtenerTotal(subTotal,totalExento);
			medioPago = obtenerMedioPago(orden);
			nroUnico = obtenerNroUnico(orden);
			try {
				ted = Util.quitarSaltos(orden.getDocumentoElectronico().getXmlTDE().getSubString(1, (int) orden.getDocumentoElectronico().getXmlTDE().length()));
				logger.traceInfo("VoucherBoletaAction","BPL OrdenInfo: "+ted);
			} catch (SQLException e) {
				e.printStackTrace();
				result = ERROR;
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("Error en la Obtención de en la Boleta");
			}	
			
			totalNeto = obtenerTotalNeto(Float.parseFloat(subTotal));
			iva = obtenerIva(Float.parseFloat(totalNeto));
			montoEscrito = Util.MontoEscrito(total, Constantes.VERDADERO);
			
			break;
		default:
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Vista no definida");
			break;
		}
		
		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		
		return result;

	}
	
	public String voucherBoletaPdf() throws JRException {
		
		//Carga de Info BD
		Optional<OrdenesDeCompra> optOrden = Optional.ofNullable(obtenerDatosTramaBoleta(oc));
		
		if(!optOrden.isPresent()) {
			error = new Error();
			error.setCodError(Constantes.NRO_MENOSUNO);
			error.setErrorMsg("No se encuentra la orden");
			return ERROR;
		}
		
		orden = optOrden.get();
		
		Optional<List<ArticulosVentaOC>> optArticulosVenta = Optional.ofNullable(obtenerArticulosTramaBoleta(oc));
		
		if(!optArticulosVenta.isPresent() || optArticulosVenta.get().isEmpty()) {
			
			error = new Error();
			error.setCodError(Constantes.NRO_MENOSUNO);
			error.setErrorMsg("No se encuentran articulos para la orden");
			return ERROR;
			
		}
		
		orden.setArticulosVenta(optArticulosVenta.get());
		
		VoucherPDF voucherPDF = conversionService.convert(orden, VoucherPDF.class);
		ArrayList<VoucherPDF> voucherPDFList = new ArrayList<>(Constantes.NRO_UNO);
		voucherPDFList.add(voucherPDF);
		JRBeanCollectionDataSource jasperDs = new JRBeanCollectionDataSource(voucherPDFList);
		
		InputStream is = orden.getTipoDoc().getCodPpl() == Constantes.TIPO_DOC_BOLETA ? 
				this.getClass().getClassLoader().getResourceAsStream("rpt_reporteBoleta.jasper") : this.getClass().getClassLoader().getResourceAsStream("rpt_reporteFactura.jasper");
		
		JasperPrint jp = jasperFillManager.fill(is, null, jasperDs);
		
		byte[] pdf = jasperExportManager.exportToPdf(jp);
		
		fileStream = new ByteArrayInputStream(pdf);
		
		fileName = (orden.getTipoDoc().getCodPpl() == Constantes.TIPO_DOC_BOLETA ? "boleta_" : "factura_") + orden.getNotaVenta().getFolioSii() + ".pdf";
		
		return SUCCESS;
	}
	
	
	
	private String obtenerTotalNeto(Float total){
		return String.valueOf((int)Math.ceil((total / 1.19)));
	}
	
	private String obtenerIva(Float totalNeto){		
		
		return String.valueOf((int)Math.floor(totalNeto*0.19));
	}
	
	private String obtenerTotal(String subTotal2, String totalExento2) {
		Float suma=0f;		
		suma = Float.parseFloat(subTotal2)+Float.parseFloat(totalExento2);
		return suma.toString();
	}

	private OrdenesDeCompra obtenerDatosTramaBoleta(Long oc){
		OrdenesDeCompra trama = new OrdenesDeCompra();
		
		List<OrdenesDeCompra> datosBoleta = new ArrayList<OrdenesDeCompra>();
		
	
		datosBoleta = ordenesCompraDAO.obtenerDatosBoleta(null, null, oc);
		if(datosBoleta!=null && datosBoleta.size()>0){
			trama = datosBoleta.get(0);
		}
		
		return trama;
	}
	
	private List<ArticulosVentaOC> obtenerArticulosTramaBoleta(Long oc){
		
		List<ArticulosVentaOC> articulos = new ArrayList<ArticulosVentaOC>();
		articulos = ordenesCompraDAO.getArticulosByOC(oc);
		
		return articulos;
		
		
	}
	
	private String crearCodigoBarra(OrdenesDeCompra orden){		
		String codigoCreado;
		String fecha = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYY2, orden.getNotaVenta().getFechaCreacion());
		String sucursal = String.format("%05d", orden.getNotaVenta().getNumeroSucursal());
		String nroCaja = String.format("%03d", orden.getNotaVenta().getNumeroCaja());
		String trx = String.format("%04d", orden.getNotaVenta().getNroTrx());
		
		codigoCreado = fecha + sucursal + nroCaja + trx;
		
		return codigoCreado;
	}
	
	private String obtenerSubTotalBoleta(List<ArticulosVentaOC> listaArticulos){
		BigDecimal suma = BigDecimal.ZERO;
		
		if(listaArticulos != null) {
			
			suma = 	listaArticulos.stream()
					.filter(articulo -> articulo.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.NRO_CERO)
					.map(articulo -> articulo.getArticuloVenta().getPrecioPorUnidades().subtract(articulo.getArticuloVenta().getMontoDescuento()))
					.reduce(suma, BigDecimal::add);
			
		}
		
		return suma.toString();
	}
	
	private String obtenerTotalExento(){
		return "0";
	}
	
	private String obtenerMedioPago(OrdenesDeCompra orden){
		String medioPago = "";
		
		if (orden.getTarjetaRipley()!=null){
			medioPago = Constantes.BOL_FORMA_PAGO_TAR_RIP;
		}
		
		if (orden.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (orden.getTarjetaBancaria().getMedioAcceso() != null && orden.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (orden.getTarjetaBancaria().getMedioAcceso() != null && orden.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (orden.getTarjetaBancaria().getVd() != null && orden.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}
			
				codAutorizadorMP = orden.getTarjetaBancaria().getCodigoAutorizador();
			if (orden.getTarjetaBancaria().getCodigoConvenio() != null){
				nroConvenioMP = orden.getTarjetaBancaria().getCodigoConvenio().toString();
			}

		}
		
		if (orden.getTarjetaRegaloEmpresa()!= null) {
			if (orden.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
				medioPago = Constantes.BOL_FORMA_PAGO_TAR_REG_EMP;
			}
			if(orden.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && orden.getTarjetaRipley().getCorrelativoVenta()!=null){
				medioPago = Constantes.BOL_FORMA_PAGO_TRECRIP;
			}
		}
		
		return medioPago.toUpperCase();
	}
	
	private String obtenerNroUnico(OrdenesDeCompra orden){
		String numeroUnico = "NU";
		String fecha = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYY, orden.getNotaVenta().getHoraBoleta());
		String nroCaja = ((orden.getNotaVenta().getNumeroCaja()!=null)?orden.getNotaVenta().getNumeroCaja().toString():Constantes.VACIO);
		String nroBoleta = orden.getNotaVenta().getNumeroBoleta()!=null?orden.getNotaVenta().getNumeroBoleta().toString():Constantes.VACIO;
		fecha = fecha!=null?fecha:Constantes.VACIO;
		numeroUnico += Constantes.STRING_CERO + Constantes.STRING_CERO + orden.getNotaVenta().getNumeroSucursal();
		numeroUnico += Constantes.GUION;
		numeroUnico += nroCaja.length()==1?Constantes.STRING_CERO+Constantes.STRING_CERO+Constantes.STRING_CERO+nroCaja:Constantes.STRING_CERO+Constantes.STRING_CERO+nroCaja;
		numeroUnico += Constantes.GUION;
		numeroUnico += nroBoleta;
		numeroUnico += Constantes.GUION;
		numeroUnico += fecha.replace(Constantes.GUION,Constantes.VACIO);
		numeroUnico += Constantes.GUION;
		numeroUnico += "120000"; //TODO BPL De donde sale esto?????? 
		
		
		return numeroUnico;
	}
	
	public String getDTEMPos() throws MalformedURLException {
		
		logger.traceInfo("getDTEMPos", "Desde el web Consulta Json: getDTE-MPos ");
		String result = ERROR;

        if (datoBoleta.isEmpty() || datoBoleta.length() != 20 ) {
        	logger.traceInfo("execute", "OC no existe: "+oc);
			mensajeError = "OC no existe";
			session.put("mensajeError", mensajeError);
			result = ERROR;
			return result;        	
        }

		oc = Long.valueOf(datoBoleta.substring(6,16));
		sucursal = Integer.parseInt(datoBoleta.substring(0,3));
		estado = Integer.parseInt(datoBoleta.substring(18,19));
		//Datos para Boleta
		
		//Carga de Info BD
		orden = obtenerDatosTramaBoleta(new Long(oc));
		listaArticulos = obtenerArticulosTramaBoleta(new Long(oc));
		if (orden.getNotaVenta()== null || orden.getNotaVenta().getFechaCreacion()==null 
				|| orden.getNotaVenta().getCorrelativoVenta() == null || listaArticulos == null){
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Error en la obtenci�n de la informaci�n de la Boleta");	
			return result;
		}
		
		if (orden.getNotaVenta().getTipoDoc()==Constantes.TIPO_DOC_BOLETA){
			result = VERBOLETA;
		}
		else if(orden.getNotaVenta().getTipoDoc()==Constantes.TIPO_DOC_FACTURA){
			result = VERFACTURA;
		}
		
		codigoBarra = crearCodigoBarra(orden);	
		razonSocial = parametroDAO.getParametro("RAZON_SOCIAL_EMISOR").getValor();
		rutEmpresa = Util.formatoRUT(parametroDAO.getParametro("RUT_EMISOR").getValor());
		giro = parametroDAO.getParametro("GIRO_EMISOR").getValor();
		direccion = parametroDAO.getParametro("DIRECCION_ORIGEN").getValor();
		nombreSucursal = parametroDAO.getParametro("TRAMA_DIRECCION_SUCURSAL_"+orden.getNotaVenta().getNumeroSucursal()).getValor();
		ciudad = parametroDAO.getParametro("CIUDAD_ORIGEN").getValor();
		comunaOrigen = parametroDAO.getParametro("COMUNA_ORIGEN").getValor();
		paginaWeb = parametroDAO.getParametro("PAGINA_WEB").getValor();
		subTotal = obtenerSubTotalBoleta(listaArticulos);
		totalExento = obtenerTotalExento();
		total = obtenerTotal(subTotal,totalExento);
		medioPago = obtenerMedioPago(orden);
		nroUnico = obtenerNroUnico(orden);
		try {
			ted = Util.quitarSaltos(orden.getDocumentoElectronico().getXmlTDE().getSubString(1, (int) orden.getDocumentoElectronico().getXmlTDE().length()));
			logger.traceInfo("VoucherBoletaAction","BPL OrdenInfo: "+ted);
		} catch (SQLException e) {
			e.printStackTrace();
			result = ERROR;
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Error en la Obtenci�n de en la Boleta");
		}		
		totalNeto = obtenerTotalNeto(Float.parseFloat(subTotal));
		iva = obtenerIva(Float.parseFloat(totalNeto));
		montoEscrito = Util.MontoEscrito(total, Constantes.VERDADERO);
		lineas = new ArrayList<Linea>();
		List<String> textos = new ArrayList<String>();
		textos.add(razonSocial);
		textos.add(rutEmpresa);
		textos.add("GIRO: "+giro);
		textos.add("www.ripley.cl");
		Linea linea = new Linea();
		//Cabecera Inicio
		linea.setTipo("IMAGE_TEXT_RIGHT");		 
		linea.setImagen("http://home.ripley.cl/assets-email/img/ripley-logo.png");
		linea.setTexts(textos);
		lineas.add(linea);
//		linea = new Linea();
//		linea.setTipo("TEXT_COLUMN_2");
//		linea.setText2(razonSocial);
//		lineas.add(linea);
//		linea = new Linea();
//		linea.setTipo("TEXT_COLUMN_2");
//		linea.setText2(rutEmpresa);
//		lineas.add(linea);
//		linea = new Linea();
//		linea.setTipo("TEXT_COLUMN_2");
//		linea.setText2("GIRO: "+giro);
//		lineas.add(linea);
//		linea = new Linea();
//		linea.setTipo("TEXT_COLUMN_2");
//		linea.setText2("www.ripley.cl");
//		lineas.add(linea);
		
		//Salto de linea
		linea = new Linea();
		linea.setTipo("LINE_BREAK");
		lineas.add(linea);
		
		//
		linea = new Linea();
		linea.setText("SUCURSAL: " + orden.getNotaVenta().getNumeroSucursal() + "   CAJA: " + orden.getNotaVenta().getNumeroCaja());
		linea.setTipo("TEXT");
		lineas.add(linea);
		linea = new Linea();
		linea.setText("FECHA EMISION: " + FechaUtil.formatDate("dd/MM/yyyy",orden.getNotaVenta().getHoraBoleta()) + "   HORA: " + FechaUtil.formatDate("HH:mm:ss",orden.getNotaVenta().getHoraBoleta()));
		linea.setTipo("TEXT");
		lineas.add(linea);
		
		//Salto de linea
		linea = new Linea();
		linea.setTipo("LINE_BREAK");
		lineas.add(linea);
		
		//Codigo de barra
		linea = new Linea();
		linea.setTipo("BARCODE_GS1_128");
		linea.setValue(codigoBarra);
		lineas.add(linea);
		
		//Salto de linea
		linea = new Linea();
		linea.setTipo("LINE_BREAK");
		lineas.add(linea);
				
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("NRO TRANSACCION/DOCUMENTO: " + orden.getNotaVenta().getNumeroBoleta());
		lineas.add(linea);
		
		//Salto de linea
		linea = new Linea();
		linea.setTipo("LINE_BREAK");
		lineas.add(linea);
		
		//parte central Sucursal Internet = 39
		if(orden.getNotaVenta().getNumeroSucursal().intValue() != 39 && nombreSucursal != null && nombreSucursal.equalsIgnoreCase("OFF")){
			linea = new Linea();
			linea.setTipo("TEXT");
			linea.setText(nombreSucursal);
			lineas.add(linea);
		}
			
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("VENDEDOR: "+orden.getVendedor().getNombreVendedor());
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("CODIGO REGALO: "+orden.getNotaVenta().getCodigoRegalo());
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("NOMBRE CLIENTE: "+orden.getDespacho().getNombreCliente() +" "
										+orden.getDespacho().getApellidoPatCliente()+" "
										+ orden.getDespacho().getApellidoMatCliente());
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("RUT CLIENTE: "+ Util.formatoRUT2(getRutClienteConDv(orden.getDespacho().getRutCliente().toString())));
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("ORDEN DE COMPRA: "+orden.getNotaVenta().getCorrelativoVenta());
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("FECHA OC: "+FechaUtil.formatDate("dd/MM/yyyy HH:mm:ss", orden.getNotaVenta().getHoraBoleta()));
		lineas.add(linea);
		
		//linea
		linea = new Linea();
		linea.setTipo("LINE");
		lineas.add(linea);
		
		
		linea = new Linea();
		linea.setTipo("TEXT_COLUMN_3");
		linea.setText1("C�DIGO");
		linea.setText2("DESCRIPCION CANTIDAD X PRECIO");
		linea.setText3("VALOR");
		linea.setBold("1");
		linea.setSizeColumn1("15");
		linea.setSizeColumn3("10");
		lineas.add(linea);
		
		//linea
		linea = new Linea();
		linea.setTipo("LINE");
		lineas.add(linea);
		
//		listaArticulos
		
		if (listaArticulos!=null && !listaArticulos.isEmpty()){
			
			ArticulosVentaOC articulo = null;
			for (int i = 0; i < listaArticulos.size(); i++) {
				
				articulo = listaArticulos.get(i);
				linea = new Linea();
				linea.setTipo("TEXT_COLUMN_3");
				linea.setText1(articulo.getArticuloVenta().getCodArticulo());
				linea.setText2(articulo.getArticuloVenta().getDescRipley()+" "+
							   articulo.getArticuloVenta().getUnidades()+" "+ 
							   Util.formatNumber2(articulo.getArticuloVenta().getPrecio()));				
				linea.setText3(Util.formatNumber2(articulo.getArticuloVenta().getPrecioPorUnidades()));
				linea.setSizeColumn1("15");
				linea.setSizeColumn3("10");
				lineas.add(linea);
				
				if (articulo.getArticuloVenta().getMontoDescuento().longValue()>0){
					linea = new Linea();
					linea.setTipo("TEXT_COLUMN_3");
					linea.setText1("");
					linea.setText2("DESCUENTO");
					linea.setText3("-"+articulo.getArticuloVenta().getMontoDescuento());
					linea.setSizeColumn1("15");
					linea.setSizeColumn3("10");
					lineas.add(linea);
				}
				
				if (articulo.getArticuloVenta().getCodDespacho()!=null && (!"".equals(articulo.getArticuloVenta().getCodDespacho()) || !"".equals(articulo.getArticuloVenta().getCodDespacho()))){
					linea = new Linea();
					linea.setTipo("TEXT");
					linea.setText1("<BT>"+articulo.getArticuloVenta().getCodDespacho()+" - ["+orden.getDespacho().getDespachoId()+"]");
					lineas.add(linea);
				}
				
			}
		}
		
		linea = new Linea();
		linea.setTipo("line");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT_LEFT_RIGHT");
		linea.setTextLeft("SUBTOTAL");
		linea.setTextRight("$ "+subTotal);
		linea.setBold("1");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT_LEFT_RIGHT");
		linea.setTextLeft("TOTAL EXENTO");
		linea.setTextRight("$ "+totalExento);
		linea.setBold("1");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT_LEFT_RIGHT");
		linea.setTextLeft("TOTAL");
		linea.setTextRight("$ "+total);
		linea.setBold("1");
		lineas.add(linea);
		
		if (orden.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null){
			linea = new Linea();
			linea.setTipo("TEXT");
			linea.setText("T.R.EMPRESA $"+Util.formatNumber2(total));
			lineas.add(linea);
		}
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText(medioPago);
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("CODIGO AUT.: "+ codAutorizadorMP);
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("NRO UNICO: "+ nroUnico);
		lineas.add(linea);

		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("VUELTO 0");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText(FechaUtil.formatDate("dd/MM/yyyy", orden.getNotaVenta().getHoraBoleta())+" - [1] - VENTA "+total);
		lineas.add(linea);
		
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("DOCUMENTOS REFERENCIADOS");
		linea.setBold("1");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("TIPO DOCUMENTO: Orden de Compra");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("FOLIO: "+orden.getNotaVenta().getFolioSii());
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("FECHA:");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("RAZON:");
		lineas.add(linea);
		
		
		
		
		//Sector Timbre Electronico.
		linea = new Linea();
		linea.setValue(ted);
		linea.setTipo("SYMBOL_PDF417_STANDARD");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("TEXT");
		linea.setText("Timbre Electr�nico SII");
		linea.setSize("small");
		linea.setAlign("center");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("LINE_BREAK");
		lineas.add(linea);
		
		linea = new Linea();
		linea.setTipo("CUT");
		lineas.add(linea);
		return SUCCESS;
	}
	private String getRutClienteConDv(String rut){
		return rut+Util.getDigitoValidadorRut2(rut);

	}

	public List<Linea> getLineas() {
		return lineas;
	}


	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}
	
	
	
	public OrdenesDeCompra getOrden() {
		return orden;
	}

	public void setOrden(OrdenesDeCompra orden) {
		this.orden = orden;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getRutEmpresa() {
		return rutEmpresa;
	}

	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<ArticulosVentaOC> getListaArticulos() {
		return listaArticulos;
	}

	public void setListaArticulos(List<ArticulosVentaOC> listaArticulos) {
		this.listaArticulos = listaArticulos;
	}

	public void setUrlDoce(String urlDoce) {
		this.urlDoce = urlDoce;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
/*
	public Map<String, Object> getSession() {
		return session;
	}
*/
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Long getOc() {
		return oc;
	}

	public void setOc(Long oc) {
		this.oc = oc;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public int getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(int tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getDatoBoleta() {
		return datoBoleta;
	}

	public void setDatoBoleta(String datoBoleta) {
		this.datoBoleta = datoBoleta;
	}

	public String getUrlDoce() {
		return urlDoce;
	}

	public void seturlDoce(String urlDoce) {
		this.urlDoce = urlDoce;
	}

	public int getTipoEjecucion() {
		return tipoEjecucion;
	}

	public void setTipoEjecucion(int tipoEjecucion) {
		this.tipoEjecucion = tipoEjecucion;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getTotalExento() {
		return totalExento;
	}

	public void setTotalExento(String totalExento) {
		this.totalExento = totalExento;
	}

	public String getTotalNeto() {
		return totalNeto;
	}

	public void setTotalNeto(String totalNeto) {
		this.totalNeto = totalNeto;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	public String getCodAutorizadorMP() {
		return codAutorizadorMP;
	}

	public void setCodAutorizadorMP(String codAutorizadorMP) {
		this.codAutorizadorMP = codAutorizadorMP;
	}

	public String getNroConvenioMP() {
		return nroConvenioMP;
	}

	public void setNroConvenioMP(String nroConvenioMP) {
		this.nroConvenioMP = nroConvenioMP;
	}

	public String getNroUnico() {
		return nroUnico;
	}

	public void setNroUnico(String nroUnico) {
		this.nroUnico = nroUnico;
	}

	public String getTed() {
		return ted;
	}

	public void setTed(String ted) {
		this.ted = ted;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	public String getComunaOrigen() {
		return comunaOrigen;
	}

	public void setComunaOrigen(String comunaOrigen) {
		this.comunaOrigen = comunaOrigen;
	}

	public String getMontoEscrito() {
		return montoEscrito;
	}

	public void setMontoEscrito(String montoEscrito) {
		this.montoEscrito = montoEscrito;
	}

	public int getSucursal() {
		return sucursal;
	}

	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}

	public String getCajaTicketCambio() {
		return cajaTicketCambio;
	}

	public void setCajaTicketCambio(String cajaTicketCambio) {
		this.cajaTicketCambio = cajaTicketCambio;
	}

	public String getSucTicketCambio() {
		return sucTicketCambio;
	}

	public void setSucTicketCambio(String sucTicketCambio) {
		this.sucTicketCambio = sucTicketCambio;
	}

	public InputStream getFileStream() {
		return fileStream;
	}

	public void setFileStream(InputStream fileStream) {
		this.fileStream = fileStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
