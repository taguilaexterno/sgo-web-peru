package cl.ripley.omnicanalidad.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Pagina;
import cl.ripley.omnicanalidad.bean.ReservaDTE;
import cl.ripley.omnicanalidad.bean.ReservasUtil;
import cl.ripley.omnicanalidad.dao.ReservasDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReservasAction extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 6149177222251878649L;
	private static final AriLog logger = new AriLog(ReservasAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReservasDAO reservas;
	
	private Map<String, Object> session;
	private Long estado;
	private String mensaje;
	private String numeroReserva;
	private String rutCliente;
	private String estadoReserva;
	private ReservaDTE reserva;
	private String paginaActual;
	private String ultimaPagina;
	private List<Pagina> paginas;
	private List<ReservaDTE> listaReservas;

	public String execute() throws Exception {
		logger.initTrace("execute", "Long estado: [" + estado + "] String numeroReserva: ["+ numeroReserva + "] "
				+ "String rutCliente: ["+ rutCliente +"] String estadoReserva: ["+estadoReserva+"]");

		String result = ERROR;
		
		if(session == null || session.get("usuario") == null) {
			return Constantes.LOGGED_OUT;
		}
		
		int caso = ReservasUtil.validaEstado(estado);
		
		switch(caso) {
		case Constantes.NRO_CERO:
			result = Constantes.SUCCESS_INICIO;
			break;			
		case Constantes.NRO_UNO:
			result = buscarReservas(numeroReserva, rutCliente, estadoReserva, paginaActual);
			break;
		default:
			result = ERROR;
			break;
		}

		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);

		return result;
	}


	private String buscarReservas(String numeroReserva, String rutCliente,
			String estadoReserva, String paginaActual) {

		logger.initTrace("buscarReservas", "String numeroReserva: [" + numeroReserva + "] String rutCliente: ["
				+ rutCliente + "] String estadoReserva: [" + estadoReserva + "]");

		this.estadoReserva 	= estadoReserva;
		this.rutCliente 	= rutCliente;
		this.numeroReserva 	= numeroReserva;
		
		String result = ERROR;
		
		List<ReservaDTE> retorno = reservas.listarReservas(numeroReserva, rutCliente, estadoReserva);
		
		if (retorno.size()<1){
			mensaje = "No se han encontrado reservas para los datos consultados.";
			result = Constantes.SUCCESS_INICIO;
		}else if(retorno.size()==1){			
			reserva = retorno.get(0);
			result = Constantes.SUCCESS_DETAIL;
		}else{
			int pag = ReservasUtil.StringToInt(paginaActual);
			int registrosPagina = reservas.getPaginas();
			double calculo = Math.ceil((double)retorno.size() / (double)registrosPagina);
			paginas = ReservasUtil.getPaginas(pag, calculo);
			listaReservas = ReservasUtil.getPagina(retorno, pag, registrosPagina);
			ultimaPagina = (int) calculo + "";
			this.paginaActual = pag+"";
			result = Constantes.SUCCESS_LIST;
		}

		logger.endTrace("buscarReservas", "Finalizado", "RESULTADO: " + result);
		return result;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public String getNumeroReserva() {
		return numeroReserva;
	}

	public void setNumeroReserva(String numeroReserva) {
		this.numeroReserva = numeroReserva;
	}

	public String getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}



	public String getEstadoReserva() {
		return estadoReserva;
	}


	public void setEstadoReserva(String estadoReserva) {
		this.estadoReserva = estadoReserva;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ReservaDTE getReserva() {
		return reserva;
	}


	public void setReserva(ReservaDTE reserva) {
		this.reserva = reserva;
	}


	public List<ReservaDTE> getListaReservas() {
		return listaReservas;
	}


	public void setListaReservas(List<ReservaDTE> listaReservas) {
		this.listaReservas = listaReservas;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
		
	}

	public List<Pagina> getPaginas() {
		return paginas;
	}


	public void setPaginas(List<Pagina> paginas) {
		this.paginas = paginas;
	}


	public Map<String, Object> getSession() {
		return session;
	}

	public String getPaginaActual() {
		return paginaActual;
	}


	public void setPaginaActual(String paginaActual) {
		this.paginaActual = paginaActual;
	}


	public String getUltimaPagina() {
		return ultimaPagina;
	}


	public void setUltimaPagina(String ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

}
