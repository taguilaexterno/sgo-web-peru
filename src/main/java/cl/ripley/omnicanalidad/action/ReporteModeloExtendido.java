package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.ReporteModeloExtendidoDTO;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ReportesDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador para reporte modelo extendido.
 *
 * @author Carlos Leal (Aligare).
 * @since 13-06-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReporteModeloExtendido extends ActionSupport implements SessionAware, UsuarioHabilitado {
	private static final AriLog logger = new AriLog(ReporteModeloExtendido.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReportesDAO reportesDAO;
	
	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings({ "rawtypes" })
	private Map session;
	private Usuario usuario;
	
	private String metodo;
	private String fechaInicio;
	private String fechaFin;
	private Long numeroOrden;
	private Long numeroDNI;
	private String estado;
	private String tipo;
	private List<ReporteModeloExtendidoDTO> reporte;
	private HashMap<String, String> estados;
	private String json;
	

	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = Constantes.VACIO;
		
		if(session == null || session.get("usuario") == null) {
			return Constantes.LOGGED_OUT;
		}
		
		metodo = (null==metodo?"index":metodo);
		estados = reportesDAO.obtenerEstados();
		
		switch(metodo) {
		case "index":
			
			respuesta = Constantes.SUCCESS_INICIO;
			break;
		case "consulta":
			String est = Constantes.GUION.equals(estado) || Constantes.VACIO.equals(estado) ? Constantes.NRO_MIL : estado;
			reporte = new ArrayList<>();
			reporte = reportesDAO.buscarInformacionME(fechaInicio, fechaFin, numeroOrden, numeroDNI, est, tipo);
			Gson gson = new Gson();
			json = gson.toJson(reporte);
			respuesta = Constantes.SUCCESS_JSON; 
			break;
		default: 
			respuesta = ERROR;
			break;
		}
			
		return respuesta;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	

	
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Long getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(Long numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public Long getNumeroDNI() {
		return numeroDNI;
	}

	public void setNumeroDNI(Long numeroDNI) {
		this.numeroDNI = numeroDNI;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<ReporteModeloExtendidoDTO> getReporte() {
		return reporte;
	}

	public void setReporte(List<ReporteModeloExtendidoDTO> reporte) {
		this.reporte = reporte;
	}

	public HashMap<String, String> getEstados() {
		return estados;
	}

	public void setEstados(HashMap<String, String> estados) {
		this.estados = estados;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	
	
	
}
