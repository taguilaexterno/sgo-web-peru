package cl.ripley.omnicanalidad.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.UsuarioDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador del Index de la aplicación.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Index extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 3031249069271519709L;
	private static final AriLog logger = new AriLog(Index.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	private static final String LOGGED_IN = "loggedin";
	
	private String res = "mobil";
	private int recaptcha= 0;
	
	@SuppressWarnings("rawtypes")
	private Map session;

	@SuppressWarnings("rawtypes")
	public Map getSession() {
		return session;
	}

	@SuppressWarnings("rawtypes")
	public void setSession(Map session) {
		this.session = session;
	}
		
	@Autowired
	private ParametroDAO parametroDAO;

	public String execute() throws Exception {
		
		logger.initTrace("execute", "Se valida la sesion del usuario para saber si llegar a login o redireccionar a welcome");
		String resultado = SUCCESS;			
		
		setRecaptcha(parametroDAO.getParamByName("RECAPTCHA_ON_OFF"));
		
		if(session != null && session.get("usuario") != null) {
			resultado = LOGGED_IN;
		}
		logger.endTrace("execute", "Resultado = " + resultado, resultado);
		return resultado;
	}
		
	
	public int getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(int recaptcha) {
		this.recaptcha = recaptcha;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}

}
