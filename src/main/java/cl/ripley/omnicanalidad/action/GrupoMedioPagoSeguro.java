package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.GrupoMPSeguro;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ReglasMPSeguroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de Grupo de Medio de Pago Seguro.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GrupoMedioPagoSeguro extends ActionSupport implements UsuarioHabilitado {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(GrupoMedioPagoSeguro.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReglasMPSeguroDAO grupoReglasMpDao;
	
	private ArrayList<GrupoMPSeguro> grupoReglasMp;
	private GrupoMPSeguro grupoMP;
	private final String UPDATEESTADO = "UPDATEESTADO";
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ArrayList<GrupoMPSeguro> getGrupoReglasMp() {
		return grupoReglasMp;
	}
	public void setGrupoReglasMp(ArrayList<GrupoMPSeguro> grupoReglasMp) {
		this.grupoReglasMp = grupoReglasMp;
	}
	public GrupoMPSeguro getGrupoMP() {
		return grupoMP;
	}
	public void setGrupoMP(GrupoMPSeguro grupoMP) {
		this.grupoMP = grupoMP;
	}

	
	/**Administrador de acciones de Grupo de Medio de Pago Seguro
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String admGrupoMPSeguro() {
		
		logger.initTrace("execute", null);
		String respuesta = SUCCESS;
		
		logger.traceInfo("Grupo Medio Pago Seguro", "Ingreso metodo");
		

		/*
		 * Update estado grupo
		 */
		if (grupoMP!=null) {
		logger.traceInfo("Grupo Medio Pago Seguro. Metodo : " + grupoMP.getMetodo(), "inicio metodo");
		
			if (grupoMP != null && grupoMP.getMetodo().equalsIgnoreCase(UPDATEESTADO)) {
				logger.traceInfo("Grupo Medio Pago Seguro. Estado : " + grupoMP.getEstado(), "inicio llamada dao");
				int estado = grupoMP.getEstado()==Constantes.ESTADO_ACTIVO?Constantes.ESTADO_INACTIVO:Constantes.ESTADO_ACTIVO;			
				logger.traceInfo("UPDATE. estado : " + estado + ", id : " + grupoMP.getId(), "fin llamada dao");
				boolean update = grupoReglasMpDao.updateEstadoGrupo(grupoMP.getId(), estado);
				logger.traceInfo("Grupo Medio Pago Seguro update estado : " + update, "fin dao");
			}
		}
		
		grupoReglasMp = grupoReglasMpDao.getGrupoReglasMP();
			
		
		logger.endTrace("execute", "Finalizado", "admGrupoMPSeguro: "+grupoReglasMp.size()+" Respuesta: "+respuesta);
		return respuesta;
	}

}
