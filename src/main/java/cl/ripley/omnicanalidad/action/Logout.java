package cl.ripley.omnicanalidad.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de cierre de sesión de usuario.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Logout extends ActionSupport implements SessionAware, UsuarioHabilitado {
	
	private static final long serialVersionUID = 6791749680919621218L;
	private static final AriLog logger = new AriLog(Logout.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	@SuppressWarnings("rawtypes")
	private Map session;
	private Usuario usuario;
	

	@SuppressWarnings("rawtypes")
	public Map getSession() {
		return session;
	}

	@SuppressWarnings("rawtypes")
	public void setSession(Map session) {
		this.session = session;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public String execute() throws Exception {
		String respuesta = null;
		
		/*SE OBTIENE EL USUARIO DE LA SESION.*/
		Usuario user = (Usuario) session.get("usuario");
		if(user == null){
			logger.traceInfo("execute", "Usuario con Sesion invalida, se retorna a Login");
			return SUCCESS;
		}
		try{			
			if (session != null) {
				logger.traceInfo("execute", "Cerrando sesion del usuario [nombreUsuario]");
				/*SE ELIMINA EL USUARIO DE LA SESION*/
				session.remove("usuario"); 
				//elimina lista de perfiles de la sesion
				session.remove("perfilesSession"); 
				//eliminar nivel de permiso
				session.remove("nivelPermiso");
				respuesta = SUCCESS;
			}
		}catch(Exception e){
			logger.traceError("Error al cerrar sesion del usuario", e);
			respuesta = ERROR;
		}
		return respuesta;
	}
}
