package cl.ripley.omnicanalidad.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.DetalleCompra;
import cl.ripley.omnicanalidad.bean.DetalleDespacho;
import cl.ripley.omnicanalidad.bean.DetalleOC;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de confirmación de boletas
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ConfirmacionBoletas extends ActionSupport implements SessionAware, UsuarioHabilitado {
	private static final AriLog logger = new AriLog(ConfirmacionBoletas.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings({ "rawtypes", "unused" })
	private Map session;
	private Usuario usuario;
	
	private int estado;
	private int nroOrdenes;
	private String metodo;
	private String seleccionados;
	private final String ANULAROC = "ANULAROC";
	private final String GENERAROC = "GENERAROC";
	private final String DETALLE = "DETALLE";
	private boolean cajaEstado;

	private List<OrdenesDeCompra> ordenesCompra;
	private DetalleCliente datosCliente;
	private List<DetalleDespacho> datosDespacho;
	private List<DetalleCompra> datosCompra;

	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = ERROR;
		Error errorBean = new Error();
		boolean cajaNulaFlag = Boolean.FALSE;
		
		List<String> nroCajas = Arrays.asList(parametroDao.getParametro("NROS_CAJAS_BOLETAS").getValor().split(Constantes.COMA));
		Integer sucursal = new Integer(parametroDao.getParametro("SUCURSAL").getValor());
		
		
		/*
		 * Anular OC
		 */
		logger.traceInfo("ConfirmacionBoletas. Metodo : " + metodo, "inicio metodo");
		Integer newEstado = estado;	
		if (metodo != null && metodo.equalsIgnoreCase(ANULAROC)) {
			
			newEstado = new Integer(Constantes.ESTADO_RECHAZO_MANUAL);
//			newEstado = new Integer(Constantes.ESTADO_PEND_IMPRESION_POST_NCT);
			logger.traceInfo("ConfirmacionBoletas anular OC : " + seleccionados, "inicio dao");
			boolean anular = ordenesCompraDAO.updateEstadoOrdenes(seleccionados, newEstado, 3, cajaNulaFlag, usuario.getUsuario(), null);
			logger.traceInfo("ConfirmacionBoletas anular OC : " + anular, "fin dao");
		} else if (metodo != null && metodo.equalsIgnoreCase(GENERAROC)) {
			newEstado = new Integer(Constantes.ESTADO_PENDIENTE_IMPRESION);
			logger.traceInfo("ConfirmacionBoletas anular OC : " + seleccionados + " ; cajaEstado : " + cajaEstado, "inicio dao");
			//se comenta porque no debiera enviarse más una caja null.
//			if(cajaEstado == true) {
//				cajaNulaFlag = true;
//			}
			boolean anular = ordenesCompraDAO.updateEstadoOrdenes(seleccionados, newEstado, 1, cajaNulaFlag, usuario.getUsuario(), Constantes.ESTADOS_VALIDAR_GENERACION_DTE);
			logger.traceInfo("ConfirmacionBoletas updateEstadoOrdenes generar OC : " + anular, "fin dao");
			
			boolean estadomkp = ordenesCompraDAO.updateEstadoOCMirakl(seleccionados, Constantes.MIRAKL_API_OR02, Constantes.MIRAKL_ESTADO_INGRESAR, usuario.getUsuario());
			logger.traceInfo("ConfirmacionBoletas updateEstadoOCMirakl: " + estadomkp, "fin dao");
			
			if(anular == true && estadomkp == true){
				addActionMessage("DTE(s)  generado(s)");
			}else{
				addActionError("Error al generar DTE");
			}
			
		} else if (metodo != null && metodo.equalsIgnoreCase(DETALLE) && seleccionados != null) {
			logger.traceInfo("ConfirmacionBoletas detalle OC : ", "inicio dao");
			DetalleOC detalleOC = ordenesCompraDAO.obtenerDetalleOC(Integer.parseInt(seleccionados));
			datosCliente = detalleOC.getDatosCliente();
			datosDespacho = detalleOC.getDatosDespacho();
			datosCompra = detalleOC.getDatosCompra();
			logger.traceInfo("ConfirmacionBoletas detalle OC : " + datosCliente.getCorrelativoVentas(), "fin dao");
			return respuesta = Constantes.SUCCESS_DETALLE_BOLETA;
		}
		
		/*
		 * Buscar OC estado 8 con caja boletas, 88 con caja boletas y/o sin caja
		 * 20191001: Se agrega estado 94 y 95 (Cybersource)
		 */
		if (estado == Constantes.ESTADO_PENDIENTE_APROBACION || estado == Constantes.ESTADO_STOCK_OK_APROBAR_MP
		 || estado == Constantes.ESTADO_PENDIENTE_CYBERSOURCE_DD || estado == Constantes.ESTADO_PENDIENTE_CYBERSOURCE_RT){
			
			ordenesCompra =	nroCajas.stream()
							.collect(ArrayList<OrdenesDeCompra>::new, 
									(lista, nroCaja) -> {
										lista.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(nroCaja), sucursal, new Integer(Constantes.ESTADO_PENDIENTE_APROBACION), null, null));
										lista.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(nroCaja), sucursal, new Integer(Constantes.ESTADO_STOCK_OK_APROBAR_MP), null, null));
										lista.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(nroCaja), sucursal, new Integer(Constantes.ESTADO_PENDIENTE_CYBERSOURCE_DD), null, null));
										lista.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(nroCaja), sucursal, new Integer(Constantes.ESTADO_PENDIENTE_CYBERSOURCE_RT), null, null));
									}, 
									List::addAll);
			
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_PENDIENTE_APROBACION), null, null));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_STOCK_OK_APROBAR_MP), null, null));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_PENDIENTE_CYBERSOURCE_DD), null, null));
			ordenesCompra.addAll(ordenesCompraDAO.obtenerOrdenes(null, sucursal, new Integer(Constantes.ESTADO_PENDIENTE_CYBERSOURCE_RT), null, null));
			
			
			respuesta = Constantes.SUCCESS_PAGO_BOLETA;
		} else
		if (estado == Constantes.ESTADO_APROBACION_MANUAL){
			
			ordenesCompra =	nroCajas.stream()
					.collect(ArrayList<OrdenesDeCompra>::new, 
							(lista, nroCaja) -> lista.addAll(ordenesCompraDAO.obtenerOrdenes(Integer.valueOf(nroCaja), sucursal, new Integer(Constantes.ESTADO_APROBACION_MANUAL), null, null)), 
							List::addAll);
			
			respuesta = Constantes.SUCCESS_APROBACION_BOLETA;
		} else {
			errorBean.setCodError(2);
			errorBean.setErrorMsg("OPCION DE MENU INEXISTENTE");
			logger.traceInfo("execute", "Estado inexistente, estado consultado: "+estado);
			respuesta = ERROR;
		}

		setOrdenesCompra(ordenesCompra);
		
		nroOrdenes = ordenesCompra.size();
		
		logger.endTrace("execute", "Finalizado", "ordenesCompra: "+ordenesCompra.size()+" Respuesta: "+respuesta);
		return respuesta;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<OrdenesDeCompra> getOrdenesCompra() {
		return ordenesCompra;
	}

	public void setOrdenesCompra(List<OrdenesDeCompra> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getNroOrdenes() {
		return nroOrdenes;
	}

	public void setNroOrdenes(int nroOrdenes) {
		this.nroOrdenes = nroOrdenes;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getSeleccionados() {
		return seleccionados;
	}

	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}

	public DetalleCliente getDatosCliente() {
		return datosCliente;
	}

	public void setDatosCliente(DetalleCliente datosCliente) {
		this.datosCliente = datosCliente;
	}

	public List<DetalleDespacho> getDatosDespacho() {
		return datosDespacho;
	}

	public void setDatosDespacho(List<DetalleDespacho> datosDespacho) {
		this.datosDespacho = datosDespacho;
	}

	public List<DetalleCompra> getDatosCompra() {
		return datosCompra;
	}

	public void setDatosCompra(List<DetalleCompra> datosCompra) {
		this.datosCompra = datosCompra;
	}

	public boolean isCajaEstado() {
		return cajaEstado;
	}

	public void setCajaEstado(boolean cajaEstado) {
		this.cajaEstado = cajaEstado;
	}
}

