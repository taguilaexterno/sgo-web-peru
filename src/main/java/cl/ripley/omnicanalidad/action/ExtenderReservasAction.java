package cl.ripley.omnicanalidad.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ReservaDTE;
import cl.ripley.omnicanalidad.bean.ReservasUtil;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.dao.ReservasDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtenderReservasAction extends ActionSupport implements SessionAware {
	private static final AriLog logger = new AriLog(ExtenderReservasAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReservasDAO reservas;
	
	private Long estado;
	private String numeroReserva;
	private String fechaVigencia;
	private String horaVigencia;
	private String mensaje;
	private ReservaDTE reserva;
	private Map<String, Object> session;


	public String execute() throws Exception {

		logger.initTrace("execute", "Long estado: " + estado);

		if(session == null || session.get("usuario") == null) {
			return Constantes.LOGGED_OUT;
		}
		
		String result = ERROR;
		int caso = ReservasUtil.validaEstado(estado);

		switch(caso) {
		case Constantes.NRO_CERO:
			result = Constantes.SUCCESS_INICIO;
			break;
		case Constantes.NRO_UNO:
			result = modificarFechaVigencia(numeroReserva, fechaVigencia, horaVigencia);
			break;
		default:
			result = ERROR;
			break;
		}

		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		return result;
	}

	private String modificarFechaVigencia(String numeroReserva2,
			String fechaVigencia2, String horaVigencia2) {

		logger.initTrace("modificarFechaVigencia", "String numeroReserva: [" + numeroReserva2 + "] "
				+ "String fechaVigencia: ["+ fechaVigencia2 +"] String horaVigencia: ["+horaVigencia2+"]");
		String resultado = "";
		Long numReserva = Long.valueOf(0);
		String fechaVigenciaFinal = "";
		try{
			numReserva = ReservasUtil.StringToLong(numeroReserva2);
			fechaVigenciaFinal = fechaVigencia2 + " " + horaVigencia2;

			if (numReserva == 0){
				mensaje = "Número de reserva ingresado no válido ["+ numeroReserva2 +"]";
				resultado = Constantes.SUCCESS_INICIO;
			}else{

				Integer retorno = reservas.extenderReserva(numReserva, fechaVigenciaFinal, (Usuario) session.get("usuario"));
				
				if (retorno == 10){
					mensaje = "La nueva fecha de reserva, no puede ser mayor a la fecha de vigencia en BigTicket ["+ numReserva +"]";
					resultado = Constantes.SUCCESS_INICIO;
				}else if(retorno == 100){
					mensaje = "No se encontraron registros para actualizar con el numero de reserva ["+ numReserva +"]";
					resultado = Constantes.SUCCESS_INICIO;
				}else if(retorno == -1){
					mensaje = "Error al actualizar la nueva fecha de vigencia para el numero de reserva ["+ numReserva +"]";
					resultado = Constantes.SUCCESS_INICIO;
				}else{
					mensaje = "Reserva ["+ numReserva +"] extendida con �xito";
					List<ReservaDTE> listaReservas = reservas.listarReservas(numeroReserva2, "0", "0");
					setReserva(listaReservas.get(0));
					resultado = Constantes.SUCCESS_EXTEND;
				}
			}

		}catch(Exception e){
			logger.traceError("modificarFechaVigencia", e);
			resultado = Constantes.SUCCESS_INICIO;
		}

		logger.endTrace("modificarFechaVigencia", "Finalizado", "RESULTADO: " + resultado);
		return resultado;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getHoraVigencia() {
		return horaVigencia;
	}

	public void setHoraVigencia(String horaVigencia) {
		this.horaVigencia = horaVigencia;
	}

	public ReservaDTE getReserva() {
		return reserva;
	}

	public void setReserva(ReservaDTE reserva) {
		this.reserva = reserva;
	}


	public String getNumeroReserva() {
		return numeroReserva;
	}

	public void setNumeroReserva(String numeroReserva) {
		this.numeroReserva = numeroReserva;
	}

	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
}
