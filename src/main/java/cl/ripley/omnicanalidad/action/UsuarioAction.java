package cl.ripley.omnicanalidad.action; 

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Perfil;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.PerfilesDAO;
import cl.ripley.omnicanalidad.dao.PermisosDAO;
import cl.ripley.omnicanalidad.dao.UsuarioDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Action para acciones de usuario
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 08-08-2016
 * Versiones:
 * <ul>
 *  <li>08-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UsuarioAction extends ActionSupport implements UsuarioHabilitado, SessionAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8897448141459037349L;
	private static final AriLog logger = new AriLog(UsuarioAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private PerfilesDAO perfilesDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@SuppressWarnings("unused")
	@Autowired
	private PermisosDAO permisosDAO;

	private List<Perfil> perfiles;

	private Error error;

	private Usuario usuarioBean;

	private String usuarioUsr;

	private String mensaje;

	private List<Usuario> usuarios;
	
	private List<String> searchEngine;

	private String yourSearchEngine;
	
	private String defaultSearchEngine;

	@SuppressWarnings("unused")
	private Usuario usuario;
	
	private Map<String, Object> session;

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;

	}

	public List<Perfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<Perfil> perfiles) {
		this.perfiles = perfiles;
	}		
	
	public List<String> getSearchEngine() {
		return searchEngine;
	}

	public void setSearchEngine(List<String> searchEngine) {
		this.searchEngine = searchEngine;
	}

	public String getYourSearchEngine() {
		return yourSearchEngine;
	}

	public void setYourSearchEngine(String yourSearchEngine) {
		this.yourSearchEngine = yourSearchEngine;
	}
	
	public String getDefaultSearchEngine() {
		return defaultSearchEngine;
	}

	public void setDefaultSearchEngine(String defaultSearchEngine) {
		this.defaultSearchEngine = defaultSearchEngine;
	}

	public UsuarioAction(){
		
		searchEngine = new ArrayList<String>();
		searchEngine.add("Activo");
		searchEngine.add("Caducado");
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Usuario getUsuarioBean() {
		return usuarioBean;
	}

	public void setUsuarioBean(Usuario usuarioBean) {
		this.usuarioBean = usuarioBean;
	}


	public String getUsuarioUsr() {
		return usuarioUsr;
	}

	public void setUsuarioUsr(String usuarioUsr) {
		this.usuarioUsr = usuarioUsr;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	/**Crea usuario nuevo
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String crearUsuario() {
		logger.traceInfo("crearUsuario", "Ingreso metodo");

		//rescatar lista de perfiles del sistema
		perfiles = perfilesDAO.getPerfiles();

		if(perfiles == null || perfiles.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se han encontrado perfiles");
			return ERROR;
		}

		return SUCCESS;
	}

	/**Guarda el usuario.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws ParseException 
	 */
	public String guardarUsuario() throws ParseException {
		logger.initTrace("guardarUsuario", "Ingreso metodo");

		String res = SUCCESS;
		

		error = new Error();
		boolean result = usuarioDAO.guardarUsuario(usuarioBean, error);

		if(!result) {
			addActionError(error.getErrorMsg());
			return INPUT;
		}	
		
		mensaje = "Usuario creado exitosamente";
		session.put("mensaje", mensaje);
		
		return res;
	}

	/**Obtiene un usuario con su perfil.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws ParseException 
	 */
	public String buscarUsuarioJson() throws ParseException {
		logger.initTrace("buscarUsuarioJson", "Ingreso metodo");
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());	
		
		usuarioBean = usuarioDAO.buscarUsuario(usuarioUsr);
		usuarioBean = usuarioDAO.getUsuarioConPerfil(usuarioBean);
		usuarioBean.setPassword(null);
		usuarioBean.setPasswordValid(null);
					
		if(usuarioBean == null) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontró usuario");
			return ERROR;
		}
		
		if(date.before(formatter.parse(usuarioBean.getFecFinVigencia()))) {			
			defaultSearchEngine = "Activo";
			usuarioBean.setVigencia("Activo");
		}else {						
			defaultSearchEngine = "Caducado";
			usuarioBean.setVigencia("Caducado");
		}
					
		logger.endTrace("buscarUsuarioJson", "Finalizado", "Usuario: " + usuarioBean);

		return SUCCESS;
	}

	/**Actualiza el usuario
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws ParseException 
	 */
	public String actualizarUsuario() throws ParseException {
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());	
		
		logger.initTrace("actualizarUsuario", "Ingreso metodo");
		
		logger.traceInfo("actualizarUsuario", "Valido password");
		//Primero valido si el usuario viene con password para validar que venga con confirmacion y que sean iguales
		if(usuarioBean != null && 
				((usuarioBean.getPassword() != null && !"".equals(usuarioBean.getPassword())) 
						|| (usuarioBean.getPasswordValid() != null && !"".equals(usuarioBean.getPasswordValid())))) {

			logger.traceInfo("actualizarUsuario", "cumple condici�n de passwords con datos");
			if(usuarioBean.getPassword() != null && !"".equals(usuarioBean.getPassword())
					&& !usuarioBean.getPassword().equals(usuarioBean.getPasswordValid())) {
				logger.traceInfo("actualizarUsuario", "Las contraseñas no son iguales 1");
				addActionError("La contrase�a no coincide");
				listaUsuarios();
				return SUCCESS;
			} else if(usuarioBean.getPasswordValid() != null && !"".equals(usuarioBean.getPasswordValid())
					&& !usuarioBean.getPasswordValid().equals(usuarioBean.getPassword())) {
				logger.traceInfo("actualizarUsuario", "Las contraseñas no son iguales 2");
				addActionError("La contrase�a no coincide");
				listaUsuarios();
				return SUCCESS;
			}

		}	
				
		//valido que usuario este caducado para actualizar fecha videncia
		if(yourSearchEngine != null) {
			if(yourSearchEngine.equals("Activo")) {
				if(date.before(formatter.parse(usuarioBean.getFecFinVigencia()))) {			
					usuarioBean.setVigencia("");
				}else {
					usuarioBean.setVigencia("Actualizar");
				}
			}		
		}
			
		//Actualizo el usuario
		logger.traceInfo("actualizarUsuario", "Actualizar usuario");
		
		boolean result = usuarioDAO.actualizarUsuario(usuarioBean);
		
		if(!result) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Problema al actualizar usuario.");
			return ERROR;
		}

		if(usuarioBean.getCodEstado() == 0) {
			mensaje = "Usuario eliminado con Éxito.";
		} else {
			mensaje = "Usuario actualizado con Éxito.";
		}

		session.put("mensaje", mensaje);

		return SUCCESS;
	}

	/**Obtiene lista de usuarios
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws ParseException 
	 */
	public String listaUsuarios() throws ParseException{
		logger.initTrace("listaUsuarios", "Ingreso metodo");

		if(usuarioBean != null && usuarioBean.getUsuario() != null && !"".equals(usuarioBean.getUsuario().trim())) {
			usuarios = usuarioDAO.buscarUsuarioLike(usuarioBean.getUsuario());
		} else {
			usuarios = usuarioDAO.getUsuarios();
		}

		if(usuarios == null || usuarios.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Problema al obtener usuarios.");
			return ERROR;
		}
		
		//fecha fin vigencia
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());		        
		//Recorro usuarios para completarlos con el perfil
		for(Iterator<Usuario> it = usuarios.iterator(); it.hasNext();) {			
			Usuario usr = it.next();						
			usuarioDAO.getUsuarioConPerfil(usr);
			if(usr.getFecFinVigencia()==null) {
				usr.setVigencia("Fecha no ingresada");
			}else {
				if(date.before(formatter.parse(usr.getFecFinVigencia()))) {
					usr.setVigencia("Activo");				
				}else {
					usr.setVigencia("Caducado");					
				}
			}
		}

		//rescatar lista de perfiles del sistema
		perfiles = perfilesDAO.getPerfiles();

		if(perfiles == null || perfiles.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se han encontrado perfiles");
			return ERROR;
		}							
		
		mensaje = (String) session.get("mensaje");
		session.remove("mensaje");

		logger.endTrace("listaUsuarios", "Finalizado", "Usuarios: " + usuarios);

		return SUCCESS;
	}


}
