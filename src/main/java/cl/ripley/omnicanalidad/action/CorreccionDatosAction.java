package cl.ripley.omnicanalidad.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CajaSucursal;
import cl.ripley.omnicanalidad.bean.IndexedValue;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ICorreccionDatosDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CorreccionDatosAction extends ActionSupport implements UsuarioHabilitado, SessionAware {
	
	private static final AriLog logger = new AriLog(CorreccionDatosAction.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ICorreccionDatosDAO correccionDatosDAO;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	private Map<String, Object> session;
	private Usuario usuario;
	
	private List<Map<String, Object>> queryResult;
	private List<Map<String, Object>> queryResultConsulta;
	private String selectedOC;
	private String selectedSuc;
//	private String selectedCaja;
	private String selectTodo;
	private String update;
	
	private String mensajeError;
	private String mensajeExito;
	
	private String consulta;
	
	private Integer tipoQuery;
	
	
	@Transactional
	public String process() {
		logger.initTrace("process", null);
		
		update = validarQuerys();
		
		Optional<String> optSelectTodo = Optional.ofNullable(selectTodo);
		Optional<String> optSelectedOC = Optional.ofNullable(selectedOC);
		Optional<String> optSelectedSuc = Optional.ofNullable(selectedSuc);
//		Optional<String> optSelectedCaja = Optional.ofNullable(selectedCaja);
		
		if((!optSelectTodo.isPresent() || optSelectTodo.get().trim().isEmpty())
				&& (!optSelectedOC.isPresent() || optSelectedOC.get().trim().isEmpty())) {
			
			logger.traceInfo("process", "No seleccionó OC");
			queryResult = correccionDatosDAO.ejecutarSelect();
			
			mensajeError = "Debe seleccionar alguna OC";
			return SUCCESS;
		}
		
		List<Long> correlativos = null;
		List<Integer> sucursales = null;
//		List<Integer> cajas = null;
		List<CajaSucursal> cajaSuc = new ArrayList<CajaSucursal>();
		
		if(optSelectTodo.isPresent() && !optSelectTodo.get().trim().isEmpty()) {
			
			correlativos =	correccionDatosDAO.ejecutarSelect().stream()
							.map(it -> (BigDecimal) it.get("CORRELATIVO_VENTA"))
							.map(BigDecimal::longValue)
							.collect(Collectors.toCollection(ArrayList::new));
			
			sucursales =	correccionDatosDAO.ejecutarSelect().stream()
					.map(it -> (BigDecimal) it.get("NUMERO_SUCURSAL"))
					.map(BigDecimal::intValue)
					.collect(Collectors.toCollection(ArrayList::new));
			
//			cajas =	correccionDatosDAO.ejecutarSelect().stream()
//					.map(it -> (BigDecimal) it.get("NUMERO_CAJA"))
//					.map(BigDecimal::intValue)
//					.collect(Collectors.toCollection(ArrayList::new));
							
		} else {
			
			correlativos =	Stream.of(optSelectedOC.get().split(Constantes.COMA))
							.map(String::trim)
							.map(Long::valueOf)
							.collect(Collectors.toCollection(ArrayList::new));
			
			sucursales = Stream.of(optSelectedSuc.get().split(Constantes.COMA))
					.map(String::trim)
					.map(Integer::valueOf)
					.collect(Collectors.toCollection(ArrayList::new));
			
//			cajas = Stream.of(optSelectedCaja.get().split(Constantes.COMA))
//					.map(String::trim)
//					.map(Integer::valueOf)
//					.collect(Collectors.toCollection(ArrayList::new));
		}
		for(int i=0;i < correlativos.size(); i++) {
			CajaSucursal cajaSucOc= new CajaSucursal();
//			cajaSucOc.setCaja(cajas.get(i));
			cajaSucOc.setSucursal(sucursales.get(i));
			cajaSucOc.setOc(correlativos.get(i));
			cajaSuc.add(cajaSucOc);
		}
		
		correccionDatosDAO.ejecutarUpdate(cajaSuc);
		
		mensajeExito = "Ejecutado.";
		
		queryResult = correccionDatosDAO.ejecutarSelect();
		
		logger.endTrace("process", "Finalizado", null);
		return SUCCESS;
	}
	
	public String view() {
		logger.initTrace("view", null);
		
		update = validarQuerys();
		queryResult = correccionDatosDAO.ejecutarSelect();
		
		logger.endTrace("view", "Finalizado", "queryResult = " + String.valueOf(queryResult));
		return SUCCESS;
	}

	public String consultaData() {
		logger.initTrace("consultaData", null);
		update = validarQuerys();
		
//		queryResult = correccionDatosDAO.ejecutarSelect();
		
		Optional<String> optConsulta = Optional.ofNullable(consulta);
		if(!optConsulta.isPresent() || optConsulta.get().trim().isEmpty()) {
//			mensajeError = "Debe ingresar una consulta.";
			return SUCCESS;
		}
		
		String[] consultaSplit = optConsulta.get().split(Constantes.PUNTO_COMA);
		//Si es consulta y viene más de una sentencia, entonces es error
		if(tipoQuery == Constantes.NRO_UNO && consultaSplit.length > Constantes.NRO_UNO) {
			
			mensajeError = "Solo puede ingresar una consulta a la vez para desplegar datos.";
			return SUCCESS;
			
		}
		
		String cons = optConsulta.get().trim().toLowerCase();
		if((cons.startsWith("insert") || cons.startsWith("update") || cons.startsWith("delete"))
				&& tipoQuery == Constantes.NRO_UNO) {
			
			mensajeError = "No puede ejecutar sentencias de modificación como una consulta de obtención de datos.";
			return SUCCESS;
		}
		
		List<IndexedValue<String>> mensajes = null;
		
		if(tipoQuery == Constantes.NRO_UNO) {
			
			queryResultConsulta =	correccionDatosDAO.ejecutarSelectConsulta(optConsulta.get());
			mensajeExito = "Éxito.";
			
		} else {
			
			mensajes =	Stream.of(consultaSplit)
						.map(String::trim)
						.map(it -> {
							try {
								correccionDatosDAO.ejecutarUpdateConsulta(it);
							} catch (DataAccessException e) {
								return "NOK; " + e.getMessage();
							}
							return "OK";
						})
						.map(IndexedValue.indexed())
						.collect(Collectors.toCollection(ArrayList::new));
			
			String hasError =	mensajes.stream()
								.filter(it -> it.value().startsWith("NOK;"))
								.map(it -> it.index() + Constantes.NRO_UNO)
								.map(String::valueOf)
								.reduce(Constantes.VACIO, (anterior, actual) -> anterior + actual + ", ")
								.trim();
			
			if(!hasError.isEmpty()) {
				
				mensajeError = "Sentencias con problemas: " + hasError.substring(Constantes.NRO_CERO, hasError.length() - Constantes.NRO_UNO);
				
			} else {
				
				mensajeExito = "Hecho.";
				
			}
			
		}
		
		logger.endTrace("consultaData", "Finalizado", " ");
		return SUCCESS;
	}
	
	private String validarQuerys() {
		
		List<Parametro> lista= parametroDao.obtenerParametros();
		Parametro parametro = new Parametro();
		for (int i = 0; i < lista.size(); i++) {
        	parametro =  lista.get(i);
        	if (parametro.getNombre().equalsIgnoreCase("SWITCH_ONOFF_CORRECCION_DATOS")){
        		break;
        	}
        }
		return parametro.getValor();
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
		
	}
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		
	}

	public List<Map<String, Object>> getQueryResult() {
		return queryResult;
	}

	public void setQueryResult(List<Map<String, Object>> queryResult) {
		this.queryResult = queryResult;
	}

	public String getSelectedOC() {
		return selectedOC;
	}

	public void setSelectedOC(String selectedOC) {
		this.selectedOC = selectedOC;
	}

	public String getSelectTodo() {
		return selectTodo;
	}

	public void setSelectTodo(String selectTodo) {
		this.selectTodo = selectTodo;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensajeExito() {
		return mensajeExito;
	}

	public void setMensajeExito(String mensajeExito) {
		this.mensajeExito = mensajeExito;
	}

	public List<Map<String, Object>> getQueryResultConsulta() {
		return queryResultConsulta;
	}

	public void setQueryResultConsulta(List<Map<String, Object>> queryResultConsulta) {
		this.queryResultConsulta = queryResultConsulta;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public Integer getTipoQuery() {
		return tipoQuery;
	}

	public void setTipoQuery(Integer tipoQuery) {
		this.tipoQuery = tipoQuery;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getSelectedSuc() {
		return selectedSuc;
	}

	public void setSelectedSuc(String selectedSuc) {
		this.selectedSuc = selectedSuc;
	}

//	public String getSelectedCaja() {
//		return selectedCaja;
//	}
//
//	public void setSelectedCaja(String selectedCaja) {
//		this.selectedCaja = selectedCaja;
//	}

}
