package cl.ripley.omnicanalidad.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ReservaDTE;
import cl.ripley.omnicanalidad.bean.ReservasUtil;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.dao.ReservasDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CancelarReservasAction extends ActionSupport implements SessionAware {
	private static final AriLog logger = new AriLog(CancelarReservasAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReservasDAO reservas;

	private Long estado;
	private String numeroReserva;
	private String mensaje;
	private Usuario usuario;
	private ReservaDTE reserva;
	private Map<String, Object> session;

	public String execute() throws Exception {

		logger.initTrace("execute", "Long estado: "+ estado + " String numero reserva: " + numeroReserva);

		if(session == null || session.get("usuario") == null) {
			return Constantes.LOGGED_OUT;
		}
		String result = ERROR;

		int caso = ReservasUtil.validaEstado(estado);

		switch(caso) {
		case Constantes.NRO_CERO:
			result = Constantes.SUCCESS_INICIO;
			break;
		case Constantes.NRO_UNO:
			result = cancelarReserva(numeroReserva);
			break;
		default:
			result = ERROR;
			break;
		}

		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);

		return result;
	}



	private String cancelarReserva(String numeroReserva) {

		logger.initTrace("cancelarReserva", "String numeroReserva: " + numeroReserva);
		String resultado = "";
		Long numReserva = Long.valueOf(0);
		try{
			numReserva = ReservasUtil.StringToLong(numeroReserva);

			if (numReserva == 0){
				mensaje = "N�mero de reserva ingresado no v�lido ["+ numeroReserva +"]";
				resultado = Constantes.SUCCESS_INICIO;
			}else{
				
				Integer retorno = reservas.cancelarReserva(numReserva, (Usuario) session.get("usuario"));
				
				if(retorno == 100){
					mensaje = "No se encontraron registros para actualizar con el numero de reserva ["+ numReserva +"]";
					resultado = Constantes.SUCCESS_INICIO;
				}else if(retorno == -1){
					mensaje = "Error al cancelar el numero de reserva ["+ numReserva +"]";
					resultado = Constantes.SUCCESS_INICIO;
				}
				else{
					mensaje = "Reserva ["+ numReserva +"] cancelada con �xito";
					List<ReservaDTE> listaReservas = reservas.listarReservas(numeroReserva, "0", "0");
					reserva = listaReservas.get(0);
					resultado = Constantes.SUCCESS_CANCEL;
				}
			}

		}catch(Exception e){
			logger.traceError("cancelarReserva", e);
			resultado = Constantes.SUCCESS_INICIO;
		}

		logger.endTrace("cancelarReserva", "Finalizado", "RESULTADO: " + resultado);
		return resultado;

	}



	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public String getNumeroReserva() {
		return numeroReserva;
	}


	public void setNumeroReserva(String numeroReserva) {
		this.numeroReserva = numeroReserva;
	}

	public String getMensaje() {
		return mensaje;
	}



	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public ReservaDTE getReserva() {
		return reserva;
	}


	public void setReserva(ReservaDTE reserva) {
		this.reserva = reserva;
	}
}
