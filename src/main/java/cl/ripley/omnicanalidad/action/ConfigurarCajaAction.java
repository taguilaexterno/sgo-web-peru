package cl.ripley.omnicanalidad.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.ProgramacionCierreCaja;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ProgramacionCierreCajaDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador para configuración de caja.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ConfigurarCajaAction extends ActionSupport implements
		UsuarioHabilitado {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(ConfigurarCajaAction.class,
			Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private ProgramacionCierreCajaDAO progCierreCajaDAO;

	private Usuario usuario;

	private List<ProgramacionCierreCaja> progList;
	private List<Long> sucursalesList;
	private List<Long> cajasList;
	private Error error;
	private ProgramacionCierreCaja progCierreNew;
	private Long numeroCierre;
	private Long sucursalSelect;
	private Long cajaSelect;

	private String json;

	public String execute() {
		logger.initTrace("execute", "Ingreso a metodo");

		numeroCierre = null;
		progList = null;
		progCierreNew = null;

		sucursalesList = progCierreCajaDAO.getSucursales();

		if (sucursalesList == null || sucursalesList.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Listado de sucursales vacio");
			logger.traceInfo("execute", error.getErrorMsg());
			return ERROR;
		}
		if (sucursalSelect == null) {
			sucursalSelect = (sucursalesList.size() > Constantes.NRO_CERO ? Long.parseLong(String.valueOf(sucursalesList.get(0))): 60l);
		}

		int index = sucursalesList.indexOf(sucursalSelect);
		
		if (index!= -1) {
			sucursalesList.remove(index);
		}
		sucursalesList.add(0, sucursalSelect);
		
		consultaCajas();

		logger.endTrace("execute", "Finalizado", "Tama�o sucursalesList: "
				+ sucursalesList.size());
		return SUCCESS;
	}

	/**Agrega cierre de caja
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String agregarCierre() {
		logger.initTrace("agregarCierre", "Ingreso a metodo; agregarHoraCierre: " + progCierreNew + " sucursalSelect=" + sucursalSelect);
		Long hora1 = Long.parseLong(progList.get(0).getHoraCierre().replace(":", ""));
		Long hora2 = Long.parseLong(progCierreNew.getHoraCierre().replace(":",""));
		if(sucursalSelect == null){
			sucursalSelect =progList.get(0).getNumeroSucursal();
			System.out.println("sucursalSelect="+sucursalSelect);
		}
		if(cajaSelect == null){
			cajaSelect = progList.get(0).getNumeroCaja();
			System.out.println("cajaSelect="+cajaSelect);
		}
		
		if (hora2 >= hora1) {
			addActionError("La hora del nuevo cierre no puede ser superior o igual al del cierre final.");
			execute();
			return SUCCESS;
		}

		Long hora3;
		// for (int i=0; i<progList.size();i++){
		for (ProgramacionCierreCaja obj : progList) {
			hora3 = Long.parseLong(obj.getHoraCierre().replace(":", ""));
			if (hora2.longValue() == hora3.longValue()) {
				addActionError("La hora del nuevo cierre ya se encuentra ingresada.");
				execute();
				return SUCCESS;
			}
		}

		boolean result = progCierreCajaDAO.updProgramacionCierreCaja(progCierreNew);

		if (!result) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo guardar cierre de caja");
			logger.traceInfo("agregarCierre", error.getErrorMsg());
			logger.endTrace("agregarCierre", "Finalizado con problemas",
					"result: " + result);
			return ERROR;
		}

		addActionMessage("Cierre agregado con �xito.");

		execute();

		logger.endTrace("agregarCierre", "Finalizado", "result: " + result);
		return SUCCESS;
	}

	/**Guarda configuración de cierre de caja.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String guardarConfiguracionCierreCaja() {
		logger.initTrace("guardarConfiguracionCierreCaja",
				"Ingreso a metodo; agregarHoraCierre: " + progCierreNew);
		


		for (ProgramacionCierreCaja prog : progList.subList(1, progList.size())) {
			Long hora1 = Long.parseLong(progList.get(0).getHoraCierre()
					.replace(":", ""));
			Long hora2 = Long.parseLong(prog.getHoraCierre().replace(":", ""));

			if (hora2 >= hora1) {
				addActionError("La hora de los cierres no puede ser superior al del cierre final.");
				execute();
				return SUCCESS;
			}
		}

		if(sucursalSelect == null){
			sucursalSelect =progList.get(0).getNumeroSucursal();
			System.out.println("sucursalSelect="+sucursalSelect);
		}
		if(cajaSelect == null){
			cajaSelect = progList.get(0).getNumeroCaja();
			System.out.println("cajaSelect="+cajaSelect);
		}
		
		boolean result = Boolean.TRUE;

		for (ProgramacionCierreCaja prog : progList) {
			// El primero es el cierre final por lo que los demas deben tener su
			// mismo volumen.
			prog.setVolumen(progList.get(0).getVolumen());
			// Guardo el cierre
			result = progCierreCajaDAO.updProgramacionCierreCaja(prog);

			if (!result) {
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("No se pudo guardar configuraci�n de cierre de caja");
				logger.traceInfo("guardarConfiguracionCierreCaja",
						error.getErrorMsg());
				logger.endTrace("guardarConfiguracionCierreCaja",
						"Finalizado con problemas", "result: " + result);
				return ERROR;
			}
		}
		
		addActionMessage("Configuraci�n guardada con �xito.");

		execute();

		logger.endTrace("guardarConfiguracionCierreCaja", "Finalizado",
				"result: " + result);
		return SUCCESS;
	}

	/**Elimina un cierre de caja.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String eliminarCierreCaja() {
		logger.initTrace("eliminarCierreCaja","Ingreso a metodo; numeroCierre: " + numeroCierre);

		boolean result = progCierreCajaDAO.delProgramacionCierreCaja(numeroCierre);

		if (!result) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Problema al eliminar cierre de caja.");
			addActionError(error.getErrorMsg());
			execute();
			return SUCCESS;
		}

		addActionMessage("Cierre eliminado correctamente.");

		execute();

		logger.endTrace("eliminarCierreCaja", "Finalizado", "numeroCierre: "
				+ numeroCierre);
		return SUCCESS;
	}

	/**Consulta la/s caja/s.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String consultaCajas() {
		logger.initTrace("consultaCajas", "Ingreso a metodo; Sucursal "
				+ sucursalSelect);

		if (cajasList != null && !cajasList.isEmpty()) {
			cajasList.clear();
		}

		cajasList = progCierreCajaDAO.getCajas(sucursalSelect);

		if (cajasList == null || cajasList.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Listado de cajas vacio");
			logger.traceInfo("consultaCajas", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("consultaCajas", "Finalizado", "Sucursal: " + sucursalSelect);
		if (cajaSelect == null) {
			cajaSelect = cajasList.get(0);
		}
		int index = cajasList.indexOf(cajaSelect);
		cajasList.remove(index);
		cajasList.add(0, cajaSelect);
		
		
		Gson gson = new Gson();
		json = gson.toJson(cajasList);
		consultaListProgramacionCierre();
		return SUCCESS;
	}

	/**Consulta la lista de programaciones de cierre de caja.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public String consultaListProgramacionCierre() {
		logger.initTrace("consultaCajas", "Ingreso a metodo; Sucursal "
				+ sucursalSelect + " cajaSelect=" + cajaSelect);

		if (progList != null && !progList.isEmpty()) {
			cajasList.clear();
		}

		progList = progCierreCajaDAO.getProgramacionCierreCajaList(
				sucursalSelect, cajaSelect);

		if (progList == null || progList.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("Listado de Programaciones vacio");
			logger.traceInfo("consultaListProgramacionCierre",
					error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("consultaCajas", "Finalizado", "Sucursal: "
				+ sucursalSelect + " cajaSelect=" + cajaSelect);

		Gson gson = new Gson();
		json = gson.toJson(progList);

		return SUCCESS;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;

	}

	public List<ProgramacionCierreCaja> getProgList() {
		return progList;
	}

	public void setProgList(List<ProgramacionCierreCaja> progList) {
		this.progList = progList;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public ProgramacionCierreCaja getProgCierreNew() {
		return progCierreNew;
	}

	public void setProgCierreNew(ProgramacionCierreCaja progCierreNew) {
		this.progCierreNew = progCierreNew;
	}

	public Long getNumeroCierre() {
		return numeroCierre;
	}

	public void setNumeroCierre(Long numeroCierre) {
		this.numeroCierre = numeroCierre;
	}

	public List<Long> getSucursalesList() {
		return sucursalesList;
	}

	public void setSucursalesList(List<Long> sucursalesList) {
		this.sucursalesList = sucursalesList;
	}

	public Long getSucursalSelect() {
		return sucursalSelect;
	}

	public void setSucursalSelect(Long sucursalSelect) {
		this.sucursalSelect = sucursalSelect;
	}

	public List<Long> getCajasList() {
		return cajasList;
	}

	public void setCajasList(List<Long> cajasList) {
		this.cajasList = cajasList;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Long getCajaSelect() {
		return cajaSelect;
	}

	public void setCajaSelect(Long cajaSelect) {
		this.cajaSelect = cajaSelect;
	}
}
