package cl.ripley.omnicanalidad.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CajaSucursal;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.ResumenReporte;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ReportesDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador para reporte cierres de caja.
 *
 * @author Carlos Leal (Aligare).
 * @since 04-06-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReporteCierreCajaAction extends ActionSupport implements SessionAware, UsuarioHabilitado {
	private static final AriLog logger = new AriLog(ReporteCierreCajaAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ReportesDAO reportesDAO;
	
	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings({ "rawtypes", "unused" })
	private Map session;
	private Usuario usuario;
	
	private String metodo;
	private String fechaInicio;
	private String fechaFin;
	private List<String> cajas;
	private List<CajaSucursal> cajasSuc;
	private String cajasConsultadas;
	private String sucursalesConsultadas;
	private List<ResumenReporte> reporteCorto;
	private String mensaje;
	private String idCaja;
	private String idSucursal;
	private Long idAperturaCierre;
	private InputStream excelStream;
	List<String> csString = new ArrayList<String>();
	

	public InputStream getExcelStream() {
		return excelStream;
	}

	public void setExcelStream(InputStream excelStream) {
		this.excelStream = excelStream;
	}

	private String nombreArchivo;

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = "";
		
		if(session == null || session.get("usuario") == null) {
			return Constantes.LOGGED_OUT;
		}
		
		metodo = (null==metodo?"index":metodo);
		
		switch(metodo) {
		case "index":
			
			cajasSuc=reportesDAO.obtenerCajas();
			
			for(CajaSucursal cs:cajasSuc) {
				csString.add(String.valueOf(cs.getSucursal())+" - Caja "+String.valueOf(cs.getCaja()));
			}
			Collections.sort(csString);
			cajas=csString;
			respuesta = Constantes.SUCCESS_INICIO;
			break;
			
		case "consulta":
			
			String[] csArray;
			String cajasSucString=cajasConsultadas;
			cajasSucString=cajasSucString.replaceAll(",","-").replaceAll(" - Caja ", ",");
			csArray=cajasSucString.split("-");
			String newCsArray="";
			for(String cs:csArray) {
				newCsArray=newCsArray+"("+cs+"),";
			}
			newCsArray= newCsArray.substring(Constantes.NRO_CERO, newCsArray.length()-Constantes.NRO_UNO);
			reporteCorto = reportesDAO.buscarCierresCaja(fechaInicio, fechaFin, newCsArray);
			
			if(reporteCorto.size()>0) {
				respuesta = Constantes.SUCCESS_DETAIL;
			}else {
				cajasSuc=reportesDAO.obtenerCajas();
				for(CajaSucursal cs:cajasSuc) {
					csString.add(String.valueOf(cs.getSucursal())+" - Caja "+String.valueOf(cs.getCaja()));
				}
				Collections.sort(csString);
				cajas=csString;
				mensaje = Constantes.NO_DATA;
				respuesta = Constantes.SUCCESS_INICIO;
			}
			break;
		case "descargar":
			logger.traceInfo("execute", "opcion:Descargar: Inicio");
			HSSFWorkbook libro = new HSSFWorkbook();
	        libro = reportesDAO.buscarInformacionCierre(idAperturaCierre, idCaja, idSucursal, Constantes.FUNCION_BOLETA);
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
				setNombreArchivo(idSucursal+"_"+idCaja+"_"+format.format(new Date())+".xls");
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				libro.write(baos);
				ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
				setExcelStream(bis);
			} catch (IOException e) {
				logger.traceInfo("execute", "opcion:Descargar: error al escribir el excel");
			}
			logger.traceInfo("execute", "opcion:Descargar: Fin");
			return "excel";
		default: 
			respuesta = ERROR;
			break;
		}
			
		return respuesta;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<String> getCajas() {
		return cajas;
	}

	public void setCajas(List<String> cajas) {
		this.cajas = cajas;
	}
	
	public List<CajaSucursal> getCajasSuc() {
		return cajasSuc;
	}

	public void setCajasSuc(List<CajaSucursal> cajasSuc) {
		this.cajasSuc = cajasSuc;
	}

	public String getSucursalesConsultadas() {
		return sucursalesConsultadas;
	}

	public void setSucursalesConsultadas(String sucursalesConsultadas) {
		this.sucursalesConsultadas = sucursalesConsultadas;
	}

	public String getCajasConsultadas() {
		return cajasConsultadas;
	}

	public void setCajasConsultadas(String cajasConsultadas) {
		this.cajasConsultadas = cajasConsultadas;
	}

	public List<ResumenReporte> getReporteCorto() {
		return reporteCorto;
	}

	public void setReporteCorto(List<ResumenReporte> reporteCorto) {
		this.reporteCorto = reporteCorto;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}

	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}

	public String getIdCaja() {
		return idCaja;
	}

	public void setIdCaja(String idCaja) {
		this.idCaja = idCaja;
	}

	public String getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	
}
