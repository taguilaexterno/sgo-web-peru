package cl.ripley.omnicanalidad.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.DetalleBackoffice;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.DetalleCompra;
import cl.ripley.omnicanalidad.bean.DetalleDespacho;
import cl.ripley.omnicanalidad.bean.DetalleOC;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador para de ordenes de compra.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@SuppressWarnings("serial")
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BuscarOrdenCompraAction extends ActionSupport implements SessionAware, UsuarioHabilitado {
	private static final AriLog logger = new AriLog(BuscarOrdenCompraAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private OrdenCompraDAO ordenesCompraDAO;
	
	@SuppressWarnings("unused")
	private Error error;
	@SuppressWarnings({ "rawtypes", "unused" })
	private Map session;
	private Usuario usuario;
	
	private int estado;
	private int nroOrdenes;
	private String metodo;
	private String mensaje;
	private String seleccionados;
	private final String BUSCAROC = "BUSCAROC";
	private final String BUSCARRUT = "BUSCARRUT";
	private final String DETALLE = "DETALLE";
	private boolean cajaEstado;

	private List<OrdenesDeCompra> ordenesCompra;
	private DetalleCliente datosCliente;
	private DetalleBackoffice datosBackoffice;
	private List<DetalleDespacho> datosDespacho;

	private List<DetalleCompra> datosCompra;

	public String execute() throws Exception {
		logger.initTrace("execute", null);
		String respuesta = ERROR;
		Error errorBean = new Error();
		
		Integer nroCaja = new Integer(parametroDao.getParametro("NRO_CAJA_BOLETAS").getValor());
		Integer sucursal = new Integer(parametroDao.getParametro("SUCURSAL").getValor());
		
		
		/*
		 * Anular OC
		 */
		logger.traceInfo("BuscarOrdenCompra. Metodo : " + metodo, "inicio metodo");

		if (metodo != null && metodo.equalsIgnoreCase(BUSCAROC) && seleccionados != null) {
			
			logger.traceInfo("BuscarOrdenCompra detalle OC : ", "inicio dao");
			DetalleOC detalleOC = ordenesCompraDAO.obtenerDetalleOC(Integer.parseInt(seleccionados));
			datosCliente = detalleOC.getDatosCliente();
			datosDespacho = detalleOC.getDatosDespacho();
			datosCompra = detalleOC.getDatosCompra();
			datosBackoffice = detalleOC.getDatosBackoffice();
			if (datosCliente == null) {
				mensaje = "ORDEN DE COMPRA "+seleccionados+" NO EXISTE";
				logger.traceInfo("execute", "Orden de compra inexistente, Orden consultada: "+seleccionados);
				return respuesta = Constantes.SUCCESS_ORDEN_COMPRA;
			} else {
				mensaje = null;
				logger.traceInfo("BuscarOrdenCompra detalle OC : " + seleccionados, "fin dao");
				return respuesta = Constantes.SUCCESS_DETALLE_OC;
			}
			
		} else if (metodo != null && metodo.equalsIgnoreCase(BUSCARRUT)  && seleccionados != null) {

			ordenesCompra = ordenesCompraDAO.obtenerOrdenes(Long.parseLong(seleccionados));
			setOrdenesCompra(ordenesCompra);
			nroOrdenes = ordenesCompra.size();

			if (nroOrdenes==0) {
				mensaje = "NO EXISTEN ORDENES DE COMPRA DEL RUT COMPRADOR CONSULTADO "+seleccionados+" ";			
			}
			
			logger.endTrace("execute", "Finalizado", "ordenesCompra: "+ordenesCompra.size()+" Respuesta: "+respuesta);

			respuesta = Constantes.SUCCESS_ORDEN_COMPRA;
			

		} else if (metodo != null && metodo.equalsIgnoreCase(DETALLE) && seleccionados != null) {
			logger.traceInfo("BuscarOrdenCompra detalle OC : ", "inicio dao");
			DetalleOC detalleOC = ordenesCompraDAO.obtenerDetalleOC(Integer.parseInt(seleccionados));
			datosCliente = detalleOC.getDatosCliente();
			datosDespacho = detalleOC.getDatosDespacho();
			datosCompra = detalleOC.getDatosCompra();
			datosBackoffice = detalleOC.getDatosBackoffice();
			logger.traceInfo("BuscarOrdenCompra detalle OC : " + datosCliente.getCorrelativoVentas(), "fin dao");
			return respuesta = Constantes.SUCCESS_DETALLE_OC;

		} else if (metodo == null)  {

			respuesta = Constantes.SUCCESS_ORDEN_COMPRA;
			
		} else {
			errorBean.setCodError(2);
			errorBean.setErrorMsg("OPCION DE MENU INEXISTENTE");
			logger.traceInfo("execute", "Estado inexistente, estado consultado: "+estado);
			respuesta = ERROR;
		}

		
		return respuesta;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<OrdenesDeCompra> getOrdenesCompra() {
		return ordenesCompra;
	}

	public void setOrdenesCompra(List<OrdenesDeCompra> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getNroOrdenes() {
		return nroOrdenes;
	}

	public void setNroOrdenes(int nroOrdenes) {
		this.nroOrdenes = nroOrdenes;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getSeleccionados() {
		return seleccionados;
	}

	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}

	public DetalleCliente getDatosCliente() {
		return datosCliente;
	}

	public void setDatosCliente(DetalleCliente datosCliente) {
		this.datosCliente = datosCliente;
	}

	public List<DetalleDespacho> getDatosDespacho() {
		return datosDespacho;
	}

	public void setDatosDespacho(List<DetalleDespacho> datosDespacho) {
		this.datosDespacho = datosDespacho;
	}

	public List<DetalleCompra> getDatosCompra() {
		return datosCompra;
	}

	public void setDatosCompra(List<DetalleCompra> datosCompra) {
		this.datosCompra = datosCompra;
	}
	
	public DetalleBackoffice getDatosBackoffice() {
		return datosBackoffice;
	}

	public void setDatosBackoffice(DetalleBackoffice datosBackoffice) {
		this.datosBackoffice = datosBackoffice;
	}

	public boolean isCajaEstado() {
		return cajaEstado;
	}

	public void setCajaEstado(boolean cajaEstado) {
		this.cajaEstado = cajaEstado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


}
