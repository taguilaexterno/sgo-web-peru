package cl.ripley.omnicanalidad.action;

import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.TemplateVoucherDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de Voucher Templates.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VoucherTemplatesAction extends ActionSupport implements UsuarioHabilitado {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(VoucherTemplatesAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private TemplateVoucherDAO templateVoucherDAO;
	
	@Autowired
	private ParametroDAO parametroDAO;

	private Usuario usuario;
	private Integer estado;
	private String llave;
	private List<TemplateVoucher> temps;
	private Error error;


	public String execute() {
		logger.initTrace("execute", "Ingreso metodo");

		temps = templateVoucherDAO.getTemplateVoucherList(null);

		if(temps == null || temps.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontraron templates de vouchers");
			logger.traceInfo("execute", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("execute", "Finalizado", SUCCESS);
		return SUCCESS;
	}

	public String editar() {
		logger.initTrace("editar", "Ingreso metodo");

		TemplateVoucher tv = new TemplateVoucher();
		tv.setLlave(llave);

		temps = templateVoucherDAO.getTemplateVoucherList(tv);

		if(temps == null || temps.isEmpty()) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se encontraron templates de vouchers");
			logger.traceInfo("editar", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("editar", "Finalizado", SUCCESS);
		return SUCCESS;
	}

	public String activar() {
		logger.initTrace("activar", "Ingreso metodo; llave=" + llave + " ; estado=" + estado);

		TemplateVoucher tv = new TemplateVoucher();
		tv.setLlave(llave);
		//Si el estado es 1 entonces debo enviar 0 y si es 0 debo enviar 1
		tv.setEstado(estado == 1 ? 0 : 1);

		//Voy a activar o desactivar el estado
		boolean resultado = templateVoucherDAO.updActivarTemplate(tv);

		if(!resultado) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo actualizar el estado del template.");
			logger.traceInfo("activar", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("activar", "Finalizado", "resultado=" + resultado);
		return SUCCESS;
	}

	public String guardarTemplate() {
		logger.initTrace("guardarTemplate", "Ingreso metodo; tv=" + temps.get(0));

		TemplateVoucher tv = temps.get(0);

		//Voy a actualizar template
		boolean resultado = templateVoucherDAO.updTemplate(tv);

		if(!resultado) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo actualizar el template.");
			logger.traceInfo("guardarTemplate", error.getErrorMsg());
			return ERROR;
		}


		//Voy a buscar el host de la url
		String host = parametroDAO.getParametro("URL_CLOUDFRONT").getValor();
		//Voy a buscar el html nuevo para guardarlo tambien
		String html = null;
		URLConnection connection = null;
		Scanner scanner = null;
		try {
			connection =  new URL(host + tv.getUrl()).openConnection();
			scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\A");
			html = scanner.next();
			logger.traceInfo("guardarTemplate", "html=" + html);
			tv.setHtml(html);
		}catch ( Exception ex ) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo obtener el template desde la url " + host + tv.getUrl());
			logger.traceInfo("guardarTemplate", error.getErrorMsg());
			return ERROR;
		} finally {
			if(scanner != null) {
				scanner.close();
			}
		}

		resultado = templateVoucherDAO.updTemplateHtml(tv);

		if(!resultado) {
			error = new Error();
			error.setCodError(1);
			error.setErrorMsg("No se pudo guardar el template desde la url " + host + tv.getUrl());
			logger.traceInfo("guardarTemplate", error.getErrorMsg());
			return ERROR;
		}

		logger.endTrace("guardarTemplate", "Finalizado", "resultado=" + resultado);
		return SUCCESS;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public List<TemplateVoucher> getTemps() {
		return temps;
	}

	public void setTemps(List<TemplateVoucher> temps) {
		this.temps = temps;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}
}
