package cl.ripley.omnicanalidad.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CategoriasProperties;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.CategoriaParametroDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.impl.CategoriaParametroDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.ParametroDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controlador de administrador de parámetros.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AdministradorParametroAction extends ActionSupport implements UsuarioHabilitado, SessionAware {
	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(AdministradorParametroAction.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Autowired
	private CategoriaParametroDAO categoriasDao;
	
	
	@SuppressWarnings("unused")
	private Usuario usuario;
	
	//scope
    private Map<String, Object> request;
	@SuppressWarnings("rawtypes")
	private Map session;
	
	//propios del m�todo
	private String mensaje;
	private String mensajeError;
	private Integer metodo;
	private Error error;
	
	//categorias
	private List<CategoriasProperties> categorias;
	
	//parametro
	private int idParametro;
	private Integer idTipo;
	private List<Parametro> parametros;
	private Parametro parametro;
	private List<Parametro> hparametros;
	
	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@SuppressWarnings("rawtypes")
	public Map getSession() {
		return session;
	}
	@SuppressWarnings("rawtypes")
	public void setSession(Map session) {
		this.session = session;
	}
	
	public Integer getMetodo() {
		return metodo;
	}

	public void setMetodo(Integer metodo) {
		this.metodo = metodo;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}
	
	public List<CategoriasProperties> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriasProperties> categorias) {
		this.categorias = categorias;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public int getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(int idParametro) {
		this.idParametro = idParametro;
	}

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	public List<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}

	public Parametro getParametro() {
		return parametro;
	}

	public void setParametro(Parametro parametro) {
		this.parametro = parametro;
	}

	public List<Parametro> getHparametros() {
		return hparametros;
	}
	public void setHparametros(List<Parametro> hparametros) {
		this.hparametros = hparametros;
	}
	//OBTENER PARAMETRO CORRECTO
	public String execute() {
		logger.initTrace("execute", null);
		mensaje = null;
		mensajeError = null;
		String respuesta = SUCCESS;
		try {
			metodo = new Integer(parametroDao.getParametro("SWITCH_ONOFF_SGO").getValor());
		} catch (Exception e) {
			logger.traceError("execute", e);
		}	

		logger.traceInfo("Administrador Parametro Action", "Ingreso metodo: "+metodo);

		logger.endTrace("execute", "Finalizado", "AdministradorParametroAction Respuesta: "+respuesta);
		return respuesta;	
	}
	
	//OBTENER PARAMETRO CORRECTO	
	public String updateApagar(){
		logger.initTrace("updateApagar", null);
		String respuesta = ERROR;
		Parametro parametroBD = parametroDao.getParametro("SWITCH_ONOFF_SGO");
		if (metodo.intValue()==Integer.parseInt(parametroBD.getValor())){
			mensaje = "No hay cambios registrados";
			logger.traceInfo("updateApagar","Administrador Parametro Action "+ mensaje);
			return SUCCESS;
		}else{
			parametroBD.setValor(metodo.toString());
		}
		
		logger.traceInfo("updateApagar: ", "nombre: " +parametroBD.getNombre());
		logger.traceInfo("updateApagar: ", "valor: " + parametroBD.getValor());
		try {
			if (parametroDao.updateParametro(parametroBD)){
				mensaje = (metodo.intValue()==Constantes.CAJA_APAGADA)?"La caja ha sido apagada":"La caja ha sido encendida";
				metodo = (metodo.intValue()==Constantes.CAJA_APAGADA)?Constantes.CAJA_APAGADA:Constantes.CAJA_ENCENDIDA;
				respuesta = SUCCESS;
				logger.traceInfo("updateApagar", "Administrador Parametro Action" + mensaje);
				String responseReset = resetParametros();
				logger.traceInfo("updateApagar","respuesta del reset: " + responseReset);
			}else{
				mensajeError = "Error al apagar la caja";
			}
		} catch (Exception e) {
			logger.traceError("updateApagar", e);
		}
		
		logger.endTrace("updateApagar", "Finalizado", "AdministradorParametroAction updateApagar Respuesta: "+respuesta);
				
		return respuesta;
	}
	
	public String mostrarCategorias(){
		logger.initTrace("mostrarCategorias", null);
		String respuesta = ERROR;
		mensaje = null;
		mensajeError = null;
		categorias = categoriasDao.getCategoriasParametros();

		if ( categorias.size() > 0 ){
			respuesta = SUCCESS;
		} else {
			error = new Error();
			error.setCodError(-1);
			error.setErrorMsg("No se encontraron categorías configuradas");
			logger.traceInfo("mostrarCategorias", error.getErrorMsg());
			respuesta = ERROR;
		}
		
		logger.endTrace("mostrarCategorias", "Finalizado", "AdministradorParametroAction mostrarCategorias Respuesta: "+respuesta);
		return respuesta;
	}
	
	public String mostrarParametrosXCategoria(){
		logger.initTrace("mostrarParametrosXCategoria", null);
		String respuesta = ERROR;

		categorias = categoriasDao.getCategoriasParametros();
		
		parametros = parametroDao.getParametrosXTipo(idTipo.intValue());

		if ( parametros.size() > 0 ){
			respuesta = SUCCESS;
		} else {
			error = new Error();
			error.setCodError(-1);
			error.setErrorMsg("No se encontraron parámetros para la categoría seleccionada");
			logger.traceInfo("mostrarCategorias", error.getErrorMsg());
			respuesta = ERROR;
		}
		
		logger.endTrace("mostrarParametrosXCategoria", "Finalizado", "AdministradorParametroAction mostrarParametrosXCategoria Respuesta: "+respuesta);
		return respuesta;
	}
	
	public String updateParametro(){
		logger.initTrace("updateParametro", null);
		String respuesta = ERROR;
		mensaje = null;
		mensajeError = null;
		Parametro parametroBD = parametroDao.getParametro(parametro.getNombre());
		if (parametro.getValor().equalsIgnoreCase(parametroBD.getValor()) && parametro.getPropertyTypeId().equalsIgnoreCase(parametroBD.getPropertyTypeId())){
			error = new Error();
			error.setCodError(-1);
			error.setErrorMsg("No hay cambios registrados, no se actualiza el parámetro.");
			logger.traceInfo("updateParametro", error.getErrorMsg());
			respuesta = ERROR;

			return respuesta;
		}

		try {
			if (parametroDao.updateParametro(parametro)){
				respuesta = SUCCESS;
				
				idTipo = Integer.parseInt(parametro.getPropertyTypeId().trim());
				parametros = parametroDao.getParametrosXTipo(idTipo.intValue());
				
				categorias = categoriasDao.getCategoriasParametros();
				String responseReset = resetParametros();
				logger.traceInfo("updateParametro","respuesta del reset: " + responseReset);
				mensaje = "Parámetro modificado exitosamente";

			}else{
				error = new Error();
				error.setCodError(-1);
				error.setErrorMsg("Error al actualizar el parámetro.");
				logger.traceInfo("updateParametro", error.getErrorMsg());
				respuesta = ERROR;
			}
		} catch (Exception e) {
			logger.traceError("updateParametro", e);
		}

		logger.endTrace("updateParametro", "Finalizado", "AdministradorParametroAction updateParametro Respuesta: "+respuesta);
		return respuesta;
	}
	public String resetParametros(){
		logger.initTrace("resetParametros","Inicia");
		String respuesta = SUCCESS;
		try {
			parametroDao.resetParam();
			hparametros = parametroDao.obtenerParametros();
		}catch (Exception e) {
			error = new Error();
			error.setCodError(-1);
			error.setErrorMsg("Error al reiniciar los parámetros.");
			logger.traceInfo("resetParametros", e.getMessage());
			respuesta = ERROR;
		}
		logger.endTrace("resetParametros", "Finaliza", respuesta);
		return respuesta;
	}
}
