package cl.ripley.omnicanalidad.action;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ActionSupport;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.Banco;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;
import cl.ripley.omnicanalidad.bean.Region;
import cl.ripley.omnicanalidad.bean.ResultadoSAP;
import cl.ripley.omnicanalidad.bean.ResultadoSAPConsulta;
import cl.ripley.omnicanalidad.bean.ResultadoSAPCreacion;
import cl.ripley.omnicanalidad.bean.ResultadoSAPModificacion;
import cl.ripley.omnicanalidad.bean.Retorno;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.DevolucionNCSAPDAO;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.soap.ws.consultarProveedor.ServicioConsultar;
import cl.ripley.omnicanalidad.soap.ws.crearProveedor.ServicioCrear;
import cl.ripley.omnicanalidad.soap.ws.modificarProveedor.ServicioModificar;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GestionDevolucionDebito extends ActionSupport implements UsuarioHabilitado, SessionAware {

	private static final long serialVersionUID = 1L;
	private static final AriLog logger = new AriLog(GestionDevolucionDebito.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	private static final String SUCCESS_CREAR = "successCrearAcreedor";
	private static final String SUCCESS_MODIFICAR = "successModificarAcreedor";
	private static final String SUCCESS = "success";
	private static final String SUCCESS_BUSQUEDA = "successBusqueda";
	private static final String SUCCESS_CANCELAR = "successCancelar";
	private static final String SUCCESS_ERROR_MOD = "errorModificarAcreedor";
	private static final String SUCCESS_ERROR_CREAR = "errorCrearAcreedor";
	private static final String SUCCESS_ERROR_EXECUTE = "errorExecute";
	private static final String SUCCESS_CONTINUAR ="successContinuar";
	private static final String METODO_CANCELAR = "cancelar";
	private static final String METODO_CONTINUAR = "continuar";
	private static final String ERROR_SAP = "errorSAP";

	@Autowired
	private OrdenCompraDAO ordenCompraDAO;
	
	@Autowired
	private DevolucionNCSAPDAO devolucionNCSAPDAO;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	private Usuario usuario;
	private List<ListaAgrupaSubOrdenes> articulosConOrdenes;
	private List<ArticulosVentaOC> articulos;
	private Long oc;
	private Long estado;
	private Error error;
	private Integer montoTotal;
	private Integer ncTotal;
	private Integer listaArtSize;
	private String mensaje;
	private Map<String, Object> session;
	private String mensajeError;
	private Integer esANCTotal;
	private DetalleCliente datosCliente;
	private ResultadoSAP acreedor;
	private ResultadoSAPCreacion creaAcreedor;
	private ResultadoSAPModificacion modAcreedor;
	private String formaPago;
	private String metodo;
	private Integer mostrar;
	private List<Banco> bancos;
	private List<Region> regiones;

	public String validarAcreedor() {
		logger.initTrace("validarAcreedor", null);
		
		if(estado == 0 || estado == 1 || estado == 2){
			List<String> articulosSelected = new ArrayList<String>();
			if(articulosConOrdenes != null && !articulosConOrdenes.isEmpty() && (estado == 1 || estado == 2)) {
				
				articulosSelected =	articulosConOrdenes.stream()
									.flatMap(item -> item.getArticulos().stream())
									.filter(art -> art.getArticuloVenta().getEsNC() != null && art.getArticuloVenta().getEsNC().intValue() == 1)
									.map(art -> String.valueOf(art.getArticuloVenta().getCorrelativoItem()))
									.collect(Collectors.toList());
				
			}else{
				
				articulosSelected =	articulos.stream()
									.filter(art -> art.getArticuloVenta().getEsNC() != null && art.getArticuloVenta().getEsNC().intValue() == 1)
									.map(art -> String.valueOf(art.getArticuloVenta().getCorrelativoItem()))
									.collect(Collectors.toList());
				
			}
			
			if(articulosSelected.isEmpty()) {
				mensajeError = "Debe seleccionar al menos un art�culo.";
				session.put("mensajeError", mensajeError);
				logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
				return SUCCESS_BUSQUEDA;
			}
		}
		
		String result = ERROR;
		
		switch(estado.intValue()) {
			case Constantes.NRO_CERO:
			case Constantes.NRO_UNO:
			case Constantes.NRO_DOS:
				
				session.put("esANCTotal", esANCTotal);
				session.put("articulosConOrdenes", articulosConOrdenes);
				session.put("articulos", articulos);
				session.put("datosCliente", datosCliente);
				session.put("oc", oc);
				session.put("estado", estado);
				session.put("montoTotal", montoTotal);
				session.put("ncTotal", ncTotal);
				session.put("listaArtSize", listaArtSize);
				session.put("formaPago", formaPago);
				
				ServicioConsultar stub =  new ServicioConsultar(parametroDao.getParametro("CONSULTAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
				DetalleCliente detalle = ordenCompraDAO.getDatosCliente(oc);
				ResultadoSAPConsulta resultado = stub.consultarProveedor(detalle.getRutCliente());
				String rutArray[] = detalle.getRutCliente().split(Constantes.GUION);
				String rut=rutArray[Constantes.NRO_CERO];
				String dv=rutArray[Constantes.NRO_UNO];
				acreedor = new ResultadoSAP();
				acreedor.setVerFacturaDoble(Constantes.VER_FACT_DOBLE);
				acreedor.setViaPago(Constantes.VIA_PAGO);
				acreedor.setCondicionPago(Constantes.CONDICION_PAGO);
				acreedor.setCuentaAsociada(Constantes.CUENTA_ASOCIADA);
				acreedor.setIdioma(Constantes.IDIOMA);
				acreedor.setPais(Constantes.PAIS);
				acreedor.setDv(dv);
				acreedor.setRut(rut);
				if(resultado.getCodigo() == Constantes.NRO_MENOSUNO){
					mensajeError = "Problemas de conexion con SAP";
					result = ERROR_SAP;
				}else if(resultado.getCodigo() == Constantes.NRO_CERO){
					result = SUCCESS;
					mensaje = resultado.getMensaje();
					acreedor.setCalle(resultado.getCalle());
					acreedor.setCuentaBancaria(resultado.getCuentaBank());
					acreedor.setDistrito(resultado.getDistrito());
					acreedor.setEstadoBloqueo(resultado.getEstadoBlock());
					acreedor.setFolioSAP(resultado.getFolio());
					acreedor.setNombre1(resultado.getNombre1());
					acreedor.setClaveBanco(resultado.getClaveBanco());
					try{
						acreedor.setNombreBanco(devolucionNCSAPDAO.obtenerBanco(acreedor.getClaveBanco()).getNombre()); 
						acreedor.setCodigoRegion(Util.rellenarString(resultado.getRegion(), Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO));
						acreedor.setNombreRegion(devolucionNCSAPDAO.obtenerRegion(acreedor.getCodigoRegion()).getNombre());
					}catch(AligareException e){
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						session.put("mensajeError", mensajeError);
						removeSessionObjects(Constantes.FALSO);
						logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
						return SUCCESS_BUSQUEDA;
					}
					acreedor.setTelefono(resultado.getTelefono());
					acreedor.setTitularCuenta(resultado.getTitularCta());
				}else{
					result = SUCCESS_CREAR;
					//mostrar = Constantes.NRO_CERO;
					creaAcreedor = new ResultadoSAPCreacion();
					try{
						bancos = devolucionNCSAPDAO.obtenerBancos();
						regiones = devolucionNCSAPDAO.obtenerRegiones();
					}catch(AligareException e){
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						session.put("mensajeError", mensajeError);
						removeSessionObjects(Constantes.FALSO);
						logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
						return SUCCESS_BUSQUEDA;
					}
					mensajeError = resultado.getMensaje();
				}
				break;
			case Constantes.NRO_TRES:
				result = SUCCESS_MODIFICAR;
				try{
					bancos = devolucionNCSAPDAO.obtenerBancos();
					regiones = devolucionNCSAPDAO.obtenerRegiones();
				}catch(AligareException e){
					mensajeError = "Error durante el proceso, favor de probar nuevamente.";
					session.put("mensajeError", mensajeError);
					removeSessionObjects(Constantes.FALSO);
					logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
					return SUCCESS_BUSQUEDA;
				}
				break;
			default:
				result = ERROR;
				error = new Error();
				error.setCodError(1);
				error.setErrorMsg("Vista no definida");
				break;
		}
		
		logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + result);
		
		return result;
	}
		
	public String crearAcreedor() {
		logger.initTrace("crearAcreedor", null);
		
		String result = ERROR;
		if(METODO_CANCELAR.equalsIgnoreCase(metodo)){
			result = SUCCESS_CANCELAR;
			removeSessionObjects(Constantes.VERDADERO);
		}else{
			ServicioCrear stub_crear =  new ServicioCrear(parametroDao.getParametro("CREAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
			creaAcreedor = stub_crear.crearProveedor(creaAcreedor);
			
			if(creaAcreedor.getCodigo() == Constantes.NRO_CERO){
				ServicioConsultar stub =  new ServicioConsultar(parametroDao.getParametro("CONSULTAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
				ResultadoSAPConsulta resultado = stub.consultarProveedor(creaAcreedor.getRut());
				if(resultado.getCodigo() == Constantes.NRO_MENOSUNO){
					mensajeError = "Problemas de conexion con SAP";
					result = ERROR_SAP;
				}else if(resultado.getCodigo() == Constantes.NRO_CERO){
					result = SUCCESS;
					mensaje = creaAcreedor.getMensaje();
					acreedor = new ResultadoSAP();
					acreedor.setVerFacturaDoble(Constantes.VER_FACT_DOBLE);
					acreedor.setViaPago(Constantes.VIA_PAGO);
					acreedor.setCondicionPago(Constantes.CONDICION_PAGO);
					acreedor.setCuentaAsociada(Constantes.CUENTA_ASOCIADA);
					acreedor.setIdioma(Constantes.IDIOMA);
					acreedor.setPais(Constantes.PAIS);
					acreedor.setRut(creaAcreedor.getRut().substring(0, creaAcreedor.getRut().indexOf(Constantes.GUION)));
					acreedor.setDv(creaAcreedor.getRut().substring(creaAcreedor.getRut().length()-1, creaAcreedor.getRut().length()));
					acreedor.setCalle(resultado.getCalle());
					acreedor.setCuentaBancaria(resultado.getCuentaBank());
					acreedor.setDistrito(resultado.getDistrito());
					acreedor.setEstadoBloqueo(resultado.getEstadoBlock());
					acreedor.setFolioSAP(resultado.getFolio());
					acreedor.setNombre1(resultado.getNombre1());
					acreedor.setClaveBanco(resultado.getClaveBanco());
					try{
						acreedor.setNombreBanco(devolucionNCSAPDAO.obtenerBanco(acreedor.getClaveBanco()).getNombre()); 
						acreedor.setCodigoRegion(Util.rellenarString(resultado.getRegion(), Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO));
						acreedor.setNombreRegion(devolucionNCSAPDAO.obtenerRegion(acreedor.getCodigoRegion()).getNombre());
					}catch(AligareException e){
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						removeSessionObjects(Constantes.VERDADERO);
						logger.endTrace("crearAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
						return SUCCESS_CANCELAR;
					}
					acreedor.setTelefono(resultado.getTelefono());
					acreedor.setTitularCuenta(resultado.getTitularCta());
				}else{
					result = SUCCESS_ERROR_CREAR;
					//mostrar = Constantes.NRO_UNO;
					try{
						bancos = devolucionNCSAPDAO.obtenerBancos();
						regiones = devolucionNCSAPDAO.obtenerRegiones();
					}catch(AligareException e){
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						removeSessionObjects(Constantes.VERDADERO);
						logger.endTrace("crearAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
						return SUCCESS_CANCELAR;
					}
					mensajeError = creaAcreedor.getMensaje();
					creaAcreedor.setCodigo(Constantes.NRO_CERO);
					creaAcreedor.setMensaje(Constantes.VACIO);
				}
			}else if(creaAcreedor.getCodigo() == Constantes.NRO_MENOSUNO){
				mensajeError = "Problemas de conexion con SAP";
				result = ERROR_SAP;
			}else{
				result = SUCCESS_ERROR_CREAR;
				try{
					bancos = devolucionNCSAPDAO.obtenerBancos();
					regiones = devolucionNCSAPDAO.obtenerRegiones();
				}catch(AligareException e){
					mensajeError = "Error durante el proceso, favor de probar nuevamente.";
					removeSessionObjects(Constantes.VERDADERO);
					logger.endTrace("crearAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
					return SUCCESS_CANCELAR;
				}
				//mostrar = Constantes.NRO_UNO;
				mensajeError = creaAcreedor.getMensaje();
				creaAcreedor.setCodigo(Constantes.NRO_CERO);
				creaAcreedor.setMensaje(Constantes.VACIO);
			}
		}
		
		logger.endTrace("crearAcreedor", "Finalizado", "RESULTADO: " + result);
		
		return result;
	}
	
	public String modificarAcreedor() { 
		logger.initTrace("modificarAcreedor", null);
		
		String result = ERROR;
		
		if(METODO_CANCELAR.equalsIgnoreCase(metodo)){
			ServicioConsultar stub =  new ServicioConsultar(parametroDao.getParametro("CONSULTAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
			ResultadoSAPConsulta resultado = stub.consultarProveedor(modAcreedor.getRut());
			String rutArray[] = modAcreedor.getRut().split(Constantes.GUION);
			String rut=rutArray[Constantes.NRO_CERO];
			String dv=rutArray[Constantes.NRO_UNO];
			acreedor = new ResultadoSAP();
			acreedor.setVerFacturaDoble(Constantes.VER_FACT_DOBLE);
			acreedor.setViaPago(Constantes.VIA_PAGO);
			acreedor.setCondicionPago(Constantes.CONDICION_PAGO);
			acreedor.setCuentaAsociada(Constantes.CUENTA_ASOCIADA);
			acreedor.setIdioma(Constantes.IDIOMA);
			acreedor.setPais(Constantes.PAIS);
			acreedor.setDv(dv);
			acreedor.setRut(rut);
			if(resultado.getCodigo() == Constantes.NRO_CERO){
				result = SUCCESS;
				mensaje = null;
				acreedor.setCalle(resultado.getCalle());
				acreedor.setCuentaBancaria(resultado.getCuentaBank());
				acreedor.setDistrito(resultado.getDistrito());
				acreedor.setEstadoBloqueo(resultado.getEstadoBlock());
				acreedor.setFolioSAP(resultado.getFolio());
				acreedor.setNombre1(resultado.getNombre1());
				acreedor.setClaveBanco(resultado.getClaveBanco());
				acreedor.setTelefono(resultado.getTelefono());
				acreedor.setTitularCuenta(resultado.getTitularCta());
				try{
					acreedor.setNombreBanco(devolucionNCSAPDAO.obtenerBanco(acreedor.getClaveBanco()).getNombre()); 
					acreedor.setCodigoRegion(Util.rellenarString(resultado.getRegion(), Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO));
					acreedor.setNombreRegion(devolucionNCSAPDAO.obtenerRegion(acreedor.getCodigoRegion()).getNombre());
				}catch(AligareException e){
					if(acreedor.getNombreBanco() == null){
						mensajeError = "Error durante el proceso, el campo Clave Banco no existe en SAP.";
					}else if(acreedor.getCodigoRegion() == null){
						mensajeError = "Error durante el proceso, el campo C�digo Regi�n no existe en SAP.";
					}else{
					mensajeError = "Error durante el proceso, favor de probar nuevamente.";
					}
					session.put("mensajeError", mensajeError);
					removeSessionObjects(Constantes.FALSO);
					logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
					return SUCCESS_BUSQUEDA;
				}
			}else{
				error = new Error();
				if(resultado.getCodigo() == Constantes.NRO_MENOSUNO){
					mensajeError = "Problemas de conexion con SAP";
					result = ERROR_SAP;
				}else{
					error.setCodError(resultado.getCodigo());
					error.setErrorMsg(resultado.getMensaje());
					result = ERROR;
				}
			}
		}else{
			ServicioModificar stub_mod =  new ServicioModificar(parametroDao.getParametro("MODIFICAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
			modAcreedor = stub_mod.modificarProveedor(modAcreedor);
			
			if(modAcreedor.getCodigo() == Constantes.NRO_CERO){
				ServicioConsultar stub =  new ServicioConsultar(parametroDao.getParametro("CONSULTAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
				ResultadoSAPConsulta resultado = stub.consultarProveedor(modAcreedor.getRut());
				if(resultado.getCodigo() == Constantes.NRO_MENOSUNO){
					mensajeError = "Problemas de conexion con SAP";
					result = ERROR_SAP;
				}else if(resultado.getCodigo() == Constantes.NRO_CERO){
					result = SUCCESS;
					mensaje = modAcreedor.getMensaje();
					acreedor = new ResultadoSAP();
					acreedor.setVerFacturaDoble(Constantes.VER_FACT_DOBLE);
					acreedor.setViaPago(Constantes.VIA_PAGO);
					acreedor.setCondicionPago(Constantes.CONDICION_PAGO);
					acreedor.setCuentaAsociada(Constantes.CUENTA_ASOCIADA);
					acreedor.setIdioma(Constantes.IDIOMA);
					acreedor.setPais(Constantes.PAIS);
					acreedor.setRut(modAcreedor.getRut().substring(0, modAcreedor.getRut().indexOf(Constantes.GUION)));
					acreedor.setDv(modAcreedor.getRut().substring(modAcreedor.getRut().length()-1, modAcreedor.getRut().length()));
					acreedor.setCalle(resultado.getCalle());
					acreedor.setCuentaBancaria(resultado.getCuentaBank());
					acreedor.setDistrito(resultado.getDistrito());
					acreedor.setEstadoBloqueo(resultado.getEstadoBlock());
					acreedor.setFolioSAP(resultado.getFolio());
					acreedor.setNombre1(resultado.getNombre1());
					acreedor.setClaveBanco(resultado.getClaveBanco());
					try{
						acreedor.setNombreBanco(devolucionNCSAPDAO.obtenerBanco(acreedor.getClaveBanco()).getNombre()); 
						acreedor.setCodigoRegion(Util.rellenarString(resultado.getRegion(), Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO));
						acreedor.setNombreRegion(devolucionNCSAPDAO.obtenerRegion(acreedor.getCodigoRegion()).getNombre());
					}catch(AligareException e){
						if(acreedor.getNombreBanco() == null){
							mensajeError = "Error durante el proceso, el campo Clave Banco no existe en SAP.";
						}else if(acreedor.getCodigoRegion() == null){
							mensajeError = "Error durante el proceso, el campo C�digo Regi�n no existe en SAP.";
						}else{
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						}
						removeSessionObjects(Constantes.VERDADERO);
						logger.endTrace("modificarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
						return SUCCESS_CANCELAR;
					}
					acreedor.setTelefono(resultado.getTelefono());
					acreedor.setTitularCuenta(resultado.getTitularCta());
				}else{
					result = SUCCESS_ERROR_MOD;
					try{
						bancos = devolucionNCSAPDAO.obtenerBancos();
						regiones = devolucionNCSAPDAO.obtenerRegiones();
					}catch(AligareException e){
						mensajeError = "Error durante el proceso, favor de probar nuevamente.";
						removeSessionObjects(Constantes.VERDADERO);
						logger.endTrace("modificarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
						return SUCCESS_CANCELAR;
					}
					mensajeError = modAcreedor.getMensaje();
					modAcreedor.setCodigo(Constantes.NRO_CERO);
					modAcreedor.setMensaje(Constantes.VACIO);
				}
			}else if(modAcreedor.getCodigo() == Constantes.NRO_MENOSUNO){
				mensajeError = "Problemas de conexion con SAP";
				result = ERROR_SAP;
			}else{
				result = SUCCESS_ERROR_MOD;
				try{
					bancos = devolucionNCSAPDAO.obtenerBancos();
					regiones = devolucionNCSAPDAO.obtenerRegiones();
				}catch(AligareException e){
					mensajeError = "Error durante el proceso, favor de probar nuevamente.";
					removeSessionObjects(Constantes.VERDADERO);
					logger.endTrace("modificarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_CANCELAR);
					return SUCCESS_CANCELAR;
				}
				mensajeError = modAcreedor.getMensaje();
				modAcreedor.setCodigo(Constantes.NRO_CERO);
				modAcreedor.setMensaje(Constantes.VACIO);
			}
		}		
		
		logger.endTrace("modificarAcreedor", "Finalizado", "RESULTADO: " + result);
		
		return result;
	}
	
	public String execute() {
		logger.initTrace("execute", null);
		String result = ERROR;
		
		try{
			if(METODO_CONTINUAR.equalsIgnoreCase(metodo)){
				removeSessionObjects(Constantes.VERDADERO);

				result = SUCCESS;
			}else if(METODO_CANCELAR.equalsIgnoreCase(metodo)){
				removeSessionObjects(Constantes.VERDADERO);

				result = SUCCESS_BUSQUEDA;
			}else{
				//acreedor = getAcreedor();
				Retorno resultado = devolucionNCSAPDAO.insertaDevNCSAP(new BigInteger(acreedor.getTicketAsociado()), Long.parseLong(acreedor.getFolioSAP()), oc, (acreedor.getRut() + Constantes.GUION + acreedor.getDv()), acreedor.getClaveBanco(), Long.parseLong(acreedor.getCuentaBancaria()), usuario.getUsuario());
				if(resultado.getCodigo() == Constantes.NRO_CERO){
					removeSessionObjects(Constantes.VERDADERO);

					result = SUCCESS;
				}else if(resultado.getCodigo() == Constantes.NRO_UNO){
					
					mensajeError = resultado.getMensaje();
					
					
					
					ServicioConsultar stub =  new ServicioConsultar(parametroDao.getParametro("CONSULTAR_ENDPOINTSAP").getValor(),parametroDao.getParametro("USERNAME_WSSAP").getValor(),parametroDao.getParametro("PASSWORD_WSSAP").getValor());
					ResultadoSAPConsulta resultadoSap = stub.consultarProveedor(acreedor.getRut() + Constantes.GUION + acreedor.getDv());
					//String rutArray[] = modAcreedor.getRut().split(Constantes.GUION);
					String rut = acreedor.getRut();
					String dv = acreedor.getDv();
					acreedor = new ResultadoSAP();
					acreedor.setVerFacturaDoble(Constantes.VER_FACT_DOBLE);
					acreedor.setViaPago(Constantes.VIA_PAGO);
					acreedor.setCondicionPago(Constantes.CONDICION_PAGO);
					acreedor.setCuentaAsociada(Constantes.CUENTA_ASOCIADA);
					acreedor.setIdioma(Constantes.IDIOMA);
					acreedor.setPais(Constantes.PAIS);
					acreedor.setDv(dv);
					acreedor.setRut(rut);
					if(resultadoSap.getCodigo() == Constantes.NRO_CERO){
						//result = SUCCESS;
						//mensaje = null;
						acreedor.setCalle(resultadoSap.getCalle());
						acreedor.setCuentaBancaria(resultadoSap.getCuentaBank());
						acreedor.setDistrito(resultadoSap.getDistrito());
						acreedor.setEstadoBloqueo(resultadoSap.getEstadoBlock());
						acreedor.setFolioSAP(resultadoSap.getFolio());
						acreedor.setNombre1(resultadoSap.getNombre1());
						acreedor.setClaveBanco(resultadoSap.getClaveBanco());
						acreedor.setTelefono(resultadoSap.getTelefono());
						acreedor.setTitularCuenta(resultadoSap.getTitularCta());
						try{
							acreedor.setNombreBanco(devolucionNCSAPDAO.obtenerBanco(acreedor.getClaveBanco()).getNombre()); 
							acreedor.setCodigoRegion(Util.rellenarString(resultadoSap.getRegion(), Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO));
							acreedor.setNombreRegion(devolucionNCSAPDAO.obtenerRegion(acreedor.getCodigoRegion()).getNombre());
						}catch(AligareException e){
							if(acreedor.getNombreBanco() == null){
								mensajeError = "Error durante el proceso, el campo Clave Banco no existe en SAP.";
							}else if(acreedor.getCodigoRegion() == null){
								mensajeError = "Error durante el proceso, el campo C�digo Regi�n no existe en SAP.";
							}else{
							mensajeError = "Error durante el proceso, favor de probar nuevamente.";
							}
							//session.put("mensajeError", mensajeError);
							removeSessionObjects(Constantes.FALSO);
							logger.endTrace("validarAcreedor", "Finalizado", "RESULTADO: " + SUCCESS_BUSQUEDA);
							return SUCCESS_BUSQUEDA;
						}
					}else{
						error = new Error();
						if(resultado.getCodigo() == Constantes.NRO_MENOSUNO){
							mensajeError = "Problemas de conexion con SAP";
							result = ERROR;
						}else{
							error.setCodError(resultado.getCodigo());
							error.setErrorMsg(resultado.getMensaje());
							result = ERROR;
						}
					}
					
					esANCTotal = (Integer)session.get("esANCTotal");
					articulosConOrdenes = (List<ListaAgrupaSubOrdenes>)session.get("articulosConOrdenes");
					articulos = (List<ArticulosVentaOC>)session.get("articulos");
					datosCliente = (DetalleCliente)session.get("datosCliente");
					oc = (Long)session.get("oc");
					estado = (Long)session.get("estado");
					montoTotal = (Integer)session.get("montoTotal");
					ncTotal = (Integer)session.get("ncTotal");
					listaArtSize = (Integer)session.get("listaArtSize");
					formaPago = (String)session.get("formaPago");	
					
					//logger.traceInfo("execute", "FIRV articulosConOrdenes.size: " + articulosConOrdenes.size());
					logger.traceInfo("execute", "FIRV articulos.size: " + articulos.size());
					
					//removeSessionObjects(Constantes.VERDADERO);
					
					result = SUCCESS_ERROR_EXECUTE;
				}else{
					removeSessionObjects(Constantes.VERDADERO);

					result = SUCCESS_BUSQUEDA;
				}
			}
		}catch(Exception e){
			logger.traceError("execute", "Error en ejecucion: ", e);
			result = SUCCESS_BUSQUEDA;
			mensajeError = "Ha ocurrido un error al generar su solicitud, favor de intentar nuevamente.";
		}
		logger.endTrace("execute", "Finalizado", "RESULTADO: " + result);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private void removeSessionObjects(boolean ambos){
		if(ambos){
			esANCTotal = (Integer)session.get("esANCTotal");
			articulosConOrdenes = (List<ListaAgrupaSubOrdenes>)session.get("articulosConOrdenes");
			articulos = (List<ArticulosVentaOC>)session.get("articulos");
			datosCliente = (DetalleCliente)session.get("datosCliente");
			oc = (Long)session.get("oc");
			estado = (Long)session.get("estado");
			montoTotal = (Integer)session.get("montoTotal");
			ncTotal = (Integer)session.get("ncTotal");
			listaArtSize = (Integer)session.get("listaArtSize");
			formaPago = (String)session.get("formaPago");
			
			session.remove("esANCTotal");
			session.remove("articulosConOrdenes");
			session.remove("articulos");
			session.remove("datosCliente");
			session.remove("oc");
			session.remove("estado");
			session.remove("montoTotal");
			session.remove("ncTotal");
			session.remove("listaArtSize");
			session.remove("formaPago");
		}else{
			session.remove("esANCTotal");
			session.remove("articulosConOrdenes");
			session.remove("articulos");
			session.remove("datosCliente");
			session.remove("oc");
			session.remove("estado");
			session.remove("montoTotal");
			session.remove("ncTotal");
			session.remove("listaArtSize");
			session.remove("formaPago");
		}
	}

	public ResultadoSAPCreacion getCreaAcreedor() {
		return creaAcreedor;
	}

	public void setCreaAcreedor(ResultadoSAPCreacion creaAcreedor) {
		this.creaAcreedor = creaAcreedor;
	}

	public ResultadoSAPModificacion getModAcreedor() {
		return modAcreedor;
	}

	public void setModAcreedor(ResultadoSAPModificacion modAcreedor) {
		this.modAcreedor = modAcreedor;
	}

	public ResultadoSAP getAcreedor() {
		return acreedor;
	}

	public void setAcreedor(ResultadoSAP acreedor) {
		this.acreedor = acreedor;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;

	}

	public List<ArticulosVentaOC> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<ArticulosVentaOC> articulos) {
		this.articulos = articulos;
	}

	public List<ListaAgrupaSubOrdenes> getArticulosConOrdenes() {
		return articulosConOrdenes;
	}

	public void setArticulosConOrdenes(
			List<ListaAgrupaSubOrdenes> articulosConOrdenes) {
		this.articulosConOrdenes = articulosConOrdenes;
	}

	public Long getOc() {
		return oc;
	}

	public void setOc(Long oc) {
		this.oc = oc;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Integer getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Integer getNcTotal() {
		return ncTotal;
	}

	public void setNcTotal(Integer ncTotal) {
		this.ncTotal = ncTotal;
	}

	public Integer getListaArtSize() {
		return listaArtSize;
	}

	public void setListaArtSize(Integer listaArtSize) {
		this.listaArtSize = listaArtSize;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
		
	public Integer getEsANCTotal() {
		return esANCTotal;
	}

	public void setEsANCTotal(Integer esANCTotal) {
		this.esANCTotal = esANCTotal;
	}
	
	public DetalleCliente getDatosCliente() {
		return datosCliente;
	}

	public void setDatosCliente(DetalleCliente datosCliente) {
		this.datosCliente = datosCliente;
	}
	
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	public Integer getMostrar() {
		return mostrar;
	}

	public void setMostrar(Integer mostrar) {
		this.mostrar = mostrar;
	}
		
	public List<Banco> getBancos() {
		return bancos;
	}

	public void setBancos(List<Banco> bancos) {
		this.bancos = bancos;
	}

	public List<Region> getRegiones() {
		return regiones;
	}

	public void setRegiones(List<Region> regiones) {
		this.regiones = regiones;
	}
	
}
