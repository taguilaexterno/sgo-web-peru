package cl.ripley.omnicanalidad;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.dispatcher.filter.StrutsPrepareAndExecuteFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cl.ripley.omnicanalidad.util.Constantes;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReportsContext;

@Configuration
public class Config {
	
	@Bean
    public FilterRegistrationBean<StrutsPrepareAndExecuteFilter> filterRegistrationBean() {
        FilterRegistrationBean<StrutsPrepareAndExecuteFilter> registrationBean = new FilterRegistrationBean<>();
        StrutsPrepareAndExecuteFilter struts = new StrutsPrepareAndExecuteFilter();
        registrationBean.setFilter(struts);
        registrationBean.setOrder(Constantes.NRO_UNO);
        List<String> urls = new ArrayList<>(Constantes.NRO_UNO);
        urls.add("/*");
        registrationBean.setUrlPatterns(urls);
        return registrationBean;
    }
	
	@Bean
	public JasperReportsContext jasperReportsContext() {
		JasperReportsContext ctx = DefaultJasperReportsContext.getInstance();
		return ctx;
	}
	
	
	@Bean
	public JasperExportManager jasperExportManager(JasperReportsContext jasperReportsContext) {
		
		return JasperExportManager.getInstance(jasperReportsContext);
		
	}
	
	@Bean
	public JasperFillManager jasperFillManager(JasperReportsContext jasperReportsContext) {
		
		return JasperFillManager.getInstance(jasperReportsContext);
		
	}
	
}
