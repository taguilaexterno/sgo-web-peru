package cl.ripley.omnicanalidad.bean;

public class ResultadoSAPCreacion {
	private int codigo;
	private String mensaje;
	
	private String rut;
	// DIRECCION
	private String nombre1;
	private String calle1;
	private String distrito;
	private String region;
	// COMUNICACION
	private String telex;
	private String telefax;
	private String telefono;
	private String email;
	// CONTROL
	private String numeroIF;
	private String numeroIFDV;
    // SOCIEDAD->BANCO
	private String claveB;
	private String cuentaB;
	private String titularC;
	
	public ResultadoSAPCreacion(){}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getCalle1() {
		return calle1;
	}

	public void setCalle1(String calle1) {
		this.calle1 = calle1;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTelex() {
		return telex;
	}

	public void setTelex(String telex) {
		this.telex = telex;
	}

	public String getTelefax() {
		return telefax;
	}

	public void setTelefax(String telefax) {
		this.telefax = telefax;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroIF() {
		return numeroIF;
	}

	public void setNumeroIF(String numeroIF) {
		this.numeroIF = numeroIF;
	}

	public String getNumeroIFDV() {
		return numeroIFDV;
	}

	public void setNumeroIFDV(String numeroIFDV) {
		this.numeroIFDV = numeroIFDV;
	}

	public String getClaveB() {
		return claveB;
	}

	public void setClaveB(String claveB) {
		this.claveB = claveB;
	}

	public String getCuentaB() {
		return cuentaB;
	}

	public void setCuentaB(String cuentaB) {
		this.cuentaB = cuentaB;
	}

	public String getTitularC() {
		return titularC;
	}

	public void setTitularC(String titularC) {
		this.titularC = titularC;
	}

	@Override
	public String toString() {
		return  "[Codigo:"+this.codigo+"][Mensaje:"+((this.mensaje != null)?this.mensaje:"null")+
				"][Rut:"+((this.rut != null)?this.rut:"null")+"](DIRECCION=>[Nombre1:"+((this.nombre1 != null)?this.nombre1:"null")+
				"][Calle1:"+((this.calle1 != null)?this.calle1:"null")+"][Distrito:"+((this.distrito != null)?this.distrito:"null")+
				"][Region:"+((this.region != null)?this.region:"null")+"])"+
				"(COMUNICACION=>[Telex:"+((this.telex != null)?this.telex:"null")+"][Telefax:"+((this.telefax != null)?this.telefax:"null")+
				"][Telefono:"+((this.telefono != null)?this.telefono:"null")+"][email:"+((this.email != null)?this.email:"null")+"])"+
				"(CONTROL=>[NumeroIF:"+((this.numeroIF != null)?this.numeroIF+"-"+this.numeroIFDV:"null")+"])"+
				"(SOCIEDAD->BANCO=>[ClaveB:"+((this.claveB != null)?this.claveB:"null")+"][CuentaB:"+((this.cuentaB != null)?this.cuentaB:"null")+
				"][TitularC:"+((this.titularC != null)?this.titularC:"null")+"])";
	}
}
