package cl.ripley.omnicanalidad.bean;

import java.sql.Clob;

public class DocumentoElectronico {
	
	private Clob tramaDTE;
	private Clob xmlTDE;
	private Clob xmlMail;
	private String DTE;
	
	public String getDTE() {
		return DTE;
	}
	public void setDTE(String dTE) {
		DTE = dTE;
	}
	public Clob getTramaDTE() {
		return tramaDTE;
	}
	public void setTramaDTE(Clob tramaDTE) {
		this.tramaDTE = tramaDTE;
	}
	public Clob getXmlTDE() {
		return xmlTDE;
	}
	public void setXmlTDE(Clob xmlTDE) {
		this.xmlTDE = xmlTDE;
	}
	public Clob getXmlMail() {
		return xmlMail;
	}
	public void setXmlMail(Clob xmlMail) {
		this.xmlMail = xmlMail;
	}

}
