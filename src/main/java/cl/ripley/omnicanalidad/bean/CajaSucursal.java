/**
 * 
 */
package cl.ripley.omnicanalidad.bean;

public class CajaSucursal{
	private Integer caja;
	private Integer sucursal;
	private Long oc;
	
	public Integer getCaja() {
		return caja;
	}
	public void setCaja(Integer caja) {
		this.caja = caja;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	
	public Long getOc() {
		return oc;
	}
	public void setOc(Long oc) {
		this.oc = oc;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CajaSucursal [caja=").append(caja).append(", sucursal=").append(sucursal).append(", oc=")
				.append(oc).append("]");
		return builder.toString();
	}

	
}