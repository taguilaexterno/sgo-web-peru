package cl.ripley.omnicanalidad.bean;

import java.util.Date;
import java.util.List;

public class ReservaDTE {
	
	private Long ordenCompra;
	private String estado;
	private String nombreCliente;
	private Date fechaCreacion;
	private String fechaVigencia;
	private List<ProductoReservaDTE> productos;
	
	public Long getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(Long ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public List<ProductoReservaDTE> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductoReservaDTE> productos) {
		this.productos = productos;
	}
	public String getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

}
