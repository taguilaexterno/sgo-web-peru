package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**Clase modelo con informacion de perfiles
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 05-08-2016
 * Versiones:
 * <ul>
 *  <li>05-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8760971805303736355L;

	private Long id;
	private String nombrePerfil;
	private boolean activo;
	private Date fecCreacion;
	private String usuarioCrea;
	private Date fecUpd;
	private String usuarioUpd;
	private List<Modulo> modulos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombrePerfil() {
		return nombrePerfil;
	}
	public void setNombrePerfil(String nombrePerfil) {
		this.nombrePerfil = nombrePerfil;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public Date getFecCreacion() {
		return fecCreacion;
	}
	public void setFecCreacion(Date fecCreacion) {
		this.fecCreacion = fecCreacion;
	}
	public String getUsuarioCrea() {
		return usuarioCrea;
	}
	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}
	public Date getFecUpd() {
		return fecUpd;
	}
	public void setFecUpd(Date fecUpd) {
		this.fecUpd = fecUpd;
	}
	public String getUsuarioUpd() {
		return usuarioUpd;
	}
	public void setUsuarioUpd(String usuarioUpd) {
		this.usuarioUpd = usuarioUpd;
	}
	public List<Modulo> getModulos() {
		return modulos;
	}
	public void setModulos(List<Modulo> modulos) {
		this.modulos = modulos;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Perfil [id=").append(id).append(", nombrePerfil=").append(nombrePerfil).append(", activo=")
				.append(activo).append(", fecCreacion=").append(fecCreacion).append(", usuarioCrea=")
				.append(usuarioCrea).append(", fecUpd=").append(fecUpd).append(", usuarioUpd=").append(usuarioUpd)
				.append(", modulos=").append(modulos).append("]");
		return builder.toString();
	}
	
	
}
