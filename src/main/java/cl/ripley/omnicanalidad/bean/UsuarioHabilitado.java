package cl.ripley.omnicanalidad.bean;


/**
 * Interfaz que permite hacer una validacien adicional, 
 * para asegurse que solo cuando las clases implementen a 
 * UsuarioHabilitado podren ser llamadas, 
 * de lo contrario se cambia el flujo al formulario de entrada. 
 * 
 * Para este proyecto solo sirve para pasar los datos del usuario a la accien. 
 * 
 * @author Fabian Riveros V.
 *
 */
public interface UsuarioHabilitado {

	/**
	 * Metodo que sirve para pasar la referencia del objeto usuario.
	 * 
	 * @param usuario : Objeto usuario que este haciendo la peticien de datos.
	 */
	public void setUsuario(Usuario usuario);
}
