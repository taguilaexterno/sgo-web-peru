package cl.ripley.omnicanalidad.bean;

public class ResultadoSAPConsulta {
	private int codigo;
	private String mensaje;
	private String folio;
	private String claveBanco;
	private String cuentaBank;
	private String titularCta;
	private String estadoBlock;
	private String calle;
	private String distrito;
	private String nombre1;
	private String region;
	private String telefono;
	
	public ResultadoSAPConsulta(){}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getClaveBanco() {
		return claveBanco;
	}

	public void setClaveBanco(String claveBanco) {
		this.claveBanco = claveBanco;
	}

	public String getCuentaBank() {
		return cuentaBank;
	}

	public void setCuentaBank(String cuentaBank) {
		this.cuentaBank = cuentaBank;
	}

	public String getTitularCta() {
		return titularCta;
	}

	public void setTitularCta(String titularCta) {
		this.titularCta = titularCta;
	}

	public String getEstadoBlock() {
		return estadoBlock;
	}

	public void setEstadoBlock(String estadoBlock) {
		this.estadoBlock = estadoBlock;
	}
		
	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return  "[Codigo:"+this.codigo+"][Mensaje:"+((this.mensaje != null)?this.mensaje:"null")+
				"][Folio:"+((this.folio != null)?this.folio:"null")+"][ClaveBanco:"+((this.claveBanco != null)?this.claveBanco:"null")+
				"][CuentaBancaria:"+((this.cuentaBank != null)?this.cuentaBank:"null")+"][TitularCuenta:"+((this.titularCta != null)?this.titularCta:"null")+
				"][EstadoBloqueo:"+((this.estadoBlock != null)?this.estadoBlock:"null")+"][Calle:"+((this.calle != null)?this.calle:"null")+
				"][Distrito:"+((this.distrito != null)?this.distrito:"null")+"][Nombre1:"+((this.nombre1 != null)?this.nombre1:"null")+
				"][Region:"+((this.region != null)?this.region:"null")+"][Telefono:"+((this.telefono != null)?this.telefono:"null")+"]";
	}
	
}
