package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;
import java.util.Date;

public class DetalleCompra {
	private BigDecimal montoCar;
	private int cuotas;
	private Date primerVenc;
	private BigDecimal valorCuota;
	private BigDecimal montoTre;
	private String esWebPay;
	private String glosaGsic;
	private String descripcion;
	private String sku;
	private BigDecimal precio;
	private String unidades;
	private String descuento;
	private BigDecimal total;
	private String despacho;
	private Date fechaDespacho;
	private String codBodega;

	public BigDecimal getMontoCar() {
		return montoCar;
	}
	public void setMontoCar(BigDecimal montoCar) {
		this.montoCar = montoCar;
	}
	public int getCuotas() {
		return cuotas;
	}
	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}
	public Date getPrimerVenc() {
		return primerVenc;
	}
	public void setPrimerVenc(Date primerVenc) {
		this.primerVenc = primerVenc;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public BigDecimal getMontoTre() {
		return montoTre;
	}
	public void setMontoTre(BigDecimal montoTre) {
		this.montoTre = montoTre;
	}
	public String getEsWebPay() {
		return esWebPay;
	}
	public void setEsWebPay(String esWebPay) {
		this.esWebPay = esWebPay;
	}
	public String getGlosaGsic() {
		return glosaGsic;
	}
	public void setGlosaGsic(String glosaGsic) {
		this.glosaGsic = glosaGsic;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	public String getUnidades() {
		return unidades;
	}
	public void setUnidades(String unidades) {
		this.unidades = unidades;
	}
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getDespacho() {
		return despacho;
	}
	public void setDespacho(String despacho) {
		this.despacho = despacho;
	}
	public Date getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	public String getCodBodega() {
		return codBodega;
	}
	public void setCodBodega(String codBodega) {
		this.codBodega = codBodega;
	}
}
