package cl.ripley.omnicanalidad.bean;

public class DetalleCliente {
	private int correlativoVentas;
	private String rutCliente;
	private String nombreCliente;
	private String mail;
	private String tipoDoc;
	private String direccion;
	private String fonoDespacho;
	private String fonoCliente;
	private String tipoOrden;
	private String rutFactura;
	private String razonSocialFactura;
	private String estadoDes;
	private String usuario;
	private String codTipoDoc;

	public String getCodTipoDoc() {
		return codTipoDoc;
	}
	public void setCodTipoDoc(String codTipoDoc) {
		this.codTipoDoc = codTipoDoc;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEstadoDes() {
		return estadoDes;
	}
	public void setEstadoDes(String estadoDes) {
		this.estadoDes = estadoDes;
	}
	public String getRutFactura() {
		return rutFactura;
	}
	public void setRutFactura(String rutFactura) {
		this.rutFactura = rutFactura;
	}
	public String getRazonSocialFactura() {
		return razonSocialFactura;
	}
	public void setRazonSocialFactura(String razonSocialFactura) {
		this.razonSocialFactura = razonSocialFactura;
	}
	public String getTipoOrden() {
		return tipoOrden;
	}
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}
	public int getCorrelativoVentas() {
		return correlativoVentas;
	}
	public void setCorrelativoVentas(int correlativoVentas) {
		this.correlativoVentas = correlativoVentas;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getFonoDespacho() {
		return fonoDespacho;
	}
	public void setFonoDespacho(String fonoDespacho) {
		this.fonoDespacho = fonoDespacho;
	}
	public String getFonoCliente() {
		return fonoCliente;
	}
	public void setFonoCliente(String fonoCliente) {
		this.fonoCliente = fonoCliente;
	}
}
