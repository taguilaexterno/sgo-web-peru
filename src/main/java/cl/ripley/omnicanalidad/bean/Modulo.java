package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.util.Date;

/**Clase modelo con informacion de modulos (vistas) por perfil
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 05-08-2016
 * Versiones:
 * <ul>
 *  <li>05-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public class Modulo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6279156108091361709L;

	private Long id;
	private String nombreModulo;
	private String url;
	private Permiso permiso;
	private Date fecCrea;
	private int activo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombreModulo() {
		return nombreModulo;
	}
	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Permiso getPermiso() {
		return permiso;
	}
	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}
	public Date getFecCrea() {
		return fecCrea;
	}
	public void setFecCrea(Date fecCrea) {
		this.fecCrea = fecCrea;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Modulo [id=").append(id).append(", nombreModulo=").append(nombreModulo).append(", url=")
				.append(url).append(", permiso=").append(permiso).append(", fecCrea=").append(fecCrea).append("]");
		return builder.toString();
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	
}
