package cl.ripley.omnicanalidad.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReservasUtil {

	public static Long StringToLong(String numeroString){

		try{
			return Long.parseLong(numeroString);
		}catch(Exception e){
			return Long.valueOf(0);
		}

	}

	public static int validaEstado(Long validaEstado) {
		int caso = 0;
		try{
			if(null == validaEstado){
				caso = 0;
			}else{
				caso = validaEstado.intValue();
			}
		}catch(Exception e){
			caso = 0;
		}
		return caso;
	}

	public static List<ReservaDTE> OrganizaReservas(List<ReservaDTE> reservas) {

		List<ReservaDTE> reservasOrganizado = new ArrayList<ReservaDTE>();
		Long ocAux = Long.valueOf(0);
		if (reservas != null){
			for(int i=0; i<reservas.size(); i++){
				if (!ocAux.equals(reservas.get(i).getOrdenCompra())){
					ReservaDTE res = new ReservaDTE();
					res.setOrdenCompra(reservas.get(i).getOrdenCompra());
					res.setNombreCliente(reservas.get(i).getNombreCliente());
					res.setEstado(reservas.get(i).getEstado());
					res.setFechaCreacion(reservas.get(i).getFechaCreacion());
					res.setFechaVigencia(reservas.get(i).getFechaVigencia());

					reservasOrganizado.add(res);
					ocAux = res.getOrdenCompra();
				}
			}

			for(int i=0; i<reservasOrganizado.size(); i++){
				List<ProductoReservaDTE> productos = new ArrayList<ProductoReservaDTE>();
				for(int j=0; j<reservas.size(); j++){

					if(reservasOrganizado.get(i).getOrdenCompra().equals(reservas.get(j).getOrdenCompra())){
						ProductoReservaDTE prod = new ProductoReservaDTE();
						prod.setSku(reservas.get(j).getProductos().get(0).getSku());
						prod.setDireccionDespacho(reservas.get(j).getProductos().get(0).getDireccionDespacho());
						prod.setMonto(reservas.get(j).getProductos().get(0).getMonto());
						prod.setTipoDespacho(reservas.get(j).getProductos().get(0).getTipoDespacho());
						prod.setUnidades(reservas.get(j).getProductos().get(0).getUnidades());
						prod.setNombreProducto(reservas.get(j).getProductos().get(0).getNombreProducto());
						productos.add(prod);
					}
				}
				reservasOrganizado.get(i).setProductos(productos);
			}
		} else {
			reservasOrganizado = null;
		}

		return reservasOrganizado;
	}

	public static Integer StringToInt(String integer) {
		try{
			return Integer.parseInt(integer);
		}catch(Exception e){
			return 0;
		}
	}

	public static Date StringToDate(String fechaIn) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			return formatter.parse(fechaIn);
		}catch(Exception e){
			return null;
		}
	}

	public static List<Pagina> getPaginas(int pagina, double calculo) {
		
		List<Pagina> paginas = new ArrayList<Pagina>();
		int contador = 1;
		
		if (pagina == 0){
			paginas.add(new Pagina("«","disabled"));
		}else{
			paginas.add(new Pagina("«","enabled"));
		}

		int bloque = 4;
		int min = 0;
		int max = (int) calculo;
		
		if(calculo>bloque){
			max = bloque;
			for(int i=0; max<calculo;i++){
				min = i;
				max = bloque+min;
				if(pagina>= min && pagina<=max){
					break;
				}
			}
		}
		
		while (contador <= calculo){
			
			if(contador-1 >= min && contador-1 <= max){
				if(pagina == contador-1){
					paginas.add(new Pagina(contador+"","active"));
				}else{
					paginas.add(new Pagina(contador+"","enabled"));
				}
			}
			
			contador = contador +1;
		}


		if ((pagina+1) == calculo){
			paginas.add(new Pagina("»","disabled"));
		}else{
			paginas.add(new Pagina("»","enabled"));
		}
		
		
		return paginas;
	}

	public static List<ReservaDTE> getPagina(List<ReservaDTE> sabana, int pag, int registrosPagina) {
		
		List<ReservaDTE> retorno = new ArrayList<ReservaDTE>();
		for(int i=0; i<sabana.size(); i++){
			if(i >= pag*registrosPagina && i < (pag+1)*registrosPagina){
				retorno.add(sabana.get(i));
			}
		}
		return retorno;
	}

}
