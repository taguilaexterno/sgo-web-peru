package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class ArticuloVenta {
	private Integer		correlativoVenta;
	private Integer		correlativoItem;
	private String		codArticulo;
	private String		descRipley;
	private BigDecimal		precio;
	private Integer		unidades;
	private Integer		color;
	private BigDecimal		montoDescuento;
	private Integer		tipoDescuento;
	private String		codDespacho;
	private Integer		tipoRegalo;
	private String		esregalo;
	private String		mensaje;
	private Integer		numNotaCredito;
	private Timestamp	fecNotaCredito;
	private Timestamp	horaNotaCredito;
	private Integer		numCajaNotaCredito;
	private Integer		correlativoNotaCredito;
	private Integer		rutNotaCredito;
	private String		dvNotaCredito;
	private Integer		indicadorEg;
	private String		tipoDespacho;
	private Date        fechaDespacho;
	private String		codBodega;
	private String		tipoPapelRegalo;
	private Integer		despachoId;
	private String		estadoVenta;
	private String		ordenMkp;
	private String		depto;
	private String		deptoGlosa;
	private BigDecimal     costo;
	private Integer		esNC;
	private Integer 	esMkp;
	private Integer 	enTransito;
	private BigDecimal     precioPorUnidades;
	
	public Integer getEnTransito() {
		return enTransito;
	}
	public void setEnTransito(Integer enTransito) {
		this.enTransito = enTransito;
	}
	public Integer getEsMkp() {
		return esMkp;
	}
	public void setEsMkp(Integer esMkp) {
		this.esMkp = esMkp;
	}
	public Integer getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Integer correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getCorrelativoItem() {
		return correlativoItem;
	}
	public void setCorrelativoItem(Integer correlativoItem) {
		this.correlativoItem = correlativoItem;
	}
	public String getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}
	public String getDescRipley() {
		return descRipley;
	}
	public void setDescRipley(String descRipley) {
		this.descRipley = descRipley;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	public Integer getUnidades() {
		return unidades;
	}
	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}
	public BigDecimal getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(BigDecimal montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public Integer getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(Integer tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	public String getCodDespacho() {
		return codDespacho;
	}
	public void setCodDespacho(String codDespacho) {
		this.codDespacho = codDespacho;
	}
	public Integer getTipoRegalo() {
		return tipoRegalo;
	}
	public void setTipoRegalo(Integer tipoRegalo) {
		this.tipoRegalo = tipoRegalo;
	}
	public String getEsregalo() {
		return esregalo;
	}
	public void setEsregalo(String esregalo) {
		this.esregalo = esregalo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(Integer numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public Timestamp getFecNotaCredito() {
		return fecNotaCredito;
	}
	public void setFecNotaCredito(Timestamp fecNotaCredito) {
		this.fecNotaCredito = fecNotaCredito;
	}
	public Timestamp getHoraNotaCredito() {
		return horaNotaCredito;
	}
	public void setHoraNotaCredito(Timestamp horaNotaCredito) {
		this.horaNotaCredito = horaNotaCredito;
	}
	public Integer getNumCajaNotaCredito() {
		return numCajaNotaCredito;
	}
	public void setNumCajaNotaCredito(Integer numCajaNotaCredito) {
		this.numCajaNotaCredito = numCajaNotaCredito;
	}
	public Integer getCorrelativoNotaCredito() {
		return correlativoNotaCredito;
	}
	public void setCorrelativoNotaCredito(Integer correlativoNotaCredito) {
		this.correlativoNotaCredito = correlativoNotaCredito;
	}
	public Integer getRutNotaCredito() {
		return rutNotaCredito;
	}
	public void setRutNotaCredito(Integer rutNotaCredito) {
		this.rutNotaCredito = rutNotaCredito;
	}
	public String getDvNotaCredito() {
		return dvNotaCredito;
	}
	public void setDvNotaCredito(String dvNotaCredito) {
		this.dvNotaCredito = dvNotaCredito;
	}
	public Integer getIndicadorEg() {
		return indicadorEg;
	}
	public void setIndicadorEg(Integer indicadorEg) {
		this.indicadorEg = indicadorEg;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public Date getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	public String getCodBodega() {
		return codBodega;
	}
	public void setCodBodega(String codBodega) {
		this.codBodega = codBodega;
	}
	public String getTipoPapelRegalo() {
		return tipoPapelRegalo;
	}
	public void setTipoPapelRegalo(String tipoPapelRegalo) {
		this.tipoPapelRegalo = tipoPapelRegalo;
	}
	public Integer getDespachoId() {
		return despachoId;
	}
	public void setDespachoId(Integer despachoId) {
		this.despachoId = despachoId;
	}
	public String getEstadoVenta() {
		return estadoVenta;
	}
	public void setEstadoVenta(String estadoVenta) {
		this.estadoVenta = estadoVenta;
	}
	public String getOrdenMkp() {
		return ordenMkp;
	}
	public void setOrdenMkp(String ordenMkp) {
		this.ordenMkp = ordenMkp;
	}
	public String getDepto() {
		return depto;
	}
	public void setDepto(String depto) {
		this.depto = depto;
	}
	public String getDeptoGlosa() {
		return deptoGlosa;
	}
	public void setDeptoGlosa(String deptoGlosa) {
		this.deptoGlosa = deptoGlosa;
	}
	public BigDecimal getCosto() {
		return costo;
	}
	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}
	public Integer getEsNC() {
		return esNC;
	}
	public void setEsNC(Integer esNC) {
		this.esNC = esNC;
	}
	public BigDecimal getPrecioPorUnidades() {
		return precioPorUnidades;
	}
	public void setPrecioPorUnidades(BigDecimal precioPorUnidades) {
		this.precioPorUnidades = precioPorUnidades;
	}
}
