package cl.ripley.omnicanalidad.bean;

import java.sql.Timestamp;
import java.util.List;

public class VoucherPDF {

	private String ripleyRazonSocial;
	private String direccionRipley;
	private String ripleyRuc;
	private String boleta;
	private Timestamp fechaHora;
	private Long caja;
	private Integer trx;
	private String serieImpresora;
	private Long sucursal;
	private String barCode;
	private String clubRipley;
	private String dniCliente;
	private String nombreCliente;
	private String emailCliente;
	private String telefonoCliente;
	private String clienteLoyalty;
	private List<ArticuloDetalleVoucherPDF> articulos;
	private String subtotal;
	private String descuentoTotal;
	private String numeroUnidades;
	private String opExonerada;
	private String opInafecta;
	private String opGravada;
	private String igv;
	private String totalPagar;
	private String totalPalabras;
	private List<String> glosas;
	private String codigoQr;
	
	public String getRipleyRazonSocial() {
		return ripleyRazonSocial;
	}
	public void setRipleyRazonSocial(String ripleyRazonSocial) {
		this.ripleyRazonSocial = ripleyRazonSocial;
	}
	public String getDireccionRipley() {
		return direccionRipley;
	}
	public void setDireccionRipley(String direccionRipley) {
		this.direccionRipley = direccionRipley;
	}
	public String getRipleyRuc() {
		return ripleyRuc;
	}
	public void setRipleyRuc(String ripleyRuc) {
		this.ripleyRuc = ripleyRuc;
	}
	public String getBoleta() {
		return boleta;
	}
	public void setBoleta(String boleta) {
		this.boleta = boleta;
	}
	public Timestamp getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Timestamp fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Long getCaja() {
		return caja;
	}
	public void setCaja(Long caja) {
		this.caja = caja;
	}
	public Integer getTrx() {
		return trx;
	}
	public void setTrx(Integer trx) {
		this.trx = trx;
	}
	public String getSerieImpresora() {
		return serieImpresora;
	}
	public void setSerieImpresora(String serieImpresora) {
		this.serieImpresora = serieImpresora;
	}
	public Long getSucursal() {
		return sucursal;
	}
	public void setSucursal(Long sucursal) {
		this.sucursal = sucursal;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getClubRipley() {
		return clubRipley;
	}
	public void setClubRipley(String clubRipley) {
		this.clubRipley = clubRipley;
	}
	public String getDniCliente() {
		return dniCliente;
	}
	public void setDniCliente(String dniCliente) {
		this.dniCliente = dniCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public String getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
	public String getClienteLoyalty() {
		return clienteLoyalty;
	}
	public void setClienteLoyalty(String clienteLoyalty) {
		this.clienteLoyalty = clienteLoyalty;
	}
	public List<ArticuloDetalleVoucherPDF> getArticulos() {
		return articulos;
	}
	public void setArticulos(List<ArticuloDetalleVoucherPDF> articulos) {
		this.articulos = articulos;
	}
	public String getTotalPalabras() {
		return totalPalabras;
	}
	public void setTotalPalabras(String totalPalabras) {
		this.totalPalabras = totalPalabras;
	}
	public List<String> getGlosas() {
		return glosas;
	}
	public void setGlosas(List<String> glosas) {
		this.glosas = glosas;
	}
	public String getCodigoQr() {
		return codigoQr;
	}
	public void setCodigoQr(String codigoQr) {
		this.codigoQr = codigoQr;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getDescuentoTotal() {
		return descuentoTotal;
	}
	public void setDescuentoTotal(String descuentoTotal) {
		this.descuentoTotal = descuentoTotal;
	}
	public String getNumeroUnidades() {
		return numeroUnidades;
	}
	public void setNumeroUnidades(String numeroUnidades) {
		this.numeroUnidades = numeroUnidades;
	}
	public String getOpExonerada() {
		return opExonerada;
	}
	public void setOpExonerada(String opExonerada) {
		this.opExonerada = opExonerada;
	}
	public String getOpInafecta() {
		return opInafecta;
	}
	public void setOpInafecta(String opInafecta) {
		this.opInafecta = opInafecta;
	}
	public String getOpGravada() {
		return opGravada;
	}
	public void setOpGravada(String opGravada) {
		this.opGravada = opGravada;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getTotalPagar() {
		return totalPagar;
	}
	public void setTotalPagar(String totalPagar) {
		this.totalPagar = totalPagar;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"ripleyRazonSocial\":\"").append(ripleyRazonSocial).append("\",\"direccionRipley\":\"")
				.append(direccionRipley).append("\",\"ripleyRuc\":\"").append(ripleyRuc).append("\",\"boleta\":\"")
				.append(boleta).append("\",\"fechaHora\":\"").append(fechaHora).append("\",\"caja\":\"").append(caja)
				.append("\",\"trx\":\"").append(trx).append("\",\"serieImpresora\":\"").append(serieImpresora)
				.append("\",\"sucursal\":\"").append(sucursal).append("\",\"barCode\":\"").append(barCode)
				.append("\",\"clubRipley\":\"").append(clubRipley).append("\",\"dniCliente\":\"").append(dniCliente)
				.append("\",\"nombreCliente\":\"").append(nombreCliente).append("\",\"emailCliente\":\"")
				.append(emailCliente).append("\",\"telefonoCliente\":\"").append(telefonoCliente)
				.append("\",\"clienteLoyalty\":\"").append(clienteLoyalty).append("\",\"articulos\":\"")
				.append(articulos).append("\",\"subtotal\":\"").append(subtotal).append("\",\"descuentoTotal\":\"")
				.append(descuentoTotal).append("\",\"numeroUnidades\":\"").append(numeroUnidades)
				.append("\",\"opExonerada\":\"").append(opExonerada).append("\",\"opInafecta\":\"").append(opInafecta)
				.append("\",\"opGravada\":\"").append(opGravada).append("\",\"igv\":\"").append(igv)
				.append("\",\"totalPagar\":\"").append(totalPagar).append("\",\"totalPalabras\":\"")
				.append(totalPalabras).append("\",\"glosas\":\"").append(glosas).append("\",\"codigoQr\":\"")
				.append(codigoQr).append("\"}");
		return builder.toString();
	}
	
	
}
