package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class ProductoReservaDTE {
	
	private String tipoDespacho;
	private String direccionDespacho;
	private BigDecimal monto;
	private Integer unidades;
	private String sku;
	private String nombreProducto;
	
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public String getDireccionDespacho() {
		return direccionDespacho;
	}
	public void setDireccionDespacho(String direccionDespacho) {
		this.direccionDespacho = direccionDespacho;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Integer getUnidades() {
		return unidades;
	}
	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

}
