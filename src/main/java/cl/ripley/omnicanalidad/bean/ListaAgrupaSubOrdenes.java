package cl.ripley.omnicanalidad.bean;
import java.math.BigDecimal;
import java.util.List;

public class ListaAgrupaSubOrdenes {
	
	private String subOrden;
	private BigDecimal montoTotal;
	private Integer idRefund;	
	private List<ArticulosVentaOC> articulos;
	private int esRequired;
	private Boolean reversado;
	
	public ListaAgrupaSubOrdenes() {
		
	}

	public ListaAgrupaSubOrdenes(String subOrden, BigDecimal montoTotal, Integer idRefund,
			List<ArticulosVentaOC> articulos, int esRequired, Boolean reversado) {
		this.subOrden = subOrden;
		this.montoTotal = montoTotal;
		this.idRefund = idRefund;
		this.articulos = articulos;
		this.esRequired = esRequired;
		this.reversado = reversado;
	}

	public List<ArticulosVentaOC> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<ArticulosVentaOC> articulos) {
		this.articulos = articulos;
	}

	public String getSubOrden() {
		return subOrden;
	}

	public void setSubOrden(String subOrden) {
		this.subOrden = subOrden;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Integer getIdRefund() {
		return idRefund;
	}

	public void setIdRefund(Integer idRefund) {
		this.idRefund = idRefund;
	}

	public int getEsRequired() {
		return esRequired;
	}

	public void setEsRequired(int esRequired) {
		this.esRequired = esRequired;
	}

	public Boolean getReversado() {
		return reversado;
	}

	public void setReversado(Boolean reversado) {
		this.reversado = reversado;
	} 
	
}
