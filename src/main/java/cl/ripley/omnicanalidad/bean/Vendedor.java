package cl.ripley.omnicanalidad.bean;

public class Vendedor {
	private String rutVendedor;
	private String nombreVendedor;
	
	public String getRutVendedor() {
		return rutVendedor;
	}
	public void setRutVendedor(String rutVendedor) {
		this.rutVendedor = rutVendedor;
	}
	public String getNombreVendedor() {
		return nombreVendedor;
	}
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

}
