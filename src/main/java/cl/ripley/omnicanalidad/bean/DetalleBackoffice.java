package cl.ripley.omnicanalidad.bean;

public class DetalleBackoffice {

	private int numeroSucursal;
	private int numeroCaja;
	private int numeroBoleta;
	private String fechaHoraCreacion;
	
	public int getNumeroSucursal() {
		return numeroSucursal;
	}
	public void setNumeroSucursal(int numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}
	public int getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(int numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public int getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(int numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public String getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}
	public void setFechaHoraCreacion(String fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}
	
	
}
