package cl.ripley.omnicanalidad.bean;

public class CategoriasProperties {

	private String propertyTypeId;
	private String description;
	
	public String getPropertyTypeId() {
		return propertyTypeId;
	}
	public void setPropertyTypeId(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
