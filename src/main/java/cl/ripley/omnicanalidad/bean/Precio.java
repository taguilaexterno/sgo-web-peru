package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class Precio {
	private String descripcion;
	private BigDecimal valor;

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
