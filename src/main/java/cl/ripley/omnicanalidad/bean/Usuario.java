package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.sql.Date;

import cl.ripley.omnicanalidad.util.Password;

public class Usuario implements Serializable {
	
	private static final long serialVersionUID = -3321643859026903779L;
	private Integer codUsuario;
	private Long codPerfil;
	private Integer codEstado;
	
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String email;
	
	private String usuario;
	private String password;
	private String passwordValid;
	
	private Date fecCreacion;
	
	private Perfil perfil;
	
	private String fecInicioVigencia;
	private String fecFinVigencia;
	private String vigencia;
	private Long activarDesactivar;

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public Long getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(Long codPerfil) {
		this.codPerfil = codPerfil;
	}

	public Integer getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordValid() {
		return passwordValid;
	}

	public void setPasswordValid(String passwordValid) {
		this.passwordValid = passwordValid;
	}

	public Date getFecCreacion() {
		return fecCreacion;
	}

	public void setFecCreacion(Date fecCreacion) {
		this.fecCreacion = fecCreacion;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	public String getEncryptedPass() throws Exception {
		Password password = new Password();
		return password.encryptPassword(this.password);
	}

	public String getFecInicioVigencia() {
		return fecInicioVigencia;
	}

	public void setFecInicioVigencia(String fecInicioVigencia) {
		this.fecInicioVigencia = fecInicioVigencia;
	}

	public String getFecFinVigencia() {
		return fecFinVigencia;
	}

	public void setFecFinVigencia(String fecFinVigencia) {
		this.fecFinVigencia = fecFinVigencia;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}	

	public Long getActivarDesactivar() {
		return activarDesactivar;
	}

	public void setActivarDesactivar(Long activarDesactivar) {
		this.activarDesactivar = activarDesactivar;
	}	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [codUsuario=").append(codUsuario).append(", codPerfil=").append(codPerfil)
				.append(", codEstado=").append(codEstado).append(", nombre=").append(nombre).append(", apePaterno=")
				.append(apePaterno).append(", apeMaterno=").append(apeMaterno).append(", email=").append(email)
				.append(", usuario=").append(usuario).append(", password=").append(password).append(", passwordValid=")
				.append(passwordValid).append(", fecCreacion=").append(fecCreacion).append(", perfil=").append(perfil)
				.append(", fecInicioVigencia=").append(fecInicioVigencia).append(", fecFinVigencia=").append(fecFinVigencia)
				.append(", vigencia=").append(vigencia).append(", activarDesactivar=").append(activarDesactivar)
				.append("]");
		return builder.toString();
	}

}
