package cl.ripley.omnicanalidad.bean;

public class ResultadoSAP {
	private String rut;
	private String dv;
	private String folioSAP;
	private String claveBanco;
	private String nombreBanco;
	private String cuentaBancaria;
	private String estadoBloqueo;
	private String nombre1;
	private String calle;
	private String distrito;
	private String codigoRegion;
	private String nombreRegion;
	private String telefono;
	private String titularCuenta;
	private String pais;
	private String idioma;
	private String cuentaAsociada;
	private String condicionPago;
	private String verFacturaDoble;
	private String viaPago;	
	private String ticketAsociado;
	
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public String getTitularCuenta() {
		return titularCuenta;
	}
	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public String getCuentaAsociada() {
		return cuentaAsociada;
	}
	public void setCuentaAsociada(String cuentaAsociada) {
		this.cuentaAsociada = cuentaAsociada;
	}
	public String getCondicionPago() {
		return condicionPago;
	}
	public void setCondicionPago(String condicionPago) {
		this.condicionPago = condicionPago;
	}
	public String getVerFacturaDoble() {
		return verFacturaDoble;
	}
	public void setVerFacturaDoble(String verFacturaDoble) {
		this.verFacturaDoble = verFacturaDoble;
	}
	public String getViaPago() {
		return viaPago;
	}
	public void setViaPago(String viaPago) {
		this.viaPago = viaPago;
	}
	public String getEstadoBloqueo() {
		return estadoBloqueo;
	}
	public void setEstadoBloqueo(String estadoBloqueo) {
		this.estadoBloqueo = estadoBloqueo;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getDv() {
		return dv;
	}
	public void setDv(String dv) {
		this.dv = dv;
	}
	public String getFolioSAP() {
		return folioSAP;
	}
	public void setFolioSAP(String folioSAP) {
		this.folioSAP = folioSAP;
	}
	public String getClaveBanco() {
		return claveBanco;
	}
	public void setClaveBanco(String claveBanco) {
		this.claveBanco = claveBanco;
	}
	public String getCuentaBancaria() {
		return cuentaBancaria;
	}
	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}
	public String getNombre1() {
		return nombre1;
	}
	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCodigoRegion() {
		return codigoRegion;
	}
	public void setCodigoRegion(String codigoRegion) {
		this.codigoRegion = codigoRegion;
	}
	public String getNombreRegion() {
		return nombreRegion;
	}
	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTicketAsociado() {
		return ticketAsociado;
	}
	public void setTicketAsociado(String ticketAsociado) {
		this.ticketAsociado = ticketAsociado;
	}
	
}
