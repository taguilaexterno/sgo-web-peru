package cl.ripley.omnicanalidad.bean;

public class DetalleDespacho {
	private String rutDespacho;
	private String nombreDespacho;
	private String sku;
	private String estadoBt;
	private String direccion;
	private String codigoBo;
	private String region;
	private String comuna;
	private String cud;
	private String articulo;
	private String fono;
	private String observacion;
	private String rutCliente;
	private String nombreFact;
	private String comunaFact;
	private String unidades;

	public String getUnidades() {
		return unidades;
	}
	public void setUnidades(String unidades) {
		this.unidades = unidades;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getCud() {
		return cud;
	}
	public void setCud(String cud) {
		this.cud = cud;
	}
	public String getRutDespacho() {
		return rutDespacho;
	}
	public void setRutDespacho(String rutDespacho) {
		this.rutDespacho = rutDespacho;
	}
	public String getNombreDespacho() {
		return nombreDespacho;
	}
	public void setNombreDespacho(String nombreDespacho) {
		this.nombreDespacho = nombreDespacho;
	}
	public String getEstadoBt() {
		return estadoBt;
	}
	public void setEstadoBt(String estadoBt) {
		this.estadoBt = estadoBt;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCodigoBo() {
		return codigoBo;
	}
	public void setCodigoBo(String codigoBo) {
		this.codigoBo = codigoBo;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getArticulo() {
		return articulo;
	}
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	public String getFono() {
		return fono;
	}
	public void setFono(String fono) {
		this.fono = fono;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getNombreFact() {
		return nombreFact;
	}
	public void setNombreFact(String nombreFact) {
		this.nombreFact = nombreFact;
	}
	public String getComunaFact() {
		return comunaFact;
	}
	public void setComunaFact(String comunaFact) {
		this.comunaFact = comunaFact;
	}
}
