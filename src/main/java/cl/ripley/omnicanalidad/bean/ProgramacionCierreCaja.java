package cl.ripley.omnicanalidad.bean;

/**Clase que representa los datos de la tabla cv_prog_cierre
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 18-08-2016
 * Versiones:
 * <ul>
 *  <li>18-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public class ProgramacionCierreCaja {

	private Long id;
	private Integer tipoCierre;
	private String horaCierre;
	private Long volumen;
	private Long numeroSucursal;
	private Long numeroCaja;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getTipoCierre() {
		return tipoCierre;
	}
	public void setTipoCierre(Integer tipoCierre) {
		this.tipoCierre = tipoCierre;
	}
	public String getHoraCierre() {
		return horaCierre;
	}
	public void setHoraCierre(String horaCierre) {
		this.horaCierre = horaCierre;
	}
	public Long getVolumen() {
		return volumen;
	}
	public void setVolumen(Long volumen) {
		this.volumen = volumen;
	}
	public Long getNumeroSucursal() {
		return numeroSucursal;
	}
	public void setNumeroSucursal(Long numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}
	public Long getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(Long numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProgramacionCierreCaja [id=").append(id).append(", tipoCierre=").append(tipoCierre)
				.append(", horaCierre=").append(horaCierre).append(", volumen=").append(volumen)
				.append(", numeroSucursal=").append(numeroSucursal).append(", numeroCaja=").append(numeroCaja).append("]");
		return builder.toString();
	}
	
}
