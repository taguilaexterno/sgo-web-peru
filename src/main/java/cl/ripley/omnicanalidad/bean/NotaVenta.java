package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class NotaVenta {

	private Long correlativoVenta;
	private Timestamp fechaCreacion;
	private Timestamp fechaHoraCreacion;
	private Integer horasAdministrativas;
	private Integer estado;
	private BigDecimal montoVenta;
	private BigDecimal montoDescuento;
	private Integer tipoDescuento;
	private Integer rutComprador;
	private String dvComprador;
	private Integer numeroSucursal;
	private Integer codigoRegalo;
	private Integer tipoRegalo;
	private Integer numeroBoleta;
	private Timestamp fechaBoleta;
	private Timestamp horaBoleta;
	private Integer numeroCaja;
	private Integer correlativoBoleta;
	private Integer rutBoleta;
	private String dvBoleta;
	private Integer numNotaCredito;
	private Timestamp fecNotaCredito;
	private Timestamp horaNotaCredito;
	private Integer numCajaNotaCredito;
	private Integer correlativoNotaCredito;
	private Integer rutNotaCredito;
	private String dvNotaCredito;
	private String tvnveGlsOreExo;
	private String tipoPromocion;
	private Integer optcounter;
	private String origenVta;
	private String ejecutivoVta;
	private Integer tipoDoc;
	private String  folioSii;
	private Integer folioNcSii;
	private String urlDoce;
	private String urlNotaCredito;
	private String usuario;
	private String indicadorMkp;
	private String tipoDespacho;
	private Integer rutCliente;
	private Integer telefonoCliente;
	private String emailCliente;
	private Integer comunaDespacho;
	private Integer regionDespacho;
	
	private String tipoPago;
	private String estadoDes;
	private String ncuotas;
	private String bloqueado;
	private String glosaBloqueo;

	private Long nroTrx;
	private Integer estado_COC;
    private Integer estado_CS;
    private String estado_CS_DESC;
    
	public String getEstadoDes() {
		return estadoDes;
	}
	public void setEstadoDes(String estadoDes) {
		this.estadoDes = estadoDes;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Timestamp getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}
	public void setFechaHoraCreacion(Timestamp fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}
	public Integer getHorasAdministrativas() {
		return horasAdministrativas;
	}
	public void setHorasAdministrativas(Integer horasAdministrativas) {
		this.horasAdministrativas = horasAdministrativas;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public BigDecimal getMontoVenta() {
		return montoVenta;
	}
	public void setMontoVenta(BigDecimal montoVenta) {
		this.montoVenta = montoVenta;
	}
	public BigDecimal getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(BigDecimal montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public Integer getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(Integer tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	public Integer getRutComprador() {
		return rutComprador;
	}
	public void setRutComprador(Integer rutComprador) {
		this.rutComprador = rutComprador;
	}
	public String getDvComprador() {
		return dvComprador;
	}
	public void setDvComprador(String dvComprador) {
		this.dvComprador = dvComprador;
	}
	public Integer getNumeroSucursal() {
		return numeroSucursal;
	}
	public void setNumeroSucursal(Integer numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}
	public Integer getCodigoRegalo() {
		return codigoRegalo;
	}
	public void setCodigoRegalo(Integer codigoRegalo) {
		this.codigoRegalo = codigoRegalo;
	}
	public Integer getTipoRegalo() {
		return tipoRegalo;
	}
	public void setTipoRegalo(Integer tipoRegalo) {
		this.tipoRegalo = tipoRegalo;
	}
	public Integer getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(Integer numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public Timestamp getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(Timestamp fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	public Timestamp getHoraBoleta() {
		return horaBoleta;
	}
	public void setHoraBoleta(Timestamp horaBoleta) {
		this.horaBoleta = horaBoleta;
	}
	public Integer getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(Integer numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public Integer getCorrelativoBoleta() {
		return correlativoBoleta;
	}
	public void setCorrelativoBoleta(Integer correlativoBoleta) {
		this.correlativoBoleta = correlativoBoleta;
	}
	public Integer getRutBoleta() {
		return rutBoleta;
	}
	public void setRutBoleta(Integer rutBoleta) {
		this.rutBoleta = rutBoleta;
	}
	public String getDvBoleta() {
		return dvBoleta;
	}
	public void setDvBoleta(String dvBoleta) {
		this.dvBoleta = dvBoleta;
	}
	public Integer getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(Integer numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public Timestamp getFecNotaCredito() {
		return fecNotaCredito;
	}
	public void setFecNotaCredito(Timestamp fecNotaCredito) {
		this.fecNotaCredito = fecNotaCredito;
	}
	public Timestamp getHoraNotaCredito() {
		return horaNotaCredito;
	}
	public void setHoraNotaCredito(Timestamp horaNotaCredito) {
		this.horaNotaCredito = horaNotaCredito;
	}
	public Integer getNumCajaNotaCredito() {
		return numCajaNotaCredito;
	}
	public void setNumCajaNotaCredito(Integer numCajaNotaCredito) {
		this.numCajaNotaCredito = numCajaNotaCredito;
	}
	public Integer getCorrelativoNotaCredito() {
		return correlativoNotaCredito;
	}
	public void setCorrelativoNotaCredito(Integer correlativoNotaCredito) {
		this.correlativoNotaCredito = correlativoNotaCredito;
	}
	public Integer getRutNotaCredito() {
		return rutNotaCredito;
	}
	public void setRutNotaCredito(Integer rutNotaCredito) {
		this.rutNotaCredito = rutNotaCredito;
	}
	public String getDvNotaCredito() {
		return dvNotaCredito;
	}
	public void setDvNotaCredito(String dvNotaCredito) {
		this.dvNotaCredito = dvNotaCredito;
	}
	public String getTvnveGlsOreExo() {
		return tvnveGlsOreExo;
	}
	public void setTvnveGlsOreExo(String tvnveGlsOreExo) {
		this.tvnveGlsOreExo = tvnveGlsOreExo;
	}
	public String getTipoPromocion() {
		return tipoPromocion;
	}
	public void setTipoPromocion(String tipoPromocion) {
		this.tipoPromocion = tipoPromocion;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}
	public String getOrigenVta() {
		return origenVta;
	}
	public void setOrigenVta(String origenVta) {
		this.origenVta = origenVta;
	}
	public String getEjecutivoVta() {
		return ejecutivoVta;
	}
	public void setEjecutivoVta(String ejecutivoVta) {
		this.ejecutivoVta = ejecutivoVta;
	}
	public Integer getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Integer tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getFolioSii() {
		return folioSii;
	}
	public void setFolioSii(String folioSii) {
		this.folioSii = folioSii;
	}
	public Integer getFolioNcSii() {
		return folioNcSii;
	}
	public void setFolioNcSii(Integer folioNcSii) {
		this.folioNcSii = folioNcSii;
	}
	public String getUrlDoce() {
		return urlDoce;
	}
	public void setUrlDoce(String urlDoce) {
		this.urlDoce = urlDoce;
	}
	public String getUrlNotaCredito() {
		return urlNotaCredito;
	}
	public void setUrlNotaCredito(String urlNotaCredito) {
		this.urlNotaCredito = urlNotaCredito;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIndicadorMkp() {
		return indicadorMkp;
	}
	public void setIndicadorMkp(String indicadorMkp) {
		this.indicadorMkp = indicadorMkp;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public Integer getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Integer rutCliente) {
		this.rutCliente = rutCliente;
	}
	public Integer getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(Integer telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public Integer getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(Integer comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public Integer getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(Integer regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getNcuotas() {
		return ncuotas;
	}
	public void setNcuotas(String ncuotas) {
		this.ncuotas = ncuotas;
	}
	public String getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}
	public String getGlosaBloqueo() {
		return glosaBloqueo;
	}
	public void setGlosaBloqueo(String glosaBloqueo) {
		this.glosaBloqueo = glosaBloqueo;
	}
	public Long getNroTrx() {
		return nroTrx;
	}
	public void setNroTrx(Long nroTrx) {
		this.nroTrx = nroTrx;
	}
	
	public Integer getEstado_COC() {
		return estado_COC;
	}
	public void setEstado_COC(Integer estado_COC) {
		this.estado_COC = estado_COC;
	}
	public Integer getEstado_CS() {
		return estado_CS;
	}
	public void setEstado_CS(Integer estado_CS) {
		this.estado_CS = estado_CS;
	}
	public String getEstado_CS_DESC() {
		return estado_CS_DESC;
	}
	public void setEstado_CS_DESC(String estado_CS_DESC) {
		this.estado_CS_DESC = estado_CS_DESC;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NotaVenta [correlativoVenta=").append(correlativoVenta).append(", fechaCreacion=")
				.append(fechaCreacion).append(", fechaHoraCreacion=").append(fechaHoraCreacion)
				.append(", horasAdministrativas=").append(horasAdministrativas).append(", estado=").append(estado)
				.append(", montoVenta=").append(montoVenta).append(", montoDescuento=").append(montoDescuento)
				.append(", tipoDescuento=").append(tipoDescuento).append(", rutComprador=").append(rutComprador)
				.append(", dvComprador=").append(dvComprador).append(", numeroSucursal=").append(numeroSucursal)
				.append(", codigoRegalo=").append(codigoRegalo).append(", tipoRegalo=").append(tipoRegalo)
				.append(", numeroBoleta=").append(numeroBoleta).append(", fechaBoleta=").append(fechaBoleta)
				.append(", horaBoleta=").append(horaBoleta).append(", numeroCaja=").append(numeroCaja)
				.append(", correlativoBoleta=").append(correlativoBoleta).append(", rutBoleta=").append(rutBoleta)
				.append(", dvBoleta=").append(dvBoleta).append(", numNotaCredito=").append(numNotaCredito)
				.append(", fecNotaCredito=").append(fecNotaCredito).append(", horaNotaCredito=").append(horaNotaCredito)
				.append(", numCajaNotaCredito=").append(numCajaNotaCredito).append(", correlativoNotaCredito=")
				.append(correlativoNotaCredito).append(", rutNotaCredito=").append(rutNotaCredito)
				.append(", dvNotaCredito=").append(dvNotaCredito).append(", tvnveGlsOreExo=").append(tvnveGlsOreExo)
				.append(", tipoPromocion=").append(tipoPromocion).append(", optcounter=").append(optcounter)
				.append(", origenVta=").append(origenVta).append(", ejecutivoVta=").append(ejecutivoVta)
				.append(", tipoDoc=").append(tipoDoc).append(", folioSii=").append(folioSii).append(", folioNcSii=")
				.append(folioNcSii).append(", urlDoce=").append(urlDoce).append(", urlNotaCredito=")
				.append(urlNotaCredito).append(", usuario=").append(usuario).append(", indicadorMkp=")
				.append(indicadorMkp).append(", tipoDespacho=").append(tipoDespacho).append(", rutCliente=")
				.append(rutCliente).append(", telefonoCliente=").append(telefonoCliente).append(", emailCliente=")
				.append(emailCliente).append(", comunaDespacho=").append(comunaDespacho).append(", regionDespacho=")
				.append(regionDespacho).append(", tipoPago=").append(tipoPago).append(", estadoDes=").append(estadoDes)
				.append(", ncuotas=").append(ncuotas).append(", bloqueado=").append(bloqueado).append(", glosaBloqueo=")
				.append(glosaBloqueo).append(", nroTrx=").append(nroTrx).append("]");
		return builder.toString();
	}

}
