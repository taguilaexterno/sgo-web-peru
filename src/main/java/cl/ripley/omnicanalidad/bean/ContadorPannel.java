package cl.ripley.omnicanalidad.bean;

public class ContadorPannel {

	private Integer medioPago;
	private Integer ordenesManuales;
	private Integer rechazoStockTotal;
	private Integer rechazoStockParcial;
	private Integer sinStock;
	private Integer validacionPendienteStock;
	
		
	public Integer getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(Integer medioPago) {
		this.medioPago = medioPago;
	}

	public Integer getRechazoStockTotal() {
		return rechazoStockTotal;
	}

	public void setRechazoStockTotal(Integer rechazoStockTotal) {
		this.rechazoStockTotal = rechazoStockTotal;
	}


	public Integer getRechazoStockParcial() {
		return rechazoStockParcial;
	}

	public void setRechazoStockParcial(Integer rechazoStockParcial) {
		this.rechazoStockParcial = rechazoStockParcial;
	}


	public Integer getSinStock() {
		return sinStock;
	}


	public void setSinStock(Integer sinStock) {
		this.sinStock = sinStock;
	}


	public Integer getValidacionPendienteStock() {
		return validacionPendienteStock;
	}


	public void setValidacionPendienteStock(Integer validacionPendienteStock) {
		this.validacionPendienteStock = validacionPendienteStock;
	}

	public Integer getOrdenesManuales() {
		return ordenesManuales;
	}

	public void setOrdenesManuales(Integer ordenesManuales) {
		this.ordenesManuales = ordenesManuales;
	}
}