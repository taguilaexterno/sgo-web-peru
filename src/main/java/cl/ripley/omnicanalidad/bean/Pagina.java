package cl.ripley.omnicanalidad.bean;

public class Pagina {
	
	private String pagina;
	private String estado;
	
	public Pagina(String pagina, String estado) {
		super();
		this.pagina = pagina;
		this.estado = estado;
	}

	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
