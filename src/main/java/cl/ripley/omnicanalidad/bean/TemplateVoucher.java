package cl.ripley.omnicanalidad.bean;

public class TemplateVoucher {

	private String llave;
	private String nombre;
	private Integer estado;
	private String html;
	private String url;
	
	public String getLlave() {
		return llave;
	}
	public void setLlave(String llave) {
		this.llave = llave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TemplateVoucher [llave=").append(llave).append(", nombre=").append(nombre).append(", estado=")
				.append(estado).append(", html=").append(html).append(", url=").append(url).append("]");
		return builder.toString();
	}
	
	
}
