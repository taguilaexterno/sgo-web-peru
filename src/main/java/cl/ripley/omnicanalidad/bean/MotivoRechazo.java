package cl.ripley.omnicanalidad.bean;

public class MotivoRechazo {
	private Integer codMotivo;
	private String motivo;
	
	public Integer getCodMotivo() {
		return codMotivo;
	}
	public void setCodMotivo(Integer codMotivo) {
		this.codMotivo = codMotivo;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
}
