package cl.ripley.omnicanalidad.bean;

public class ResultadoSAPModificacion {
	private int codigo;
	private String mensaje;
	
	private String rut;
	// DIRECCION
	private String nombreProv;
	private String direccionProv;
	private String distritoProv;
	private String regionProv;
	// COMUNICACION
	private String telefono;
	// BANCO
	private String claveBanco;
	private String cuentaBank;
	private String titular;
	
	public ResultadoSAPModificacion(){}
		
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombreProv() {
		return nombreProv;
	}

	public void setNombreProv(String nombreProv) {
		this.nombreProv = nombreProv;
	}

	public String getDireccionProv() {
		return direccionProv;
	}

	public void setDireccionProv(String direccionProv) {
		this.direccionProv = direccionProv;
	}

	public String getDistritoProv() {
		return distritoProv;
	}

	public void setDistritoProv(String distritoProv) {
		this.distritoProv = distritoProv;
	}

	public String getRegionProv() {
		return regionProv;
	}

	public void setRegionProv(String regionProv) {
		this.regionProv = regionProv;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getClaveBanco() {
		return claveBanco;
	}

	public void setClaveBanco(String claveBanco) {
		this.claveBanco = claveBanco;
	}

	public String getCuentaBank() {
		return cuentaBank;
	}

	public void setCuentaBank(String cuentaBank) {
		this.cuentaBank = cuentaBank;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	@Override
	public String toString() {
		return  "[Codigo:"+this.codigo+"][Mensaje:"+((this.mensaje != null)?this.mensaje:"null")+
				"][Rut:"+((this.rut != null)?this.rut:"null")+"](DIRECCION=>[Nombre prov:"+((this.nombreProv != null)?this.nombreProv:"null")+
				"][Direccion prov:"+((this.direccionProv != null)?this.direccionProv:"null")+"][Distrito prov:"+((this.distritoProv != null)?this.distritoProv:"null")+
				"][Region prov:"+((this.regionProv != null)?this.regionProv:"null")+"])(COMUNICACION=>[Telefono:"+((this.telefono != null)?this.telefono:"null")+"])"+
				"(BANCO=>[Clave banco:"+((this.claveBanco != null)?this.claveBanco:"null")+"][Cuenta banco:"+((this.cuentaBank != null)?this.cuentaBank:"null")+"][Titular:"+((this.titular != null)?this.titular:"null")+"])";
	}
}
