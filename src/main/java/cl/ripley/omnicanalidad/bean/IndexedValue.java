package cl.ripley.omnicanalidad.bean;

import java.util.function.Function;

public class IndexedValue<T> {

	private Long index;
	private T value;
	
	private IndexedValue(Long index, T value) {
		this.index = index;
		this.value = value;
	}
	
	public Long index() {
		return index;
	}
	
	public T value() {
		return value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"index\":\"").append(index).append("\",\"value\":\"").append(value).append("\"}");
		return builder.toString();
	}
	
	public static <T> Function<T, IndexedValue<T>> indexed() {
		return new Function<T, IndexedValue<T>>() {

			private Long index = 0L;
			
			@Override
			public IndexedValue<T> apply(T value) {
				return new IndexedValue<>(index++, value);
			}
			
		};
	}
	
}
