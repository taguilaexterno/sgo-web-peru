package cl.ripley.omnicanalidad.bean;

public class ReglaSku {
	private int id;
	private int idValidacion;
	private int idParam;
	private String descripcion;
	private boolean estado;
	private String metodo;
	private String valor;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdParam() {
		return idParam;
	}
	public void setIdParam(int idParam) {
		this.idParam = idParam;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public int getIdValidacion() {
		return idValidacion;
	}
	public void setIdValidacion(int idValidacion) {
		this.idValidacion = idValidacion;
	}
}
