package cl.ripley.omnicanalidad.bean;

public class GrupoMPSeguro {
	private int id;
	private String descripcion;
	private int idTipoValidacion;	
	private int estado;
	private int estadoFinalNv;
	private String metodo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIdTipoValidacion() {
		return idTipoValidacion;
	}
	public void setIdTipoValidacion(int idTipoValidacion) {
		this.idTipoValidacion = idTipoValidacion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getEstadoFinalNv() {
		return estadoFinalNv;
	}
	public void setEstadoFinalNv(int estadoFinalNv) {
		this.estadoFinalNv = estadoFinalNv;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
}
