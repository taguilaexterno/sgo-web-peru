package cl.ripley.omnicanalidad.bean;

public class ReporteModeloExtendidoDTO {
	
	private String numeroOrden;
	private String fechaBoleta;
	private String estado;
	private String ordenMKP;
	private String estadoMKP;
	private String moneda;
	private String monto;
	private String despacho;
	private String descuento;
	private String tipoDescuento;
	private String tipoDocumento;
	private String documento;
	private String longonID;
	private String sucursal;
	private String ejecutivoVenta;
	private String numeroItem;
	private String codigoItem;
	private String skuItem;
	private String descripcionItem;
	private String descripcionPMM;
	private String precioitem;
	private String cantidaditem;
	private String descuentoItem;
	private String tipoDescuentoItem;
	private String regaloItem;
	private String mensajeItem;
	private String referenciaDireccion;
	private String indicadorExtraItem;
	private String bodegaitem;
	private String sucursalReserva;
	private String tipoDespachoItem;
	private String fechaDespachoItem;
	private String dniItem;
	private String rutDespacho;
	private String nombreDespacho;
	private String dpto;
	private String provincia;
	private String distrito;
	private String direccionDespacho;
	private String telefonoDespacho1;
	private String telefonoDespacho2;
	private String tipoDocumentoDespacho;
	private String rutCliente;
	private String dptoFact;
	private String provFact;
	private String distFact;
	private String direccionCliente;
	private String nombreCliente;
	private String apePatCliente;
	private String apeMatCliente;
	private String emailCliente;
	private String codigoAutorizador;
	private String tipotarjeta;
	private String binNumber;
	private String numeroTrajeta;
	private String numeroCuotas;	
	private String valorCuota;
	private String vencimiento;
	private String certificacion;
	private String numeroOperacionBancaria;
	private String comprobante;
	private String ruc;
	private String razonSocial;
	private String kioskoVendedorMac;
	private String fechaTicket;
	private String numeroBoleta;
	private String ticket;
	private String caja;
	private String sucursalTiendaFisica;
	private String tipoOrdenCompra;
	private String tipoSubOrden;
	private String esNC;
	
	public String getTipoOrdenCompra() {
		return tipoOrdenCompra;
	}
	public void setTipoOrdenCompra(String tipoOrdenCompra) {
		this.tipoOrdenCompra = tipoOrdenCompra;
	}
	public String getTipoSubOrden() {
		return tipoSubOrden;
	}
	public void setTipoSubOrden(String tipoSubOrden) {
		this.tipoSubOrden = tipoSubOrden;
	}
	public String getNumeroOrden() {
		return numeroOrden;
	}
	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}
	public String getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(String fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getOrdenMKP() {
		return ordenMKP;
	}
	public void setOrdenMKP(String ordenMKP) {
		this.ordenMKP = ordenMKP;
	}
	public String getEstadoMKP() {
		return estadoMKP;
	}
	public void setEstadoMKP(String estadoMKP) {
		this.estadoMKP = estadoMKP;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getDespacho() {
		return despacho;
	}
	public void setDespacho(String despacho) {
		this.despacho = despacho;
	}
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	public String getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getLongonID() {
		return longonID;
	}
	public void setLongonID(String longonID) {
		this.longonID = longonID;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getEjecutivoVenta() {
		return ejecutivoVenta;
	}
	public void setEjecutivoVenta(String ejecutivoVenta) {
		this.ejecutivoVenta = ejecutivoVenta;
	}
	public String getNumeroItem() {
		return numeroItem;
	}
	public void setNumeroItem(String numeroItem) {
		this.numeroItem = numeroItem;
	}
	public String getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	public String getSkuItem() {
		return skuItem;
	}
	public void setSkuItem(String skuItem) {
		this.skuItem = skuItem;
	}
	public String getDescripcionItem() {
		return descripcionItem;
	}
	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}
	public String getDescripcionPMM() {
		return descripcionPMM;
	}
	public void setDescripcionPMM(String descripcionPMM) {
		this.descripcionPMM = descripcionPMM;
	}
	public String getPrecioitem() {
		return precioitem;
	}
	public void setPrecioitem(String precioitem) {
		this.precioitem = precioitem;
	}
	public String getCantidaditem() {
		return cantidaditem;
	}
	public void setCantidaditem(String cantidaditem) {
		this.cantidaditem = cantidaditem;
	}
	public String getDescuentoItem() {
		return descuentoItem;
	}
	public void setDescuentoItem(String descuentoItem) {
		this.descuentoItem = descuentoItem;
	}
	public String getTipoDescuentoItem() {
		return tipoDescuentoItem;
	}
	public void setTipoDescuentoItem(String tipoDescuentoItem) {
		this.tipoDescuentoItem = tipoDescuentoItem;
	}
	public String getRegaloItem() {
		return regaloItem;
	}
	public void setRegaloItem(String regaloItem) {
		this.regaloItem = regaloItem;
	}
	public String getMensajeItem() {
		return mensajeItem;
	}
	public void setMensajeItem(String mensajeItem) {
		this.mensajeItem = mensajeItem;
	}
	public String getReferenciaDireccion() {
		return referenciaDireccion;
	}
	public void setReferenciaDireccion(String referenciaDireccion) {
		this.referenciaDireccion = referenciaDireccion;
	}
	public String getIndicadorExtraItem() {
		return indicadorExtraItem;
	}
	public void setIndicadorExtraItem(String indicadorExtraItem) {
		this.indicadorExtraItem = indicadorExtraItem;
	}
	public String getBodegaitem() {
		return bodegaitem;
	}
	public void setBodegaitem(String bodegaitem) {
		this.bodegaitem = bodegaitem;
	}
	public String getSucursalReserva() {
		return sucursalReserva;
	}
	public void setSucursalReserva(String sucursalReserva) {
		this.sucursalReserva = sucursalReserva;
	}
	public String getTipoDespachoItem() {
		return tipoDespachoItem;
	}
	public void setTipoDespachoItem(String tipoDespachoItem) {
		this.tipoDespachoItem = tipoDespachoItem;
	}
	public String getFechaDespachoItem() {
		return fechaDespachoItem;
	}
	public void setFechaDespachoItem(String fechaDespachoItem) {
		this.fechaDespachoItem = fechaDespachoItem;
	}
	public String getDniItem() {
		return dniItem;
	}
	public void setDniItem(String dniItem) {
		this.dniItem = dniItem;
	}
	public String getRutDespacho() {
		return rutDespacho;
	}
	public void setRutDespacho(String rutDespacho) {
		this.rutDespacho = rutDespacho;
	}
	public String getNombreDespacho() {
		return nombreDespacho;
	}
	public void setNombreDespacho(String nombreDespacho) {
		this.nombreDespacho = nombreDespacho;
	}
	public String getDpto() {
		return dpto;
	}
	public void setDpto(String dpto) {
		this.dpto = dpto;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getDireccionDespacho() {
		return direccionDespacho;
	}
	public void setDireccionDespacho(String direccionDespacho) {
		this.direccionDespacho = direccionDespacho;
	}
	public String getTelefonoDespacho1() {
		return telefonoDespacho1;
	}
	public void setTelefonoDespacho1(String telefonoDespacho1) {
		this.telefonoDespacho1 = telefonoDespacho1;
	}
	public String getTelefonoDespacho2() {
		return telefonoDespacho2;
	}
	public void setTelefonoDespacho2(String telefonoDespacho2) {
		this.telefonoDespacho2 = telefonoDespacho2;
	}
	public String getTipoDocumentoDespacho() {
		return tipoDocumentoDespacho;
	}
	public void setTipoDocumentoDespacho(String tipoDocumentoDespacho) {
		this.tipoDocumentoDespacho = tipoDocumentoDespacho;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getDptoFact() {
		return dptoFact;
	}
	public void setDptoFact(String dptoFact) {
		this.dptoFact = dptoFact;
	}
	public String getProvFact() {
		return provFact;
	}
	public void setProvFact(String provFact) {
		this.provFact = provFact;
	}
	public String getDistFact() {
		return distFact;
	}
	public void setDistFact(String distFact) {
		this.distFact = distFact;
	}
	public String getDireccionCliente() {
		return direccionCliente;
	}
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getApePatCliente() {
		return apePatCliente;
	}
	public void setApePatCliente(String apePatCliente) {
		this.apePatCliente = apePatCliente;
	}
	public String getApeMatCliente() {
		return apeMatCliente;
	}
	public void setApeMatCliente(String apeMatCliente) {
		this.apeMatCliente = apeMatCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public String getCodigoAutorizador() {
		return codigoAutorizador;
	}
	public void setCodigoAutorizador(String codigoAutorizador) {
		this.codigoAutorizador = codigoAutorizador;
	}
	public String getTipotarjeta() {
		return tipotarjeta;
	}
	public void setTipotarjeta(String tipotarjeta) {
		this.tipotarjeta = tipotarjeta;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public String getNumeroTrajeta() {
		return numeroTrajeta;
	}
	public void setNumeroTrajeta(String numeroTrajeta) {
		this.numeroTrajeta = numeroTrajeta;
	}
	public String getNumeroCuotas() {
		return numeroCuotas;
	}
	public void setNumeroCuotas(String numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}
	public String getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	public String getCertificacion() {
		return certificacion;
	}
	public void setCertificacion(String certificacion) {
		this.certificacion = certificacion;
	}
	public String getNumeroOperacionBancaria() {
		return numeroOperacionBancaria;
	}
	public void setNumeroOperacionBancaria(String numeroOperacionBancaria) {
		this.numeroOperacionBancaria = numeroOperacionBancaria;
	}
	public String getComprobante() {
		return comprobante;
	}
	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getKioskoVendedorMac() {
		return kioskoVendedorMac;
	}
	public void setKioskoVendedorMac(String kioskoVendedorMac) {
		this.kioskoVendedorMac = kioskoVendedorMac;
	}
	public String getFechaTicket() {
		return fechaTicket;
	}
	public void setFechaTicket(String fechaTicket) {
		this.fechaTicket = fechaTicket;
	}
	public String getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(String numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String getSucursalTiendaFisica() {
		return sucursalTiendaFisica;
	}
	public void setSucursalTiendaFisica(String sucursalTiendaFisica) {
		this.sucursalTiendaFisica = sucursalTiendaFisica;
	}
	public String getEsNC() {
		return esNC;
	}
	public void setEsNC(String esNC) {
		this.esNC = esNC;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"numeroOrden\":\"").append(numeroOrden).append("\",\"fechaBoleta\":\"").append(fechaBoleta)
				.append("\",\"estado\":\"").append(estado).append("\",\"ordenMKP\":\"").append(ordenMKP)
				.append("\",\"estadoMKP\":\"").append(estadoMKP).append("\",\"moneda\":\"").append(moneda)
				.append("\",\"monto\":\"").append(monto).append("\",\"despacho\":\"").append(despacho)
				.append("\",\"descuento\":\"").append(descuento).append("\",\"tipoDescuento\":\"").append(tipoDescuento)
				.append("\",\"tipoDocumento\":\"").append(tipoDocumento).append("\",\"documento\":\"").append(documento)
				.append("\",\"longonID\":\"").append(longonID).append("\",\"sucursal\":\"").append(sucursal)
				.append("\",\"ejecutivoVenta\":\"").append(ejecutivoVenta).append("\",\"numeroItem\":\"")
				.append(numeroItem).append("\",\"codigoItem\":\"").append(codigoItem).append("\",\"skuItem\":\"")
				.append(skuItem).append("\",\"descripcionItem\":\"").append(descripcionItem)
				.append("\",\"descripcionPMM\":\"").append(descripcionPMM).append("\",\"precioitem\":\"")
				.append(precioitem).append("\",\"cantidaditem\":\"").append(cantidaditem)
				.append("\",\"descuentoItem\":\"").append(descuentoItem).append("\",\"tipoDescuentoItem\":\"")
				.append(tipoDescuentoItem).append("\",\"regaloItem\":\"").append(regaloItem)
				.append("\",\"mensajeItem\":\"").append(mensajeItem).append("\",\"referenciaDireccion\":\"")
				.append(referenciaDireccion).append("\",\"indicadorExtraItem\":\"").append(indicadorExtraItem)
				.append("\",\"bodegaitem\":\"").append(bodegaitem).append("\",\"sucursalReserva\":\"")
				.append(sucursalReserva).append("\",\"tipoDespachoItem\":\"").append(tipoDespachoItem)
				.append("\",\"fechaDespachoItem\":\"").append(fechaDespachoItem).append("\",\"dniItem\":\"")
				.append(dniItem).append("\",\"rutDespacho\":\"").append(rutDespacho).append("\",\"nombreDespacho\":\"")
				.append(nombreDespacho).append("\",\"dpto\":\"").append(dpto).append("\",\"provincia\":\"")
				.append(provincia).append("\",\"distrito\":\"").append(distrito).append("\",\"direccionDespacho\":\"")
				.append(direccionDespacho).append("\",\"telefonoDespacho1\":\"").append(telefonoDespacho1)
				.append("\",\"telefonoDespacho2\":\"").append(telefonoDespacho2)
				.append("\",\"tipoDocumentoDespacho\":\"").append(tipoDocumentoDespacho).append("\",\"rutCliente\":\"")
				.append(rutCliente).append("\",\"dptoFact\":\"").append(dptoFact).append("\",\"provFact\":\"")
				.append(provFact).append("\",\"distFact\":\"").append(distFact).append("\",\"direccionCliente\":\"")
				.append(direccionCliente).append("\",\"nombreCliente\":\"").append(nombreCliente)
				.append("\",\"apePatCliente\":\"").append(apePatCliente).append("\",\"apeMatCliente\":\"")
				.append(apeMatCliente).append("\",\"emailCliente\":\"").append(emailCliente)
				.append("\",\"codigoAutorizador\":\"").append(codigoAutorizador).append("\",\"tipotarjeta\":\"")
				.append(tipotarjeta).append("\",\"binNumber\":\"").append(binNumber).append("\",\"numeroTrajeta\":\"")
				.append(numeroTrajeta).append("\",\"numeroCuotas\":\"").append(numeroCuotas)
				.append("\",\"valorCuota\":\"").append(valorCuota).append("\",\"vencimiento\":\"").append(vencimiento)
				.append("\",\"certificacion\":\"").append(certificacion).append("\",\"numeroOperacionBancaria\":\"")
				.append(numeroOperacionBancaria).append("\",\"comprobante\":\"").append(comprobante)
				.append("\",\"ruc\":\"").append(ruc).append("\",\"razonSocial\":\"").append(razonSocial)
				.append("\",\"kioskoVendedorMac\":\"").append(kioskoVendedorMac).append("\",\"fechaTicket\":\"")
				.append(fechaTicket).append("\",\"numeroBoleta\":\"").append(numeroBoleta).append("\",\"ticket\":\"")
				.append(ticket).append("\",\"caja\":\"").append(caja).append("\",\"sucursalTiendaFisica\":\"")
				.append(sucursalTiendaFisica).append("\",\"tipoOrdenCompra\":\"").append(tipoOrdenCompra)
				.append("\",\"tipoSubOrden\":\"").append(tipoSubOrden).append("\",\"esNC\":\"").append(esNC)
				.append("\"}");
		return builder.toString();
	}
	
	
	
	
}
