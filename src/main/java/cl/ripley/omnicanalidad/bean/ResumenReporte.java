package cl.ripley.omnicanalidad.bean;

public class ResumenReporte {
	
	private String caja;
	private String sucursal;
	private String fechaApertura;
	private String fechaCierre;
	private Long idAperturaCierre;
	
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(String fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	
	@Override
	public String toString() {
		return "ResumenReporte [caja=" + caja + ", sucursal="+sucursal+", fechaApertura=" + fechaApertura + ", fechaCierre=" + fechaCierre
				+ ", idAperturaCierre=" + idAperturaCierre + "]";
	}
	
	

}
