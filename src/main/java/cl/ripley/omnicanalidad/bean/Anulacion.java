/**
 * 
 */
package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author NTB-109
 *
 */
public class Anulacion implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6558421669838405379L;
	/**
	 * 
	 */

	
	private long codigoComercio;
	private long folioNc;
	private String rutComprador = null;
	private long numBoleta;
	private long numNotaCredito;
	private String ordenCompra = null;
	private BigDecimal montoAutorizado = null;
	private String codigoAutorizacion = null;
	private BigDecimal montoAnulado = null;
	private String codigoError = null;
	private String descripcionError = null;
	private int estado;
	private Date fechaAnualacion=null;
	private Date fechaCompra=null;
	private int intento;
	
	
	
	public int getIntento() {
		return intento;
	}
	public void setIntento(int intento) {
		this.intento = intento;
	}
	public Date getFechaAnualacion() {
		return fechaAnualacion;
	}
	public void setFechaAnualacion(Date fechaAnualacion) {
		this.fechaAnualacion = fechaAnualacion;
	}
	public long getCodigoComercio() {
		return codigoComercio;
	}
	public void setCodigoComercio(long codigoComercio) {
		this.codigoComercio = codigoComercio;
	}
	public String getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public BigDecimal getMontoAutorizado() {
		return montoAutorizado;
	}
	public void setMontoAutorizado(BigDecimal montoAutorizado) {
		this.montoAutorizado = montoAutorizado;
	}
	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public BigDecimal getMontoAnulado() {
		return montoAnulado;
	}
	public void setMontoAnulado(BigDecimal montoAnulado) {
		this.montoAnulado = montoAnulado;
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public long getNumBoleta() {
		return numBoleta;
	}
	public void setNumBoleta(long numBoleta) {
		this.numBoleta = numBoleta;
	}
	public long getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(long numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public long getFolioNc() {
		return folioNc;
	}
	public void setFolioNc(long folioNc) {
		this.folioNc = folioNc;
	}
	public String getRutComprador() {
		return rutComprador;
	}
	public void setRutComprador(String rutComprador) {
		this.rutComprador = rutComprador;
	}
	public Date getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	
	

}
