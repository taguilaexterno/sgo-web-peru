package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.util.Date;

/**Clase modelo con informacion de permiso por {@linkplain Modulo}
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 05-08-2016
 * Versiones:
 * <ul>
 *  <li>05-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public class Permiso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombrePermiso;
	private Date fecCrea;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombrePermiso() {
		return nombrePermiso;
	}
	public void setNombrePermiso(String nombrePermiso) {
		this.nombrePermiso = nombrePermiso;
	}
	public Date getFecCrea() {
		return fecCrea;
	}
	public void setFecCrea(Date fecCrea) {
		this.fecCrea = fecCrea;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Permiso [id=").append(id).append(", nombrePermiso=").append(nombrePermiso).append(", fecCrea=")
				.append(fecCrea).append("]");
		return builder.toString();
	}
	
	
	
}
