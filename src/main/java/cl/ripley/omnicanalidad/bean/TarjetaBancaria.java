package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class TarjetaBancaria {
	private Long	correlativoVenta;
	private String	tipoTarjeta;
	private String	codigoAutorizador;
	private BigDecimal	montoTarjeta;
	private Integer	optcounter;
	private String	vd;
	private String	medioAcceso;
	private String	oneclickBuyorder;
	private Integer codigoConvenio;
	private String descripcionFormaPago;
	private String glosa;
	private String binNumber;
	private String ultimosDigitos;
	private String descripcionCanal;
	private Long identificadorTransaccion;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getCodigoAutorizador() {
		return codigoAutorizador;
	}
	public void setCodigoAutorizador(String codigoAutorizador) {
		this.codigoAutorizador = codigoAutorizador;
	}
	public BigDecimal getMontoTarjeta() {
		return montoTarjeta;
	}
	public void setMontoTarjeta(BigDecimal montoTarjeta) {
		this.montoTarjeta = montoTarjeta;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}
	public String getVd() {
		return vd;
	}
	public void setVd(String vd) {
		this.vd = vd;
	}
	public String getMedioAcceso() {
		return medioAcceso;
	}
	public void setMedioAcceso(String medioAcceso) {
		this.medioAcceso = medioAcceso;
	}
	public String getOneclickBuyorder() {
		return oneclickBuyorder;
	}
	public void setOneclickBuyorder(String oneclickBuyorder) {
		this.oneclickBuyorder = oneclickBuyorder;
	}
	public Integer getCodigoConvenio() {
		return codigoConvenio;
	}
	public void setCodigoConvenio(Integer codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}
	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}
	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public String getUltimosDigitos() {
		return ultimosDigitos;
	}
	public void setUltimosDigitos(String ultimosDigitos) {
		this.ultimosDigitos = ultimosDigitos;
	}
	public String getDescripcionCanal() {
		return descripcionCanal;
	}
	public void setDescripcionCanal(String descripcionCanal) {
		this.descripcionCanal = descripcionCanal;
	}
	public Long getIdentificadorTransaccion() {
		return identificadorTransaccion;
	}
	public void setIdentificadorTransaccion(Long identificadorTransaccion) {
		this.identificadorTransaccion = identificadorTransaccion;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"correlativoVenta\":\"").append(correlativoVenta).append("\",\"tipoTarjeta\":\"")
				.append(tipoTarjeta).append("\",\"codigoAutorizador\":\"").append(codigoAutorizador)
				.append("\",\"montoTarjeta\":\"").append(montoTarjeta).append("\",\"optcounter\":\"").append(optcounter)
				.append("\",\"vd\":\"").append(vd).append("\",\"medioAcceso\":\"").append(medioAcceso)
				.append("\",\"oneclickBuyorder\":\"").append(oneclickBuyorder).append("\",\"codigoConvenio\":\"")
				.append(codigoConvenio).append("\",\"descripcionFormaPago\":\"").append(descripcionFormaPago)
				.append("\",\"glosa\":\"").append(glosa).append("\",\"binNumber\":\"").append(binNumber)
				.append("\",\"ultimosDigitos\":\"").append(ultimosDigitos).append("\",\"descripcionCanal\":\"")
				.append(descripcionCanal).append("\",\"identificadorTransaccion\":\"").append(identificadorTransaccion)
				.append("\"}");
		return builder.toString();
	}

}
