package cl.ripley.omnicanalidad.bean;

public class ReglasMPSeguro {
	
	private String descGrupo;
	private int id;
	private String descripcion;
	private int estado;
	private String metodo;
	//Valor menor
	private int idValidacion;
	private String valor;
	
	public String getDescGrupo() {
		return descGrupo;
	}
	public void setDescGrupo(String descGrupo) {
		this.descGrupo = descGrupo;
	}
	public int getIdValidacion() {
		return idValidacion;
	}
	public void setIdValidacion(int idValidacion) {
		this.idValidacion = idValidacion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
}
