package cl.ripley.omnicanalidad.bean;

public class ArticuloDetalleVoucherPDF {

	private String codigo;
	private String descripcion;
	private String cantidad;
	private String precio;
	private String total;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"codigo\":\"").append(codigo).append("\",\"descripcion\":\"").append(descripcion)
				.append("\",\"cantidad\":\"").append(cantidad).append("\",\"precio\":\"").append(precio)
				.append("\",\"total\":\"").append(total).append("\"}");
		return builder.toString();
	}
	
	
}
