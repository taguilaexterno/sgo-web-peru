package cl.ripley.omnicanalidad.bean;

public class InfoSubOrdenIdRefund {
	
	private String subOrden;
	private Integer idRefund;
	
	public InfoSubOrdenIdRefund() {
		
	}
	
	public InfoSubOrdenIdRefund(String subOrden, Integer idRefund) {
		this.subOrden = subOrden;
		this.idRefund = idRefund;
	}

	public String getSubOrden() {
		return subOrden;
	}

	public void setSubOrden(String subOrden) {
		this.subOrden = subOrden;
	}

	public Integer getIdRefund() {
		return idRefund;
	}

	public void setIdRefund(Integer idRefund) {
		this.idRefund = idRefund;
	}

	@Override
	public String toString() {
		return "InfoSubOrdenIdRefund [subOrden=" + subOrden + ", idRefund=" + idRefund + "]";
	}
	
}
