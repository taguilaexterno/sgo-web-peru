package cl.ripley.omnicanalidad.bean;

public class ClienteClub {
	private String rut;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	@Override
	public String toString(){
		String salida="";
		salida = salida + "Rut:["+this.getRut()+"]";
		salida = salida + "Nombre:["+this.getNombre()+"]";
		salida = salida + "Apellido Paterno:["+this.getApellidoPaterno()+"]";
		salida = salida + "Apellido Materno:["+this.getApellidoMaterno()+"]";

		return salida;
	}
}
