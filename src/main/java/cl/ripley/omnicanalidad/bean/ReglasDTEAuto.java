package cl.ripley.omnicanalidad.bean;

public class ReglasDTEAuto {
	
	private ReglaPrecio reglaPrecio;
	private ReglaSku reglaSku;
	private ReglaTodoManual reglaTodoManual;
	private String metodo;
	
	public ReglaPrecio getReglaPrecio() {
		return reglaPrecio;
	}
	public void setReglaPrecio(ReglaPrecio reglaPrecio) {
		this.reglaPrecio = reglaPrecio;
	}
	public ReglaSku getReglaSku() {
		return reglaSku;
	}
	public void setReglaSku(ReglaSku reglaSku) {
		this.reglaSku = reglaSku;
	}
	public ReglaTodoManual getReglaTodoManual() {
		return reglaTodoManual;
	}
	public void setReglaTodoManual(ReglaTodoManual reglaTodoManual) {
		this.reglaTodoManual = reglaTodoManual;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
}
