package cl.ripley.omnicanalidad.bean;

import java.util.List;

public class DetalleOC {
	private DetalleCliente datosCliente;
	private DetalleBackoffice datosBackoffice;
	private List<DetalleDespacho> datosDespacho;
	private List<DetalleCompra> datosCompra;
	
	public DetalleBackoffice getDatosBackoffice() {
		return datosBackoffice;
	}
	public void setDatosBackoffice(DetalleBackoffice datosBackoffice) {
		this.datosBackoffice = datosBackoffice;
	}
	public DetalleCliente getDatosCliente() {
		return datosCliente;
	}
	public void setDatosCliente(DetalleCliente datosCliente) {
		this.datosCliente = datosCliente;
	}
	public List<DetalleDespacho> getDatosDespacho() {
		return datosDespacho;
	}
	public void setDatosDespacho(List<DetalleDespacho> datosDespacho) {
		this.datosDespacho = datosDespacho;
	}
	public List<DetalleCompra> getDatosCompra() {
		return datosCompra;
	}
	public void setDatosCompra(List<DetalleCompra> datosCompra) {
		this.datosCompra = datosCompra;
	}

}
