package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.Permiso;

/**DAO para conectar con datos de Permisos de los modulos.
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 10-08-2016
 * Versiones:
 * <ul>
 *  <li>10-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public interface PermisosDAO {

	/**Obtiene los permisos del sistema para los modulos.
	 * Versiones:
	 * <ul>
	 * <li>10-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	List<Permiso> getPermisos();
}
