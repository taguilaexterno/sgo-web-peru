package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ContadorPannel;
import cl.ripley.omnicanalidad.dao.PannelCounterDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

@Repository
public class PannelCounterDAOImpl implements PannelCounterDAO{

	
	private static final AriLog logger = new AriLog(PannelCounterDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	public PannelCounterDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContadorPannel cuentaOrdenesPendientes(int sucursal) {

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		ContadorPannel contadores =  new ContadorPannel();
		
		try {
			conn = poolBD.getConnection();
			cs = conn.prepareCall("{call CAVIRA_MANEJO_BOLETAS.PROC_OBT_ORDENES_PANEL(?,?,?,?,?,?,?,?)}");
			cs.setInt(1, sucursal);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.execute();
			
			contadores.setMedioPago(cs.getInt(2));
			contadores.setOrdenesManuales(cs.getInt(3));
			contadores.setRechazoStockTotal(cs.getInt(4));
			contadores.setRechazoStockParcial(cs.getInt(5));
			contadores.setSinStock(cs.getInt(6));
			contadores.setValidacionPendienteStock(cs.getInt(7));
			
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor Medio Pago " + contadores.getMedioPago());
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor Manuales " + contadores.getOrdenesManuales());
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor rechazo Stock Total " + contadores.getRechazoStockTotal());
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor rechazo Stock Parcial " + contadores.getRechazoStockParcial());
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor Sin Stock " + contadores.getSinStock());
			logger.traceInfo("cuentaOrdenesPendientes : ", "Valor Pendientes " + contadores.getValidacionPendienteStock());
			
			
			
			int estado = cs.getInt(8);
			
			
			
		}catch(SQLException e){
			logger.traceError("getModulos Exception", e);
			
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		return contadores;
	}

}
