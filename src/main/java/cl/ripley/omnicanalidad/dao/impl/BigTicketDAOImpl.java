package cl.ripley.omnicanalidad.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;
import cl.ripley.omnicanalidad.dao.BigTicketDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import cl.ripley.omnicanalidad.util.Validaciones;

@Repository
public class BigTicketDAOImpl implements BigTicketDAO {
	private static final AriLog logger = new AriLog(BigTicketDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<ArticulosVentaOC> getArtMarcadosConDespacho(List<ArticulosVentaOC> articulos) {
		
		logger.initTrace("getArtMarcadosConDespacho", "Ingreso a metodo articulos: "+articulos.size());
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
				
		try {
			conn = poolBD.getConnectionBT();
			String sql = "SELECT  count(1) FROM bigt_despachos d, bigt_productos p, bigt_estado_despacho e Where d.codigo_vta = p.codigo_vta "
					+ "and d.cud = ? and d.estado_despacho = e.estado_despacho "
					+ "and d.estado_despacho in (select estado from bigt_ref_cam_estado where new_estado = '9')";
			
			System.out.println(sql);
			ps = conn.prepareStatement(sql);
			Iterator itr = articulos.iterator();
			while(itr.hasNext()) {
				ArticulosVentaOC obj = (ArticulosVentaOC)itr.next();
				ps.setString(1, obj.getArticuloVenta().getCodDespacho());		
				if (ps != null) {
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						if(rs.getInt(1) > 0 || Validaciones.validarVacio(obj.getArticuloVenta().getCodDespacho())){
							obj.getArticuloVenta().setEnTransito(Constantes.NRO_CERO);
						}else{
							obj.getArticuloVenta().setEnTransito(Constantes.NRO_UNO);
						}
					}
				}
		    }
			
		} catch(Exception e) {
			logger.traceError("getArtMarcadosConDespacho", "Error en getArtMarcadosConDespacho: ", e);
		} finally {
			poolBD.closeConnection(conn);
			poolBD.closeConnection(conn, rs, ps);
		}
		logger.endTrace("getArtMarcadosConDespacho", "Finalizado", "articulos: "+articulos.size());
		return articulos;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<ListaAgrupaSubOrdenes> getArtMarcadosConDespachoSubOrdenes(
			List<ListaAgrupaSubOrdenes> articulosConOrdenes) {

		logger.initTrace("getArtMarcadosConDespachoSubOrdenes", "Ingreso a metodo articulosConOrdenes: "+articulosConOrdenes.size());
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		try {
			conn = poolBD.getConnectionBT();
			String sql = "SELECT count(1) FROM bigt_despachos d, bigt_productos p, bigt_estado_despacho e Where d.codigo_vta = p.codigo_vta "
					+ "and d.cud = ? and d.estado_despacho = e.estado_despacho "
					+ "and d.estado_despacho in (select estado from bigt_ref_cam_estado where new_estado = '9')";
			ps = conn.prepareStatement(sql);
			Iterator itr = articulosConOrdenes.iterator();
			while(itr.hasNext()) {
				ListaAgrupaSubOrdenes obj = (ListaAgrupaSubOrdenes)itr.next();
				Iterator itr2 = obj.getArticulos().iterator();
				while(itr2.hasNext()) {
					ArticulosVentaOC obj2 = (ArticulosVentaOC)itr2.next();
					ps.setString(1, obj2.getArticuloVenta().getCodDespacho());		
					if (ps != null) {
						rs = ps.executeQuery();
						if (rs != null && rs.next()) {
							if(rs.getInt(1) > 0 || Validaciones.validarVacioOCero(obj2.getArticuloVenta().getCodDespacho())){
								obj2.getArticuloVenta().setEnTransito(Constantes.NRO_CERO);
							}else{
								obj2.getArticuloVenta().setEnTransito(Constantes.NRO_UNO);
							}
						}
					}
				}
		    }
			
		} catch(Exception e) {
			logger.traceError("getArtMarcadosConDespachoSubOrdenes", "Error en getArtMarcadosConDespachoSubOrdenes: ", e);
		} finally {
			poolBD.closeConnection(conn);
			poolBD.closeConnection(conn, rs, ps);
		}
		logger.endTrace("getArtMarcadosConDespachoSubOrdenes", "Finalizado", "articulosConOrdenes: "+articulosConOrdenes.size());

		return articulosConOrdenes;
	}

}
