package cl.ripley.omnicanalidad.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CajaSucursal;
import cl.ripley.omnicanalidad.dao.ICorreccionDatosDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@Repository
public class CorreccionDatosDAO implements ICorreccionDatosDAO {
	
	private static final AriLog logger = new AriLog(CorreccionDatosDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Override
	public void ejecutarUpdate(List<CajaSucursal> cajaSuc) {
		logger.initTrace("ejecutarUpdate", "List<Long> correlativos", new KeyLog("CORRELATIVOS", String.valueOf(cajaSuc)));
		
		String sucParam=parametroDao.getParametro("SUCURSAL").getValor();
		List<Integer> results=new ArrayList<Integer>();
		String sqlTV = "UPDATE NOTA_VENTA "
				+ "SET ESTADO = 1, NUMERO_CAJA= 9999 "
				+ "WHERE CORRELATIVO_VENTA = ?";
		String sqlRpos = "UPDATE NOTA_VENTA "
				+ "SET ESTADO = 1 "
				+ "WHERE CORRELATIVO_VENTA = ?";
		
		for(CajaSucursal cajaSucOC : cajaSuc) {
			if(cajaSucOC.getSucursal() != Integer.parseInt(sucParam)) {
				results.add(jdbcTemplate.update(sqlRpos, cajaSucOC.getOc()));
			}else {
				results.add(jdbcTemplate.update(sqlTV, cajaSucOC.getOc()));
			}
		}
//		List<Object[]> batchArgs =	correlativos.stream()
//									.map(it -> new Object[] {it})
//									.collect(Collectors.toCollection(ArrayList::new));
//		
//		int[] results = jdbcTemplate.batchUpdate(sql, batchArgs);
		
		logger.endTrace("ejecutarUpdate", "Finalizado", String.valueOf(results));

	}

	@Override
	public List<Map<String,Object>> ejecutarSelect() {
		
		return jdbcTemplate.queryForList("SELECT NV.CORRELATIVO_VENTA, " + 
				"NV.NUMERO_SUCURSAL, " + 
				"NV.NUMERO_CAJA, " +
				"NV.ORIGEN_VTA, " + 
				"TRP.GET_OC_TV, " +
				"TRP.INSERTA_OC, " + 
				"TRP.VALIDA_OC, " +
                "TRP.IMPR_OC, " +
                "TRP.GENERA_JSON, "+
				"TIM.PPL ," +
				"TIM.BO ," +
				"TIM.BT ," +
				"TIM.BCV ," +
				"TIM.MKP ," +
				"NV.FECHA_CREACION, " + 
				"NV.FECHA_HORA_CREACION, " + 
				"NV.ESTADO, " + 
				"NV.RUT_COMPRADOR, " + 
				"NV.NUMERO_BOLETA, " + 
				"NV.HORA_BOLETA, " + 
				"NV.RUT_BOLETA, " + 
				"NV.EJECUTIVO_VTA, " + 
				"NV.TIPO_DOC, " + 
				"NV.FOLIO_SII, " + 
				"NV.URL_DOCE, " + 
				"NV.TIPO_DOCUMENTO" + 
				" FROM NOTA_VENTA NV " + 
				" LEFT JOIN CV_TRACE_RPOS TRP ON TRP.CORRELATIVO_VENTA = NV.CORRELATIVO_VENTA " + 
				" LEFT JOIN CV_TRACE_IMPR TIM ON TIM.CORRELATIVO_VENTA = NV.CORRELATIVO_VENTA " + 
				" WHERE ESTADO = 29");
		
	}

	@Override
	public void ejecutarUpdateConsulta(String consulta) {
		jdbcTemplate.update(consulta);
		
	}

	@Override
	public List<Map<String, Object>> ejecutarSelectConsulta(String consulta) {
		return jdbcTemplate.queryForList(consulta);
	}

}
