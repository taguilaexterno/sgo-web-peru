package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Permiso;
import cl.ripley.omnicanalidad.dao.ModulosDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementacion de DAO para obetener data de modulos desde la base de datos.
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 09-08-2016
 * Versiones:
 * <ul>
 *  <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Repository
public class ModulosDAOImpl implements ModulosDAO {

	private static final AriLog logger = new AriLog(ModulosDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private PoolBD poolBD;
	
	@Override
	public List<Modulo> getModulos() {
		logger.initTrace("getModulos", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		List<Modulo> modulos = new ArrayList<Modulo>(20);

		try {
			conn = poolBD.getConnection();

			cs = conn.prepareCall("{call SGO_MODULOS.PROC_GET_MODULOS(?,?,?,?)}");
			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();

			String query = cs.getString(1);
			BigDecimal codigo = cs.getBigDecimal(3);
			String mensaje = cs.getString(4);

			logger.traceInfo("getModulos", "query: " + query);
			logger.traceInfo("getModulos", "codigo: " + codigo);
			logger.traceInfo("getModulos", "mensaje: " + mensaje);

			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}

			rs = (ResultSet) cs.getObject(2);

			while(rs != null && rs.next()) {
				Modulo mod = new Modulo();
				mod.setFecCrea(rs.getDate("FEC_CREACION"));
				mod.setId(rs.getLong("ID"));
				mod.setNombreModulo(rs.getString("NOMBRE_MODULO"));
				mod.setUrl(rs.getString("URL"));
				modulos.add(mod);
			}

		} catch(Exception e) {
			logger.traceError("getModulos Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getModulos", "Finalizado", "Tama�o lista: " + modulos.size());

		return modulos;
	}

	@Override
	public List<Modulo> getModulosByPerfil(Long codPerfil) {
		logger.initTrace("getModulosByPerfil", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		List<Modulo> modulos = new ArrayList<Modulo>(20);

		try {
			conn = poolBD.getConnection();

			cs = conn.prepareCall("{call SGO_MODULOS.PROC_GET_MODS_X_PERFIL(?,?,?,?,?)}");
			cs.setLong(1, codPerfil);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();

			String query = cs.getString(2);
			BigDecimal codigo = cs.getBigDecimal(4);
			String mensaje = cs.getString(5);

			logger.traceInfo("getModulosByPerfil", "query: " + query);
			logger.traceInfo("getModulosByPerfil", "codigo: " + codigo);
			logger.traceInfo("getModulosByPerfil", "mensaje: " + mensaje);

			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}

			rs = (ResultSet) cs.getObject(3);

			while(rs != null && rs.next()) {
				Modulo mod = new Modulo();
				mod.setFecCrea(rs.getDate("FEC_CREA_MOD"));
				mod.setId(rs.getLong("ID_MOD"));
				mod.setNombreModulo(rs.getString("NOMBRE_MODULO"));
				mod.setUrl(rs.getString("URL"));
				
				Permiso perm = new Permiso();
				perm.setFecCrea(rs.getDate("FEC_CREA_PERMISO"));
				perm.setId(rs.getLong("ID_PERMISO"));
				perm.setNombrePermiso(rs.getString("NOMBRE_PERMISO"));
				
				mod.setPermiso(perm);
				
				modulos.add(mod);
			}

		} catch(Exception e) {
			logger.traceError("getModulosByPerfil Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getModulosByPerfil", "Finalizado", "Tama�o lista: " + modulos.size());

		return modulos;
	}

	@Override
	public boolean updPermisosModulosByPerfil(Long codPerfil, Long codModulo, Long codPermiso) {
		logger.initTrace("updPermisosModulosByPerfil", "Ingreso a metodo; codPerfil=" + codPerfil + "; codModulo=" + codModulo + "; codPermiso=" + codPermiso);

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		boolean result = Boolean.TRUE;

		try {
			conn = poolBD.getConnection();

			cs = conn.prepareCall("{call SGO_MODULOS.PROC_UPD_PERMISOS_MOD_X_PERFIL(?,?,?,?,?,?)}");
			cs.setLong(1, codPerfil);
			cs.setLong(2, codModulo);
			cs.setLong(3, codPermiso);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();

			String query = cs.getString(4);
			BigDecimal codigo = cs.getBigDecimal(5);
			String mensaje = cs.getString(6);

			logger.traceInfo("updPermisosModulosByPerfil", "query: " + query);
			logger.traceInfo("updPermisosModulosByPerfil", "codigo: " + codigo);
			logger.traceInfo("updPermisosModulosByPerfil", "mensaje: " + mensaje);

			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}


		} catch(Exception e) {
			logger.traceError("updPermisosModulosByPerfil Exception", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("updPermisosModulosByPerfil", "Finalizado", "codPerfil=" + codPerfil + "; codModulo=" + codModulo + "; codPermiso=" + codPermiso);

		return result;
	}

}
