package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ProductoReservaDTE;
import cl.ripley.omnicanalidad.bean.ReservaDTE;
import cl.ripley.omnicanalidad.bean.ReservasUtil;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.dao.ReservasDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;
@Repository
public class ReservasImpl implements ReservasDAO{
	private static final AriLog logger = new AriLog(ReservasImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public List<ReservaDTE> listarReservas(String numeroReserva,
			String rutCliente, String estadoReserva) {
		
		logger.initTrace("listarReservas",  "String numeroReserva: [" + numeroReserva + "] String rutCliente: ["
				+ rutCliente + "] String estadoReserva: [" + estadoReserva + "]");
		
		List<ReservaDTE> reservas = new ArrayList<ReservaDTE>();
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_RESERVAS.PROC_GET_RESERVAS(?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, ReservasUtil.StringToLong(numeroReserva));
			cs.setLong(2, ReservasUtil.StringToLong(rutCliente));
			cs.setInt(3, ReservasUtil.StringToInt(estadoReserva));
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.execute();    

			String consulta = cs.getString(4);
			Integer codigoError = cs.getInt(6);
			String mensajeError = cs.getString(7);
			
			logger.traceInfo("listarReservas", "query: " + consulta);
			logger.traceInfo("listarReservas", "codigo: " + codigoError);
			logger.traceInfo("listarReservas", "mensaje: " + mensajeError);

			if(codigoError.intValue() != 0) {
				reservas = null;
			}else{
				rs = (ResultSet) cs.getObject(5);
				
				while (null != rs && rs.next()){
					
					ReservaDTE reserva = new ReservaDTE();
					reserva.setOrdenCompra(rs.getLong("CORRELATIVO_VENTA"));
					reserva.setEstado(rs.getString("ESTADO"));
					reserva.setNombreCliente(rs.getString("CLIENTE"));
					reserva.setFechaCreacion(ReservasUtil.StringToDate(rs.getString("FECHA_CREACION")));
					reserva.setFechaVigencia(rs.getString("FECHA_VIGENCIA"));
					
					List<ProductoReservaDTE> productos = new ArrayList<ProductoReservaDTE>();
					
					ProductoReservaDTE producto = new ProductoReservaDTE();
					producto.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
					producto.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
					producto.setMonto(BigDecimal.valueOf(rs.getLong("PRECIO")));
					producto.setUnidades(rs.getInt("UNIDADES"));
					producto.setSku(rs.getString("COD_ARTICULO"));
					producto.setNombreProducto(rs.getString("DESC_RIPLEY"));

					productos.add(producto);
					reserva.setProductos(productos);
					
					reservas.add(reserva);
				}
			}
			
			reservas = ReservasUtil.OrganizaReservas(reservas);

		} catch (Exception e) {
			logger.traceError("listarReservas Exception", "Error durante la ejecuci�n de listarReservas", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("listarReservas", "Finalizado", "");
		return reservas;
			
	}

	@Override
	public Integer cancelarReserva(Long numeroReserva, Usuario usuario) {
		logger.initTrace("cancelarReserva",  "Long numeroReserva: [" + numeroReserva + "]" + "Usuario usuario: " + usuario.toString());
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Integer respuesta = 0;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_RESERVAS.PROC_CANCELAR_RESERVA(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, numeroReserva);
			cs.setString(2, usuario.getUsuario());
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();    

			Integer codigoError = cs.getInt(3);
			String mensajeError = cs.getString(4);

			logger.traceInfo("cancelarReserva", "codigo: " + codigoError);
			logger.traceInfo("cancelarReserva", "mensaje: " + mensajeError);

			if(codigoError.intValue() == 100) {
				respuesta = 100;
			}else if(codigoError.intValue() == 0){
				respuesta = 0;
			}else{
				respuesta= -1;
			}
		} catch (Exception e) {
			logger.traceError("cancelarReserva Exception", "Error durante la ejecuci�n de cancelarReserva", e);
			respuesta = -1;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("cancelarReserva", "Finalizado", "");
		return respuesta;
	}

	@Override
	public Integer extenderReserva(Long numReserva, String fechaVigenciaFinal, Usuario usuario) {
		logger.initTrace("extenderReserva",  "Long numeroReserva: [" + numReserva + "]" + "String fechaVigenciaFinal: ["+fechaVigenciaFinal+"]" + "Usuario usuario: ["+usuario.toString()+"]");
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Integer respuesta = 0;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_RESERVAS.PROC_EXTENDER_RESERVA(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, numReserva);
			cs.setString(2, fechaVigenciaFinal);
			cs.setString(3, usuario.getUsuario());
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();    

			Integer codigoError = cs.getInt(4);
			String mensajeError = cs.getString(5);

			logger.traceInfo("extenderReserva", "codigo: " + codigoError);
			logger.traceInfo("extenderReserva", "mensaje: " + mensajeError);

			respuesta = codigoError.intValue();
			
		} catch (Exception e) {
			logger.traceError("extenderReserva Exception", "Error durante la ejecuci�n de extenderReserva", e);
			respuesta = -1;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("extenderReserva", "Finalizado", "");
		return respuesta;
	}

	@Override
	public int getPaginas() {
		logger.initTrace("getPaginas", "");
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		int respuesta = 0;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_RESERVAS.PROC_GET_REGISTROS_PAG(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.NUMBER);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();    

			respuesta = cs.getInt(1);
			Integer codigoError = cs.getInt(2);
			String mensajeError = cs.getString(3);

			logger.traceInfo("getPaginas", "codigo: " + codigoError);
			logger.traceInfo("getPaginas", "mensaje: " + mensajeError);
		}catch (Exception e) {
			logger.traceError("getPaginas Exception", "Error durante la ejecuci�n de getPaginas", e);
			respuesta = 20;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		return respuesta;
	}

}
