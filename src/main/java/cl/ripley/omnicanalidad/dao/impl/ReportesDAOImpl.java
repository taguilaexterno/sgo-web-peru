package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CajaSucursal;
import cl.ripley.omnicanalidad.bean.ReporteModeloExtendidoDTO;
import cl.ripley.omnicanalidad.bean.ResumenReporte;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.ReportesDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import cl.ripley.omnicanalidad.util.Util;
import oracle.jdbc.OracleTypes;


/**Implementación de {@linkplain ReportesDAO}
 *
 * @author Carlos Leal (Aligare).
 * @since 04-06-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class ReportesDAOImpl implements ReportesDAO{
	private static final AriLog logger = new AriLog(ReportesDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private PoolBD poolBD;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Override
	public List<CajaSucursal> obtenerCajas() {
		logger.initTrace("obtenerCajas", "Sin parametros");
		
		List <CajaSucursal> retorno = new ArrayList<>();
		Connection conn = poolBD.getConnection();
		if(null != conn) {
			CallableStatement cs = null;
			try {
				logger.traceInfo("obtenerCajas", "preparando llamada");
				String procSQL = "{CALL SGO_REPORTES.PROC_OBTENER_CAJAS(?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.CURSOR);
				cs.registerOutParameter(2, OracleTypes.NUMBER);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.execute();    
				
				ResultSet rs = (ResultSet) cs.getObject(1);
				logger.traceInfo("obtenerCajas", "Llamada finalizada",new KeyLog("cod_err", String.valueOf(cs.getInt(2))),new KeyLog("msg_err", cs.getString(3)));
				while (null != rs && rs.next()) {
					CajaSucursal cajaSuc= new CajaSucursal();
					cajaSuc.setSucursal(rs.getInt("NRO_SUCURSAL"));
					cajaSuc.setCaja(rs.getInt("NRO_CAJA"));
					retorno.add(cajaSuc);
				}
				cs.close();
			}catch (Exception e) {
				logger.traceError("obtenerCajas", "Error durante la llamada", e);
			} finally {
				poolBD.closeConnection(conn);		
			}
		}
		logger.endTrace("obtenerCajas", "Finalizado", retorno.toString());
		return retorno;
	}

	@Override
	public List<ResumenReporte> buscarCierresCaja(String fechaInicio, String fechaFin, String csArray) {
		logger.initTrace("buscarCierresCaja", "String: "+fechaInicio + " String: "+fechaFin + " String[]: "+csArray);
		
		List <ResumenReporte> retorno = new ArrayList<>();
		Connection conn = poolBD.getConnection();
		if(null != conn) {
			CallableStatement cs = null;
			try {
				logger.traceInfo("buscarCierresCaja", "preparando llamada");
				String procSQL = "{CALL SGO_REPORTES.PROC_OBTENER_CIERRES_CAJA(?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, fechaInicio);
				cs.setString(2, fechaFin);
				cs.setString(3, csArray);
				cs.registerOutParameter(4, OracleTypes.CURSOR);
				cs.registerOutParameter(5, OracleTypes.NUMBER);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.execute();
				
				ResultSet rs = (ResultSet) cs.getObject(4);
				logger.traceInfo("buscarCierresCaja", "Llamada finalizada",new KeyLog("cod_err", String.valueOf(cs.getInt(5))),new KeyLog("msg_err", cs.getString(6)));
				
				while (null != rs && rs.next()) {
					ResumenReporte resumen = new ResumenReporte();
					resumen.setSucursal(rs.getString(Constantes.NRO_SUCURSAL));
					resumen.setCaja(rs.getString(Constantes.NRO_CAJA));
					resumen.setFechaApertura(rs.getString(Constantes.FEC_HOR_APERTURA));
					resumen.setFechaCierre(rs.getString(Constantes.FEC_HOR_CIERRE));
					resumen.setIdAperturaCierre(rs.getLong(Constantes.ID_APERTURA_CIERRE));
					retorno.add(resumen);
				}	
				cs.close();
			}catch (Exception e) {
				logger.traceError("buscarCierresCaja", "Error durante la llamada", e);
			} finally {
				poolBD.closeConnection(conn);		
			}
		}
		logger.endTrace("buscarCierresCaja", "Finalizado", retorno.toString());
		return retorno;
	}

	@Override
	public HSSFWorkbook buscarInformacionCierre(Long idAperturaCierre, String idCaja, String idSucursal, int funcionBoleta) {
		logger.initTrace("buscarInformacionCierre", "Parametros: Long idAperturaCierre: " + idAperturaCierre +" String idCaja:" + idCaja + 
				" String idSucursal:" + idSucursal +" int funcionBoleta:" + funcionBoleta);
		HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet("Resumen");
        HSSFRow fila = hoja.createRow(0);
        String titulo = "Ventas y Recaudaciones";
        BigDecimal sumaError=Constantes.NRO_CERO_B,sumaMail=Constantes.NRO_CERO_B,sumaImp=Constantes.NRO_CERO_B,sumaMonto=Constantes.NRO_CERO_B,sumaOC=Constantes.NRO_CERO_B;
        BigDecimal sumaBol=Constantes.NRO_CERO_B, sumaRR=Constantes.NRO_CERO_B, sumaRM=Constantes.NRO_CERO_B, sumaFac=Constantes.NRO_CERO_B;
		Connection conn = poolBD.getConnection();
		if (conn!=null){
			int funcion = funcionBoleta;
		
		String procSQL ="{CALL CAVIRA_MANEJO_CAJA.PROC_INFORME_DE_CIERRE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		String sql = "";
		CallableStatement cs = null;
		try {
			ResultSet rs = null;
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, idAperturaCierre);
			cs.setInt(2, funcion);
			cs.setInt(3, Integer.parseInt(idCaja));
			cs.setInt(4, Integer.parseInt(idSucursal));
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.registerOutParameter(9, OracleTypes.CURSOR);
			cs.registerOutParameter(10, OracleTypes.NUMBER);
			cs.registerOutParameter(11, OracleTypes.NUMBER);
			cs.registerOutParameter(12, OracleTypes.NUMBER);
			cs.registerOutParameter(13, OracleTypes.NUMBER);
			cs.registerOutParameter(14, OracleTypes.NUMBER);
			cs.registerOutParameter(15, OracleTypes.VARCHAR);
			cs.registerOutParameter(16, OracleTypes.VARCHAR);
			
			sql = "{CALL CAVIRA_MANEJO_CAJA.PROC_INFORME_DE_CIERRE("+idAperturaCierre
					+ ","+funcion
					+ ","+Integer.parseInt(idCaja)
					+ ","+Integer.parseInt(idSucursal)
					+ ",outV,outV,outN,outN,outC,outN,outN,outN,outN,outN,outV,outV)}";
			logger.traceInfo("buscarInformacionCierre", sql);
			cs.execute();
			rs = (ResultSet) cs.getObject(9);

			HSSFCell celda = null;
			int cont =9;
			if (rs!=null) {
				
				ResultSetMetaData rsmd = rs.getMetaData();	
				int ancho = rsmd.getColumnCount();
				
				celda = fila.createCell(1);
					
				//Defino estilos
                HSSFCellStyle miEstilo = libro.createCellStyle();
                HSSFFont miFuente=libro.createFont();
                miFuente.setBold(true);
                miFuente.setFontHeight((short)400);
                //miFuente.setUnderline((byte)1);Underline
                miEstilo.setFont(miFuente);
                hoja.addMergedRegion(new CellRangeAddress(0,0,1,10));
                fila = hoja.createRow(0);
                celda = fila.createCell(1);
                celda.setCellValue(titulo);                                
                celda.setCellStyle(miEstilo);
                
				//Resumen
                fila = hoja.createRow(2);
                celda = fila.createCell(0);
                celda.setCellValue("Sucursal N°: ");
                celda = fila.createCell(1);
                celda.setCellValue(idSucursal);
                
                fila = hoja.createRow(3);
                celda = fila.createCell(0);
                celda.setCellValue("Caja N°: ");
                celda = fila.createCell(1);
                celda.setCellValue(idCaja);
                
                fila = hoja.createRow(4);
                celda = fila.createCell(0);
                celda.setCellValue("Fec.Proceso: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getString(5));
                
                fila = hoja.createRow(5);
                celda = fila.createCell(0);
                celda.setCellValue("N°Trx Apertura: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getInt(7));
                celda = fila.createCell(3);
                celda.setCellValue("Fec. Apertura: ");
                celda = fila.createCell(4);
                celda.setCellValue(cs.getString(5));
                
                fila = hoja.createRow(6);
                celda = fila.createCell(0);
                celda.setCellValue("N°Trx Cierre: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getInt(8));
                celda = fila.createCell(3);
                celda.setCellValue("Fec. Cierre: ");
                celda = fila.createCell(4);
                celda.setCellValue(cs.getString(6));
                
				miFuente = null;
				miEstilo = null;
				
				miEstilo = libro.createCellStyle();
                miFuente=libro.createFont();
				miFuente.setBold(true);
				miEstilo.setFont(miFuente);
                
				//coloca los titulos a las cabeceras
				fila = hoja.createRow(cont);
				for (int cc=0;cc<ancho-1;cc++){
					celda = fila.createCell(cc);
					String res = rsmd.getColumnName(cc + 1);
					celda.setCellValue(res);
					celda.setCellStyle(miEstilo);
				
				}
				
				//llena la informacion
				cont++;
				while(rs.next()){
					int i=0; 				
					
					fila = hoja.createRow(cont);
					for (i = 0; i < ancho; i++) {
//						hoja.autoSizeColumn((short)i);
						celda = fila.createCell(i);
						String res = rs.getString(i + 1);
						if ((i+1)==6 && res.equalsIgnoreCase("ERROR"))		sumaError = sumaError.add(BigDecimal.ONE);
						if ((i+1)==6 && res.equalsIgnoreCase("MAIL"))		sumaMail = sumaMail.add(BigDecimal.ONE);
						if ((i+1)==6 && res.equalsIgnoreCase("IMPRESION"))	sumaImp = sumaImp.add(BigDecimal.ONE);
						if ((i+1)==11 && ((res.equalsIgnoreCase("B")) || (res.equalsIgnoreCase("BM"))))		sumaBol = sumaBol.add(BigDecimal.ONE);
						if ((i+1)==11 && ((res.equalsIgnoreCase("F")) || (res.equalsIgnoreCase("FM"))))		sumaFac = sumaFac.add(BigDecimal.ONE);
						if ((i+1)==6 && ((res.equalsIgnoreCase("RR")) || (res.equalsIgnoreCase("ANRR"))))	sumaRR = sumaRR.add(BigDecimal.ONE);
						if ((i+1)==6 && ((res.equalsIgnoreCase("RM")) || (res.equalsIgnoreCase("ANRM"))))	sumaRM = sumaRM.add(BigDecimal.ONE);
						if ((i+1)==7) {sumaMonto = sumaMonto.add(new BigDecimal(res)); sumaOC = sumaOC.add(BigDecimal.ONE);}
						if ((i+1)<=12)celda.setCellValue(res);						
					}
					cont++;
				}			
				//totales
				cont=cont+2;
				fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Monto");
                celda = fila.createCell(10);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaMonto));               
                
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Resumen");
                celda.setCellStyle(miEstilo);
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total imp"); 
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaImp));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Mail");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaMail));

                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (Constantes.FUNCION_BOLETA!= funcion)	{
					celda.setCellValue("NC/Boletas");
				}
				else{
					celda.setCellValue("Boletas");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaBol));//cs.getInt(9));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (Constantes.FUNCION_BOLETA!= funcion)	{
					celda.setCellValue("NC/Facturas");
				}
				else{
					celda.setCellValue("Facturas");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaFac));//cs.getInt(10));
                
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (Constantes.FUNCION_BOLETA!= funcion)	{
					celda.setCellValue("Anulación RR");
				}
				else{
					celda.setCellValue("Total RR");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaRR));//cs.getInt(11));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (Constantes.FUNCION_BOLETA!= funcion)	{
					celda.setCellValue("Anulación RM");
				}
				else{
					celda.setCellValue("Total RM");
				}
				celda.setCellStyle(miEstilo);
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaRM));//cs.getInt(12));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total OC");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaOC));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Err");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaError));


				rs.close();
				cs.close();
			}	
			libro.close();
			
		} catch (Exception e) {
			logger.traceError("buscarInformacionCierre", "Error durante la ejecución de generaArchivo", e);
		} finally {
			poolBD.closeConnection(conn);			
		}
	}
		logger.endTrace("buscarInformacionCierre", "Fin", "");
		return libro;
	}

	@Override
	public List<ReporteModeloExtendidoDTO> buscarInformacionME(String fechaInicio, String fechaFin, Long numeroOrden,
			Long numeroDNI, String estado, String tipo) {
		logger.initTrace("buscarInformacionME", "String fechaInicio, String fechaFin, Long numeroOrden,Long numeroDNI, String estado, String tipo "+tipo,
				new KeyLog("fechaInicio",fechaInicio),
				new KeyLog("fechaFin",fechaFin),
				new KeyLog("numeroOrden",String.valueOf(numeroOrden)),
				new KeyLog("numeroDNI",String.valueOf(numeroDNI)),
				new KeyLog("estado",estado)
				);
		
		List <ReporteModeloExtendidoDTO> retorno = new ArrayList<>();
		Connection conn = poolBD.getConnection();
		if(null != conn) {
			CallableStatement cs = null;
			try {
				logger.traceInfo("buscarInformacionME", "preparando llamada");
				String procSQL = "{CALL PRC_REPORT_EXTEND_MODEL_2(?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, fechaInicio);
				cs.setString(2, fechaFin);
				cs.setString(3, (null==numeroOrden)?"":String.valueOf(numeroOrden));
				cs.setString(4, (null==numeroDNI)?"":String.valueOf(numeroDNI));
				cs.setString(5, estado);
				try {
					logger.traceInfo("buscarInformacionME", "Parámetro CANTIDAD_MESES_REPORTE_ME " + Integer.parseInt(parametroDao.getParametro("CANTIDAD_MESES_REPORTE_ME").getValor()));
					cs.setInt(6, Integer.parseInt(parametroDao.getParametro("CANTIDAD_MESES_REPORTE_ME").getValor()));
				} catch (Exception e){
					logger.traceInfo("buscarInformacionME", "Parámetro CANTIDAD_MESES_REPORTE_ME no existe");
					cs.setInt(6, Constantes.NRO_DOS);
				}
				cs.setString(7, tipo);
				cs.registerOutParameter(8, OracleTypes.CURSOR);
				cs.execute();
				
				ResultSet rs = (ResultSet) cs.getObject(8);
				logger.traceInfo("buscarInformacionME", "Llamada finalizada");
				while (null != rs && rs.next()) {
					ReporteModeloExtendidoDTO row = new ReporteModeloExtendidoDTO();
					row.setNumeroOrden(validaNull(rs.getString("ORDEN")));
					row.setFechaBoleta(validaNull(rs.getString("FECHA_BOLETA")));
					row.setEstado(validaNull(rs.getString("ESTADO")));
					row.setOrdenMKP(validaNull(rs.getString("ORDEN_MKP")));
					row.setEstadoMKP(validaNull(rs.getString("ESTADO_MKP")));
					row.setMoneda(validaNull(rs.getString("MONEDA")));
					row.setMonto(Util.formatNumber3(rs.getBigDecimal("MONTO")));
					row.setDespacho(validaNull(rs.getString("DESPACHO")));
					row.setDescuento(Util.formatNumber3(rs.getBigDecimal("DESCUENTO")));
					row.setTipoDescuento(validaNull(rs.getString("TIPO_DESCUENTO")));
					row.setTipoDocumento(validaNull(rs.getString("TIPO_DOCUMENTO")));
					row.setDocumento(validaNull(rs.getString("DOCUMENTO")));
					row.setLongonID(validaNull(rs.getString("LOGON_ID")));
					row.setSucursal(validaNull(rs.getString("SUCURSAL")));
					row.setEjecutivoVenta(validaNull(rs.getString("EJECUTIVO_VENTA")));
					row.setNumeroItem(validaNull(rs.getString("NUMERO_ITEM")));
					row.setCodigoItem(validaNull(rs.getString("CODIGO_ITEM")));
					row.setSkuItem(validaNull(rs.getString("SKU_ITEM")));
					row.setDescripcionItem(validaNull(rs.getString("DESCRIPCION_ITEM")));
					row.setDescripcionPMM(validaNull(rs.getString("DESCRIPCION_PMM_ITEM")));
					row.setPrecioitem(Util.formatNumber3(rs.getBigDecimal("PRECIO_ITEM")));
					row.setCantidaditem(validaNull(rs.getString("CANTIDAD_ITEM")));
					row.setDescuentoItem(Util.formatNumber3(rs.getBigDecimal("DESCUENTO_ITEM")));
					row.setTipoDescuentoItem(validaNull(rs.getString("TIPO_DESCUENTO_ITEM")));
					row.setRegaloItem(validaNull(rs.getString("REGALO_ITEM")));
					row.setMensajeItem(validaNull(rs.getString("MENSAJE_ITEM")));
					row.setReferenciaDireccion(validaNull(rs.getString("REFERENCIA_DIRECCION")));
					row.setIndicadorExtraItem(validaNull(rs.getString("INDICADOR_EXTRA_ITEM")));
					row.setBodegaitem(validaNull(rs.getString("BODEGA_ITEM")));
					row.setSucursalReserva(validaNull(rs.getString("SUSCURSA_RESERVA")));
					row.setTipoDespachoItem(validaNull(rs.getString("TIPO_DESPACHO_ITEM")));
					row.setFechaDespachoItem(validaNull(rs.getString("FECHA_DESPACHO_ITEM")));
					row.setDniItem(validaNull(rs.getString("DNI_ITEM")));
					row.setRutDespacho(validaNull(rs.getString("RUT_DESPACHO")));
					row.setNombreDespacho(validaNull(rs.getString("NOMBRE_DESPACHO")));
					row.setDpto(validaNull(rs.getString("DPTO")));
					row.setProvincia(validaNull(rs.getString("PROVINCIA")));
					row.setDistrito(validaNull(rs.getString("DISTRITO")));
					row.setDireccionDespacho(validaNull(rs.getString("DIRECCION_DESPACHO")));
					row.setTelefonoDespacho1(validaNull(rs.getString("TELEFONO_DESPACHO1")));
					row.setTelefonoDespacho2(validaNull(rs.getString("TELEFONO_DESPACHO2")));
					row.setTipoDocumentoDespacho(validaNull(rs.getString("TIPO_DOCUMENTO_DESPACHO")));
					row.setRutCliente(validaNull(rs.getString("RUT_CLIENTE")));
					row.setDptoFact(validaNull(rs.getString("DPTO_FAC")));
					row.setProvFact(validaNull(rs.getString("PROV_FAC")));
					row.setDistFact(validaNull(rs.getString("DIST_FAC")));
					row.setDireccionCliente(validaNull(rs.getString("DIRECCION_CLIENTE")));
					row.setNombreCliente(validaNull(rs.getString("NOMBRE_CLIENTE")));
					row.setApePatCliente(validaNull(rs.getString("APELLIDO_PAT_CLIENTE")));
					row.setApeMatCliente(validaNull(rs.getString("APELLIDO_MAT_CLIENTE")));
					row.setEmailCliente(validaNull(rs.getString("EMAIL_CLIENTE")));
					row.setCodigoAutorizador(validaNull(rs.getString("CODIGO_AUTORIZADOR")));
					row.setTipotarjeta(validaNull(rs.getString("TIPO_TARJETA")));
					row.setBinNumber(validaNull(rs.getString("BIN_NUMBER")));
					row.setNumeroTrajeta(validaNull(rs.getString("NUMERO_TARJETA")));
					row.setNumeroCuotas(validaNull(rs.getString("NUMERO_CUOTAS")));
					row.setValorCuota(Util.formatNumber3(rs.getBigDecimal("VALOR_CUOTA")));
					row.setVencimiento(validaNull(rs.getString("VENCIMIENTO")));
					row.setCertificacion(validaNull(rs.getString("CERTIFICACION")));
					row.setNumeroOperacionBancaria(validaNull(rs.getString("NRO_OPERACION_BANCARIA")));
					row.setComprobante(validaNull(rs.getString("COMPROBANTE")));
					row.setRuc(validaNull(rs.getString("RUC")));
					row.setRazonSocial(validaNull(rs.getString("RAZON_SOCIAL")));
					row.setKioskoVendedorMac(validaNull(rs.getString("KIOSKO_VENDEDOR_MAC")));
					row.setFechaTicket(validaNull(rs.getString("FECHA_TICKET")));
					row.setNumeroBoleta(validaNull(rs.getString("NUMERO_BOLETA")));
					row.setTicket(validaNull(rs.getString("TICKET")));
					row.setCaja(validaNull(rs.getString("CAJA")));
					row.setSucursalTiendaFisica(validaNull(rs.getString("SUCURSAL_TIENDA_FISICA")));
					row.setTipoSubOrden(validaNull(rs.getString("TIPO_SUBORDEN")));
					row.setTipoOrdenCompra(validaNull(rs.getString("INDICADOR_MKP")));
					Integer esNC = rs.getInt("ES_NC");
					row.setEsNC(esNC.intValue() == Constantes.NRO_UNO || esNC.intValue() == Constantes.NRO_DOS ? Constantes.SI : Constantes.NO);
					retorno.add(row);
				}
				cs.close();
			}catch (Exception e) {
				logger.traceError("buscarInformacionME", "Error durante la llamada", e);
			} finally {
				poolBD.closeConnection(conn);		
			}
		}
		logger.endTrace("buscarInformacionME", "Finalizado", retorno.toString());
		return retorno;
	}

	private String validaNull(String string) {
		return (null== string)?"":string;
	}

	@Override
	public HashMap<String, String> obtenerEstados() {
		logger.initTrace("obtenerEstados", "Sin parametros");
		
		HashMap<String, String> retorno = new HashMap<>();
		Connection conn = poolBD.getConnection();
		if(null != conn) {
			CallableStatement cs = null;
			try {
				logger.traceInfo("obtenerEstados", "preparando llamada");
				String procSQL = "{CALL SGO_REPORTES.PROC_OBTENER_ESTADOS(?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.CURSOR);
				cs.registerOutParameter(2, OracleTypes.NUMBER);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.execute();    
				
				ResultSet rs = (ResultSet) cs.getObject(1);
				logger.traceInfo("obtenerEstados", "Llamada finalizada", new KeyLog("cod_err", String.valueOf(cs.getInt(2))),new KeyLog("msg_err", cs.getString(3)));
				while (null != rs && rs.next()) {
					retorno.put(rs.getString("XESTADO_ID"), rs.getString("XESTADO_DESC"));
				}
				cs.close();
			}catch (Exception e) {
				logger.traceError("obtenerEstados", "Error durante la llamada", e);
			} finally {
				poolBD.closeConnection(conn);		
			}
		}
		logger.endTrace("obtenerEstados", "Finalizado", retorno.toString());
		return retorno;
	}
}
