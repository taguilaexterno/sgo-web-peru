package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.GrupoMPSeguro;
import cl.ripley.omnicanalidad.bean.ReglasMPSeguro;
import cl.ripley.omnicanalidad.dao.ReglasMPSeguroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación {@linkplain ReglasMPSeguroDAO}.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class ReglasMPSeguroImpl implements ReglasMPSeguroDAO {

	private static final AriLog logger = new AriLog(ReglasMPSeguroImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public ArrayList<ReglasMPSeguro> getReglasMPSeguro() {
		
		ArrayList<ReglasMPSeguro> reglasMPSeguro = new ArrayList<ReglasMPSeguro>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.GET_REGLAS_MP_SEGURO(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(2) == 0) {
				rs = (ResultSet) cs.getObject(1);
				
				while (rs.next()) {
					ReglasMPSeguro mps = new ReglasMPSeguro();
					int id = rs.getInt("ID");
					String descripcion = rs.getString("DESCRIPCION");
					int estado = rs.getInt("ESTADO");
					int id_validacion = rs.getInt("ID_VALIDACION");
					String valor = rs.getString("VALOR");
					
					mps.setId(id);
					mps.setDescripcion(descripcion);
					mps.setEstado(estado);
					mps.setIdValidacion(id_validacion);
					mps.setValor(valor);
					reglasMPSeguro.add(mps);
				}
				
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en ReglasMPSeguroImpl: ", new Exception(cs.getString(3)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en ReglasMPSeguroImpl: ", e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "ReglasMPSeguroImpl: "+reglasMPSeguro.size());
		return reglasMPSeguro;
	}

	@Override
	public boolean updateEstado(int id, int id_validacion, int estado) {
		Connection conn = null;
		CallableStatement cs = null;
		boolean ret = false;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.UPDATE_REGLA_MP_SEGURO(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, id);
			cs.setInt(2, estado);
			cs.setInt(3, id_validacion);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(4) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en updateEstado: ", new Exception(cs.getString(5)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en updateEstado: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "updateEstado");
		
		return ret;
	}

	@Override
	public boolean updateValMenor(int id, int id_validacion, String valor) {
		Connection conn = null;
		CallableStatement cs = null;
		boolean ret = false;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.UPDATE_REGLA_VALOR_MENOR(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, id);
			cs.setInt(2, id_validacion);
			cs.setString(3, valor);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(4) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en updateValMenor: ", new Exception(cs.getString(5)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en updateValMenor: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "updateValMenor");
		return ret;
	}

	@Override
	public ArrayList<GrupoMPSeguro> getGrupoReglasMP() {
		
		ArrayList<GrupoMPSeguro> grupoReglasMP = new ArrayList<GrupoMPSeguro>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		logger.traceInfo("getGrupoReglasMP", "Ingreso metodo");
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.GET_GRUPO_REGLAS_MP(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();
			
			logger.traceInfo("getGrupoReglasMP cod : " + cs.getInt(2), "Ingreso metodo");
		
			if (cs.getInt(2) == 0) {
				rs = (ResultSet) cs.getObject(1);
				
				while (rs.next()) {					
					GrupoMPSeguro gmp = new GrupoMPSeguro();
					int id = rs.getInt("ID");
					String descripcion = rs.getString("DESCRIPCION");
					int tipoValidacion = rs.getInt("ID_TIPO_VALIDACION");
					int estadoFinal = rs.getInt("ESTADO_FINAL_NV");
					int estado = rs.getInt("ESTADO");
					
					gmp.setId(id);
					gmp.setDescripcion(descripcion);
					gmp.setIdTipoValidacion(tipoValidacion);
					gmp.setEstadoFinalNv(estadoFinal);
					gmp.setEstado(estado);
					grupoReglasMP.add(gmp);
				}
				
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en getGrupoReglasMP: ", new Exception(cs.getString(3)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en getGrupoReglasMP: ", e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "getGrupoReglasMP: "+grupoReglasMP.size());
		return grupoReglasMP;
	}

	@Override
	public boolean updateEstadoGrupo(int id, int estado) {
		Connection conn = null;
		CallableStatement cs = null;
		boolean ret = false;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.UPDATE_GRUPO_MP_SEGURO(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, id);
			cs.setInt(2, estado);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(3) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en updateEstadoGrupo: ", new Exception(cs.getString(4)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en updateEstadoGrupo: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "updateEstadoGrupo");
		
		return ret;
	}

}
