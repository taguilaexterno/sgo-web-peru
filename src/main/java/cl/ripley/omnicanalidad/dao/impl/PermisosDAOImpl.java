package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Permiso;
import cl.ripley.omnicanalidad.dao.PermisosDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementacion {@linkplain PermisosDAO}
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 10-08-2016
 * Versiones:
 * <ul>
 *  <li>10-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Repository
public class PermisosDAOImpl implements PermisosDAO {

	private static final AriLog logger = new AriLog(PermisosDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public List<Permiso> getPermisos() {
		logger.initTrace("getPermisos", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Permiso> permisos = new ArrayList<Permiso>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_PERMISOS.PROC_GET_PERMISOS(?,?,?,?)}");
			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
			
			String query = cs.getString(1);
			BigDecimal codigo = cs.getBigDecimal(3);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("getPermisos", "query: " + query);
			logger.traceInfo("getPermisos", "codigo: " + codigo);
			logger.traceInfo("getPermisos", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(2);
			
			while(rs != null && rs.next()) {
				Permiso mod = new Permiso();
				mod.setFecCrea(rs.getDate("FEC_CREA"));
				mod.setId(rs.getLong("ID"));
				mod.setNombrePermiso(rs.getString("NOMBRE_PERMISO"));
				permisos.add(mod);
			}
			
		} catch(Exception e) {
			logger.traceError("getPermisos Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getPermisos", "Finalizado", "Tama�o lista: " + permisos.size());
		
		return permisos;
	}

}
