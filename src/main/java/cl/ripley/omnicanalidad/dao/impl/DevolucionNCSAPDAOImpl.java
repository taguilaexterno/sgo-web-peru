package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Banco;
import cl.ripley.omnicanalidad.bean.Region;
import cl.ripley.omnicanalidad.bean.Retorno;
import cl.ripley.omnicanalidad.dao.DevolucionNCSAPDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

@Repository
public class DevolucionNCSAPDAOImpl implements DevolucionNCSAPDAO {
	private static final AriLog logger = new AriLog(DevolucionNCSAPDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public Banco obtenerBanco(String codBanco) throws AligareException{
		logger.initTrace("obtenerBanco", "String codBanco: "+codBanco);

		Banco resultado = null;
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_SAP.PROC_GET_BANCO(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, codBanco);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			
			//logger.traceInfo("obtenerBanco", procSQL);
			//logger.traceInfo("obtenerBanco", "{CALL SGO_MANEJO_SAP.PROC_GET_BANCO('"+codBanco+"',outv1,outv2,outn,outv3)}");
			
			cs.execute();
			
			//logger.traceInfo("obtenerBanco", "1nombre: " + cs.getString(2));
			//logger.traceInfo("obtenerBanco", "2pais: " + cs.getString(3));
			//logger.traceInfo("obtenerBanco", "codigo: " + cs.getInt(4));
			//logger.traceInfo("obtenerBanco", "3mensaje: " + cs.getString(5));
			
			
			if(cs.getInt(4) == Constantes.NRO_CERO){
				resultado = new Banco();
				resultado.setCodigo(codBanco);
				resultado.setNombre(cs.getString(2));
				resultado.setPais(cs.getString(3));
			}else{
				throw new AligareException("Codigo: "+cs.getInt(4)+" - Mensaje: "+cs.getString(5));
			}
		} catch (Exception e) {
			logger.traceError("obtenerBanco", "Error en obtenerBanco[SGO_MANEJO_SAP.PROC_GET_BANCO]: ", e);
			logger.endTrace("obtenerBanco", "Finalizado", "Banco: AligareException");
			throw new AligareException(e);
		} finally {
			poolBD.closeConnection(conn,null,cs);
		}

		logger.endTrace("obtenerBanco", "Finalizado", "Banco [Codigo: "+resultado.getCodigo()+"][Nombre: "+resultado.getNombre()+"]");
		
		return resultado;
	}

	@Override
	public Region obtenerRegion(String codRegion) throws AligareException{
		logger.initTrace("obtenerRegion", "String codRegion: "+codRegion);

		Region resultado = null;
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_SAP.PROC_GET_NOMBRE_REGION(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, codRegion);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
			
			if(cs.getInt(3) == Constantes.NRO_CERO){
				resultado = new Region();
				resultado.setCodigo(codRegion);
				resultado.setNombre(cs.getString(2));
			}else{
				throw new AligareException("Codigo: "+cs.getInt(3)+" - Mensaje: "+cs.getString(4));
			}
		} catch (Exception e) {
			logger.traceError("obtenerRegion", "Error en obtenerRegion[SGO_MANEJO_SAP.PROC_GET_NOMBRE_REGION]: ", e);
			logger.endTrace("obtenerRegion", "Finalizado", "Region: AligareException");
			throw new AligareException(e);
		} finally {
			poolBD.closeConnection(conn,null,cs);
		}

		logger.endTrace("obtenerRegion", "Finalizado", "Region [Codigo: "+resultado.getCodigo()+"][Nombre: "+resultado.getNombre()+"]");
		
		return resultado;
	}

	@Override
	public List<Banco> obtenerBancos() throws AligareException{
		logger.initTrace("obtenerBancos", null);		
		
		List<Banco> bancos = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_SAP.PROC_GET_BANCOS(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();

			if(cs.getInt(2) == Constantes.NRO_CERO){
				rs = (ResultSet) cs.getObject(1);
				if (rs != null) {
					bancos = new ArrayList<Banco>();
					while (rs.next()) {
						Banco banco = new Banco();
						banco.setCodigo(rs.getString("COD_BANCO"));						
						banco.setPais(rs.getString("COD_PAIS"));
						banco.setNombre(rs.getString("NOMBRE"));
						
						bancos.add(banco);
					}
				}
			}else{
				throw new AligareException("Codigo: "+cs.getInt(2)+" - Mensaje: "+cs.getString(3));
			}
							
		} catch (Exception e) {
			logger.traceError("obtenerBancos", "Error en obtenerBancos[SGO_MANEJO_SAP.PROC_GET_BANCOS]: ", e);
			logger.endTrace("obtenerBancos", "Finalizado", "bancos: AligareException");
			throw new AligareException(e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}

		logger.endTrace("obtenerBancos", "Finalizado", "bancos: "+bancos.size());
		return bancos;
	}

	@Override
	public List<Region> obtenerRegiones() throws AligareException{
		logger.initTrace("obtenerRegiones", null);		
		
		List<Region> regiones = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_SAP.PROC_GET_REGIONES(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();

			if(cs.getInt(2) == Constantes.NRO_CERO){
				rs = (ResultSet) cs.getObject(1);
				if (rs != null) {
					regiones = new ArrayList<Region>();
					while (rs.next()) {
						Region region = new Region();
						region.setCodigo(rs.getString("CODIGO"));						
						region.setNombre(rs.getString("NOMBRE"));
						
						regiones.add(region);
					}
				}
			}else{
				throw new AligareException("Codigo: "+cs.getInt(2)+" - Mensaje: "+cs.getString(3));
			}
							
		} catch (Exception e) {
			logger.traceError("obtenerRegiones", "Error en obtenerRegiones[SGO_MANEJO_SAP.PROC_GET_REGIONES]: ", e);
			logger.endTrace("obtenerRegiones", "Finalizado", "regiones: AligareException");
			throw new AligareException(e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}

		logger.endTrace("obtenerRegiones", "Finalizado", "regiones: "+regiones.size());
		return regiones;
	}

	@Override
	public Retorno insertaDevNCSAP(BigInteger ticket, long folio, long oc,
			String rut, String codBanco, long nroCuenta, String usuario) {
		logger.initTrace("insertaDevNCSAP", "BigInteger ticket: "+ticket.toString()+", long folio: "+folio+", long oc: "+oc+", String rut: "+
			rut+", String codBanco: "+codBanco+", long nroCuenta: "+nroCuenta+", String usuario: "+usuario, new KeyLog("Correlativo venta", String.valueOf(oc)));

		Retorno resultado = new Retorno();
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_SAP.PROC_INSERT_SAP_DEV(?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setBigDecimal(1, new BigDecimal(ticket));
			cs.setLong(2, folio);
			cs.setLong(3, oc);
			cs.setString(4, rut);
			cs.setString(5, codBanco);
			cs.setLong(6, nroCuenta);
			cs.setString(7, usuario);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.execute();
			
			resultado.setCodigo(cs.getInt(8));
			resultado.setMensaje(cs.getString(9));
			
		} catch (Exception e) {
			logger.traceError("insertaDevNCSAP", "Error en insertaDevNCSAP[SGO_MANEJO_SAP.PROC_INSERT_SAP_DEV]: ", e);
			resultado.setCodigo(Constantes.NRO_MENOSUNO);
			resultado.setMensaje("Error en la ejecucion, favor reintente.");
		} finally {
			poolBD.closeConnection(conn,null,cs);
		}

		logger.endTrace("insertaDevNCSAP", "Finalizado", "Retorno "+resultado.toString());
		
		return resultado;
	}
	
}
