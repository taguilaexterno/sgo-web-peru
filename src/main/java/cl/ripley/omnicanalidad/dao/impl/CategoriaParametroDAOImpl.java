package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.CategoriasProperties;
import cl.ripley.omnicanalidad.dao.CategoriaParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación de {@linkplain CategoriaParametroDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class CategoriaParametroDAOImpl implements CategoriaParametroDAO {
	private static final AriLog logger = new AriLog(CategoriaParametroDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public List<CategoriasProperties> getCategoriasParametros() {
		logger.initTrace("getCategoriasParametros", "Ingreso a metodo");

		List<CategoriasProperties> categorias = new ArrayList<CategoriasProperties>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_CAJA.PROC_GET_PROPERTY_TYPES(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(3) == 0) {
				rs = (ResultSet) cs.getObject(1);
				logger.traceInfo("getCategoriasParametros", "Query: " + ((String) cs.getObject(2)));

				while (rs.next()) {
					CategoriasProperties cat = new CategoriasProperties();
					
					cat.setDescription(rs.getString("DESCRIPTION"));
					cat.setPropertyTypeId(rs.getString("XPROPERTYTYPE_ID").trim());
					categorias.add(cat);
					
				}
				
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en getCategoriasParametros: ", new Exception(cs.getString(4)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en getCategoriasParametros: ", e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "getCategoriasParametros: "+categorias.size());

		return categorias;
	}

}
