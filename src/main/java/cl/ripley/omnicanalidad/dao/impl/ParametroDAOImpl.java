package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación de {@linkplain ParametroDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class ParametroDAOImpl implements ParametroDAO {
	private static final AriLog logger = new AriLog(ParametroDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	List<Parametro> parametros = null;
	
	public ParametroDAOImpl() {
//		parametros = (List<Parametro>) obtenerParametros();
	}

	public ParametroDAOImpl(boolean flag) {
		if (flag){
			parametros = (List<Parametro>) obtenerParametros();
		}
	}

	
	public List<Parametro> obtenerParametros() {
		logger.initTrace("obtenerParametros",null);
		List<Parametro> parametrosList = new ArrayList<Parametro>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL CAVIRA_CONF.PROC_OBTIENE_PROPERTIES(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, Constantes.STORE_ID);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
			if (cs.getInt(4) == 0) {
				rs = (ResultSet) cs.getObject(3);
				logger.traceInfo("obtenerOrdenes", "Query: " + ((String) cs.getObject(2)));
				
				if (rs != null) {
					while (rs.next()) {
						Parametro par = new Parametro();
		            	par.setNombre(rs.getString("NAME"));
		            	par.setValor(rs.getString("VALUE"));
		            	parametrosList.add(par);
					}
				}
			}
		} catch (Exception e) {
			logger.traceError("obtenerParametros", "Error en obtenerParametros: ", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("obtenerParametros", "Finalizado", "parametros: "+parametrosList.size());
		return parametrosList;
	}

	@Override
	public Parametro getParametro(String nombre) {
		logger.initTrace("getParametro","String nombre: "+nombre);
		int j;
		String vacio = "";
		
		if(parametros == null || parametros.isEmpty()) {
			parametros = obtenerParametros();
		}
		
		Parametro parametro = new Parametro();
		
		j = 0;
		int i;
        for (i = 0; i < parametros.size(); i++) {
        	parametro =  parametros.get(i);
        	if (parametro.getNombre().equalsIgnoreCase(nombre)){
        		j = 1;
        		break;
        	}
        }
		
        if (j <= 0) {
        	parametro.setNombre(nombre);
        	parametro.setValor(vacio);
        }
		logger.endTrace("getParametro", "Finalizado", "parametro: " + ((parametro != null) ? parametro.getNombre()+": "+parametro.getValor() : "null"));
		return parametro;
	}

	@Override
	public boolean updateParametro(Parametro parametro) {
		logger.initTrace("updateParametro","String parametro: "+parametro.getNombre());
		
		boolean result = Boolean.TRUE;

		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_CAJA.UPD_XSTOREPROPERTIES(?,?,?,?,?,?)}";

			cs = conn.prepareCall(procSQL);

			cs.setString(1, parametro.getNombre());
			cs.setString(2, parametro.getValor());
			cs.setInt(3, Integer.parseInt(parametro.getPropertyTypeId()));

			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.execute();

			String query = cs.getString(4);
			String mensaje = cs.getString(5);
			BigDecimal codigo = cs.getBigDecimal(6);

			logger.traceInfo("updateParametro", "Query: " + query);
			logger.traceInfo("updateParametro", "mensaje: " + mensaje);
			logger.traceInfo("updateParametro", "codigo: " + codigo);

			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}

		} catch (Exception e) {
			logger.traceError("updateParametro Exception", "Error durante la ejecuci�n de updateParametro", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("updateParametro", "Finalizado", "result: " + result + "; Parametro: " + parametro.getNombre() + "; Valor: " + parametro.getValor());
		return result;
	}

	@Override
	public List<Parametro> getParametrosXTipo(int tipo) {
		logger.initTrace("getParametrosXTipo",null);
		List<Parametro> parametrosList = new ArrayList<Parametro>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_CAJA.PROC_GET_PROPERTIES_X_TYPE(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, tipo);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
			if (cs.getInt(4) == 0) {
				rs = (ResultSet) cs.getObject(2);
				logger.traceInfo("getParametrosXTipo", "Query: " + ((String) cs.getObject(3)));
				if (rs != null) {
					while (rs.next()) {
						Parametro par = new Parametro();
		            	par.setNombre(rs.getString("NAME"));
		            	par.setValor(rs.getString("VALUE"));
		            	par.setPropertyTypeId(rs.getString("XPROPERTYTYPE_ID").trim());
		            	par.setIdParametro(rs.getInt("XSTOREPROPERTIES_ID"));
		            	parametrosList.add(par);
					}
				}
			} else {
				logger.traceError("getParametrosXTipo", "Error en getParametrosXTipo: ", new Exception(cs.getString(5)));
			}
		} catch (Exception e) {
			logger.traceError("getParametrosXTipo", "Error en getParametrosXTipo: ", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getParametrosXTipo", "Finalizado", "parametros: "+parametrosList.size());
		return parametrosList;
	}

	public void resetParam() {
		if(parametros != null) {
			parametros.clear();
			parametros = null;
		}
	}
	
	public int getParamByName(String param) {
		
		String result = "";		
		String selectSQL = null;
		Connection conn = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();			
			selectSQL ="SELECT VALUE FROM XSTOREPROPERTIES WHERE NAME = '"+param+"'";
						        	
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			rs = ps.executeQuery();		
			
			while (rs.next()) {
				result = rs.getString(1);
			} 			
			
		}catch (Exception e) {
			// TODO: handle exception
			logger.traceError("getParamByName", "Error en getParamByName: ", e);
			poolBD.closeConnection(conn);
		
		} finally {
			poolBD.closeConnection(conn);
		}
		logger.endTrace("getParamByName", "Finalizado", "parametros: "+result);
		return Integer.parseInt(result);
	}
	
}
