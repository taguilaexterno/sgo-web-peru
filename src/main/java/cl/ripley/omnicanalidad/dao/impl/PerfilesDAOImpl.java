package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Perfil;
import cl.ripley.omnicanalidad.bean.Permiso;
import cl.ripley.omnicanalidad.dao.PerfilesDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación de {@linkplain PerfilesDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class PerfilesDAOImpl implements PerfilesDAO {

	private static final AriLog logger = new AriLog(PerfilesDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;

	@Override
	public List<Perfil> getPerfiles() {
		logger.initTrace("getPerfiles", "obtengo perfiles");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Perfil> perfiles;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_PERFILES.PRC_GET_PERFILES(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.execute();    

			String query = cs.getString(1);
			String mensaje = cs.getString(3);
			BigDecimal cod = cs.getBigDecimal(4);

			logger.traceInfo("getPerfiles", "Query: " + query);
			logger.traceInfo("getPerfiles", "Mensaje: " + mensaje);
			logger.traceInfo("getPerfiles", "Codigo: " + cod);

			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}

			rs = (ResultSet) cs.getObject(2);
			logger.traceInfo("getPerfiles", "Query: "+((String)cs.getObject(1)));
			perfiles = new ArrayList<Perfil>(20);
			while (rs != null && rs.next()) {
				Perfil perfil = new Perfil();
				perfil.setActivo(rs.getLong("ACTIVO") == 1);
				perfil.setFecCreacion(rs.getDate("FEC_CREACION"));
				perfil.setFecUpd(rs.getDate("FEC_UPD"));
				perfil.setId(rs.getLong("ID"));
				perfil.setNombrePerfil(rs.getString("NOMBRE_PERFIL"));
				perfil.setUsuarioCrea("USUARIO_CREA");
				perfil.setUsuarioUpd("USUARIO_UPD");
				perfiles.add(perfil);
			}
		} catch (Exception e) {
			logger.traceError("getPerfiles Exception", "Error durante la ejecuci�n de getPerfiles", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		for(Perfil perf : perfiles) {
			logger.endTrace("getPerfiles", "Finalizado", "Nombre perfil: " + perf.getNombrePerfil());
		}

		logger.endTrace("getPerfiles", "Finalizado", null);

		return perfiles;
	}

	@Override
	public boolean guardarPerfilNuevo(Perfil perfil) {
		logger.initTrace("guardarPerfilNuevo", "Guardo perfil: " + perfil.getNombrePerfil());

		boolean result = Boolean.TRUE;

		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_PERFILES.PRC_GUARDAR_PERFIL(?,?,?,?,?,?,?,?,?)}";

			for(Modulo mod : perfil.getModulos()) {
				cs = conn.prepareCall(procSQL);

				if(perfil.getId() == null) {
					cs.setNull(1, OracleTypes.NUMBER);
				} else {
					cs.setLong(1, perfil.getId());
				}

				cs.setString(2, perfil.getNombrePerfil());
				cs.setLong(3, mod.getId());
				cs.setLong(4, mod.getPermiso().getId());
				cs.setString(5, perfil.getUsuarioCrea());
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.registerOutParameter(7, OracleTypes.NUMBER);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				cs.registerOutParameter(9, OracleTypes.NUMBER);
				cs.execute();

				String query = cs.getString(6);
				BigDecimal idPerfil = cs.getBigDecimal(7);
				String mensaje = cs.getString(8);
				BigDecimal codigo = cs.getBigDecimal(9);

				logger.traceInfo("guardarPerfilNuevo", "Query: " + query);
				logger.traceInfo("guardarPerfilNuevo", "idPerfil: " + idPerfil);
				logger.traceInfo("guardarPerfilNuevo", "mensaje: " + mensaje);
				logger.traceInfo("guardarPerfilNuevo", "codigo: " + codigo);

				if(codigo.intValue() != 0) {
					throw new Exception(mensaje);
				}

				perfil.setId(idPerfil.longValue());

			}

		} catch (Exception e) {
			logger.traceError("guardarPerfilNuevo Exception", "Error durante la ejecuci�n de guardarPerfilNuevo", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("guardarPerfilNuevo", "Finalizado", "result: " + result);
		return result;
	}

	@Override
	public Perfil getPerfilById(Long id) {
		logger.initTrace("getPerfilById", "obtengo perfiles");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Perfil perfil = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_PERFILES.PRC_GET_PERFIL_X_ID(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, id);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();  

			String query = cs.getString(2);
			String mensaje = cs.getString(4);
			BigDecimal cod = cs.getBigDecimal(5);

			logger.traceInfo("getPerfilById", "Query: " + query);
			logger.traceInfo("getPerfilById", "Mensaje: " + mensaje);
			logger.traceInfo("getPerfilById", "Codigo: " + cod);

			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}

			rs = (ResultSet) cs.getObject(3);
			while (rs != null && rs.next()) {
				if(perfil == null) {
					perfil = new Perfil();
					perfil.setActivo(rs.getLong("ACTIVO_PERF") == 1);
					perfil.setFecCreacion(rs.getDate("FEC_CREA_PERF"));
					perfil.setFecUpd(rs.getDate("FEC_UPD_PERF"));
					perfil.setId(rs.getLong("ID_PERF"));
					perfil.setNombrePerfil(rs.getString("NOMBRE_PERFIL"));
					perfil.setUsuarioCrea("USUARIO_CREA_PERF");
					perfil.setUsuarioUpd("USUARIO_UPD_PERF");
					perfil.setModulos(new ArrayList<Modulo>());
				}

				//Creacion modulo
				Modulo mod = new Modulo();
				mod.setFecCrea(rs.getDate("FEC_CREA_MOD"));
				mod.setId(rs.getLong("ID_MOD"));
				mod.setNombreModulo(rs.getString("NOMBRE_MODULO"));
				mod.setUrl(rs.getString("URL"));

				//Creacion permiso del modulo
				Permiso perm = new Permiso();
				perm.setFecCrea(rs.getDate("FEC_CREA_PERM"));
				perm.setId(rs.getLong("ID_PERM"));
				perm.setNombrePermiso(rs.getString("NOMBRE_PERMISO"));

				//seteo permiso al modulo
				mod.setPermiso(perm);

				//agrego modulo a lista de modulos del perfil
				perfil.getModulos().add(mod);
			}
		} catch (Exception e) {
			logger.traceError("getPerfilById Exception", "Error durante la ejecuci�n de getPerfilById", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getPerfilById", "Finalizado", null);

		return perfil;
	}

	@Override
	public List<Perfil> getPerfilByNombrePerfil(String nombre) {
		logger.initTrace("getPerfilByNombrePerfil", "obtengo perfiles");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Perfil> perfiles = new ArrayList<Perfil>(50);
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_PERFILES.PRC_GET_PERFIL_X_NOMBRE(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, nombre);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();  

			String query = cs.getString(2);
			String mensaje = cs.getString(4);
			BigDecimal cod = cs.getBigDecimal(5);

			logger.traceInfo("getPerfilByNombrePerfil", "Query: " + query);
			logger.traceInfo("getPerfilByNombrePerfil", "Mensaje: " + mensaje);
			logger.traceInfo("getPerfilByNombrePerfil", "Codigo: " + cod);

			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}

			Perfil perfil = null;

			rs = (ResultSet) cs.getObject(3);
			while (rs != null && rs.next()) {

				boolean distintoPerfil = perfil != null && perfil.getId().longValue() != rs.getLong("ID_PERF");

				if(perfil == null || distintoPerfil) {
					logger.traceInfo("getPerfilByNombrePerfil", "Perfil: " + rs.getString("NOMBRE_PERFIL"));
					perfil = new Perfil();
					perfil.setActivo(rs.getLong("ACTIVO_PERF") == 1);
					perfil.setFecCreacion(rs.getDate("FEC_CREA_PERF"));
					perfil.setFecUpd(rs.getDate("FEC_UPD_PERF"));
					perfil.setId(rs.getLong("ID_PERF"));
					perfil.setNombrePerfil(rs.getString("NOMBRE_PERFIL"));
					perfil.setUsuarioCrea("USUARIO_CREA_PERF");
					perfil.setUsuarioUpd("USUARIO_UPD_PERF");
					perfil.setModulos(new ArrayList<Modulo>());
					perfiles.add(perfil);
				}

				//Creacion modulo
				Modulo mod = new Modulo();
				mod.setFecCrea(rs.getDate("FEC_CREA_MOD"));
				mod.setId(rs.getLong("ID_MOD"));
				mod.setNombreModulo(rs.getString("NOMBRE_MODULO"));
				mod.setUrl(rs.getString("URL"));

				//Creacion permiso del modulo
				Permiso perm = new Permiso();
				perm.setFecCrea(rs.getDate("FEC_CREA_PERM"));
				perm.setId(rs.getLong("ID_PERM"));
				perm.setNombrePermiso(rs.getString("NOMBRE_PERMISO"));

				//seteo permiso al modulo
				mod.setPermiso(perm);

				//agrego modulo a lista de modulos del perfil
				perfil.getModulos().add(mod);
			}
		} catch (Exception e) {
			logger.traceError("getPerfilByNombrePerfil Exception", "Error durante la ejecuci�n de getPerfilByNombrePerfil", e);
			return perfiles;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getPerfilByNombrePerfil", "Finalizado", "Tama�o lista: " + perfiles.size());

		return perfiles;
	}

	@Override
	public boolean updPerfil(Perfil perfil) {
		logger.initTrace("updPerfil", "Guardo perfil: " + perfil.getNombrePerfil());

		boolean result = Boolean.TRUE;

		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_PERFILES.PRC_UPD_PERFIL(?,?,?,?,?,?,?)}";

			cs = conn.prepareCall(procSQL);

			cs.setLong(1, perfil.getId());
			cs.setString(2, perfil.getNombrePerfil());
			cs.setLong(3, 1L);
			
			if(perfil.getUsuarioUpd() == null) {
				cs.setNull(4, OracleTypes.VARCHAR);
			} else {
				cs.setString(4, perfil.getUsuarioUpd());
			}
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.execute();

			String query = cs.getString(5);
			String mensaje = cs.getString(6);
			BigDecimal codigo = cs.getBigDecimal(7);

			logger.traceInfo("guardarPerfilNuevo", "Query: " + query);
			logger.traceInfo("guardarPerfilNuevo", "mensaje: " + mensaje);
			logger.traceInfo("guardarPerfilNuevo", "codigo: " + codigo);

			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}


		} catch (Exception e) {
			logger.traceError("guardarPerfilNuevo Exception", "Error durante la ejecuci�n de guardarPerfilNuevo", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("guardarPerfilNuevo", "Finalizado", "result: " + result + "; Perfil: " + perfil);
		return result;
	}

}
