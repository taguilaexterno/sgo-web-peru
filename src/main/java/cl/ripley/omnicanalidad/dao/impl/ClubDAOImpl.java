package cl.ripley.omnicanalidad.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ClienteClub;
import cl.ripley.omnicanalidad.dao.ClubDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;

@Repository
public class ClubDAOImpl implements ClubDAO {
	
	private static final AriLog logger = new AriLog(ClubDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private PoolBD poolBD;
	
	@Override
	public ClienteClub getClienteClub(Integer numeroEvento, int indicadorTitular) throws AligareException{
		logger.initTrace("getClienteClub", "Integer numeroEvento: "+numeroEvento+" - int indicadorTitular: "+indicadorTitular);
		
		ClienteClub clienteClub = null;
		
		Connection conn = poolBD.getConnectionCLUB();
		if (conn!=null){
			String selectSQL = "";
			try {
				selectSQL = "SELECT RGMCL_COC_IDE AS RUT, RGMCL_NOM_CLI AS NOMBRE, RGMCL_GLS_APL_PAT AS APELLIDOP, RGMCL_GLS_APL_MAT AS APELLIDOM " + 
							"FROM LGLREG_MAE_CLI_EVT " + 
							"WHERE RGMCL_COD_EVT = ? " + 
							"AND RGMCL_NRO_IDC_MON = ?";
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				if (ps != null) {
					ps.setInt(1, numeroEvento);
					ps.setInt(2, indicadorTitular);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						clienteClub = new ClienteClub();
						clienteClub.setRut(rs.getString("RUT"));
						clienteClub.setNombre(rs.getString("NOMBRE"));
						clienteClub.setApellidoPaterno(rs.getString("APELLIDOP"));
						clienteClub.setApellidoMaterno(rs.getString("APELLIDOM"));
						rs.close();
					}
					ps.close();
				}
			} catch (Exception e) {
				logger.traceError("getClienteClub", "Error durante la ejecuci�n de getClienteClub", e);
				logger.endTrace("getClienteClub", "Finalizado", "ERROR");
			} finally {
				poolBD.closeConnection(conn);			
			}
		} else {
			logger.traceInfo("getClienteClub", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("getClienteClub", "Finalizado", "clienteClub: " + ((clienteClub!=null)?clienteClub.toString():"null"));
		return clienteClub;
	}

}
