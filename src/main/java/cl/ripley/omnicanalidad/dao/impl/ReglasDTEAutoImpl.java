package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ProductosDTE;
import cl.ripley.omnicanalidad.bean.ReglaPrecio;
import cl.ripley.omnicanalidad.bean.ReglaSku;
import cl.ripley.omnicanalidad.bean.ReglaTodoManual;
import cl.ripley.omnicanalidad.bean.ReglasDTEAuto;
import cl.ripley.omnicanalidad.dao.ReglasDTEAutoDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación {@linkplain ReglasDTEAutoDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class ReglasDTEAutoImpl implements ReglasDTEAutoDAO {

	private static final AriLog logger = new AriLog(ReglasDTEAutoImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;

	@Override
	public ReglasDTEAuto getReglasDTEAuto() {
		
		ReglasDTEAuto reglasDTEAuto = new ReglasDTEAuto();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.GET_REGLAS_DTE_AUTO(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(2) == 0) {
				rs = (ResultSet) cs.getObject(1);
				
				while (rs.next()) {
					String nombre = rs.getString("NOMBRE");
					int id = rs.getInt("ID");
					int idParam = rs.getInt("IDPARAM");
					String descripcion = rs.getString("DESCRIPCION");
					int estado = rs.getInt("ESTADO");
					String valor = rs.getString("VALOR");
					int idValidacion = rs.getInt("IDVALIDACION");
					
					if (nombre.equalsIgnoreCase("SKU")) {
						ReglaSku sku = new ReglaSku();
						sku.setId(id);
						sku.setIdParam(idParam);
						sku.setDescripcion(descripcion);
						sku.setEstado(estado==1?true:false);
						sku.setValor(valor);
						sku.setIdValidacion(idValidacion);
						
						reglasDTEAuto.setReglaSku(sku);
					} else if(nombre.equalsIgnoreCase("CP")) {
						ReglaPrecio cp = new ReglaPrecio();
						cp.setId(id);
						cp.setIdParam(idParam);
						cp.setDescripcion(descripcion);
						cp.setEstado(estado==1?true:false);
						cp.setValor(valor);
						cp.setIdValidacion(idValidacion);
						
						reglasDTEAuto.setReglaPrecio(cp);
					} else {
						ReglaTodoManual todo = new ReglaTodoManual();
						todo.setId(id);
						todo.setIdParam(idParam);
						todo.setDescripcion(descripcion);
						todo.setEstado(estado==1?true:false);
						todo.setValor(valor);
						todo.setIdValidacion(idValidacion);
						
						reglasDTEAuto.setReglaTodoManual(todo);
					}					
				}
				
			} else {
				logger.traceError("ReglasDTEAutoImpl", "Error en getReglasDTEAuto: ", new Exception(cs.getString(3)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasDTEAutoImpl", "Error en getReglasDTEAuto: ", e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}
	
		logger.endTrace("ReglasDTEAutoImpl", "Finalizado", "getReglasDTEAuto");
		return reglasDTEAuto;
	}

	@Override
	public boolean updateEstadoRegla(ReglaPrecio reglaPrecio,
			ReglaSku reglaSku, ReglaTodoManual reglaTodoManual) {
		boolean ret = false;

		int	estadoRP = reglaPrecio.isEstado()?Constantes.ESTADO_ACTIVO:Constantes.ESTADO_INACTIVO;
		int estadosku = reglaSku.isEstado()?Constantes.ESTADO_ACTIVO:Constantes.ESTADO_INACTIVO;
		int estadoTodo = reglaTodoManual.isEstado()?Constantes.ESTADO_ACTIVO:Constantes.ESTADO_INACTIVO;
		
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			
			//Actualizar estado Reglas
			logger.traceInfo("Reglas Precio. ID: " + reglaPrecio.getId() + ", Estado: " + estadoRP, "inicio DAO");
			logger.traceInfo("SKU. ID: " + reglaSku.getId() + ", Estado: " + estadosku, "inicio DAO");
			logger.traceInfo("Todo Manual. ID: " + reglaTodoManual.getId() + ", Estado: " + estadoTodo, "inicio DAO");
			
			String procSQL = "{CALL SGO_REGLA.UPDATE_REGLA_DTE_AUTO(?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, reglaPrecio.getId());
			cs.setInt(2, estadoRP);
			cs.setInt(3, reglaSku.getId());
			cs.setInt(4, estadosku);
			cs.setInt(5, reglaTodoManual.getId());
			cs.setInt(6, estadoTodo);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.execute();
			
			logger.traceInfo("ReglasDTEAutoImpl", "updateEstadoRegla cod : " + cs.getInt(7));
			
			if (cs.getInt(7) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasDTEAutoImpl", "Error en updateEstadoRegla: ", new Exception(cs.getString(8)));
			}			
			
		} catch (Exception e) {
			logger.traceError("ReglasDTEAutoImpl", "Error en getReglasDTEAuto: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}

		logger.endTrace("ReglasDTEAutoImpl", "Finalizado ret : " + ret, "getReglasDTEAuto");
		
		return ret;
	}

	@Override
	public boolean updateFactorCP(String valor, int id, int idValidacion) {
		boolean ret = false;
		
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			
			//Actualizar Factor Regla CP
			logger.traceInfo("Reglas Precio. Valor : " + valor, "inicio DAO");
			
			String procSQL = "{CALL SGO_REGLA.UPDATE_REGLA_FACTOR_CP(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, valor);
			cs.setInt(2, id);
			cs.setInt(3, idValidacion);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			
			logger.traceInfo("ReglasDTEAutoImpl", "params valor : " + valor + ", idValidacion : " + idValidacion);
			
			cs.execute();
			
			logger.traceInfo("ReglasDTEAutoImpl", "updateFactorCP cod : " + cs.getInt(4));
			
			if (cs.getInt(4) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasDTEAutoImpl", "Error en updateFactorCP: ", new Exception(cs.getString(5)));
			}			
			
		} catch (Exception e) {
			logger.traceError("ReglasDTEAutoImpl", "Error en updateFactorCP: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}

		logger.endTrace("ReglasDTEAutoImpl", "Finalizado ret : " + ret, "updateFactorCP");
		
		return ret;
	}

	@Override
	public ArrayList<ProductosDTE> getProductosDTE() {
		ArrayList<ProductosDTE> productosList= new ArrayList<ProductosDTE>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.GET_PRODUCTOS_LIST(?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();
		
			if (cs.getInt(2) == 0) {
				rs = (ResultSet) cs.getObject(1);
				
				while (rs.next()) {
					ProductosDTE prod = new ProductosDTE();
					int id = rs.getInt("ID");
					String codigoArticulo = rs.getString("CODIGO_ARTICULO");
					Date fechaIngreso = rs.getDate("FECHA_INGRESO");
					String usuarioCreacion = rs.getString("USUARIO_CREACION");
					String usuarioModificacion = rs.getString("USUARIO_MODIFICACION");
					Date fechaModificacion = rs.getDate("FECHA_MODIFICACION");
					int estado = rs.getInt("ESTADO");
					String descripcion = rs.getString("DESCRIPCION");
					
					prod.setId(id);
					prod.setCodigoArticulo(codigoArticulo);
					prod.setFechaIngreso(fechaIngreso);
					prod.setUsuarioCreacion(usuarioCreacion);
					prod.setUsuarioModificacion(usuarioModificacion);
					prod.setFechaModificacion(fechaModificacion);
					prod.setEstado(estado);
					prod.setDescripcion(descripcion);
					productosList.add(prod);
				}
				
			} else {
				logger.traceError("ReglasMPSeguroImpl", "Error en getProductosDTE: ", new Exception(cs.getString(3)));
			}
			
		} catch (Exception e) {
			logger.traceError("ReglasMPSeguroImpl", "Error en getProductosDTE: ", e);
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}
	
		logger.endTrace("ReglasMPSeguroImpl", "Finalizado", "getProductosDTE: "+productosList.size());
		return productosList;
	}

	@Override
	public boolean saveSkuProd(ProductosDTE productoSku) {
		boolean ret = false;
		
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			
			//Actualizar estado Reglas
			logger.traceInfo("Producto. SKU : " + productoSku.getCodigoArticulo() + ", Descripcion: " + productoSku.getDescripcion(), "inicio DAO");

			String procSQL = "{CALL SGO_REGLA.SAVE_ARTICULO(?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, productoSku.getCodigoArticulo());
			cs.setInt(2, productoSku.getCodUsuarioCreacion());
			cs.setInt(3, Constantes.ESTADO_ACTIVO);
			cs.setString(4, productoSku.getDescripcionCorta());
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();
			
			logger.traceInfo("ReglasDTEAutoImpl", "saveSkuProd cod : " + cs.getInt(5));
			
			if (cs.getInt(5) == 0) {
				ret = true;
			} else {
				logger.traceError("ReglasDTEAutoImpl", "Error en saveSkuProd: ", new Exception(cs.getString(6)));
			}			
			
		} catch (Exception e) {
			logger.traceError("ReglasDTEAutoImpl", "Error en saveSkuProd: ", e);
		} finally {
			poolBD.closeConnection(conn);
		}

		logger.endTrace("ReglasDTEAutoImpl", "Finalizado ret : " + ret, "saveSkuProd");
		
		return ret;
	}

	@Override
	public boolean delSkuProd(String seleccionados) {
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.FALSE;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_REGLA.DELETE_ARTICULO(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, seleccionados);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
			logger.traceInfo("delSkuProd", "Query: " + ((String) cs.getObject(2)));
			
			//exito
			if (cs.getInt(3) == 0) {
				logger.traceInfo("delSkuProd", "Eliminar Exitoso");
				respuesta = Boolean.TRUE;
			} else {
				logger.traceInfo("delSkuProd", "Eliminar Error : " + cs.getObject(4));
				respuesta = Boolean.FALSE;
			}
			
		} catch (Exception e) {
			logger.traceError("delSkuProd", "Error en delSkuProd: ", e);
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("delSkuProd", "Finalizado", "delSkuProd: " + respuesta);
		return respuesta;
	}
	
}
