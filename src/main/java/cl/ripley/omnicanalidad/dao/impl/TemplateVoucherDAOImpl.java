package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.dao.TemplateVoucherDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementacion de {@linkplain TemplateVoucherDAO}
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 19-08-2016
 * Versiones:
 * <ul>
 *  <li>19-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
@Repository
public class TemplateVoucherDAOImpl implements TemplateVoucherDAO {
	
	private static final AriLog logger = new AriLog(TemplateVoucherDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;

	@Override
	public List<TemplateVoucher> getTemplateVoucherList(TemplateVoucher tv) {
		logger.initTrace("getTemplateVoucherList", "Ingreso a metodo; tv=" + tv);

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<TemplateVoucher> temps = new ArrayList<TemplateVoucher>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_TEMPLATE_VOUCHER.GET_TEMPLATE_VOUCHER_LIST(?,?,?,?,?)}");
			if(tv != null && tv.getLlave() != null) {
				cs.setString(1, tv.getLlave());
			} else {
				cs.setNull(1, OracleTypes.VARCHAR);
			}
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(3);
			BigDecimal codigo = cs.getBigDecimal(5);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("getTemplateVoucherList", "query: " + query);
			logger.traceInfo("getTemplateVoucherList", "codigo: " + codigo);
			logger.traceInfo("getTemplateVoucherList", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(2);
			
			while(rs != null && rs.next()) {
				TemplateVoucher temp = new TemplateVoucher();
				temp.setEstado(rs.getInt("ESTADO"));
				temp.setHtml(rs.getString("HTML"));
				temp.setLlave(rs.getString("LLAVE"));
				temp.setNombre(rs.getString("NOMBRE"));
				temp.setUrl(rs.getString("URL"));
				temps.add(temp);
			}
			
		} catch(Exception e) {
			logger.traceError("getTemplateVoucherList Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getTemplateVoucherList", "Finalizado", "Tama�o lista: " + temps.size());
		
		return temps;
	}

	@Override
	public boolean updActivarTemplate(TemplateVoucher tv) {
		logger.initTrace("updActivarTemplate", "Ingreso a metodo; tv=" + tv);

		Connection conn = null;
		CallableStatement cs = null;
		boolean resultado = Boolean.TRUE;
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_TEMPLATE_VOUCHER.UPD_ACTIVA_TEMPLATE(?,?,?,?,?)}");
			cs.setString(1, tv.getLlave());
			cs.setInt(2, tv.getEstado());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(3);
			BigDecimal codigo = cs.getBigDecimal(5);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("updActivarTemplate", "query: " + query);
			logger.traceInfo("updActivarTemplate", "codigo: " + codigo);
			logger.traceInfo("updActivarTemplate", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch(Exception e) {
			logger.traceError("updActivarTemplate Exception", e);
			resultado = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("updActivarTemplate", "Finalizado", "resultado: " + resultado);
		
		return resultado;
	}

	@Override
	public boolean updTemplate(TemplateVoucher tv) {
		logger.initTrace("updTemplate", "Ingreso a metodo; tv=" + tv);

		Connection conn = null;
		CallableStatement cs = null;
		boolean resultado = Boolean.TRUE;
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_TEMPLATE_VOUCHER.UPD_TEMPLATE(?,?,?,?,?)}");
			cs.setString(1, tv.getLlave());
			cs.setString(2, tv.getUrl());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(3);
			BigDecimal codigo = cs.getBigDecimal(5);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("updTemplate", "query: " + query);
			logger.traceInfo("updTemplate", "codigo: " + codigo);
			logger.traceInfo("updTemplate", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch(Exception e) {
			logger.traceError("updTemplate Exception", e);
			resultado = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("updTemplate", "Finalizado", "resultado: " + resultado);
		
		return resultado;
	}

	@Override
	public boolean updTemplateHtml(TemplateVoucher tv) {
		logger.initTrace("updTemplateHtml", "Ingreso a metodo; tv=" + tv);

		Connection conn = null;
		CallableStatement cs = null;
		boolean resultado = Boolean.TRUE;
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_TEMPLATE_VOUCHER.UPD_TEMPLATE_HTML(?,?,?,?,?)}");
			cs.setString(1, tv.getLlave());
			cs.setString(2, tv.getHtml());
			cs.registerOutParameter(3, OracleTypes.CLOB);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			Scanner s = new Scanner(cs.getCharacterStream(3));
			s.useDelimiter("\\A");
			
			String query = s.next();
			BigDecimal codigo = cs.getBigDecimal(5);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("updTemplateHtml", "query: " + query);
			logger.traceInfo("updTemplateHtml", "codigo: " + codigo);
			logger.traceInfo("updTemplateHtml", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch(Exception e) {
			logger.traceError("updTemplateHtml Exception", e);
			resultado = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("updTemplateHtml", "Finalizado", "resultado: " + resultado);
		
		return resultado;
	}

}
