package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ProgramacionCierreCaja;
import cl.ripley.omnicanalidad.dao.ProgramacionCierreCajaDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementacion de {@linkplain ProgramacionCierreCajaDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class ProgramacionCierreCajaDAOImpl implements ProgramacionCierreCajaDAO {

	private static final AriLog logger = new AriLog(ProgramacionCierreCajaDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Override
	public List<ProgramacionCierreCaja> getProgramacionCierreCajaList() {
		logger.initTrace("getProgramacionCierreCajaList", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ProgramacionCierreCaja> progs = new ArrayList<ProgramacionCierreCaja>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.GET_PROG_CIERRE_CAJA_LIST(?,?,?,?,?)}");
			
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(2);
			BigDecimal codigo = cs.getBigDecimal(4);
			String mensaje = cs.getString(3);
			
			logger.traceInfo("getProgramacionCierreCajaList", "query: " + query);
			logger.traceInfo("getProgramacionCierreCajaList", "codigo: " + codigo);
			logger.traceInfo("getProgramacionCierreCajaList", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(1);
			
			while(rs != null && rs.next()) {
				ProgramacionCierreCaja prog = new ProgramacionCierreCaja();
				prog.setId(rs.getLong("ID"));
				prog.setHoraCierre(rs.getString("HORA_CIERRE"));
				prog.setTipoCierre(rs.getInt("ID_TIPO_CIERRE"));
				prog.setVolumen(rs.getLong("VOLUMEN"));
				prog.setNumeroCaja(rs.getLong("NUMERO_CAJA"));
				prog.setNumeroSucursal(rs.getLong("NUMERO_SUCURSAL"));
				progs.add(prog);
			}
			
		} catch(Exception e) {
			logger.traceError("getProgramacionCierreCajaList Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getProgramacionCierreCajaList", "Finalizado", "Tama�o lista: " + progs.size());
		
		return progs;
	}
	@Override
	public List<ProgramacionCierreCaja> getProgramacionCierreCajaList(Long sucursal) {
		logger.initTrace("getProgramacionCierreCajaList", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ProgramacionCierreCaja> progs = new ArrayList<ProgramacionCierreCaja>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.GET_SUC_PROG_CIERRE_CAJA_LIST(?,?,?,?,?)}");
			
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(2);
			BigDecimal codigo = cs.getBigDecimal(4);
			String mensaje = cs.getString(3);
			
			logger.traceInfo("getProgramacionCierreCajaList", "query: " + query);
			logger.traceInfo("getProgramacionCierreCajaList", "codigo: " + codigo);
			logger.traceInfo("getProgramacionCierreCajaList", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(1);
			
			while(rs != null && rs.next()) {
				ProgramacionCierreCaja prog = new ProgramacionCierreCaja();
				prog.setId(rs.getLong("ID"));
				prog.setHoraCierre(rs.getString("HORA_CIERRE"));
				prog.setTipoCierre(rs.getInt("ID_TIPO_CIERRE"));
				prog.setVolumen(rs.getLong("VOLUMEN"));
				prog.setNumeroCaja(rs.getLong("NUMERO_CAJA"));
				prog.setNumeroSucursal(rs.getLong("NUMERO_SUCURSAL"));
				progs.add(prog);
			}
			
		} catch(Exception e) {
			logger.traceError("getProgramacionCierreCajaList Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getProgramacionCierreCajaList", "Finalizado", "Tama�o lista: " + progs.size());
		
		return progs;
	}

	@Override
	public boolean updProgramacionCierreCaja(ProgramacionCierreCaja prog) {
		logger.initTrace("updProgramacionCierreCaja", "Ingreso a metodo; ProgramacionCierreCaja prog=" + prog);

		boolean result = Boolean.TRUE;
		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = poolBD.getConnection();
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.UPD_PROG_CIERRE_CAJA(?,?,?,?,?,?,?,?,?)}");
			if(prog.getId() != null) {
				cs.setLong(1, prog.getId());
			} else {
				cs.setNull(1, OracleTypes.NUMBER);
			}
			cs.setInt(2, prog.getTipoCierre());
			cs.setString(3, prog.getHoraCierre());
			cs.setLong(4, prog.getVolumen());
			cs.setLong(5, prog.getNumeroSucursal());
			cs.setLong(6, prog.getNumeroCaja());
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(7);
			BigDecimal codigo = cs.getBigDecimal(9);
			String mensaje = cs.getString(8);
			
			logger.traceInfo("updProgramacionCierreCaja", "query: " + query);
			logger.traceInfo("updProgramacionCierreCaja", "codigo: " + codigo);
			logger.traceInfo("updProgramacionCierreCaja", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch(Exception e) {
			logger.traceError("updProgramacionCierreCaja Exception", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("updProgramacionCierreCaja", "Finalizado", "result: " + result);
		
		return result;
	}

	@Override
	public boolean delProgramacionCierreCaja(Long numCaja) {
		logger.initTrace("delProgramacionCierreCaja", "Ingreso a metodo; numCaja=" + numCaja);

		boolean result = Boolean.TRUE;
		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.DEL_PROG_CIERRE_CAJA(?,?,?,?)}");
			cs.setLong(1, numCaja);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(2);
			BigDecimal codigo = cs.getBigDecimal(4);
			String mensaje = cs.getString(3);
			
			logger.traceInfo("delProgramacionCierreCaja", "query: " + query);
			logger.traceInfo("delProgramacionCierreCaja", "codigo: " + codigo);
			logger.traceInfo("delProgramacionCierreCaja", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			
		} catch(Exception e) {
			logger.traceError("delProgramacionCierreCaja Exception", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("delProgramacionCierreCaja", "Finalizado", "result: " + result);
		
		return result;
	}

	@Override
	public List<Long> getSucursales() {
		logger.initTrace("getSucursales", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Long> sucursales = new ArrayList<Long>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.GET_SUCURSALES(?,?,?,?)}");
			
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(2);
			BigDecimal codigo = cs.getBigDecimal(4);
			String mensaje = cs.getString(3);
			
			logger.traceInfo("getSucursales", "query: " + query);
			logger.traceInfo("getSucursales", "codigo: " + codigo);
			logger.traceInfo("getSucursales", "mensaje: " + mensaje);
			
			if(codigo.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(1);
			
			while(rs != null && rs.next()) {
				
				sucursales.add(rs.getLong("NUMERO_SUCURSAL"));
			}
			
		} catch(Exception e) {
			logger.traceError("getSucursales Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getSucursales", "Finalizado", "Tama�o lista: " + sucursales.size());
		
		return sucursales;
	}

	@Override
	public List<Long> getCajas(Long numeroSucursal) {
		logger.initTrace("getCajas", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Long> sucursales = new ArrayList<Long>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.GET_CAJAS_X_SUCURSAL(?,?,?,?,?)}");
			cs.setLong(1, numeroSucursal);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.execute();
			
			String query = cs.getString(3);
			int codigo = cs.getInt(5);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("getSucursales", "query: " + query);
			logger.traceInfo("getSucursales", "codigo: " + codigo);
			logger.traceInfo("getSucursales", "mensaje: " + mensaje);
			
			if(codigo != 0) {
				throw new Exception(""+mensaje);
			}
			
			rs = (ResultSet) cs.getObject(2);
			
			while(rs != null && rs.next()) {
				
				sucursales.add(rs.getLong("NUMERO_CAJA"));
			}
			
		} catch(Exception e) {
			logger.traceError("getCajas Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getCajas", "Finalizado", "Tama�o lista: " + sucursales.size());
		
		return sucursales;
	}
	
	
	@Override
	public List<ProgramacionCierreCaja> getProgramacionCierreCajaList(Long numeroSucursal, Long numeroCaja) {
		logger.initTrace("getProgramacionCierreCajaList", "Ingreso a metodo");

		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ProgramacionCierreCaja> progs = new ArrayList<ProgramacionCierreCaja>();
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_CAJA.GET_PROG_CIERRE_CAJA_SUC_LIST(?,?,?,?,?,?)}");
			cs.setLong(1, numeroSucursal);
			cs.setLong(2, numeroCaja);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			
			cs.execute();
			
			String query = cs.getString(4);
			int codigo = cs.getInt(6);
			String mensaje = cs.getString(5);
			
			logger.traceInfo("getProgramacionCierreCajaList", "query: " + query);
			logger.traceInfo("getProgramacionCierreCajaList", "codigo: " + codigo);
			logger.traceInfo("getProgramacionCierreCajaList", "mensaje: " + mensaje);
			
			if(codigo != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(3);
			
			while(rs != null && rs.next()) {
				ProgramacionCierreCaja prog = new ProgramacionCierreCaja();
				prog.setId(rs.getLong("ID"));
				prog.setHoraCierre(rs.getString("HORA_CIERRE"));
				prog.setTipoCierre(rs.getInt("ID_TIPO_CIERRE"));
				prog.setVolumen(rs.getLong("VOLUMEN"));
				prog.setNumeroCaja(rs.getLong("NUMERO_CAJA"));
				prog.setNumeroSucursal(rs.getLong("NUMERO_SUCURSAL"));
				progs.add(prog);
			}
			
		} catch(Exception e) {
			logger.traceError("getProgramacionCierreCajaList Exception", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		logger.endTrace("getProgramacionCierreCajaList", "Finalizado", "Tama�o lista: " + progs.size());
		
		return progs;
	}

	
}
