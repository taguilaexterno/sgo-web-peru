package cl.ripley.omnicanalidad.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Precio;
import cl.ripley.omnicanalidad.bean.ProductosDTE;
import cl.ripley.omnicanalidad.dao.SKURestDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Implementación de {@linkplain SKURestDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class SKURestDAOImpl implements SKURestDAO {
	private static final AriLog logger = new AriLog(SKURestDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	public String ConsultaSKURestSvcRest(String sku, String url) {
		String Respuesta = "";

	  try {
		  
		  String urlFinal = url.replace(Constantes.SKU_HOLDER, sku);

		URL url2 = new URL(urlFinal);

		logger.traceInfo("SKURestDAO llamada REST. URL : " + url2, "Ingreso metodo");
		
		HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
		conn.setConnectTimeout(180000);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		
		logger.traceInfo("SKURestDAO llamada REST. Response Code : " + conn.getResponseCode(), "Salida metodo");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			Respuesta = Respuesta + output;
		}
		logger.traceInfo("SKURestDAO llamada REST. Respuesta : " + Respuesta, "Salida Servicio Rest");
		
		//Cerrar conexion
		conn.disconnect();
	

	  } catch (MalformedURLException e) {
		  logger.traceError("SKURestDAO ERROR", "Mensaje : " + e.getMessage(), e);
		  Respuesta=null;
	  } catch (IOException e) {
		  logger.traceError("SKURestDAO ERROR", "Mensaje : " + e.getMessage(), e);
		  Respuesta=null;
	  }
	  
	  return Respuesta;
	}
	
	public ProductosDTE ConsultaSKURest(String sku, String url) {
		
		JsonParser parser = new JsonParser();
		JsonObject response = null;
		ProductosDTE producto = new ProductosDTE();
		List<Precio> preciosList = new ArrayList<Precio>();
		Precio precio = null;
		String linea = "";
		
		try {
		
		//Servicio Rest, obtener campos en vez del dummy
		linea=ConsultaSKURestSvcRest(sku, url);
		
		//Si consulta Rest es nula
		if(linea == null) {
			throw new Exception("Error consulta rest SKU");
		}
		
		response = parser.parse(linea).getAsJsonObject();
		
		JsonArray catalogo = response.getAsJsonArray("CatalogEntryView");
		JsonElement element = catalogo.get(0);
		JsonObject obj = element.getAsJsonObject();
		
		producto.setNombre(obj.get("name").getAsString());
		producto.setDescripcion(obj.get("longDescription").getAsString());
		producto.setDescripcionCorta(obj.get("shortDescription").getAsString());
		producto.setCodigoArticulo(sku);
		
		for(int i=0; i<obj.get("Price").getAsJsonArray().size(); i++) {
			JsonElement priceElement = obj.get("Price").getAsJsonArray().get(i);
			JsonObject objPrice = priceElement.getAsJsonObject();			
			String descripcion = objPrice.get("priceDescription").getAsString();
			float valor = objPrice.get("priceValue").getAsFloat();
			
			precio = new Precio();
			precio.setDescripcion(descripcion);
			precio.setValor(new BigDecimal(valor));
			preciosList.add(precio);
		}
		
		producto.setPrecios(preciosList);
		
		} catch(Exception e) {
			logger.traceError("SKURestDAO ConsultaSKURest ERROR", "Mensaje : " + e.getMessage(), e);
			producto = null;
		}
		
		return producto;
	}
}
