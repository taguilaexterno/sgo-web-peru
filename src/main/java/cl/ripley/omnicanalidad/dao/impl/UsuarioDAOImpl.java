package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Perfil;
import cl.ripley.omnicanalidad.bean.Permiso;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.dao.UsuarioDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

/**Implementación de {@linkplain UsuarioDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li> 
 * </ul>
 */
@Repository
public class UsuarioDAOImpl implements UsuarioDAO {
	private static final AriLog logger = new AriLog(UsuarioDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;

	public Usuario buscarUsuario(String usuario){
		logger.initTrace("buscarUsuario", "String usuario: "+usuario);
		Usuario usuarioBD = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_OBTENER_USUARIO(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, usuario);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();    
			rs = (ResultSet) cs.getObject(3);
			logger.traceInfo("buscarUsuario", "Query: "+((String)cs.getObject(2)));
			if (rs != null && rs.next()) {
				usuarioBD = new Usuario();
				usuarioBD.setCodUsuario(rs.getInt("COD_USUARIO"));
				usuarioBD.setCodPerfil(rs.getLong("COD_PERFIL"));
				usuarioBD.setCodEstado(rs.getInt("COD_ESTADO"));
				usuarioBD.setNombre(rs.getString("NOMBRE"));
				usuarioBD.setApePaterno(rs.getString("APE_PATERNO"));
				usuarioBD.setApeMaterno(rs.getString("APE_MATERNO"));
				usuarioBD.setEmail(rs.getString("EMAIL"));
				usuarioBD.setUsuario(rs.getString("USUARIO"));
				usuarioBD.setPassword(rs.getString("PASSWORD"));
				usuarioBD.setFecCreacion(rs.getDate("FEC_CREACION"));
				usuarioBD.setFecFinVigencia(rs.getString("FEC_FIN_VIGENCIA")); 
			}
		} catch (Exception e) {
			logger.traceError("buscarUsuario Exception", "Error durante la ejecuci�n de buscarUsuario", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("buscarUsuario", "Finalizado", "Usuario: "+((usuarioBD != null)?usuarioBD.getNombre():"null"));
		return usuarioBD;
	}

	@Override
	public Usuario getUsuarioConPerfil(Usuario usuario) {
		logger.initTrace("getUsuarioConPerfil", "Id usuario: "+usuario.getCodUsuario());

		Usuario usuarioBD = usuario;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_OBTENER_PERFIL_USUARIO(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, usuario.getCodPerfil());
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();  

			rs = (ResultSet) cs.getObject(3);

			logger.traceInfo("getUsuarioConPerfil", "Query: "+((String)cs.getObject(2)));

			Perfil perfil = null;

			if(rs != null) {
				while(rs.next()) {
					if(perfil == null) {
						perfil = new Perfil();
						perfil.setId(rs.getLong("ID_PERF"));
						perfil.setActivo(rs.getLong("ACTIVO_PERF") == 1);
						perfil.setFecCreacion(rs.getDate("FEC_CREACION_PERF"));
						perfil.setFecUpd(rs.getDate("FEC_UPD_PERF"));
						perfil.setNombrePerfil(rs.getString("NOMBRE_PERFIL"));
						perfil.setUsuarioCrea(rs.getString("USUARIO_CREA_PERF"));
						perfil.setUsuarioUpd(rs.getString("USUARIO_UPD_PERF"));
						perfil.setModulos(new ArrayList<Modulo>());
					}

					Modulo mod = new Modulo();
					mod.setId(rs.getLong("ID_MOD"));
					mod.setNombreModulo(rs.getString("NOMBRE_MODULO"));
					mod.setUrl(rs.getString("URL"));
					mod.setFecCrea(rs.getDate("FEC_CREACION_MOD"));

					Permiso permiso = new Permiso();
					permiso.setId(rs.getLong("ID_PERMISO"));
					permiso.setNombrePermiso(rs.getString("NOMBRE_PERMISO"));
					permiso.setFecCrea(rs.getDate("FEC_CREA_PERMISO"));

					mod.setPermiso(permiso);

					perfil.getModulos().add(mod);
				}
			}

			usuarioBD.setCodPerfil(perfil != null ? perfil.getId() : null);
			usuarioBD.setPerfil(perfil);

		} catch (Exception e) {
			logger.traceError("getUsuarioConPerfil Exception", "Error durante la ejecuci�n de getUsuarioConPerfil", e);
			return null;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getUsuarioConPerfil", "Finalizado", "Usuario: "+((usuarioBD != null)?usuarioBD.getNombre():"null"));
		return usuarioBD;
	}

	@Override
	public boolean guardarUsuario(Usuario usuario, Error error) throws ParseException {
		logger.initTrace("guardarUsuario", "Usuario: "+usuario.getUsuario());

		boolean result = Boolean.TRUE;
		
		
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		Connection conn = null;
		CallableStatement cs = null;						
		int valor = getParamFechaFin();
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(date);
		calendar.add(Calendar.MONTH,  valor);
		java.sql.Date fechaFin = new java.sql.Date(calendar.getTime().getTime());

		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_GUARDAR_USUARIO(?,?,?,?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, usuario.getCodPerfil());
			cs.setString(2, usuario.getNombre().trim().toUpperCase());
			cs.setString(3, usuario.getApePaterno().trim().toUpperCase());
			cs.setString(4, usuario.getApeMaterno().trim().toUpperCase());
			cs.setString(5, usuario.getEmail().trim().toUpperCase());
			cs.setString(6, usuario.getUsuario().trim().toUpperCase());
			cs.setString(7, usuario.getEncryptedPass());
			cs.setString(8, format.format(date));
			cs.setString(9, format.format(fechaFin));
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.registerOutParameter(11, OracleTypes.NUMBER);
			cs.registerOutParameter(12, OracleTypes.VARCHAR);
			cs.execute();  

			String query = (String)cs.getObject(10);
			BigDecimal codSalida = (BigDecimal)cs.getObject(11);
			String mensaje = (String)cs.getObject(12);
			
			logger.traceInfo("guardarUsuario", "Query: "+query);
			logger.traceInfo("guardarUsuario", "Codigo Salida: "+codSalida);
			logger.traceInfo("guardarUsuario", "Mensaje Salida: "+mensaje);
			
			if(codSalida.intValue() != 0) {
				error.setCodError(codSalida.intValue());
				error.setErrorMsg(mensaje);
				throw new Exception(mensaje);
			}


		} catch (Exception e) {
			logger.traceError("guardarUsuario Exception", "Error durante la ejecuci�n de getUsuarioConPerfil", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("guardarUsuario", "Finalizado", "Usuario creado: " + (result ? usuario.getUsuario():"null"));
		return result;
	}

	private Date Date(String format) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean actualizarUsuario(Usuario usuario) {
		logger.initTrace("actualizarUsuario", "Usuario: "+usuario.getUsuario());

		boolean result = Boolean.TRUE;
		
		Connection conn = null;
		CallableStatement cs = null;
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		int valor = getParamFechaFin();
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(date);
		calendar.add(Calendar.MONTH,  valor);
		java.sql.Date fechaFin = new java.sql.Date(calendar.getTime().getTime());
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_ACTUALIZAR_USUARIO(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, usuario.getCodUsuario());
			
			if(usuario.getNombre() != null) {
				cs.setString(2, usuario.getNombre().trim().toUpperCase());
			} else {
				cs.setNull(2, OracleTypes.VARCHAR);
			}
			
			if(usuario.getApePaterno() != null) {
				cs.setString(3, usuario.getApePaterno().trim().toUpperCase());
			} else {
				cs.setNull(3, OracleTypes.VARCHAR);
			}
			
			if(usuario.getApeMaterno() != null) {
				cs.setString(4, usuario.getApeMaterno().trim().toUpperCase());
			} else {
				cs.setNull(4, OracleTypes.VARCHAR);
			}
			
			if(usuario.getEmail() != null) {
				cs.setString(5, usuario.getEmail().trim().toUpperCase());
			} else {
				cs.setNull(5, OracleTypes.VARCHAR);
			}
			
			if(usuario.getCodPerfil() != null) {
				cs.setLong(6, usuario.getCodPerfil());
			} else {
				cs.setNull(6, OracleTypes.NUMBER);
			}
			
			if(usuario.getPassword() != null && !"".equals(usuario.getPassword())) {
				cs.setString(7, usuario.getEncryptedPass());
			} else {
				cs.setNull(7, OracleTypes.VARCHAR);
			}
			
			if(usuario.getCodEstado() != null) {
				cs.setLong(8, usuario.getCodEstado());
			} else {
				cs.setNull(8, OracleTypes.NUMBER);
			}			
			if(usuario.getVigencia() != null) {
				if(usuario.getVigencia().equals("Actualizar")) {
					cs.setString(9, format.format(date));
					cs.setString(10, format.format(fechaFin));	
				}else {
					cs.setNull(9, OracleTypes.VARCHAR);
					cs.setNull(10, OracleTypes.VARCHAR);	
				}
			}else {
				cs.setNull(9, OracleTypes.VARCHAR);
				cs.setNull(10, OracleTypes.VARCHAR);	
			}			
			
			cs.registerOutParameter(11, OracleTypes.VARCHAR);
			cs.registerOutParameter(12, OracleTypes.NUMBER);
			cs.registerOutParameter(13, OracleTypes.VARCHAR);
			cs.execute();  

			String query = (String)cs.getObject(11);
			BigDecimal codSalida = (BigDecimal)cs.getObject(12);
			String mensaje = (String)cs.getObject(13);
			
			logger.traceInfo("actualizarUsuario", "Query: "+query);
			logger.traceInfo("actualizarUsuario", "Codigo Salida: "+codSalida);
			logger.traceInfo("actualizarUsuario", "Mensaje Salida: "+mensaje);
			
			if(codSalida.intValue() != 0) {
				throw new Exception(mensaje);
			}


		} catch (Exception e) {
			logger.traceError("actualizarUsuario Exception", "Error durante la ejecuci�n de getUsuarioConPerfil", e);
			result = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("actualizarUsuario", "Finalizado", "Usuario actualizado: " + (result ? usuario.getUsuario():"null"));
		return result;
	}

	@Override
	public List<Usuario> getUsuarios() {
		logger.initTrace("getUsuarios", "Ingreso a metodo");
		Usuario usuarioBD = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Usuario> usuarios = new ArrayList<Usuario>(50);
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_OBTENER_USUARIOS(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute(); 
			
			String query = cs.getString(1);
			BigDecimal codSalida = cs.getBigDecimal(3);
			String mensaje = cs.getString(4);
			
			logger.traceInfo("getUsuarios", "Query: "+query);
			logger.traceInfo("getUsuarios", "Codigo Salida: "+codSalida);
			logger.traceInfo("getUsuarios", "Mensaje Salida: "+mensaje);
			
			if(codSalida.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(2);
			while(rs != null && rs.next()) {
				usuarioBD = new Usuario();
				usuarioBD.setCodUsuario(rs.getInt("COD_USUARIO"));
				usuarioBD.setCodPerfil(rs.getLong("COD_PERFIL"));
				usuarioBD.setCodEstado(rs.getInt("COD_ESTADO"));
				usuarioBD.setNombre(rs.getString("NOMBRE"));
				usuarioBD.setApePaterno(rs.getString("APE_PATERNO"));
				usuarioBD.setApeMaterno(rs.getString("APE_MATERNO"));
				usuarioBD.setEmail(rs.getString("EMAIL"));
				usuarioBD.setUsuario(rs.getString("USUARIO"));
				usuarioBD.setPassword(rs.getString("PASSWORD"));
				usuarioBD.setFecCreacion(rs.getDate("FEC_CREACION"));
				usuarioBD.setFecFinVigencia(rs.getString("FEC_FIN_VIGENCIA"));
				usuarios.add(usuarioBD);
			}
		} catch (Exception e) {
			
			logger.traceError("getUsuarios Exception", "Error durante la ejecuci�n de getUsuarios", e);
			
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getUsuarios", "Finalizado", "Tama�o lista: " + usuarios.size());
		return usuarios;
	}

	@Override
	public List<Usuario> getUsuarios(Long codPerfil) {
		logger.initTrace("getUsuarios(Long)", "Ingreso a metodo");
		Usuario usuarioBD = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Usuario> usuarios = new ArrayList<Usuario>(50);
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_OBTENER_USR_X_PERFIL(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, codPerfil);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute(); 
			
			String query = cs.getString(2);
			BigDecimal codSalida = cs.getBigDecimal(4);
			String mensaje = cs.getString(5);
			
			logger.traceInfo("getUsuarios", "Query: "+query);
			logger.traceInfo("getUsuarios", "Codigo Salida: "+codSalida);
			logger.traceInfo("getUsuarios", "Mensaje Salida: "+mensaje);
			
			if(codSalida.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(3);
			while(rs != null && rs.next()) {
				usuarioBD = new Usuario();
				usuarioBD.setCodUsuario(rs.getInt("COD_USUARIO"));
				usuarioBD.setCodPerfil(rs.getLong("COD_PERFIL"));
				usuarioBD.setCodEstado(rs.getInt("COD_ESTADO"));
				usuarioBD.setNombre(rs.getString("NOMBRE"));
				usuarioBD.setApePaterno(rs.getString("APE_PATERNO"));
				usuarioBD.setApeMaterno(rs.getString("APE_MATERNO"));
				usuarioBD.setEmail(rs.getString("EMAIL"));
				usuarioBD.setUsuario(rs.getString("USUARIO"));
				usuarioBD.setPassword(rs.getString("PASSWORD"));
				usuarioBD.setFecCreacion(rs.getDate("FEC_CREACION"));
				usuarios.add(usuarioBD);
			}
		} catch (Exception e) {
			
			logger.traceError("getUsuarios(Long) Exception", "Error durante la ejecuci�n de getUsuarios", e);
			
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getUsuarios(Long)", "Finalizado", "Tama�o lista: " + usuarios.size());
		return usuarios;
	}

	@Override
	public List<Usuario> buscarUsuarioLike(String usuario) {
		logger.initTrace("buscarUsuarioLike", "String usuario: "+usuario);
		List<Usuario> usrs = new ArrayList<Usuario>(20);
		Usuario usuarioBD = null;
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_USUARIO.PROC_OBTENER_USUARIO_LIKE(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, usuario);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();    
			rs = (ResultSet) cs.getObject(3);
			logger.traceInfo("buscarUsuarioLike", "Query: "+((String)cs.getObject(2)));
			while (rs != null && rs.next()) {
				usuarioBD = new Usuario();
				usuarioBD.setCodUsuario(rs.getInt("COD_USUARIO"));
				usuarioBD.setCodPerfil(rs.getLong("COD_PERFIL"));
				usuarioBD.setCodEstado(rs.getInt("COD_ESTADO"));
				usuarioBD.setNombre(rs.getString("NOMBRE"));
				usuarioBD.setApePaterno(rs.getString("APE_PATERNO"));
				usuarioBD.setApeMaterno(rs.getString("APE_MATERNO"));
				usuarioBD.setEmail(rs.getString("EMAIL"));
				usuarioBD.setUsuario(rs.getString("USUARIO"));
				usuarioBD.setPassword(rs.getString("PASSWORD"));
				usuarioBD.setFecCreacion(rs.getDate("FEC_CREACION"));
				usuarioBD.setFecFinVigencia(rs.getString("FEC_FIN_VIGENCIA"));
				usrs.add(usuarioBD);
			}
		} catch (Exception e) {
			logger.traceError("buscarUsuarioLike Exception", "Error durante la ejecuci�n de buscarUsuarioLike", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("buscarUsuarioLike", "Finalizado", "Tama�o Lista: "+usrs.size());
		return usrs;
	}
	
	public int getParamFechaFin() {
		
		String result = "";
		String param = "PARAM_MES_VIGENCIA";
		String selectSQL = null;
		Connection conn = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();			
			selectSQL ="SELECT VALUE FROM XSTOREPROPERTIES WHERE NAME = '"+param+"'";
						        	
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			rs = ps.executeQuery();		
			
			while (rs.next()) {
				result = rs.getString(1);
			} 			
			
		}catch (Exception e) {
			// TODO: handle exception
			logger.traceError("getParamFechaFin", "Error en getParamFechaFin: ", e);
			poolBD.closeConnection(conn);
		
		} finally {
			poolBD.closeConnection(conn);
		}
		logger.endTrace("getParametrosXTipo", "Finalizado", "parametros: "+result);
		return Integer.parseInt(result);
	}
}
