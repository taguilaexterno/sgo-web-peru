package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.text.StrBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVenta;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.DatoFacturacion;
import cl.ripley.omnicanalidad.bean.Despacho;
import cl.ripley.omnicanalidad.bean.DetalleBackoffice;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.DetalleCompra;
import cl.ripley.omnicanalidad.bean.DetalleDespacho;
import cl.ripley.omnicanalidad.bean.DetalleOC;
import cl.ripley.omnicanalidad.bean.DocumentoElectronico;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;
import cl.ripley.omnicanalidad.bean.MotivoRechazo;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.NotaVentaRechazo;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.TarjetaBancaria;
import cl.ripley.omnicanalidad.bean.TarjetaRegaloEmpresa;
import cl.ripley.omnicanalidad.bean.TarjetaRipley;
import cl.ripley.omnicanalidad.bean.TipoDespacho;
import cl.ripley.omnicanalidad.bean.TipoDoc;
import cl.ripley.omnicanalidad.bean.Validacion;
import cl.ripley.omnicanalidad.bean.Vendedor;
import cl.ripley.omnicanalidad.bean.XValidacionOc;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.ParametroDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

/**Implementación de {@linkplain OrdenCompraDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class OrdenCompraDAOImpl implements OrdenCompraDAO {
	private static final AriLog logger = new AriLog(OrdenCompraDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private PoolBD poolBD;
	
	@Autowired
	private ParametroDAO parametroDao;
	
	@Override
	public List<OrdenesDeCompra> obtenerOrdenes(Integer nroCaja, Integer nroSucusal, Integer nroEstado, Integer indicadorMkp, Long oc) {
		logger.initTrace("obtenerOrdenes", "Integer nroCaja: "+nroCaja+" Integer nroSucusal: "+nroSucusal+" Integer nroEstado: "+nroEstado+" Integer indicadorMkp: "+indicadorMkp + " Long oc: " + oc);

		ArrayList<OrdenesDeCompra> ordenesCompra = new ArrayList<OrdenesDeCompra>();
		Parametro parametroBD = parametroDao.getParametro("GLOSA_BLOQUEO_OC");
		Parametro parametroBDDespachoMr = parametroDao.getParametro("DESPACHO_MERCADO_RIPLEY");
		Parametro parametroBDDespachoClub = parametroDao.getParametro("DESPACHO_CLUBES");
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_OBTENER_OC(?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			if (nroCaja == null){
				cs.setNull(1, OracleTypes.NUMBER);
			} else{
				cs.setInt(1, nroCaja);
			}
			cs.setInt(2, nroSucusal);
			cs.setInt(3, nroEstado);
			if (indicadorMkp == null){
				cs.setNull(4, OracleTypes.NUMBER);
			} else{
				cs.setInt(4, indicadorMkp);
			}
			if(oc == null) {
				cs.setNull(5, OracleTypes.NUMBER);
			} else {
				cs.setLong(5, oc);
			}
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.CURSOR);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.execute();
			rs = (ResultSet) cs.getObject(7);
			logger.traceInfo("obtenerOrdenes", "SGO_MANEJO_OC.PROC_OBTENER_OC - Query: " + ((String) cs.getObject(6)));
			if (rs != null) {
				while (rs.next()) {
					String tipoPago = "";
					String nCuotas = "-";
					
					
					NotaVenta notaVenta = new NotaVenta();
					notaVenta.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
					notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
					notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
					notaVenta.setHorasAdministrativas(rs.getInt("HORAS_ADMINISTRATIVAS"));
					notaVenta.setEstado(rs.getInt("ESTADO"));
					notaVenta.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
					notaVenta.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
					notaVenta.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
					notaVenta.setRutComprador(rs.getInt("RUT_COMPRADOR"));
					notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
					notaVenta.setNumeroSucursal(rs.getInt("NUMERO_SUCURSAL"));
					notaVenta.setCodigoRegalo(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_REGALO")));
					notaVenta.setTipoRegalo(rs.getInt("TIPO_REGALO"));
					notaVenta.setNumeroBoleta(rs.getInt("NUMERO_BOLETA"));
					notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
					notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
					notaVenta.setNumeroCaja(rs.getInt("NUMERO_CAJA"));
					notaVenta.setCorrelativoBoleta(rs.getInt("CORRELATIVO_BOLETA"));
					notaVenta.setRutBoleta(rs.getInt("RUT_BOLETA"));
					notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
					notaVenta.setNumNotaCredito(rs.getInt("NUM_NOTA_CREDITO"));
					notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
					notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
					notaVenta.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
					notaVenta.setCorrelativoNotaCredito(rs.getInt("CORRELATIVO_NOTA_CREDITO"));
					notaVenta.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
					notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
					notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
					notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					notaVenta.setTipoDoc(rs.getInt("NOTAVENTA_TIPODOC"));
					notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
					notaVenta.setFolioNcSii(rs.getInt("FOLIO_NC_SII"));
					notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
					notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					notaVenta.setUsuario(rs.getString("USUARIO"));
					notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					notaVenta.setEstado_COC(rs.getInt("ESTADO_COC"));
                    notaVenta.setEstado_CS(rs.getInt("ESTADO_CS"));
                    notaVenta.setEstado_CS_DESC(rs.getString("ESTADO_CS_DESC"));
					
                    /*
					if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 88){
						notaVenta.setBloqueado("Si");
						notaVenta.setGlosaBloqueo(parametroBD.getValor());
					}else{
						if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 8){
							notaVenta.setBloqueado("Si");
							notaVenta.setGlosaBloqueo(parametroBD.getValor());
						
					}else{
						notaVenta.setBloqueado("No");
						notaVenta.setGlosaBloqueo("");
						}
					}
					*/
					/* 20191001: Se agrega estados Cybersource 94, 95 */
					if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 88){
                        notaVenta.setBloqueado("Si");
                        notaVenta.setGlosaBloqueo(parametroBD.getValor());
					}else if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 8){
                        notaVenta.setBloqueado("Si");
                        notaVenta.setGlosaBloqueo(parametroBD.getValor());
                    }else if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 94){
                        notaVenta.setBloqueado("Si");
                        notaVenta.setGlosaBloqueo(parametroBD.getValor());
                    }else if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 95){
                        notaVenta.setBloqueado("Si");
                        notaVenta.setGlosaBloqueo(parametroBD.getValor());
                    }else{
                        notaVenta.setBloqueado("No");
                        notaVenta.setGlosaBloqueo("");
					}
				
					TarjetaRipley tarjetaRipley = new TarjetaRipley();
					tarjetaRipley.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
					tarjetaRipley.setAdministradora(rs.getInt("ADMINISTRADORA"));
					tarjetaRipley.setEmisor(rs.getInt("EMISOR"));
					tarjetaRipley.setTarjeta(rs.getInt("TARJETA"));
					tarjetaRipley.setRutTitular(rs.getInt("RUT_TITULAR"));
					tarjetaRipley.setRutPoder(rs.getInt("RUT_PODER"));
					tarjetaRipley.setPlazo(rs.getInt("PLAZO"));
					tarjetaRipley.setDiferido(rs.getInt("DIFERIDO"));
					tarjetaRipley.setMontoCapital(rs.getBigDecimal("MONTO_CAPITAL"));
					tarjetaRipley.setMontoPie(rs.getBigDecimal("MONTO_PIE"));
					tarjetaRipley.setDescuentoCar(rs.getBigDecimal("DESCUENTO_CAR"));
					tarjetaRipley.setCodigoDescuento(rs.getInt("CODIGO_DESCUENTO"));
					tarjetaRipley.setPrefijoTitular(rs.getInt("PREFIJO_TITULAR"));
					tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
					tarjetaRipley.setTipoCliente(rs.getInt("TIPO_CLIENTE"));
					tarjetaRipley.setTipoCredito(rs.getInt("TIPO_CREDITO"));
					tarjetaRipley.setPrefijoPoder(rs.getInt("PREFIJO_PODER"));
					tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
					tarjetaRipley.setValorCuota(rs.getBigDecimal("VALOR_CUOTA"));
					tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
					tarjetaRipley.setSernacFinanCae(rs.getInt("SERNAC_FINAN_CAE"));
					tarjetaRipley.setSernacFinanCt(rs.getInt("SERNAC_FINAN_CT"));
					tarjetaRipley.setTvtriMntMntFnd(rs.getInt("TVTRI_MNT_MNT_FND"));
					tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
					tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
					tarjetaRipley.setTvtriCodImpTsaEfe(rs.getInt("TVTRI_COD_IMP_TSA_EFE"));
					tarjetaRipley.setTvtriMntMntEva(rs.getInt("TVTRI_MNT_MNT_EVA"));
					tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
					tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
					tarjetaRipley.setCodigoCore(rs.getInt("CODIGO_CORE"));
					tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));
					if (tarjetaRipley.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_RIPLEY_;
						nCuotas = String.valueOf(tarjetaRipley.getPlazo());
					}

					TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
					tarjetaBancaria.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
					tarjetaBancaria.setTipoTarjeta(rs.getString("TIPO_TARJETA"));
					tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
					tarjetaBancaria.setMontoTarjeta(rs.getBigDecimal("MONTO_TARJETA"));
					//tarjetaBancaria.setVd(rs.getString("VD"));
					//tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
					//tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
					if (tarjetaBancaria.getCorrelativoVenta()!= null){
						if (tarjetaBancaria.getVd() != null){
							if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_M)){
								tipoPago = Constantes.TIPO_PAGO_MERCADO_PAGO_;
							}
							else if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_S)){
								tipoPago = Constantes.TIPO_PAGO_TBK_DEBITO_;
							} else {
								tipoPago = Constantes.TIPO_PAGO_TBK_CREDITO_;
							}
						}
					}
					//tarjetaBancaria.setCodigoConvenio(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));

					TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
					tarjetaRegaloEmpresa.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
					tarjetaRegaloEmpresa.setAdministradora(rs.getInt("TRE_ADMINISTRADORA"));
					tarjetaRegaloEmpresa.setEmisor(rs.getInt("TRE_EMISOR"));
					tarjetaRegaloEmpresa.setTarjeta(rs.getInt("TRE_TARJETA"));
					tarjetaRegaloEmpresa.setCodigoAutorizador(rs.getInt("TRE_COD_AUTORIZADOR"));
					tarjetaRegaloEmpresa.setRutCliente(rs.getInt("RUT_CLIENTE"));
					tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
					tarjetaRegaloEmpresa.setMonto(rs.getBigDecimal("MONTO"));
					tarjetaRegaloEmpresa.setNroTarjeta(rs.getInt("NRO_TARJETA"));
					tarjetaRegaloEmpresa.setFlag(rs.getInt("FLAG"));
					if (tarjetaRegaloEmpresa.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_TRE;
					}

					DatoFacturacion datoFacturacion = new DatoFacturacion();
					datoFacturacion.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
					datoFacturacion.setAddressId(rs.getInt("FACT_ADDRESS_ID"));
					datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
					datoFacturacion.setComunaFacturaDes(rs.getString("COMUNA_FACTURA_DES"));
					datoFacturacion.setCiudadFacturaDes(rs.getString("CIUDAD_FACTURA_DES"));
					datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
					datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
					datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
					datoFacturacion.setGiroFacturaId(rs.getInt("GIRO_FACTURA_ID"));
					datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
					datoFacturacion.setRegionFacturaId(rs.getInt("REGION_FACTURA_ID"));
					datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

					Despacho despacho = new Despacho();
					despacho.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
					despacho.setRutDespacho(rs.getInt("RUT_DESPACHO"));
					despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
					despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
					despacho.setSucursalDespacho(rs.getInt("SUCURSAL_DESPACHO"));
					despacho.setComunaDespacho(rs.getInt("COMUNA_DESPACHO"));
					despacho.setRegionDespacho(rs.getInt("REGION_DESPACHO"));
					despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
					despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
					despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
					despacho.setObservacion(rs.getString("OBSERVACION"));
					despacho.setRutCliente(rs.getInt("DESPACHO_RUT_CLIENTE"));
					despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
					despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
					despacho.setTipoCliente(rs.getInt("DESACHO_TIPO_CLIENTE"));
					despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
					despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
					despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
					despacho.setTipoDespacho(rs.getInt("TIPO_DESPACHO"));
					despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					despacho.setCodigoRegalo(rs.getInt("DESPACHO_CODIGO_REGALO"));
					despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
					despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
					despacho.setRegionFacturacion(rs.getInt("REGION_FACTURACION"));
					despacho.setComunaFacturacion(rs.getInt("COMUNA_FACTURACION"));
					despacho.setEnvioDte(rs.getInt("ENVIO_DTE"));
					despacho.setAddressId(rs.getInt("ADDRESS_ID"));
					despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
					despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
					
					TipoDespacho tipoDespacho = new TipoDespacho();
					tipoDespacho.setTipDespachoId(rs.getInt("TIP_DESPACHO_ID"));
					tipoDespacho.setDescTipDespacho(rs.getString("DESC_TIP_DESPACHO"));
					tipoDespacho.setNomTipoDespacho(rs.getString("NOM_TIPO_DESPACHO"));
					tipoDespacho.setGlosaDespacho(rs.getString("GLOSA_DESPACHO"));
					
					if(notaVenta.getIndicadorMkp().equalsIgnoreCase("3")){
						
						tipoDespacho.setNomTipoDespacho("Despacha ");
						tipoDespacho.setGlosaDespacho(parametroBDDespachoClub.getValor());
						
					}
					
					if(notaVenta.getIndicadorMkp().equalsIgnoreCase("1")){
						
						tipoDespacho.setNomTipoDespacho("Despacha ");
						tipoDespacho.setGlosaDespacho(parametroBDDespachoMr.getValor());
						
					}
					
					TipoDoc tipoDoc = new TipoDoc();
					tipoDoc.setTipoDoc(rs.getInt("TIPO_DOC"));
					tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
					tipoDoc.setDocOrigen(rs.getInt("DOC_ORIGEN"));
					tipoDoc.setCodBo(rs.getInt("COD_BO"));
					tipoDoc.setCodPpl(rs.getInt("COD_PPL"));
					tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

					Vendedor vendedor = new Vendedor();
					vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
					vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));
					
					NotaVentaRechazo notaVentaRechazo = new NotaVentaRechazo();
					notaVentaRechazo.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("NVR_CORRELATIVO_VENTA")));
					notaVentaRechazo.setCodMotivo(rs.getInt("COD_MOTIVO"));
					notaVentaRechazo.setFechaBoleta(rs.getTimestamp("NVR_FECHA_BOLETA"));
					notaVentaRechazo.setIdValidacion(rs.getInt("ID_VALIDACION"));
					
					MotivoRechazo motivoRechazo = new MotivoRechazo();
					motivoRechazo.setCodMotivo(rs.getInt("MR_COD_MOTIVO"));
					motivoRechazo.setMotivo(rs.getString("MR_MOTIVO"));
					
					Validacion validacion = new Validacion();
					validacion.setId(rs.getInt("ID"));
					validacion.setDescripcion(rs.getString("DESCRIPCION"));
					validacion.setQuery(rs.getString("QUERY"));
					validacion.setEstado(rs.getInt("VAL_ESTADO"));

					XValidacionOc xValidacionOc = new XValidacionOc();
					xValidacionOc.setIdValidacion(rs.getInt("XV_ID_VALIDACION"));
					xValidacionOc.setMensajeError(rs.getString("MENSAJE_ERROR"));
					xValidacionOc.setQueryValidacion(rs.getString("QUERY_VALIDACION"));
					xValidacionOc.setValorOkErr(rs.getString("VALOR_OK_ERR"));
					xValidacionOc.setQueryActivo(rs.getInt("QUERY_ACTIVO"));
					xValidacionOc.setEstadoFinalOc(rs.getInt("ESTADO_FINAL_OC"));
					
					notaVenta.setTipoPago(tipoPago);
					notaVenta.setNcuotas(nCuotas);
					
					OrdenesDeCompra ordenCompra = new OrdenesDeCompra();
					ordenCompra.setNotaVenta(notaVenta);
					ordenCompra.setTarjetaRipley(tarjetaRipley);
					ordenCompra.setTarjetaBancaria(tarjetaBancaria);
					ordenCompra.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
					ordenCompra.setDatoFacturacion(datoFacturacion);
					ordenCompra.setDespacho(despacho);
					ordenCompra.setTipoDespacho(tipoDespacho);
					ordenCompra.setTipoDoc(tipoDoc);
					ordenCompra.setVendedor(vendedor);
					ordenCompra.setNotaVentaRechazo(notaVentaRechazo);
					ordenCompra.setMotivoRechazo(motivoRechazo);
					ordenCompra.setValidacion(validacion);
					ordenCompra.setxValidacionOc(xValidacionOc);
					
					ordenesCompra.add(ordenCompra);
				}
			}

		} catch (Exception e) {
			logger.traceError("obtenerOrdenes", "Error en obtenerOrdenes: ", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("obtenerOrdenes", "Finalizado", "ordenesCompra: "+ordenesCompra.size());
		return ordenesCompra;
	}

	@Override
	public List<OrdenesDeCompra> obtenerOrdenes(Long rutComprador) {
		logger.initTrace("obtenerOrdenes", " Long rutComprador: " + rutComprador);

		ArrayList<OrdenesDeCompra> ordenesCompra = new ArrayList<OrdenesDeCompra>();
		Parametro parametroBD = parametroDao.getParametro("GLOSA_BLOQUEO_OC");
		Parametro parametroBDDespachoMr = parametroDao.getParametro("DESPACHO_MERCADO_RIPLEY");
		Parametro parametroBDDespachoClub = parametroDao.getParametro("DESPACHO_CLUBES");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_OBTENER_ORDENES(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			if(rutComprador == null) {
				cs.setNull(1, OracleTypes.NUMBER);
			} else {
				cs.setLong(1, rutComprador);
			}
			cs.registerOutParameter(2, OracleTypes.VARCHAR); //6
			cs.registerOutParameter(3, OracleTypes.CURSOR); //7
			cs.registerOutParameter(4, OracleTypes.NUMBER); //8
			cs.registerOutParameter(5, OracleTypes.VARCHAR); //9
			cs.execute();
			rs = (ResultSet) cs.getObject(3);
			logger.traceInfo("obtenerOrdenes", "SGO_MANEJO_OC.PROC_OBTENER_ORDENES - Query: " + ((String) cs.getObject(2)));
			if (rs != null) {
				while (rs.next()) {
					String tipoPago = "";
					String nCuotas = "-";
					NotaVenta notaVenta = new NotaVenta();
					notaVenta.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
					notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
					notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
					notaVenta.setHorasAdministrativas(rs.getInt("HORAS_ADMINISTRATIVAS"));
					notaVenta.setEstado(rs.getInt("ESTADO"));
					notaVenta.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
					notaVenta.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
					notaVenta.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
					notaVenta.setRutComprador(rs.getInt("RUT_COMPRADOR"));
					notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
					notaVenta.setNumeroSucursal(rs.getInt("NUMERO_SUCURSAL"));
					notaVenta.setCodigoRegalo(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_REGALO")));
					notaVenta.setTipoRegalo(rs.getInt("TIPO_REGALO"));
					notaVenta.setNumeroBoleta(rs.getInt("NUMERO_BOLETA"));
					notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
					notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
					notaVenta.setNumeroCaja(rs.getInt("NUMERO_CAJA"));
					notaVenta.setCorrelativoBoleta(rs.getInt("CORRELATIVO_BOLETA"));
					notaVenta.setRutBoleta(rs.getInt("RUT_BOLETA"));
					notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
					notaVenta.setNumNotaCredito(rs.getInt("NUM_NOTA_CREDITO"));
					notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
					notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
					notaVenta.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
					notaVenta.setCorrelativoNotaCredito(rs.getInt("CORRELATIVO_NOTA_CREDITO"));
					notaVenta.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
					notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
					notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
					notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					notaVenta.setTipoDoc(rs.getInt("NOTAVENTA_TIPODOC"));
					notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
					notaVenta.setFolioNcSii(rs.getInt("FOLIO_NC_SII"));
					notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
					notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					notaVenta.setUsuario(rs.getString("USUARIO"));
					notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					notaVenta.setEstadoDes(rs.getString("ESTADO_DESC"));
					
					if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 88){
						notaVenta.setBloqueado("Si");
						notaVenta.setGlosaBloqueo(parametroBD.getValor());
					}else{
						if (notaVenta.getNumeroCaja() == 0 && notaVenta.getEstado() == 8){
							notaVenta.setBloqueado("Si");
							notaVenta.setGlosaBloqueo(parametroBD.getValor());
						
					
					}else{
						notaVenta.setBloqueado("No");
						notaVenta.setGlosaBloqueo(parametroBD.getValor());
					}
					}
									
					TarjetaRipley tarjetaRipley = new TarjetaRipley();
					tarjetaRipley.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
					tarjetaRipley.setAdministradora(rs.getInt("ADMINISTRADORA"));
					tarjetaRipley.setEmisor(rs.getInt("EMISOR"));
					tarjetaRipley.setTarjeta(rs.getInt("TARJETA"));
					tarjetaRipley.setRutTitular(rs.getInt("RUT_TITULAR"));
					tarjetaRipley.setRutPoder(rs.getInt("RUT_PODER"));
					tarjetaRipley.setPlazo(rs.getInt("PLAZO"));
					tarjetaRipley.setDiferido(rs.getInt("DIFERIDO"));
					tarjetaRipley.setMontoCapital(rs.getBigDecimal("MONTO_CAPITAL"));
					tarjetaRipley.setMontoPie(rs.getBigDecimal("MONTO_PIE"));
					tarjetaRipley.setDescuentoCar(rs.getBigDecimal("DESCUENTO_CAR"));
					tarjetaRipley.setCodigoDescuento(rs.getInt("CODIGO_DESCUENTO"));
					tarjetaRipley.setPrefijoTitular(rs.getInt("PREFIJO_TITULAR"));
					tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
					tarjetaRipley.setTipoCliente(rs.getInt("TIPO_CLIENTE"));
					tarjetaRipley.setTipoCredito(rs.getInt("TIPO_CREDITO"));
					tarjetaRipley.setPrefijoPoder(rs.getInt("PREFIJO_PODER"));
					tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
					tarjetaRipley.setValorCuota(rs.getBigDecimal("VALOR_CUOTA"));
					tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
					tarjetaRipley.setSernacFinanCae(rs.getInt("SERNAC_FINAN_CAE"));
					tarjetaRipley.setSernacFinanCt(rs.getInt("SERNAC_FINAN_CT"));
					tarjetaRipley.setTvtriMntMntFnd(rs.getInt("TVTRI_MNT_MNT_FND"));
					tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
					tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
					tarjetaRipley.setTvtriCodImpTsaEfe(rs.getInt("TVTRI_COD_IMP_TSA_EFE"));
					tarjetaRipley.setTvtriMntMntEva(rs.getInt("TVTRI_MNT_MNT_EVA"));
					tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
					tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
					tarjetaRipley.setCodigoCore(rs.getInt("CODIGO_CORE"));
					tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));
					if (tarjetaRipley.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_RIPLEY_;
						nCuotas = String.valueOf(tarjetaRipley.getPlazo());
					}

					TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
					tarjetaBancaria.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
					tarjetaBancaria.setTipoTarjeta(rs.getString("TIPO_TARJETA"));
					tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
					tarjetaBancaria.setMontoTarjeta(rs.getBigDecimal("MONTO_TARJETA"));
					//tarjetaBancaria.setVd(rs.getString("VD"));
					//tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
					//tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
					if (tarjetaBancaria.getCorrelativoVenta()!= null){
						if (tarjetaBancaria.getVd() != null){
							if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_M)){
								tipoPago = Constantes.TIPO_PAGO_MERCADO_PAGO_;
							}
							else if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_S)){
								tipoPago = Constantes.TIPO_PAGO_TBK_DEBITO_;
							} else {
								tipoPago = Constantes.TIPO_PAGO_TBK_CREDITO_;
							}
						}
					}
					tarjetaBancaria.setCodigoConvenio(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));

					TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
					tarjetaRegaloEmpresa.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
					tarjetaRegaloEmpresa.setAdministradora(rs.getInt("TRE_ADMINISTRADORA"));
					tarjetaRegaloEmpresa.setEmisor(rs.getInt("TRE_EMISOR"));
					tarjetaRegaloEmpresa.setTarjeta(rs.getInt("TRE_TARJETA"));
					tarjetaRegaloEmpresa.setCodigoAutorizador(rs.getInt("TRE_COD_AUTORIZADOR"));
					tarjetaRegaloEmpresa.setRutCliente(rs.getInt("RUT_CLIENTE"));
					tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
					tarjetaRegaloEmpresa.setMonto(rs.getBigDecimal("MONTO"));
					tarjetaRegaloEmpresa.setNroTarjeta(rs.getInt("NRO_TARJETA"));
					tarjetaRegaloEmpresa.setFlag(rs.getInt("FLAG"));
					if (tarjetaRegaloEmpresa.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_TRE;
					}

					DatoFacturacion datoFacturacion = new DatoFacturacion();
					datoFacturacion.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
					datoFacturacion.setAddressId(rs.getInt("FACT_ADDRESS_ID"));
					datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
					datoFacturacion.setComunaFacturaDes(rs.getString("COMUNA_FACTURA_DES"));
					datoFacturacion.setCiudadFacturaDes(rs.getString("CIUDAD_FACTURA_DES"));
					datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
					datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
					datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
					datoFacturacion.setGiroFacturaId(rs.getInt("GIRO_FACTURA_ID"));
					datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
					datoFacturacion.setRegionFacturaId(rs.getInt("REGION_FACTURA_ID"));
					datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

					Despacho despacho = new Despacho();
					despacho.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
					despacho.setRutDespacho(rs.getInt("RUT_DESPACHO"));
					despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
					despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
					despacho.setSucursalDespacho(rs.getInt("SUCURSAL_DESPACHO"));
					despacho.setComunaDespacho(rs.getInt("COMUNA_DESPACHO"));
					despacho.setRegionDespacho(rs.getInt("REGION_DESPACHO"));
					despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
					despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
					despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
					despacho.setObservacion(rs.getString("OBSERVACION"));
					despacho.setRutCliente(rs.getInt("DESPACHO_RUT_CLIENTE"));
					despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
					despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
					despacho.setTipoCliente(rs.getInt("DESACHO_TIPO_CLIENTE"));
					despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
					despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
					despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
					despacho.setTipoDespacho(rs.getInt("TIPO_DESPACHO"));
					despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					despacho.setCodigoRegalo(rs.getInt("DESPACHO_CODIGO_REGALO"));
					despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
					despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
					despacho.setRegionFacturacion(rs.getInt("REGION_FACTURACION"));
					despacho.setComunaFacturacion(rs.getInt("COMUNA_FACTURACION"));
					despacho.setEnvioDte(rs.getInt("ENVIO_DTE"));
					despacho.setAddressId(rs.getInt("ADDRESS_ID"));
					despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
					despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
					
					TipoDespacho tipoDespacho = new TipoDespacho();
					tipoDespacho.setTipDespachoId(rs.getInt("TIP_DESPACHO_ID"));
					tipoDespacho.setDescTipDespacho(rs.getString("DESC_TIP_DESPACHO"));
					tipoDespacho.setNomTipoDespacho(rs.getString("NOM_TIPO_DESPACHO"));
					tipoDespacho.setGlosaDespacho(rs.getString("GLOSA_DESPACHO"));

					if(notaVenta.getIndicadorMkp().equalsIgnoreCase("3")){
						
						tipoDespacho.setNomTipoDespacho("Despacha ");
						tipoDespacho.setGlosaDespacho(parametroBDDespachoClub.getValor());
						
					}
					
					if(notaVenta.getIndicadorMkp().equalsIgnoreCase("1")){
						
						tipoDespacho.setNomTipoDespacho("Despacha ");
						tipoDespacho.setGlosaDespacho(parametroBDDespachoMr.getValor());
						
					}
										
					TipoDoc tipoDoc = new TipoDoc();
					tipoDoc.setTipoDoc(rs.getInt("TIPO_DOC"));
					tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
					tipoDoc.setDocOrigen(rs.getInt("DOC_ORIGEN"));
					tipoDoc.setCodBo(rs.getInt("COD_BO"));
					tipoDoc.setCodPpl(rs.getInt("COD_PPL"));
					tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

					Vendedor vendedor = new Vendedor();
					vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
					vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));
					
					NotaVentaRechazo notaVentaRechazo = new NotaVentaRechazo();
					notaVentaRechazo.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("NVR_CORRELATIVO_VENTA")));
					notaVentaRechazo.setCodMotivo(rs.getInt("COD_MOTIVO"));
					notaVentaRechazo.setFechaBoleta(rs.getTimestamp("NVR_FECHA_BOLETA"));
					notaVentaRechazo.setIdValidacion(rs.getInt("ID_VALIDACION"));
					
					MotivoRechazo motivoRechazo = new MotivoRechazo();
					motivoRechazo.setCodMotivo(rs.getInt("MR_COD_MOTIVO"));
					motivoRechazo.setMotivo(rs.getString("MR_MOTIVO"));
					
					Validacion validacion = new Validacion();
					validacion.setId(rs.getInt("ID"));
					validacion.setDescripcion(rs.getString("DESCRIPCION"));
					validacion.setQuery(rs.getString("QUERY"));
					validacion.setEstado(rs.getInt("VAL_ESTADO"));

					XValidacionOc xValidacionOc = new XValidacionOc();
					xValidacionOc.setIdValidacion(rs.getInt("XV_ID_VALIDACION"));
					xValidacionOc.setMensajeError(rs.getString("MENSAJE_ERROR"));
					xValidacionOc.setQueryValidacion(rs.getString("QUERY_VALIDACION"));
					xValidacionOc.setValorOkErr(rs.getString("VALOR_OK_ERR"));
					xValidacionOc.setQueryActivo(rs.getInt("QUERY_ACTIVO"));
					xValidacionOc.setEstadoFinalOc(rs.getInt("ESTADO_FINAL_OC"));
					
					notaVenta.setTipoPago(tipoPago);
					notaVenta.setNcuotas(nCuotas);
					
					OrdenesDeCompra ordenCompra = new OrdenesDeCompra();
					ordenCompra.setNotaVenta(notaVenta);
					ordenCompra.setTarjetaRipley(tarjetaRipley);
					ordenCompra.setTarjetaBancaria(tarjetaBancaria);
					ordenCompra.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
					ordenCompra.setDatoFacturacion(datoFacturacion);
					ordenCompra.setDespacho(despacho);
					ordenCompra.setTipoDespacho(tipoDespacho);
					ordenCompra.setTipoDoc(tipoDoc);
					ordenCompra.setVendedor(vendedor);
					ordenCompra.setNotaVentaRechazo(notaVentaRechazo);
					ordenCompra.setMotivoRechazo(motivoRechazo);
					ordenCompra.setValidacion(validacion);
					ordenCompra.setxValidacionOc(xValidacionOc);
					
					ordenesCompra.add(ordenCompra);
				}
			}

		} catch (Exception e) {
			logger.traceError("obtenerOrdenes", "Error en obtenerOrdenes: ", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("obtenerOrdenes", "Finalizado", "ordenesCompra: "+ordenesCompra.size());
		return ordenesCompra;
	}

	@Override
	public boolean updateEstadoOrdenes(String seleccionados, Integer nroEstado, Integer nroEstado_CS, boolean cajaNula, String usuario, String estadosValidacion) {
		logger.initTrace("updateEstadoOrdenes", "String seleccionados, Integer nroEstado, Integer nroEstado_CS, boolean cajaNula, String usuario, String estadosValidacion", 
				new KeyLog("seleccionados", seleccionados),
				new KeyLog("nroEstado", String.valueOf(nroEstado) + " - " + String.valueOf(nroEstado_CS)),
				new KeyLog("cajaNula", String.valueOf(cajaNula)),
				new KeyLog("usuario", usuario),
				new KeyLog("estadosValidacion", estadosValidacion));
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.TRUE;
		try {
			logger.traceInfo("Codigo de Caja es Nula ? : " + cajaNula, "inicio updateEstadoOrdenes");
			
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_UPDATE_ESTADO_OC(?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, seleccionados);
			cs.setInt(2, nroEstado);
			cs.setInt(3, nroEstado_CS);
			//Flag para saber si debe dejar la caja en null solo para confirmacion de medio pago
			cs.setString(4, cajaNula?"S":"N");
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.setString(8, usuario.substring(0, (usuario.length()<=10)?usuario.length():10));
			if(estadosValidacion == null){
				cs.setNull(9, OracleTypes.VARCHAR);
			}else{
				cs.setString(9, estadosValidacion);
			}
			cs.execute();
			logger.traceInfo("updateEstadoOrdenes", "codigo: "+cs.getInt(6)+" msj: "+cs.getString(7)+" Query: " + cs.getString(5));
		} catch (Exception e) {
			logger.traceError("updateEstadoOrdenes", "Error en updateEstadoOrdenes: ", e);
			respuesta = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("updateEstadoOrdenes", "Finalizado", "updateEstadoOrdenes: " + respuesta);
		return respuesta;
	}

	@Override
	public List<ArticulosVentaOC> getArticulosByOC(Long correlativoOC) {
		logger.initTrace("getArticulosByOC", "Ingreso a metodo; correlativoOC=" + correlativoOC);
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		List<ArticulosVentaOC> articulos = new ArrayList<ArticulosVentaOC>(20);
		
		try {
			conn = poolBD.getConnection();
			
			cs = conn.prepareCall("{call SGO_MANEJO_OC.PROC_GET_ARTICULOS_OC(?,?,?,?,?)}");
			cs.setLong(1, correlativoOC);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
			
			String query = cs.getString(3);
			BigDecimal cod = cs.getBigDecimal(4);
			String mensaje = cs.getString(5);
			
			logger.traceInfo("getArticulosByOC", "query: " + query);
			logger.traceInfo("getArticulosByOC", "cod: " + cod);
			logger.traceInfo("getArticulosByOC", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			rs = (ResultSet) cs.getObject(2);
			
			while(rs != null && rs.next()) {
				ArticulosVentaOC articulosVenta = new ArticulosVentaOC();
				
				ArticuloVenta a = new ArticuloVenta();
				a.setCodArticulo(rs.getString("COD_ARTICULO"));
				a.setCodBodega(rs.getString("COD_BODEGA"));
				a.setCodDespacho(rs.getString("COD_DESPACHO"));
				a.setColor(rs.getInt("COLOR"));
				a.setCorrelativoItem(rs.getInt("CORRELATIVO_ITEM"));
				a.setCorrelativoNotaCredito(rs.getInt("CORRELATIVO_NOTA_CREDITO"));
				a.setCorrelativoVenta(rs.getInt("CORRELATIVO_VENTA"));
				a.setCosto(rs.getBigDecimal("COSTO"));
				a.setDepto(rs.getString("DEPTO"));
				a.setDeptoGlosa(rs.getString("DEPTO_GLOSA"));
				a.setDescRipley(rs.getString("DESC_RIPLEY"));
				a.setDespachoId(rs.getInt("DESPACHO_ID"));
				a.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
				a.setEsNC(rs.getInt("ES_NC"));
				a.setEsregalo(rs.getString("ESREGALO"));
				a.setEstadoVenta(rs.getString("ESTADO_VENTA"));
				a.setFechaDespacho(rs.getDate("FECHA_DESPACHO"));
				a.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
				a.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
				a.setIndicadorEg(rs.getInt("INDICADOR_EG"));
				a.setMensaje(rs.getString("MENSAJE"));
				a.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
				a.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
				a.setNumNotaCredito(rs.getInt("NUM_NOTA_CREDITO"));
				a.setOrdenMkp(rs.getString("ORDEN_MKP"));
				a.setPrecio(rs.getBigDecimal("PRECIO"));
				a.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
				a.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
				a.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
				a.setTipoPapelRegalo(rs.getString("TIPO_PAPEL_REGALO"));
				a.setTipoRegalo(rs.getInt("TIPO_REGALO"));
				a.setUnidades(rs.getInt("UNIDADES"));
				a.setEsMkp(rs.getInt("ES_MKP"));
				
				//Precio * Unidades para construcci�n de boleta.
				try{
					a.setPrecioPorUnidades(a.getPrecio().multiply(new BigDecimal(a.getUnidades())));
				}
				catch(Exception e){
					logger.traceError("getArticulosByOC", "Error en getArticulosByOC: Precio Por Unidades", e);
				}
				IdentificadorMarketplace i = new IdentificadorMarketplace();
				i.setOrdenMkp(rs.getString("MKP_ORDEN_MKP"));
				i.setRutProveedor(rs.getString("RUT_PROVEEDOR"));
				i.setEstado(Validaciones.validaIntegerCeroXNull(rs.getInt("ESTADO")));
				i.setEsMkp(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ES_MKP")));
				i.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
				i.setNroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_BOLETA")));
				i.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
				i.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
				i.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NOTA_CREDITO")));
				i.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NUM_CAJA_NOTA_CREDITO")));
				i.setFecNotaCredito(rs.getTimestamp("MKP_FEC_NOTA_CREDITO"));
				i.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
				i.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
				i.setRazonSocial(rs.getString("RAZON_SOCIAL"));

				
				articulosVenta.setArticuloVenta(a);
				articulosVenta.setIdentificadorMarketplace(i);
				
				articulos.add(articulosVenta);
			}
			
		} catch(Exception e) {
			logger.traceError("getArticulosByOC", "Error en getArticulosByOC: ", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}
		
		return articulos;
	}

	@Override
	public boolean updateEstadoNcArticulos(String correlativosArticulos, Long correlativoVenta, Long estadoNC) {
		logger.initTrace("updateEstadoNcArticulos", "Ingreso a metodo; correlativosArticulos=" + correlativosArticulos + "; estadoNC=" + estadoNC);
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.TRUE;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_UPDATE_ESTADO_NC_ART(?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, correlativosArticulos);
			cs.setLong(2, correlativoVenta);
			cs.setLong(3, estadoNC);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();
			
			String query = cs.getString(4);
			BigDecimal cod = cs.getBigDecimal(5);
			String mensaje = cs.getString(6);
			
			logger.traceInfo("updateEstadoNcArticulos", "query: " + query);
			logger.traceInfo("updateEstadoNcArticulos", "cod: " + cod);
			logger.traceInfo("updateEstadoNcArticulos", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch (Exception e) {
			logger.traceError("updateEstadoNcArticulos", "Error en updateEstadoNcArticulos: ", e);
			respuesta = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("updateEstadoNcArticulos", "Finalizado", "updateEstadoNcArticulos: " + respuesta);
		return respuesta;
	}

	@Override
	public DetalleOC obtenerDetalleOC(Integer correlativo) {
			logger.initTrace("obtenerDetalleOC", "correlativo venta: "+correlativo);

			DetalleOC detalleOC = new DetalleOC();
			List<DetalleDespacho> despachoList = new ArrayList<DetalleDespacho>();
			List<DetalleCompra> detalleCompra = new ArrayList<DetalleCompra>();
			
			Connection conn = null;
			CallableStatement cs = null;
			ResultSet rs = null;
			
			try {
				conn = poolBD.getConnection();
				String procSQL = "{CALL SGO_MANEJO_OC.PROC_GET_DETALLE_OC(?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, correlativo);
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.registerOutParameter(4, OracleTypes.CURSOR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.registerOutParameter(6, OracleTypes.NUMBER);
				cs.registerOutParameter(7, OracleTypes.VARCHAR);
				cs.execute();

				//Datos cliente
				rs = (ResultSet) cs.getObject(3);				
				if (rs != null) {
					while (rs.next()) {
						DetalleCliente datosCliente = new DetalleCliente();
						
						DetalleBackoffice datosBackoffice = new DetalleBackoffice();
						
						datosCliente.setCorrelativoVentas(rs.getInt("CORRELATIVO_VENTA"));						
						String rut=rs.getString("RUT_CLIENTE");
						if(rut == null) {
							datosCliente.setRutCliente("-");	
						} else {
							datosCliente.setRutCliente(rut);
						}						
						datosCliente.setNombreCliente(rs.getString("NOMBRE_CLIENTE")+" "
								+rs.getString("APELLIDO_PAT_CLIENTE")
								+" "+ rs.getString("APELLIDO_MAT_CLIENTE"));
						datosCliente.setMail(rs.getString("EMAIL_CLIENTE"));
						datosCliente.setTipoDoc(rs.getString("DESC_TIPODOC"));
						datosCliente.setDireccion(rs.getString("DIRECCION_CLIENTE"));
						datosCliente.setFonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						datosCliente.setFonoCliente(rs.getString("TELEFONO_CLIENTE"));
						datosCliente.setTipoOrden(rs.getString("TIPO_ORDEN"));
						datosCliente.setRutFactura(rs.getString("RUT_FACTURA"));
						datosCliente.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datosCliente.setEstadoDes(rs.getString("ESTADO"));
						datosCliente.setUsuario(rs.getString("USUARIO"));
						datosCliente.setCodTipoDoc(rs.getString("TIPO_DOC"));
						
						detalleOC.setDatosCliente(datosCliente);
						
						//Datos Backoffice
						
						datosBackoffice.setNumeroCaja(rs.getInt("NUMERO_CAJA"));
						datosBackoffice.setNumeroSucursal(rs.getInt("NUMERO_SUCURSAL"));
						datosBackoffice.setNumeroBoleta(rs.getInt("NUMERO_BOLETA"));
						datosBackoffice.setFechaHoraCreacion(rs.getDate("FECHA_HORA_CREACION").toString());
						
						detalleOC.setDatosBackoffice(datosBackoffice);
						
					}
				}
				
				rs.close();
				
				//Datos despacho
				rs = (ResultSet) cs.getObject(4);				
				if (rs != null) {
					while (rs.next()) {
						DetalleDespacho datosDespacho = new DetalleDespacho();
						
						String rut=rs.getString("RUT_DESPACHO");
						if(rut == null) {
							datosDespacho.setRutDespacho("-");	
						} else {
                                                        rut = rut.trim();
							datosDespacho.setRutDespacho(rut+"-"+Util.getDigitoValidadorRut(rut));
						}
						datosDespacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						datosDespacho.setSku(rs.getString("SKU"));
						datosDespacho.setEstadoBt(rs.getString("ESTADO_BT"));
						datosDespacho.setDireccion(rs.getString("DIRECCION_DESPACHO"));
						datosDespacho.setCodigoBo(rs.getString("COD_BO"));
						datosDespacho.setRegion(rs.getString("REGION_DESPACHO"));
						datosDespacho.setComuna(rs.getString("COMUNA_DESPACHO"));
						datosDespacho.setCud(rs.getString("CUD"));
						datosDespacho.setArticulo(rs.getString("ARTICULO"));
						datosDespacho.setFono(rs.getString("TELEFONO_CLIENTE"));
						datosDespacho.setObservacion(rs.getString("OBSERVACION"));
						datosDespacho.setRutCliente(rs.getString("DESPACHO_RUT_CLIENTE"));
						datosDespacho.setNombreFact(rs.getString("RAZON_SOCIAL_FACTURA"));
						datosDespacho.setComunaFact(rs.getString("COMUNA_FACTURACION"));
						datosDespacho.setUnidades(rs.getString("UNIDADES"));
						
						despachoList.add(datosDespacho);
					}
					detalleOC.setDatosDespacho(despachoList);
				}
				
				rs.close();
				//Datos compra
				rs = (ResultSet) cs.getObject(5);				
				if (rs != null) {
					while (rs.next()) {
						DetalleCompra datosCompra = new DetalleCompra();
						
						datosCompra.setMontoCar(rs.getBigDecimal("MONTO_CAPITAL"));						
						datosCompra.setCuotas(rs.getInt("NROCUOTAS"));
						datosCompra.setPrimerVenc(rs.getDate("FECHA_PRIMER_VENCTO"));
						datosCompra.setValorCuota(rs.getBigDecimal("VALOR_CUOTA"));
						datosCompra.setMontoTre(rs.getBigDecimal("MONTO_TRE"));
						datosCompra.setEsWebPay(rs.getString("ISWEBPAY"));

						String glosaFinanciera = "";
						if (rs.getString("GLOSA_FINANCIERA_GSIC") != null) {				
							glosaFinanciera = rs.getString("GLOSA_FINANCIERA_GSIC")
									.replaceAll("["+parametroDao.getParametro("LISTA_CARACTER_RARO").getValor()+"]", "");
							StrBuilder respuesta = new StrBuilder();
							glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
							String glosas[] = glosaFinanciera.split("@@");
							for (int g = 0; g < glosas.length; g++){
								respuesta.append(glosas[g]).appendNewLine();
							}
							glosaFinanciera = respuesta.toString();
						}
						datosCompra.setGlosaGsic(glosaFinanciera);
						
						logger.initTrace("glosaFinanciera : " + glosaFinanciera, "");

						datosCompra.setDescripcion(rs.getString("DESC_TB"));
						datosCompra.setSku(rs.getString("SKU_ART"));
						datosCompra.setPrecio(rs.getBigDecimal("PRECIO"));
						datosCompra.setUnidades(rs.getString("UNIDADES"));
						datosCompra.setDescuento(rs.getString("MONTO_DESCUENTO"));
						datosCompra.setTotal(rs.getBigDecimal("TOTAL"));
						datosCompra.setDespacho(rs.getString("PRECIO_DESPACHO"));
						datosCompra.setFechaDespacho(rs.getDate("D_FECHA_DESPACHO"));
						datosCompra.setCodBodega(rs.getString("COD_BODEGA"));
						
						detalleCompra.add(datosCompra);
					}
					detalleOC.setDatosCompra(detalleCompra);
				}

			} catch (Exception e) {
				logger.traceError("obtenerDetalleOC", "Error en obtenerDetalleOC: ", e);
				return null;
			} finally {
				poolBD.closeConnection(conn,rs,cs);
			}

			logger.endTrace("obtenerDetalleOC", "Finalizado", "obtenerDetalleOC");
			return detalleOC;
	}

	@Override
	public List<ListaAgrupaSubOrdenes> getSubOrdenesByOC(Long correlativoOC) {
		logger.initTrace("getSubOrdenesByOC", "Ingreso a metodo; correlativoOC=" + correlativoOC);
		
		List<ArticulosVentaOC> articulos = getArticulosByOC(correlativoOC);
		
		Map<String, List<ArticulosVentaOC>> agrupado =	articulos.stream()
														.collect(Collectors.groupingBy(it -> it.getArticuloVenta().getOrdenMkp(), 
																LinkedHashMap::new, 
																Collectors.toCollection(ArrayList::new)));
		
		List<ListaAgrupaSubOrdenes> listaSubOrden =	agrupado.entrySet().stream()
													.map(it -> {
														ListaAgrupaSubOrdenes lista = new ListaAgrupaSubOrdenes();
														
														lista.setSubOrden(it.getKey());
														lista.setArticulos(it.getValue());
														
														BigDecimal montoTotal =	lista.getArticulos().stream()
																				.map(art -> art.getArticuloVenta().getPrecioPorUnidades().subtract(art.getArticuloVenta().getMontoDescuento()))
																				.reduce(BigDecimal.ZERO, BigDecimal::add);
														
														lista.setMontoTotal(montoTotal);
														
														boolean isRequired =	lista.getArticulos().stream()
																				.anyMatch(art -> "Sin Stock".equalsIgnoreCase(art.getArticuloVenta().getEstadoVenta()) || art.getArticuloVenta().getEsNC().intValue() == Constantes.NRO_UNO);
														
														lista.setEsRequired(isRequired ? 1 : 0);
														
														boolean reversado =	lista.getArticulos().stream()
																.anyMatch(art -> art.getArticuloVenta().getEsNC() == Constantes.NRO_UNO || art.getArticuloVenta().getEsNC() == Constantes.NRO_DOS);
														
														lista.setReversado(reversado);
														
														return lista;
													})
													.collect(Collectors.toCollection(ArrayList::new));
		
		return listaSubOrden;
	}

	@Override
	public boolean updateRefundMKP(
			List<ListaAgrupaSubOrdenes> articulosConOrdenes,
			Long correlativoVenta, String usuario) {
		logger.initTrace("updateRefundMKP", "Ingreso a metodo; articulosConOrdenes=" + articulosConOrdenes.size() + "; correlativoVenta=" + correlativoVenta + "; usuario="+usuario);
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.TRUE;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_MKP.PROC_UPDATE_REFUND_MKP(?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoVenta);
			String subOrdenes = Constantes.VACIO;
			String montos = Constantes.VACIO;
			String refunds = Constantes.VACIO;
			for (ListaAgrupaSubOrdenes obj : articulosConOrdenes) {
				if(obj.getSubOrden() != null && !Constantes.VACIO.equalsIgnoreCase(obj.getSubOrden())
				   && obj.getMontoTotal().compareTo(new BigDecimal(Constantes.NRO_MENOSUNO))== Constantes.NRO_UNO && obj.getIdRefund() != null && obj.getIdRefund().intValue() > -1){
					subOrdenes = subOrdenes + Constantes.PIPE + obj.getSubOrden();
					montos = montos + Constantes.PIPE + obj.getMontoTotal();
					refunds = refunds + Constantes.PIPE + obj.getIdRefund();
				}
			}
			logger.traceInfo("updateRefundMKP", "SubOrdenes: "+subOrdenes+" Montos:"+montos+" Refunds: "+refunds);
			cs.setString(2, subOrdenes);
			cs.setString(3, montos);
			cs.setString(4, refunds);
			cs.setString(5, usuario.substring(0, (usuario.length()<=10)?usuario.length():10));
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.execute();
			
			String query = cs.getString(6);
			BigDecimal cod = cs.getBigDecimal(7);
			String mensaje = cs.getString(8);
			
			logger.traceInfo("updateRefundMKP", "query: " + query);
			logger.traceInfo("updateRefundMKP", "cod: " + cod);
			logger.traceInfo("updateRefundMKP", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new AligareException(mensaje);
			}
			
		} catch (Exception e) {
			logger.traceError("updateRefundMKP", "Error en updateRefundMKP: ", e);
			respuesta = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("updateRefundMKP", "Finalizado", "updateRefundMKP: " + respuesta);
		return respuesta;
	}

	@Override
	public boolean esRechazo(Long correlativoVenta) {
		logger.initTrace("esRechazo", "Ingreso a metodo; correlativoVenta=" + correlativoVenta);
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.TRUE;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_ES_RECHAZO(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoVenta);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.execute();
			
			BigDecimal cod = cs.getBigDecimal(2);
			String mensaje = cs.getString(3);
			
			logger.traceInfo("esRechazo", "cod: " + cod);
			logger.traceInfo("esRechazo", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new AligareException(mensaje);
			}
			
			respuesta = (cs.getInt(4) == 0);
		} catch (Exception e) {
			logger.traceError("esRechazo", "Error en esRechazo: ", e);
			throw new AligareException(e);
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}
		
		logger.endTrace("esRechazo", "Finalizado", "esRechazo: " + respuesta);
		return respuesta;
	}

	@Override
	public DetalleCliente getDatosCliente(Long correlativo) {
		logger.initTrace("getDatosCliente", "correlativo venta: "+correlativo);

		DetalleCliente datosCliente = new DetalleCliente();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_GET_DATOS_CLIENTE_OC(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativo);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();

			//Datos cliente
			rs = (ResultSet) cs.getObject(3);				
			if (rs != null) {
				while (rs.next()) {
					datosCliente = new DetalleCliente();
					
					datosCliente.setCorrelativoVentas(rs.getInt("CORRELATIVO_VENTA"));						
					String rut=rs.getString("RUT_CLIENTE");
					if(rut == null) {
						datosCliente.setRutCliente("-");	
					} else {
						datosCliente.setRutCliente(rut+"-"+Util.getDigitoValidadorRut(rut));
					}						
					datosCliente.setNombreCliente(rs.getString("NOMBRE_CLIENTE")+" "
							+rs.getString("APELLIDO_PAT_CLIENTE")
							+" "+ rs.getString("APELLIDO_MAT_CLIENTE"));
					datosCliente.setMail(rs.getString("EMAIL_CLIENTE"));
					datosCliente.setTipoDoc(rs.getString("DESC_TIPODOC"));
					datosCliente.setDireccion(rs.getString("DIRECCION_CLIENTE"));
					datosCliente.setFonoDespacho(rs.getString("TELEFONO_DESPACHO"));
					datosCliente.setFonoCliente(rs.getString("TELEFONO_CLIENTE"));
					datosCliente.setTipoOrden(rs.getString("TIPO_ORDEN"));
					datosCliente.setRutFactura(rs.getString("RUT_FACTURA"));
					datosCliente.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
				}
			}
		} catch (Exception e) {
			logger.traceError("getDatosCliente", "Error en getDatosCliente: ", e);
			return null;
		} finally {
			poolBD.closeConnection(conn,rs,cs);
		}

		logger.endTrace("getDatosCliente", "Finalizado", "getDatosCliente");
		return datosCliente;
	}

	@Override
	public String obtenerFormaPago(Long correlativoOC) {
		logger.initTrace("obtenerFormaPago", "correlativo venta: "+correlativoOC);

		String resultado = null;
		Connection conn = null;
		CallableStatement cs = null;
		
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_GET_FORMA_PAGO_OC(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoOC);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
			
			resultado = cs.getString(2);
		} catch (Exception e) {
			logger.traceError("obtenerFormaPago", "Error en obtenerFormaPago[SGO_MANEJO_OC.PROC_GET_FORMA_PAGO_OC]: ", e);
			return null;
		} finally {
			poolBD.closeConnection(conn,null,cs);
		}

		logger.endTrace("obtenerFormaPago", "Finalizado", "String formaPago: "+resultado);
		return resultado;
	}

	public List<IdentificadorMarketplace> obtenerIdentificadorMarketplace(Integer correlativoVenta) {

		logger.initTrace("obtenerIdentificadorMarketplace", "Integer correlativoVenta: " + correlativoVenta, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));

		ArrayList<IdentificadorMarketplace> listado = new ArrayList<IdentificadorMarketplace>();

		Connection conn = poolBD.getConnection();

		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_MARKETPLACE(?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerIdentificadorMarketplace", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_MARKETPLACE("+correlativoVenta+",outv,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(3);
				//logger.traceInfo("obtenerIdentificadorMarketplace", "Query: " + ((String) cs.getObject(2)), new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				if (rs != null) {
					while (rs.next()) {
						IdentificadorMarketplace marketplace = new IdentificadorMarketplace();
						marketplace.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_VENTA")));
						marketplace.setOrdenMkp(rs.getString("ORDEN_MKP"));
						marketplace.setRutProveedor(rs.getString("RUT_PROVEEDOR"));
						marketplace.setEstado(Validaciones.validaIntegerCeroXNull(rs.getInt("ESTADO")));
						marketplace.setEsMkp(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ES_MKP")));
						marketplace.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						marketplace.setCorrelativoBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_BOLETA")));
						marketplace.setNroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_BOLETA")));
						marketplace.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						marketplace.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						marketplace.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_NOTA_CREDITO")));
						marketplace.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						marketplace.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						marketplace.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						marketplace.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						marketplace.setRazonSocial(rs.getString("RAZON_SOCIAL"));
						//marketplace.setAmount(new Integer(rs.getInt("AMOUNT")));
						//marketplace.setIdRefund(new Integer(rs.getInt("REFUND_ID")));
						
						listado.add(marketplace);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerIdentificadorMarketplace", "Error en obtenerIdentificadorMarketplace: ", e);
				logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: Null");
				return null;
			} finally {
				poolBD.closeConnection(conn,null,cs);
			}
		} else {
			logger.traceInfo("obtenerIdentificadorMarketplace", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: " + ((listado != null) ? listado.size() : 0));

		return listado;

	}

	@Override
	public boolean updateEstadoOCMirakl(String seleccionados, String api, int estado, String usuario) {
		logger.initTrace("updateEstadoOCMirakl", "Ingreso a metodo; correlativosVentas=" + seleccionados + "; api=" + api);
		Connection conn = null;
		CallableStatement cs = null;
		boolean respuesta = Boolean.TRUE;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_MKP.PROC_INS_MIRAKL(?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, seleccionados);
			cs.setString(2, usuario);
			cs.setString(3, api);
			cs.setInt(4, estado);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			
			cs.execute();
			
			String query = cs.getString(7);
			BigDecimal cod = cs.getBigDecimal(5);
			String mensaje = cs.getString(6);
			
			logger.traceInfo("updateEstadoOCMirakl", "query: " + query);
			logger.traceInfo("updateEstadoOCMirakl", "cod: " + cod);
			logger.traceInfo("updateEstadoOCMirakl", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
		} catch (Exception e) {
			logger.traceError("updateEstadoOCMirakl", "Error en updateEstadoOCMirakl: ", e);
			respuesta = Boolean.FALSE;
		} finally {
			poolBD.closeConnection(conn, null, cs);
		}

		logger.endTrace("updateEstadoOCMirakl", "Finalizado", "updateEstadoOCMirakl: " + respuesta);
		return respuesta;	}

	@Override
	public List<OrdenesDeCompra> obtenerDatosBoleta(Integer nroCaja, Integer nroSucusal, Long oc) {
		logger.initTrace("obtenerDatosBoleta","nroCaja: "+ nroCaja + "nroSucusal: "+nroSucusal, new KeyLog("Correlativo venta", oc+Constantes.VACIO));
		ArrayList<OrdenesDeCompra> ordenesCompra = new ArrayList<OrdenesDeCompra>();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = poolBD.getConnection();
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_OBTENER_OC_BOLETA(?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			if (nroCaja == null){
				cs.setNull(1, OracleTypes.NUMBER);
			} else{
				cs.setInt(1, nroCaja);
			}
			
			
			if (nroSucusal == null){
				cs.setNull(2, OracleTypes.NUMBER);
			} else{
				cs.setInt(2, nroSucusal);
			}
			

			if(oc == null) {
				cs.setNull(3, OracleTypes.NUMBER);
			} else {
				cs.setLong(3, oc);
			}
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.execute();
			rs = (ResultSet) cs.getObject(5);
			logger.traceInfo("obtenerDatosBoleta", "Query: " + ((String) cs.getObject(4)));
			if (rs != null) {
				while (rs.next()) {
					String tipoPago = "";
					
					NotaVenta notaVenta = new NotaVenta();
					notaVenta.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
					notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
					notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
					notaVenta.setHorasAdministrativas(rs.getInt("HORAS_ADMINISTRATIVAS"));
					notaVenta.setEstado(rs.getInt("ESTADO"));
					notaVenta.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
					notaVenta.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
					notaVenta.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
					notaVenta.setRutComprador(rs.getInt("RUT_COMPRADOR"));
					notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
					notaVenta.setNumeroSucursal(rs.getInt("NUMERO_SUCURSAL"));
					notaVenta.setCodigoRegalo(rs.getInt("CODIGO_REGALO"));
					notaVenta.setTipoRegalo(rs.getInt("TIPO_REGALO"));
					notaVenta.setNumeroBoleta(rs.getInt("NUMERO_BOLETA"));
					notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
					notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
					notaVenta.setNumeroCaja(rs.getInt("NUMERO_CAJA"));
					notaVenta.setCorrelativoBoleta(rs.getInt("CORRELATIVO_BOLETA"));
					notaVenta.setRutBoleta(rs.getInt("RUT_BOLETA"));
					notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
					notaVenta.setNumNotaCredito(rs.getInt("NUM_NOTA_CREDITO"));
					notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
					notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
					notaVenta.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
					notaVenta.setCorrelativoNotaCredito(rs.getInt("CORRELATIVO_NOTA_CREDITO"));
					notaVenta.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
					notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
					notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
					notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					notaVenta.setTipoDoc(rs.getInt("NOTAVENTA_TIPODOC"));
					notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
					notaVenta.setFolioNcSii(rs.getInt("FOLIO_NC_SII"));
					notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
					notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					notaVenta.setUsuario(rs.getString("USUARIO"));
					notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					notaVenta.setNroTrx(rs.getLong("NV_NRO_TRX"));
					
					TarjetaRipley tarjetaRipley = new TarjetaRipley();
					tarjetaRipley.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
					tarjetaRipley.setAdministradora(rs.getInt("ADMINISTRADORA"));
					tarjetaRipley.setEmisor(rs.getInt("EMISOR"));
					tarjetaRipley.setTarjeta(rs.getInt("TARJETA"));
					tarjetaRipley.setRutTitular(rs.getInt("RUT_TITULAR"));
					tarjetaRipley.setRutPoder(rs.getInt("RUT_PODER"));
					tarjetaRipley.setPlazo(rs.getInt("PLAZO"));
					tarjetaRipley.setDiferido(rs.getInt("DIFERIDO"));
					tarjetaRipley.setMontoCapital(rs.getBigDecimal("MONTO_CAPITAL"));
					tarjetaRipley.setMontoPie(rs.getBigDecimal("MONTO_PIE"));
					tarjetaRipley.setDescuentoCar(rs.getBigDecimal("DESCUENTO_CAR"));
					tarjetaRipley.setCodigoDescuento(rs.getInt("CODIGO_DESCUENTO"));
					tarjetaRipley.setPrefijoTitular(rs.getInt("PREFIJO_TITULAR"));
					tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
					tarjetaRipley.setTipoCliente(rs.getInt("TIPO_CLIENTE"));
					tarjetaRipley.setTipoCredito(rs.getInt("TIPO_CREDITO"));
					tarjetaRipley.setPrefijoPoder(rs.getInt("PREFIJO_PODER"));
					tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
					tarjetaRipley.setValorCuota(rs.getBigDecimal("VALOR_CUOTA"));
					tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
					tarjetaRipley.setSernacFinanCae(rs.getInt("SERNAC_FINAN_CAE"));
					tarjetaRipley.setSernacFinanCt(rs.getInt("SERNAC_FINAN_CT"));
					tarjetaRipley.setTvtriMntMntFnd(rs.getInt("TVTRI_MNT_MNT_FND"));
					tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
					tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
					tarjetaRipley.setTvtriCodImpTsaEfe(rs.getInt("TVTRI_COD_IMP_TSA_EFE"));
					tarjetaRipley.setTvtriMntMntEva(rs.getInt("TVTRI_MNT_MNT_EVA"));
					tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
					tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
					tarjetaRipley.setCodigoCore(rs.getInt("CODIGO_CORE"));
					tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));
//					if (tarjetaRipley.getCorrelativoVenta() != null){
//						tipoPago = Constantes.TIPO_PAGO_RIPLEY;
//					}

					TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
					tarjetaBancaria.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
					tarjetaBancaria.setTipoTarjeta(rs.getString("TIPO_TARJETA"));
					tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
					tarjetaBancaria.setMontoTarjeta(rs.getBigDecimal("MONTO_TARJETA"));
					tarjetaBancaria.setDescripcionFormaPago(rs.getString("FORMA_PAGO"));
					tarjetaBancaria.setGlosa(rs.getString("MP_GLOSA"));
					tarjetaBancaria.setBinNumber(rs.getString("MP_BIN"));
					tarjetaBancaria.setUltimosDigitos(rs.getString("MP_ULTIMOS_DIGITOS"));
					tarjetaBancaria.setDescripcionCanal(rs.getString("MP_DESC_CANAL"));
					tarjetaBancaria.setIdentificadorTransaccion(Util.validaLongCeroXNull(rs.getLong("MP_IDENT_TRANSAC")));
//					tarjetaBancaria.setVd(rs.getString("VD"));
					//tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
					//tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
//					if (tarjetaBancaria.getCorrelativoVenta()!= null){
//						if (tarjetaBancaria.getVd() != null){
//							if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_M)){
//								tipoPago = Constantes.TIPO_PAGO_MERCADO_PAGO;
//							}
//							else if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_S)){
//								tipoPago = Constantes.TIPO_PAGO_TBK_DEBITO;
//							} else {
//								tipoPago = Constantes.TIPO_PAGO_TBK_CREDITO;
//							}
//						}
//					}
//					tarjetaBancaria.setCodigoConvenio(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));

					TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
					tarjetaRegaloEmpresa.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
					tarjetaRegaloEmpresa.setAdministradora(rs.getInt("TRE_ADMINISTRADORA"));
					tarjetaRegaloEmpresa.setEmisor(rs.getInt("TRE_EMISOR"));
					tarjetaRegaloEmpresa.setTarjeta(rs.getInt("TRE_TARJETA"));
					tarjetaRegaloEmpresa.setCodigoAutorizador(rs.getInt("TRE_COD_AUTORIZADOR"));
					tarjetaRegaloEmpresa.setRutCliente(rs.getInt("RUT_CLIENTE"));
					tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
					tarjetaRegaloEmpresa.setMonto(rs.getBigDecimal("MONTO"));
					tarjetaRegaloEmpresa.setNroTarjeta(rs.getInt("NRO_TARJETA"));
					tarjetaRegaloEmpresa.setFlag(rs.getInt("FLAG"));
					if (tarjetaRegaloEmpresa.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_TRE;
					}

					DatoFacturacion datoFacturacion = new DatoFacturacion();
					datoFacturacion.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
					datoFacturacion.setAddressId(rs.getInt("FACT_ADDRESS_ID"));
					datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
					datoFacturacion.setComunaFacturaDes(rs.getString("COMUNA_FACTURA_DES"));
					datoFacturacion.setCiudadFacturaDes(rs.getString("CIUDAD_FACTURA_DES"));
					datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
					datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
					datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
					datoFacturacion.setGiroFacturaId(rs.getInt("GIRO_FACTURA_ID"));
					datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
					datoFacturacion.setRegionFacturaId(rs.getInt("REGION_FACTURA_ID"));
					datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

					Despacho despacho = new Despacho();
					despacho.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
					despacho.setRutDespacho(rs.getInt("RUT_DESPACHO"));
					despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
					despacho.setDespachoId(rs.getLong("DESPACHO_ID"));
					despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
					despacho.setSucursalDespacho(rs.getInt("SUCURSAL_DESPACHO"));
					despacho.setComunaDespacho(rs.getInt("COMUNA_DESPACHO"));
					despacho.setRegionDespacho(rs.getInt("REGION_DESPACHO"));
					despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
					despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
					despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
					despacho.setObservacion(rs.getString("OBSERVACION"));
					despacho.setRutCliente(rs.getInt("DESPACHO_RUT_CLIENTE"));
					despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
					despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
					despacho.setTipoCliente(rs.getInt("DESACHO_TIPO_CLIENTE"));
					despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
					despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
					despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
					despacho.setTipoDespacho(rs.getInt("TIPO_DESPACHO"));
					despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					despacho.setCodigoRegalo(rs.getInt("DESPACHO_CODIGO_REGALO"));
					despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
					despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
					despacho.setRegionFacturacion(rs.getInt("REGION_FACTURACION"));
					despacho.setComunaFacturacion(rs.getInt("COMUNA_FACTURACION"));
					despacho.setEnvioDte(rs.getInt("ENVIO_DTE"));
					despacho.setAddressId(rs.getInt("ADDRESS_ID"));
					despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
					despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
					
					TipoDespacho tipoDespacho = new TipoDespacho();
					tipoDespacho.setTipDespachoId(rs.getInt("TIP_DESPACHO_ID"));
					tipoDespacho.setDescTipDespacho(rs.getString("DESC_TIP_DESPACHO"));
					tipoDespacho.setNomTipoDespacho(rs.getString("NOM_TIPO_DESPACHO"));
					tipoDespacho.setGlosaDespacho(rs.getString("GLOSA_DESPACHO"));

					TipoDoc tipoDoc = new TipoDoc();
					tipoDoc.setTipoDoc(rs.getInt("TIPO_DOC"));
					tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
					tipoDoc.setDocOrigen(rs.getInt("DOC_ORIGEN"));
					tipoDoc.setCodBo(rs.getInt("COD_BO"));
					tipoDoc.setCodPpl(rs.getInt("COD_PPL"));
					tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

					Vendedor vendedor = new Vendedor();
					vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
					vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR")!=null?rs.getString("NOMBRE_VENDEDOR"):"INTERNET");
					
					NotaVentaRechazo notaVentaRechazo = new NotaVentaRechazo();
					notaVentaRechazo.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getInt("NVR_CORRELATIVO_VENTA")));
					notaVentaRechazo.setCodMotivo(rs.getInt("COD_MOTIVO"));
					notaVentaRechazo.setFechaBoleta(rs.getTimestamp("NVR_FECHA_BOLETA"));
					notaVentaRechazo.setIdValidacion(rs.getInt("ID_VALIDACION"));
					
					MotivoRechazo motivoRechazo = new MotivoRechazo();
					motivoRechazo.setCodMotivo(rs.getInt("MR_COD_MOTIVO"));
					motivoRechazo.setMotivo(rs.getString("MR_MOTIVO"));
					
					Validacion validacion = new Validacion();
					validacion.setId(rs.getInt("ID"));
					validacion.setDescripcion(rs.getString("DESCRIPCION"));
					validacion.setQuery(rs.getString("QUERY"));
					validacion.setEstado(rs.getInt("VAL_ESTADO"));

					XValidacionOc xValidacionOc = new XValidacionOc();
					xValidacionOc.setIdValidacion(rs.getInt("XV_ID_VALIDACION"));
					xValidacionOc.setMensajeError(rs.getString("MENSAJE_ERROR"));
					xValidacionOc.setQueryValidacion(rs.getString("QUERY_VALIDACION"));
					xValidacionOc.setValorOkErr(rs.getString("VALOR_OK_ERR"));
					xValidacionOc.setQueryActivo(rs.getInt("QUERY_ACTIVO"));
					xValidacionOc.setEstadoFinalOc(rs.getInt("ESTADO_FINAL_OC"));
					
					notaVenta.setTipoPago(tipoPago);
					
					DocumentoElectronico documentoElectronico = new DocumentoElectronico();
					
					documentoElectronico.setXmlTDE(rs.getClob("XML_TDE"));
										
					OrdenesDeCompra ordenCompra = new OrdenesDeCompra();
					ordenCompra.setNotaVenta(notaVenta);
					ordenCompra.setTarjetaRipley(tarjetaRipley);
					ordenCompra.setTarjetaBancaria(tarjetaBancaria);
//					ordenCompra.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
					ordenCompra.setDatoFacturacion(datoFacturacion);
					ordenCompra.setDespacho(despacho);
					ordenCompra.setTipoDespacho(tipoDespacho);
					ordenCompra.setTipoDoc(tipoDoc);
					ordenCompra.setVendedor(vendedor);
					ordenCompra.setNotaVentaRechazo(notaVentaRechazo);
					ordenCompra.setMotivoRechazo(motivoRechazo);
					ordenCompra.setValidacion(validacion);
					ordenCompra.setxValidacionOc(xValidacionOc);
					ordenCompra.setDocumentoElectronico(documentoElectronico);
					
					ordenesCompra.add(ordenCompra);
				}
			}

		} catch (Exception e) {
			logger.traceError("obtenerDatosBoleta", "Error en obtenerOrdenes: ", e);
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("obtenerDatosBoleta", "Finalizado", "ordenesCompra: "+ordenesCompra.size());
		return ordenesCompra;
	}


}

