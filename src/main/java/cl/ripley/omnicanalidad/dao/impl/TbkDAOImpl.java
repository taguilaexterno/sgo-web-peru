package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Anulacion;
import cl.ripley.omnicanalidad.dao.TbkDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBD;
import oracle.jdbc.OracleTypes;

@Repository
public class TbkDAOImpl implements TbkDAO {

	private static final AriLog logger = new AriLog(TbkDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	private PoolBD poolBD;

	@Override
	public List<Anulacion> getAnulaTBKPorFecha(Date fechaDesde, Date fechaHasta) {
		
		logger.initTrace("getAnulaTBKPorFecha", "obtengo lista de anulaciones TBK");
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Anulacion> anulaciones= new ArrayList<Anulacion>();
		try {
			
			conn = poolBD.getConnection();
			String procSQL = "{CALL CAVIRA_TBK.PROC_OBT_ANULA_TBK(?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setDate(1, fechaDesde);
			cs.setDate(2, fechaHasta);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();  

			String query = cs.getString(3);
			BigDecimal cod = cs.getBigDecimal(5);
			String mensaje = cs.getString(6);

			logger.traceInfo("getAnulaTBKPorFecha", "Query: " + query);
			logger.traceInfo("getAnulaTBKPorFecha", "Mensaje: " + mensaje);
			logger.traceInfo("getAnulaTBKPorFecha", "Codigo: " + cod);

			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}

			Anulacion anulacion = null;

			rs = (ResultSet) cs.getObject(4);
			while (rs != null && rs.next()) {

				
				
				logger.traceInfo("getAnulaTBKPorFecha", "anulacion: " + rs.getString("ORDEN_COMPRA"));
				anulacion = new Anulacion();
				anulacion.setNumNotaCredito(rs.getLong("NUM_NOTA_CREDITO"));
				anulacion.setFolioNc(rs.getLong("FOLIO_NOTA_CREDITO"));
				anulacion.setFechaCompra(rs.getDate("FECHA_COMPRA"));
				anulacion.setRutComprador(rs.getString("RUT_CLIENTE"));
				anulacion.setNumBoleta(rs.getLong("CORRELATIVO_BOLETA"));
				anulacion.setCodigoAutorizacion(rs.getString("COD_AUTORIZACION"));
				anulacion.setOrdenCompra(rs.getString("ORDEN_COMPRA"));
				anulacion.setMontoAutorizado(rs.getBigDecimal("MONTO_AUTORIZADO"));
				anulacion.setMontoAnulado(rs.getBigDecimal("MONTO_ANULADO"));
				anulacion.setCodigoComercio(rs.getLong("CODIGO_COMERCIO"));
				anulacion.setFechaAnualacion(rs.getDate("FECHA_ANULACION"));
				anulacion.setCodigoError(rs.getString("COD_ERROR"));
				anulacion.setDescripcionError(rs.getString("DESC_ERROR"));
				anulacion.setIntento(rs.getInt("INTENTO"));
				
				
							
				//agrego anulaci�n a la lista
				anulaciones.add(anulacion);
			}
		} catch (Exception e) {
			logger.traceError("getAnulaTBKPorFecha Exception", "Error durante la ejecuci�n de getAnulaTBKPorFecha", e);
			return anulaciones;
		} finally {
			poolBD.closeConnection(conn, rs, cs);
		}

		logger.endTrace("getAnulaTBKPorFecha", "Finalizado", "Tama�o lista: " + anulaciones.size());

		return anulaciones;
	}



}
