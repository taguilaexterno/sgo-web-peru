package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.ReservaDTE;
import cl.ripley.omnicanalidad.bean.Usuario;

public interface ReservasDAO {
	

	public List<ReservaDTE> listarReservas(String numeroReserva, String rutCliente, String estadoReserva);
	public Integer cancelarReserva(Long numeroReserva, Usuario usuario);
	public Integer extenderReserva(Long numReserva, String fechaVigenciaFinal, Usuario usuario);
	public int getPaginas();
}
