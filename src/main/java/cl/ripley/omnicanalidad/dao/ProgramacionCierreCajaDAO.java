package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.ProgramacionCierreCaja;

/**DAO para conectar a base de datos para manipular datos de {@linkplain ProgramacionCierreCaja}
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 18-08-2016
 * Versiones:
 * <ul>
 *  <li>18-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public interface ProgramacionCierreCajaDAO {

	/**Obtiene lista de {@linkplain ProgramacionCierreCaja}
	 * Versiones:
	 * <ul>
	 * <li>18-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
List<ProgramacionCierreCaja> getProgramacionCierreCajaList();
	
	/**Actualiza o inserta {@linkplain ProgramacionCierreCaja}
	 * Versiones:
	 * <ul>
	 * <li>18-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param prog
	 * @return
	 */
List<ProgramacionCierreCaja> getProgramacionCierreCajaList(Long sucursal);

/**Actualiza o inserta {@linkplain ProgramacionCierreCaja}
 * Versiones:
 * <ul>
 * <li>18-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 * @param prog
 * @return
 */
	boolean updProgramacionCierreCaja(ProgramacionCierreCaja prog);
	
	/**Elimina una programacion de cierre de caja segun ID
	 * Versiones:
	 * <ul>
	 * <li>19-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param numCaja
	 * @return
	 */
	boolean delProgramacionCierreCaja(Long numCaja);
	
	/**Obtiene lista de {@linkplain Long}
	 * Versiones:
	 * <ul>
	 * <li>04-10-2017 - MCORRALES: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
List<Long> getSucursales();
	/**Obtiene lista de {@linkplain Long}
	 * Versiones:
	 * <ul>
	 * <li>05-10-2017 - MCORRALES: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
List<Long> getCajas(Long numeroSucursal);
/**Obtiene lista de {@linkplain Long}
 * Versiones:
 * <ul>
 * <li>05-10-2017 - MCORRALES: Version Inicial.</li>
 *</ul>
 * @return
 */
List<ProgramacionCierreCaja> getProgramacionCierreCajaList(Long numeroSucursal , Long numeroCaja);
}
