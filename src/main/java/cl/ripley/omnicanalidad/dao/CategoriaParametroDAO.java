package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.CategoriasProperties;

/**Interface DAO para manejo de Categorías de Parámetros.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface CategoriaParametroDAO {

	/**Obtiene Categorías de Parámetros
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return {@linkplain List} de {@linkplain CategoriasProperties}
	 */
	public List<CategoriasProperties> getCategoriasParametros();
	
}
