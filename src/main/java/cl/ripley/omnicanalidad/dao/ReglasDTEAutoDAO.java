package cl.ripley.omnicanalidad.dao;

import java.util.ArrayList;

import cl.ripley.omnicanalidad.bean.ProductosDTE;
import cl.ripley.omnicanalidad.bean.ReglaPrecio;
import cl.ripley.omnicanalidad.bean.ReglaSku;
import cl.ripley.omnicanalidad.bean.ReglaTodoManual;
import cl.ripley.omnicanalidad.bean.ReglasDTEAuto;

/**Interface DAO para manejo de reglas DTE
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ReglasDTEAutoDAO {

	
	/**Obtiene reglas DTE
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public ReglasDTEAuto getReglasDTEAuto();

	/**Actualiza regla DTE
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param reglaPrecio
	 * @param reglaSku
	 * @param reglaTodoManual
	 * @return
	 */
	public boolean updateEstadoRegla(ReglaPrecio reglaPrecio,
			ReglaSku reglaSku, ReglaTodoManual reglaTodoManual);

	/**Actualiza Factor CP
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param valor
	 * @param id
	 * @param idValidacion
	 * @return
	 */
	public boolean updateFactorCP(String valor, int id, int idValidacion);

	/**Obtiene productos DTE con SKU
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public ArrayList<ProductosDTE> getProductosDTE();

	/**Guarda el SKU de un producto DTE
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param productoSku
	 * @return
	 */
	public boolean saveSkuProd(ProductosDTE productoSku);

	/**Elimina SKU de los productos seleccionados.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param seleccionados
	 * @return
	 */
	public boolean delSkuProd(String seleccionados);

}
