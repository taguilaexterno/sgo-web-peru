package cl.ripley.omnicanalidad.dao;

import java.math.BigInteger;
import java.util.List;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Banco;
import cl.ripley.omnicanalidad.bean.Region;
import cl.ripley.omnicanalidad.bean.Retorno;

public interface DevolucionNCSAPDAO {
		
	public Banco obtenerBanco(String codBanco) throws AligareException;
	
	public Region obtenerRegion(String codRegion) throws AligareException;
	
	public List<Banco> obtenerBancos() throws AligareException;
	
	public List<Region> obtenerRegiones() throws AligareException;
	
	public Retorno insertaDevNCSAP(BigInteger ticket, long folio, long oc, String rut, String codBanco, long nroCuenta, String usuario);
}
