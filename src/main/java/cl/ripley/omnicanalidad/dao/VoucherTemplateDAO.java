package cl.ripley.omnicanalidad.dao;

import cl.ripley.omnicanalidad.bean.TemplateVoucher;

/**Interface DAO para manejar Plantillas de Voucher.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface VoucherTemplateDAO {

	/**Obtiene una plantilla/template de Voucher por una clave/llave
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param llave
	 * @return
	 */
	public TemplateVoucher getTemplateByLlave(String llave);
}
