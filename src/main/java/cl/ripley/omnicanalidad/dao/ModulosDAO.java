package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.Modulo;

/**DAO para obtener datos de modulos desde la base de datos
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 09-08-2016
 * Versiones:
 * <ul>
 *  <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public interface ModulosDAO {

	/**Obtiene los modulos del sistema desde la base de datos
	 * Versiones:
	 * <ul>
	 * <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return Lista de modulos
	 */
	List<Modulo> getModulos();
	
	/**Obtiene modulos por perfil, ya que se podrian crear modulos pero no encontrarse relacionado con el perfil del usuario
	 * Versiones:
	 * <ul>
	 * <li>11-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param codPerfil
	 * @return
	 */
	List<Modulo> getModulosByPerfil(Long codPerfil);
	
	/**Actualiza permisos de los modulos por perfil
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param codPerfil
	 * @param codModulo
	 * @param codPermiso
	 * @return true si la operacion es exitosa
	 */
	boolean updPermisosModulosByPerfil(Long codPerfil, Long codModulo, Long codPermiso);
}
