package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;

public interface BigTicketDAO {
	
	public List<ArticulosVentaOC> getArtMarcadosConDespacho(List<ArticulosVentaOC> articulos);
	
	public List<ListaAgrupaSubOrdenes> getArtMarcadosConDespachoSubOrdenes(List<ListaAgrupaSubOrdenes> articulosConOrdenes);

}
