package cl.ripley.omnicanalidad.dao;

import cl.ripley.omnicanalidad.bean.ProductosDTE;

/**Interface DAO para consultar servicio REST de productos de Tienda Virtual.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface SKURestDAO {

	/**Consulta SKU SVC
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param sku
	 * @return
	 */
	public String ConsultaSKURestSvcRest(String sku, String url);
	
	/**Obtiene productos DTE a partir del SKU pasado por parámetro.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param sku
	 * @return {@linkplain ProductosDTE}
	 */
	public ProductosDTE ConsultaSKURest(String sku, String url);
}
