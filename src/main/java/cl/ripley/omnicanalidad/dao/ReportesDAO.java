package cl.ripley.omnicanalidad.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import cl.ripley.omnicanalidad.bean.CajaSucursal;
import cl.ripley.omnicanalidad.bean.ReporteModeloExtendidoDTO;
import cl.ripley.omnicanalidad.bean.ResumenReporte;

/**Interface DAO para manejar Reportes.
 *
 * @author Carlos Leal (Aligare).
 * @since 04-06-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ReportesDAO {

	/**Obtiene lista de cajas
	 *
	 * @author Carlos Leal (Aligare).
	 * @since 04-06-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param none
	 * @return List<String>
	 */
	public List<CajaSucursal> obtenerCajas();

	/**Obtiene resumen de cajas consultadas
	 *
	 * @author Carlos Leal (Aligare).
	 * @since 04-06-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param fechaInicio, fechaFin, cajasConsultadas
	 * @return List<ResumenReporte>
	 */
	public List<ResumenReporte> buscarCierresCaja(String fechaInicio, String fechaFin, String cajasSuc);

	
	/**Obtiene resumen de cajas consultadas
	 *
	 * @author Carlos Leal (Aligare).
	 * @since 04-06-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param idAperturaCierre, idCaja, funcionBoleta
	 * @return InformacionExcel
	 */
	public HSSFWorkbook buscarInformacionCierre(Long idAperturaCierre, String idCaja, String idSucursal, int funcionBoleta);

	public List<ReporteModeloExtendidoDTO> buscarInformacionME(String fechaInicio, String fechaFin, Long numeroOrden,
			Long numeroDNI, String estado, String tipo);

	public HashMap<String, String> obtenerEstados();
}
