package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.Parametro;

/**Interface para implementar con llamadas a parámetros desde la base de datos
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ParametroDAO {
	/**Obtiene parámetro por nombre.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param nombre
	 * @return
	 */
	public Parametro getParametro(String nombre);
	/**Actualiza el parámetro
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param parametro {@linkplain Parametro}
	 * @return true si la operación se realiza con éxito.
	 */
	public boolean updateParametro(Parametro parametro);
	/**Obtiene lista de parámetros.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	public List<Parametro> obtenerParametros();
	
	/**Obtiene lista de parámetros por tipo de parámetro.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param tipo
	 * @return
	 */
	public List<Parametro> getParametrosXTipo(int tipo);
	
	public int getParamByName(String param);
	
	public void resetParam();
	
}
