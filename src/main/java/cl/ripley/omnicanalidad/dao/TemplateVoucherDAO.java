package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.TemplateVoucher;

/**Interfaz para manipulacion de base de datos de TemplateVoucher
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 19-08-2016
 * Versiones:
 * <ul>
 *  <li>19-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public interface TemplateVoucherDAO {

	/**Obtiene lista de templates de voucher
	 * Versiones:
	 * <ul>
	 * <li>19-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	List<TemplateVoucher> getTemplateVoucherList(TemplateVoucher tv);
	
	/**Cambia estado del template (activar o desactivar)
	 * Versiones:
	 * <ul>
	 * <li>22-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param tv {@linkplain TemplateVoucher}
	 * @return
	 */
	boolean updActivarTemplate(TemplateVoucher tv);
	
	/**Actualiza el template
	 * Versiones:
	 * <ul>
	 * <li>22-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param tv
	 * @return
	 */
	boolean updTemplate(TemplateVoucher tv);
	
	/**Actualiza el html del template
	 * Versiones:
	 * <ul>
	 * <li>22-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param tv
	 * @return
	 */
	boolean updTemplateHtml(TemplateVoucher tv);
}
