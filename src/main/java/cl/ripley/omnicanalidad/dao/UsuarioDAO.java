package cl.ripley.omnicanalidad.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.naming.NamingException;

import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Usuario;

/**
 * Interface para la construcción del dao que maneja 
 * los datos del Usuario en Sesión de la aplicación.
 * 
 * @author Fabian Riveros V.
 *
 */
public interface UsuarioDAO {

	/**
	 * Método que busca un objeto UsuarioDTO por un parámetro dado.
	 * @param usuario : Parámetro de búsqueda, String con el valor de usuario de red.
	 * @return UsuarioDTO : Objeto UsuarioDTO encontrado.
	 * @throws SQLException
	 * @throws NamingException
	 */
	public Usuario buscarUsuario(String usuario);
	
	/**Metodo que devuelte un usuario con el perfil asociado en caso de tener.
	 * Versiones:
	 * <ul>
	 * <li>05-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param usuario
	 * @return {@linkplain Usuario}
	 */
	Usuario getUsuarioConPerfil(Usuario usuario);
	
	/**Metodo que guarda un nuevo usuario en la base de datos
	 * Versiones:
	 * <ul>
	 * <li>09-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param usuario
	 * @return
	 * @throws ParseException 
	 */
	boolean guardarUsuario(Usuario usuario, Error error) throws ParseException;
	
	/**Actualiza el usuario con la informacion enviada por parametro.
	 * <br>
	 * Si el usuario va con la password en blanco, esta queda tal cual en la base de datos, de lo contrario se actualiza.
	 * Versiones:
	 * <ul>
	 * <li>11-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param usuario
	 * @return
	 */
	boolean actualizarUsuario(Usuario usuario);
	
	/**Obtiene lista de usuarios de sistema
	 * Versiones:
	 * <ul>
	 * <li>11-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	List<Usuario> getUsuarios();
	
	/**Obtiene lista de usuarios por perfil
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param codPerfil
	 * @return Lista de usuarios por perfil
	 */
	List<Usuario> getUsuarios(Long codPerfil);
	
	/**Obtiene lista de usuarios buscados en mantenedor
	 * Versiones:
	 * <ul>
	 * <li>22-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param usuario
	 * @return
	 */
	List<Usuario> buscarUsuarioLike(String usuario);
	
	/**Obtiene valor del mes para el calculo de la fecha vigencia para el usuario creado.
	 *
	 * @author Tamara Aguila Rodriguez (Aligare).
	 * @since 12-10-2021
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param tipo
	 * @return
	 */
	 int getParamFechaFin();
}
