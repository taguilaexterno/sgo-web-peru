package cl.ripley.omnicanalidad.dao;

import java.util.ArrayList;

import cl.ripley.omnicanalidad.bean.GrupoMPSeguro;
import cl.ripley.omnicanalidad.bean.ReglasMPSeguro;

/**Interface de DAO para manejo de Reglas de Medio de Pago Seguro
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ReglasMPSeguroDAO{
	/**Obtiene reglas de Medio de Pago Seguro
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return {@linkplain ArrayList} de {@linkplain ReglasMPSeguro}
	 */
	public ArrayList<ReglasMPSeguro> getReglasMPSeguro();

	/**Actualiza estado de regla.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param id
	 * @param id_valor
	 * @param estado
	 * @return
	 */
	public boolean updateEstado(int id,int id_valor, int estado);

	/**Actualiza valor menor de regla.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param id
	 * @param id_validacion
	 * @param valor
	 * @return
	 */
	public boolean updateValMenor(int id, int id_validacion, String valor);

	/**Obtiene lista de Grupo de Reglas de Medio de Pago.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return {@linkplain ArrayList} de {@linkplain GrupoMPSeguro}
	 */
	public ArrayList<GrupoMPSeguro> getGrupoReglasMP();

	/**Actualiza el estado del Grupo de Reglas de Medio de Pago.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param id
	 * @param estado
	 * @return
	 */
	public boolean updateEstadoGrupo(int id, int estado);
}
