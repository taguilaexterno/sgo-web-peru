package cl.ripley.omnicanalidad.dao;

import java.util.List;
import java.util.Map;

import cl.ripley.omnicanalidad.bean.CajaSucursal;

public interface ICorreccionDatosDAO {

	void ejecutarUpdate(List<CajaSucursal> cajaSuc);
	List<Map<String,Object>> ejecutarSelect();
	
	void ejecutarUpdateConsulta(String consulta);
	List<Map<String,Object>> ejecutarSelectConsulta(String consulta);
	
}
