package cl.ripley.omnicanalidad.dao;

import java.sql.Date;
import java.util.List;

import cl.ripley.omnicanalidad.bean.Anulacion;


public interface TbkDAO {
	
	public List<Anulacion> getAnulaTBKPorFecha(Date fechaDesde, Date fechaHasta);

}
