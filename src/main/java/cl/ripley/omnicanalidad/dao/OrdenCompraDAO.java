package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.DetalleCliente;
import cl.ripley.omnicanalidad.bean.DetalleOC;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.ListaAgrupaSubOrdenes;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;

/**Interface DAO para manejo de órdenes de compra.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface OrdenCompraDAO {


	/**Obtiene órdenes de compra por numero de caja, sucursal, estado, indicador market place, y número de orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param nroCaja
	 * @param nroSucusal
	 * @param nroEstado
	 * @param indicadorMkp
	 * @param oc
	 * @return
	 */
	List<OrdenesDeCompra> obtenerOrdenes(Integer nroCaja, Integer nroSucusal, Integer nroEstado, Integer indicadorMkp, Long oc);
	
	/**
	 * Método para obtener datos para pintar boleta html. De uso exclusivo.
	 * @param nroCaja
	 * @param nroSucusal
	 * @param oc
	 * @return
	 */
	List<OrdenesDeCompra> obtenerDatosBoleta(Integer nroCaja, Integer nroSucusal, Long oc);

	List<OrdenesDeCompra> obtenerOrdenes(Long rutComprador);

	/**Obtiene articulos por orden de compra (correlativo de venta)
	 * Versiones:
	 * <ul>
	 * <li>17-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param correlativoOC
	 * @return
	 */
	List<ArticulosVentaOC> getArticulosByOC(Long correlativoOC);
	
	/**Actualiza estado de articulos de venta para nota de credito o anulacion
	 * Versiones:
	 * <ul>
	 * <li>17-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param correlativosArticulos
	 * @param correlativoVenta
	 * @param estadoNC
	 * @return
	 */
	
	/**Obtiene subórdenes de compra por número de orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoOC
	 * @return
	 */
	List<ListaAgrupaSubOrdenes> getSubOrdenesByOC(Long correlativoOC);
	
	/**Actualiza el estado de artículos NC
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativosArticulos
	 * @param correlativoVenta
	 * @param estadoNC
	 * @return
	 */
	boolean updateEstadoNcArticulos(String correlativosArticulos, Long correlativoVenta, Long estadoNC);

	/**Obtiene detalle de la OC
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativo
	 * @return
	 */
	public DetalleOC obtenerDetalleOC(Integer correlativo);

	/**Actualiza el estado de las órdenes de compra seleccionados
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param seleccionados
	 * @param nroEstado
	 * @param cajaNula
	 * @param usuario
	 * @param estadosValidacion
	 * @return
	 */
	boolean updateEstadoOrdenes(String seleccionados, Integer nroEstado, Integer nroEstado_CS, boolean cajaNula, String usuario, String estadosValidacion);

	/**Actualiza artículos market place para Refund por número de orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param articulosConOrdenes
	 * @param correlativoVenta
	 * @param usuario
	 * @return
	 */
	boolean updateRefundMKP(List<ListaAgrupaSubOrdenes> articulosConOrdenes, Long correlativoVenta, String usuario);
	
	/**Obtiene true si la Orden de Compra está rechazada.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoVenta
	 * @return
	 */
	boolean esRechazo(Long correlativoVenta);
	
	/**Obtiene datos del cliente de la orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoOC
	 * @return {@linkplain DetalleCliente}
	 */
	DetalleCliente getDatosCliente(Long correlativoOC);
	
	/**Obtiene la forma de pago de la orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoOC
	 * @return {@linkplain String}
	 */
	String obtenerFormaPago(Long correlativoOC);
	
	/**Obtiene los indentificadores market place de la orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoVenta
	 * @return {@linkplain List} de {@linkplain IdentificadorMarketplace}
	 */
	List<IdentificadorMarketplace> obtenerIdentificadorMarketplace(Integer correlativoVenta);
	
	/**Actualiza el estado de la orden de compra Mirakl.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 22-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param seleccionados
	 * @param api
	 * @param estado
	 * @param usuario
	 * @return
	 */
	boolean updateEstadoOCMirakl(String seleccionados, String api, int estado, String usuario);
}
