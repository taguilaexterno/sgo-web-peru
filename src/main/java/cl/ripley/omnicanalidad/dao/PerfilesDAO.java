package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.Perfil;

/**DAO para perfiles
 * @author Jose Matias Ortuzar (Aligare)
 * @Date 08-08-2016
 * Versiones:
 * <ul>
 *  <li>08-08-2016 - JORTUZAR: Version Inicial.</li>
 *</ul>
 */
public interface PerfilesDAO {

	/**Obtiene perfiles del sistema.
	 * Versiones:
	 * <ul>
	 * <li>08-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @return
	 */
	List<Perfil> getPerfiles();
	
	/**Guarda un perfil nuevo
	 * Versiones:
	 * <ul>
	 * <li>10-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param perfil
	 * @return
	 */
	boolean guardarPerfilNuevo(Perfil perfil);
	
	/**Obtiene perfil por id
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param id id del perfil
	 * @return {@linkplain Perfil} con informacion del perfil requerido
	 */
	Perfil getPerfilById(Long id);
	
	/**Obtiene perfil por nombre
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param nombre nombre del perfil
	 * @return {@linkplain List} de {@linkplain Perfil} con informacion del/los perfil/es requerido/s
	 */
	List<Perfil> getPerfilByNombrePerfil(String nombre);
	
	/**Actualiza la informacion del perfil
	 * Versiones:
	 * <ul>
	 * <li>12-08-2016 - JORTUZAR: Version Inicial.</li>
	 *</ul>
	 * @param perfil
	 * @return true si la operacion es exitosa
	 */
	boolean updPerfil(Perfil perfil);
}
