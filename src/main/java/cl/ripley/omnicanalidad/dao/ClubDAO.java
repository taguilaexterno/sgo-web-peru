package cl.ripley.omnicanalidad.dao;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ClienteClub;

public interface ClubDAO {

	public ClienteClub getClienteClub(Integer numeroEvento, int indicadorTitular) throws AligareException;

}
