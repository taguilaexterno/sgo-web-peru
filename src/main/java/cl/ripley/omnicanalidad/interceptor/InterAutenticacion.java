package cl.ripley.omnicanalidad.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Error;
import cl.ripley.omnicanalidad.bean.Modulo;
import cl.ripley.omnicanalidad.bean.Perfil;
import cl.ripley.omnicanalidad.bean.Usuario;
import cl.ripley.omnicanalidad.bean.UsuarioHabilitado;
import cl.ripley.omnicanalidad.dao.PerfilesDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Interceptor de autenticación de usuario.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 22-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
public class InterAutenticacion implements Interceptor {
	
	
	private static final long serialVersionUID = -8556560326650151682L;
	private static final AriLog logger = new AriLog(InterAutenticacion.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	@Autowired
	private PerfilesDAO perfilesDAO;
	
	@Override
	public void destroy() {
		logger.traceInfo("destroy", "Destruyendo el interceptor");
	}

	@Override
	public void init() {
		logger.traceInfo("init", "Inicializando el interceptor");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		logger.initTrace("intercept", "Ingreso a metodo");
		/*
		 * Se toma de la Sesi�n y se verifica si ya existe una instancia
		 * de la clase Usuario, buscando al objeto referenciado
		 * por la cadena "usuario".
		 */
		Map session = actionInvocation.getInvocationContext().getSession();
		
		session.remove("error");
		
		if (session.containsKey("usuario")){
			logger.traceInfo("intercept", "Usuario existe en sesi�n");
		}
		
		Usuario usuario = (Usuario) session.get("usuario");

		/*
		 * Si todo está bien, se hace una petición a una acción a la que el usuario tiene permiso, 
		 * entonces se pasa la referencia del objeto usuario a la acción para que la pueda usar.
		 */
		if (usuario == null) {
			logger.traceInfo("intercept", "Usuario no existe en sesión");
			session.remove("perfilesSession");
			session.remove("nombreAction");
			return Action.LOGIN;
		}else{
			logger.traceInfo("intercept", "Usuario existe en sesión e invoco");
			Action action = (Action) actionInvocation.getAction();
			if (action instanceof UsuarioHabilitado) {
				((UsuarioHabilitado) action).setUsuario(usuario);
			}
			
			if(session.containsKey("perfilesSession")) {
				
				List<Perfil> perfiles = (List<Perfil>) session.get("perfilesSession");
				
				if(perfiles == null || perfiles.isEmpty()) {
					
					session.put("perfilesSession", perfilesDAO.getPerfiles());
					
				}
				
			} else {
				
				session.put("perfilesSession", perfilesDAO.getPerfiles());
				
			}
			
			HttpServletRequest req = (HttpServletRequest) actionInvocation.getInvocationContext().get(ServletActionContext.HTTP_REQUEST);
			String nombreAction = actionInvocation.getInvocationContext().getName();
			
			if((nombreAction.toLowerCase().contains("admCorreccionDatosView".toLowerCase()) 
					|| nombreAction.toLowerCase().contains("admCorreccionDatos".toLowerCase())
					|| nombreAction.toLowerCase().contains("admConsultaData".toLowerCase()))) {
				
				Error error = new Error();
				error.setCodError(Constantes.NRO_MENOSUNO);
				error.setErrorMsg("No tiene permisos para esta función.");
				session.put("error", error);
				return Action.ERROR;
			}
			
			if(req.getParameter("estado") != null && !"".equals(req.getParameter("estado"))) {
				nombreAction += "?estado=" + req.getParameter("estado");
			}
			int aux=0;
			for(Modulo mod : usuario.getPerfil().getModulos()) {
				if(mod.getUrl().equals(nombreAction)) {
					aux = 1;
					break;
				}
			}
			for(Modulo mod : usuario.getPerfil().getModulos()) {
				if(mod.getUrl().equals(nombreAction)) {
					mod.setActivo(1);
					session.put("nivelPermiso", mod.getPermiso().getId());
					logger.traceInfo("intercept", "nivelPermiso: " + session.get("nivelPermiso"));
				}else {
					if (aux == 1){
						mod.setActivo(0);}
				}
			}
			
			return actionInvocation.invoke();
		}

	}

}
